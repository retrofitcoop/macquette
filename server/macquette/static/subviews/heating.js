console.log('debug heating.js');

function heating_UpdateUI() {
    add_heating_systems();

    //Add "Hot water storage control type" and "Pipework insulation" if any of the systems requires it
    data.heating_systems.forEach(function (system) {
        var show = false;
        if (system.primary_circuit_loss == 'Yes') {
            show = true;
        }
        if (show == true) {
            $('.if-primary-circuit-loss').show();
        }
    });
    show_hide_if_master();

    // Measures applied
    if (data.measures.space_heating_control_type != undefined) {
        for (var id in data.measures.space_heating_control_type) {
            $('.space_heating_control_type-measure-applied[item-id=' + id + ']').show();
        }
    }
    if (data.measures.heating_systems != undefined) {
        for (var id in data.measures.heating_systems) {
            $('.heating_systems-measure-applied[item-id=' + id + ']').show();
        }
    }

    window.Macquette.uiModuleShims.heatingTop.update()
    window.Macquette.uiModuleShims.heatingBottom.update()
}

// Measures
function heating_initUI(firstTime = true) {
    if (data.measures == undefined) {
        data.measures = {};
    }

    if (firstTime) {
        window.Macquette.uiModuleShims.heatingTop.init(
            document.getElementById("module-mountpoint-top"), ''
        )
        window.Macquette.uiModuleShims.heatingBottom.init(
            document.getElementById("module-mountpoint-bottom"), ''
        )
    }
}

function heating_UnloadUI() {
    window.Macquette.uiModuleShims.heatingTop.unmount()
    window.Macquette.uiModuleShims.heatingBottom.unmount()
}

$('#openbem').on('click', '.add-heating-system-from-lib', function () {
    library_helper.type = 'heating_systems';
    library_helper.onAddItemFromLib();
});

$('#openbem').on('click', '.apply-space-heating-control', function (event) {
    const button = event.target;
    const item_index = button.attributes.item_index.nodeValue;

    window.Macquette.selectHeatingControlMeasure(
        document.getElementById("modal-mountpoint"),
        (libraryItem) => {
            if (data.measures.space_heating_control_type === undefined) {
                data.measures.space_heating_control_type = {};
            }

            const item = data.heating_systems[item_index];

            if (data.measures.space_heating_control_type[item.id] === undefined) {
                data.measures.space_heating_control_type[item.id] = { original: item };
            }

            add_quantity_and_cost_to_measure(libraryItem, data.TFA);

            data.measures.space_heating_control_type[item.id].measure = libraryItem;
            data.heating_systems[item_index].heating_controls = libraryItem.control_type;

            update();
        }
    )
})

$('#openbem').on('click', '#replace-heating-system-with-measure', function (event) {
    const button = event.target;
    const item_index = button.attributes.item_index.nodeValue;

    window.Macquette.selectHeatingSystemMeasure(
        document.getElementById("modal-mountpoint"),
        (libraryItem) => {
            if (data.measures.heating_systems === undefined) {
                data.measures.heating_systems = {};
            }

            const item = data.heating_systems[item_index];

            if (data.measures.heating_systems[item.id] === undefined) {
                data.measures.heating_systems[item.id] = { original: item };
            }

            if (libraryItem.category == 'Warm air systems') {
                libraryItem.fans_and_supply_pumps = 0.4 * libraryItem.sfp * data.volume;
            }
            add_quantity_and_cost_to_measure(libraryItem, data.TFA);
            // Add properties that were in the original item
            for (z in item) {
                if (libraryItem[z] === undefined) {
                    libraryItem[z] = item[z];
                }
            }

            // Update data object and add measure
            data.measures.heating_systems[item.id].measure = libraryItem;
            data.heating_systems[item_index] = libraryItem;

            heating_initUI(false);
            update();
        }
    )
})

$('#openbem').on('click', '#add-heating-system-as-measure', function () {
    window.Macquette.selectHeatingSystemMeasure(
        document.getElementById("modal-mountpoint"),
        (libraryItem) => {
            if (data.measures.heating_systems === undefined) {
                data.measures.heating_systems = {};
            }

            const item_id = get_HS_max_id() + 1;

            if (data.measures.heating_systems[item_id] == undefined) {
                data.measures.heating_systems[item_id] = { original: 'empty', measure: {} };
            }

            libraryItem.id = item_id;
            if (libraryItem.category == 'Warm air systems') {
                libraryItem.fans_and_supply_pumps = 0.4 * libraryItem.sfp * data.volume;
            }
            add_quantity_and_cost_to_measure(libraryItem, data.TFA);

            libraryItem.fuel = 'Standard Tariff';
            libraryItem.fraction_space = 1;
            libraryItem.fraction_water_heating = 1;
            libraryItem.main_space_heating_system = 'secondaryHS';
            libraryItem.temperature_adjustment = 0;
            libraryItem.provides = 'heating_and_water';
            libraryItem.instantaneous_water_heating = false;
            libraryItem.heating_controls = 1;

            data.measures.heating_systems[item_id].measure = libraryItem;
            data.heating_systems.push(libraryItem);

            heating_initUI(false);
            update();
        }
    )
})

$('#openbem').on('click', '.add-heating-system', function () {
    const tag = $(this).attr('tag');
    const library = library_helper.get_library_by_id($(this).attr('library')).data;
    const item = {
        ...library[tag],
        tag,
        id: get_HS_max_id() + 1,
        fuel: 'Standard Tariff',
        fraction_space: 1,
        fraction_water_heating: 1,
        main_space_heating_system: 'secondaryHS',
        temperature_adjustment: 0,
        provides: 'heating_and_water',
        instantaneous_water_heating: false,
        heating_controls: 1,
    };

    data.heating_systems.push(item);
    update();
});
$('#openbem').on('click', '.delete-heating-system', function () {
    var row = $(this).attr('row');
    data.heating_systems.splice(row, 1);
    update();
});
$('#openbem').on('click', '.edit-item-heating-system', function () {
    library_helper.type = 'heating_systems';
    library_helper.onEditItem($(this));
});
function get_HS_max_id() {
    var max_id = 0;
    // Find the max id
    for (z in data.heating_systems) {
        if (data.heating_systems[z].id != undefined && data.heating_systems[z].id > max_id) {
            max_id = data.heating_systems[z].id;
        }
    }
    for (z in data.measures.heating_systems) {
        if (z > max_id) {
            max_id = z;
        }
    }
    return 1.0 * max_id;
}
function get_heating_system_options(mainHSs) {
    var out = '';
    if (mainHSs.mainHS1 === false) {
        out += '<option value="mainHS1">Main heating system</option>';
        out += '<option value="mainHS2_whole_house" disabled>2<sup>nd</sup> Main heating system - whole house</option>';
        out += '<option value="mainHS2_part_of_the_house" disabled>2<sup>nd</sup> Main heating system - different part of the house</option>';
    } else if (mainHSs.mainHS2 === false) {
        out += '<option value="mainHS1" disabled>Main heating system</option>';
        out += '<option value="mainHS2_whole_house">2<sup>nd</sup> Main heating system - whole house</option>';
        out += '<option value="mainHS2_part_of_the_house">2<sup>nd</sup> Main heating system - different part of the house</option>';
    } else {
        out += '<option value="mainHS1" disabled>Main heating system</option>';
        out += '<option value="mainHS2_whole_house" disabled>2<sup>nd</sup> Main heating system - whole house</option>';
        out += '<option value="mainHS2_part_of_the_house" disabled>2<sup>nd</sup> Main heating system - different part of the house</option>';
    }

    out += '<option value="secondaryHS">Secondary heating system</option>';
    return out;
}

function add_heating_systems() {
    if (data.heating_systems.length > 0) {
        $('#heating-systems').show();
    } else {
        $('#heating-systems').hide();
    }

    $('#heating-systems tbody').html('');

    // Generate html string
    var mainHSs = { mainHS1: false, mainHS2: false };// Used to enable/disable options in the "main_space_heating_system" select
    var isMaster = scenario === 'master';

    for (z in data.heating_systems) {
        const item = data.heating_systems[z];
        const providesWH = item.provides === 'water' || item.provides === 'heating_and_water';
        const providesSH = item.provides === 'heating' || item.provides === 'heating_and_water';

        out = `
            <tr style="background-color: var(--grey-800)">
                <td colspan="10">
                    <b>${item.tag}: ${item.name}</b>

                    <span class="heating_systems-measure-applied" item-id="${item.id}" style="display: none">
                        (measure applied)
                    </span>

                    <button class="btn edit-item-heating-system if-master ml-7" row="${z}" tag="${item.tag}" item='${JSON.stringify(item)}'>
                        <i class="icon-edit"></i> Edit
                    </button>

                    <button class="btn delete-heating-system if-master ml-7" row="${z}">
                        <i class="icon-trash"></i> Delete
                    </button>

                    <button id="replace-heating-system-with-measure" class="btn if-not-master if-not-locked ml-7" item_index="${z}">
                        Apply measure
                    </button>
                </td>
            </tr>
            <tr class="tr-no-top-border">
                <td>
                    <span class="small-caps">Provides</span><br>
                    <select style="width: 200px" key="data.heating_systems.${z}.provides">
                        <option value="heating">Space heating</option>
                        <option value="water">Water heating</option>
                        <option value="heating_and_water">Space and water heating</option>
                    </select>

                    <br>

                    <span class="small-caps">Fuel</span><br>
                    <select style="width: 230px" key="data.heating_systems.${z}.fuel">
                        ${get_fuels_for_select()}
                    </select>
                </td>
                <td class="stack-v">
                    ${providesSH ? `
                    <section>
                        <h4 class="mt-0">Space Heating</h4>

                        <div class="form-grid__root">
                            <label>Space heating / Winter efficiency</label>
                            <div class="form-control">
                                ${item.winter_efficiency}
                            </div>

                            <label>
                                Main heating system?
                                <i class='icon-question-sign' title='The main heating system is that which heats the largest proportion of the dwelling, and often provided hot water as well as space heating. If there is more than one main heating system identified, main system 1 always heats the living space. Secondary heating systems are room heaters - such as open fires or wood-stoves. If portable room heaters are used, they should be included in the calculations (note this is a deviation from standard SAP - ref p.40 SAP 9.92)'></i>
                            </label>
                            <div>
                                <select style="width: 200px" key="data.heating_systems.${z}.main_space_heating_system">
                                    ${get_heating_system_options(mainHSs)}
                                </select>
                            </div>

                            <label>
                                Fraction of space heating provided
                                <i class='icon-question-sign' title='This defines what proportion of the space heating or water heating for a home is provided by the system listed. For example, a standard assumption might be that a gas boiler provides 90% (0.9) of the space heating, and an open fire or room stove provides 10% (0.1). However, this can be adjusted using assessors informed judgement as required - and as many systems as are present can be included. If there are two main heating systems identified, the proportion of heat provided by each system should be taken as the relative heated floor area served by each system'></i>
                            </label>
                            <div>
                                <input style="width: 55px" type="number" key="data.heating_systems.${z}.fraction_space" max="1" step="0.01" min="0">
                            </div>

                            <label>Responsiveness <i class='icon-question-sign' title='Refer to Table 4d, p.209 SAP 9.92'></i></label>
                            <div>
                                <input style="width: 55px" type="number" key="data.heating_systems.${z}.responsiveness" max="1" step="0.01" min="0">
                            </div>

                            <label>Temperature adjustment <i class='icon-question-sign' title='SAP2012, table 4e, p.210'></i></label>
                            <div>
                                <input style="width: 55px" type="number" key="data.heating_systems.${z}.temperature_adjustment" max="1" step="0.01" min="0">
                            </div>

                            <label>Space heating controls <i class='icon-question-sign' title='Refer to Table 4e, p.210 SAP 9.92'></i></label>
                            <div>
                                <input type="number" key="data.heating_systems.${z}.heating_controls" ${isMaster ? '' : 'readonly'} style="width: 55px">
                                <button class="btn if-not-locked apply-space-heating-control if-not-master" item_index="${z}">
                                    Apply measure
                                </button>

                                <div class="space_heating_control_type-measure-applied" item-id="${item.id}" style="margin-top: 7px; display: none">
                                    Measure applied
                                </div>
                            </div>

                            <label>Central heating pump inside dwelling?</label>
                            <div>
                                <input type="checkbox" key="data.heating_systems.${z}.central_heating_pump_inside">
                            </div>
                        </div>
                    </section>
                    ` : ''}

                    ${providesWH ? `
                    <section>
                        <h4 class="mt-0">Water Heating</h4>

                        <div class="form-grid__root">
                            <label>Water heating / Summer efficiency</label>
                            <span>${item.summer_efficiency}</span>

                            <label>
                                Fraction of water heating provided
                                <i class='icon-question-sign' title='This defines what proportion of the space heating or water heating for a home is provided by the system listed. For example, a standard assumption might be that a gas boiler provides 90% (0.9) of the space heating, and an open fire or room stove provides 10% (0.1). However, this can be adjusted using assessors informed judgement as required - and as many systems as are present can be included. If there are two main heating systems identified, the proportion of heat provided by each system should be taken as the relative heated floor area served by each system'></i>
                            </label>
                            <div>
                                <input class="form-control" style="width: 55px" type="number" key="data.heating_systems.${z}.fraction_water_heating">
                            </div>

                            <label>Instantaneous water heating at point of use?</label>
                            <div>
                                <input type="checkbox" key="data.heating_systems.${z}.instantaneous_water_heating">
                            </div>
                        </div>
                    </section>
                    `: ''}
                </td>
            </tr>`;

        if (item.main_space_heating_system == 'mainHS1') {
            mainHSs.mainHS1 = true;
        } else if (item.main_space_heating_system == 'mainHS2_whole_house' || item.main_space_heating_system == 'mainHS2_part_of_the_house') {
            mainHSs.mainHS2 = true;
        }

        $('#heating-systems tbody').append(out);
    }
}

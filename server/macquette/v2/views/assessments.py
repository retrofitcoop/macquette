import io
import json
import os

import PIL
from django.core.files.base import ContentFile
from django.db.models import Case, Value, When
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import exceptions, filters, generics, parsers, serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import Assessment, Image, Organisation, Report
from ..permissions import (
    IsAdminOfConnectedOrganisation,
    IsAssessmentOwner,
    IsInOrganisation,
    IsMemberOfAssessmentOrganisation,
)
from ..reports import render_template, render_to_pdf
from ..serializers import (
    AssessmentFullSerializer,
    AssessmentFullWithoutDataSerializer,
    AssessmentMetadataSerializer,
    AssessmentReportInputSerializer,
    FeaturedImageSerializer,
    ImageSerializer,
    OrganisationAssignmentSerializer,
    get_access,
)
from .helpers import get_assessments_for_user, get_images
from .mixins import AssessmentQuerySetMixin

status_ordering = Case(
    When(status="In progress", then=Value("1")),
    When(status="For review", then=Value("2")),
    When(status="Paused", then=Value("3")),
    When(status="Complete", then=Value("4")),
    When(status="Template", then=Value("5")),
    When(status="Test", then=Value("6")),
    When(status="Abandoned", then=Value("7")),
)


class ListCreateAssessments(AssessmentQuerySetMixin, generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = AssessmentMetadataSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ["name", "description"]

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)

        return (
            queryset.prefetch_related("owner", "organisation")
            .defer("data")
            .order_by(status_ordering, "-updated_at")
        )

    class PostInputSerializer(serializers.Serializer):
        name = serializers.CharField()
        description = serializers.CharField(allow_blank=True, required=False)
        # SAFETY: this field shadows a property of the same name but with a
        # different type. This is fine at runtime but not in typechecking (yet).
        data = serializers.JSONField(allow_null=True, default=dict)  # type: ignore[assignment]

    def post(self, request, *args, **kwargs):
        serializer = self.PostInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        assessment = Assessment.objects.create(
            **serializer.data,
            owner=request.user,
        )

        result = AssessmentMetadataSerializer(assessment)
        return Response(result.data, status=status.HTTP_201_CREATED)


class RetrieveUpdateDestroyAssessment(
    AssessmentQuerySetMixin, generics.RetrieveUpdateDestroyAPIView
):
    serializer_class = AssessmentFullSerializer
    permission_classes = [IsAuthenticated]

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", False)
        assessment = self.get_object()

        serializer = self.get_serializer(assessment, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        if "data" in request.data and assessment.status == "Complete":
            return Response(
                {"detail": "can't update data when status is 'complete'"},
                status.HTTP_400_BAD_REQUEST,
            )

        if "owner" in request.data:
            from .helpers import check_assessment_reassign_permissions

            can_reassign, message = check_assessment_reassign_permissions(
                assessment, request, request.data["owner"]
            )
            if not can_reassign:
                return Response({"detail": message}, status.HTTP_400_BAD_REQUEST)

        serializer.save()

        non_data_fields = {*request.data.keys()} - {"data"}
        if len(non_data_fields) > 0:
            return Response(
                AssessmentFullWithoutDataSerializer(
                    assessment, context={"request": request}
                ).data,
                status.HTTP_200_OK,
            )
        else:
            return Response(None, status.HTTP_204_NO_CONTENT)


class ShareUnshareAssessment(AssessmentQuerySetMixin, generics.GenericAPIView):
    permission_classes = [
        IsAuthenticated,
        IsInOrganisation,
        IsAssessmentOwner | IsAdminOfConnectedOrganisation,
    ]

    def put(self, request, pk, userid):
        assessment = self.get_object()

        if not assessment.organisation.members.filter(id=userid).exists():
            return Response(
                {"detail": "can't share to users outside organisation"},
                status.HTTP_400_BAD_REQUEST,
            )

        assessment.shared_with.add(userid)

        return Response(get_access(assessment), status.HTTP_200_OK)

    def delete(self, request, pk, userid):
        assessment = self.get_object()

        if not assessment.organisation:
            return Response(
                {"detail": "can't unshare private assessment"},
                status.HTTP_400_BAD_REQUEST,
            )

        assessment.shared_with.remove(userid)

        return Response(get_access(assessment), status.HTTP_200_OK)


class AssignAssessmentToOrganisation(AssessmentQuerySetMixin, generics.GenericAPIView):
    permission_classes = [IsAuthenticated, IsAssessmentOwner]

    def put(self, request, pk, orgid):
        assessment = self.get_object()

        if assessment.organisation:
            raise exceptions.PermissionDenied(
                "Can only assign private assessments to organisations"
            )

        try:
            organisation = Organisation.objects.filter(
                members__in=[request.user.pk]
            ).get(pk=orgid)
        except Organisation.DoesNotExist:
            raise exceptions.PermissionDenied("Organisation not found")

        assessment.organisation = organisation
        assessment.save()

        return Response(
            OrganisationAssignmentSerializer(assessment).data, status.HTTP_200_OK
        )


class DuplicateAssessment(AssessmentQuerySetMixin, generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        assessment = self.get_object()

        assessment.pk = None
        assessment.name = f"Copy of {assessment.name}"
        assessment.owner = request.user
        assessment.save()

        response = AssessmentMetadataSerializer(assessment).data

        return Response(response, status.HTTP_200_OK)


class SetFeaturedImage(AssessmentQuerySetMixin, generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        assessment = self.get_object()

        serializer = FeaturedImageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({"detail": "invalid id"}, status.HTTP_400_BAD_REQUEST)

        try:
            image = Image.objects.get(pk=serializer.validated_data["id"])
        except Image.DoesNotExist:
            return Response(
                {"detail": "image ID doesn't exist"}, status.HTTP_400_BAD_REQUEST
            )

        if image not in assessment.images.all():
            return Response(
                {"detail": "image ID provided doesn't belong to this assessment"},
                status.HTTP_400_BAD_REQUEST,
            )

        assessment.featured_image = image
        assessment.updated_at = timezone.now()
        assessment.save(update_fields=["updated_at", "featured_image"])

        return Response(None, status.HTTP_204_NO_CONTENT)


class UploadAssessmentImage(AssessmentQuerySetMixin, generics.GenericAPIView):
    parser_class = [parsers.FileUploadParser]
    permission_classes = [IsAuthenticated]

    @staticmethod
    def _make_thumbnail(image, record: Image):
        image = PIL.ImageOps.exif_transpose(image)

        record.width = image.width
        record.height = image.height

        # We paste transparent images onto a new image with a white background,
        # and then use that as our image.  This is because we're saving as JPEG, which
        # famously does not support transparency.
        if image.mode in ["RGBA", "LA"]:
            background = PIL.Image.new(image.mode[:-1], image.size, "white")
            background.paste(image, image.split()[-1])
            image = background

        # 600x600 is a substantial size saving on bigger images while still not looking
        # super-lossy on a high DPI screen
        image.thumbnail((600, 600))

        # Save thumbnail to in-memory file
        temp_thumb = io.BytesIO()
        image.save(temp_thumb, "JPEG")
        temp_thumb.seek(0)

        # save=False is because otherwise it will run in an infinite loop
        record.thumbnail.save("thumb.jpg", ContentFile(temp_thumb.read()), save=False)
        temp_thumb.close()

        record.thumbnail_width = image.width
        record.thumbnail_height = image.height

    @staticmethod
    def _set_note(record: Image):
        leaf, ext = os.path.splitext(os.path.basename(record.image.name))
        record.note = leaf

    def post(self, request, pk):
        if "file" not in request.FILES:
            return Response({"detail": "no file provided"}, status.HTTP_400_BAD_REQUEST)

        assessment = self.get_object()
        file = request.FILES["file"]
        record = Image(assessment=assessment, image=file)

        try:
            image = PIL.Image.open(record.image)
        except PIL.UnidentifiedImageError:
            raise exceptions.ParseError(detail="Could not process image format")

        self._make_thumbnail(image, record)
        self._set_note(record)
        record.save()
        response = ImageSerializer(record).data

        assessment.updated_at = timezone.now()
        assessment.save(update_fields=["updated_at"])

        return Response(response, status.HTTP_200_OK)


class CreateAssessmentReports(AssessmentQuerySetMixin, generics.CreateAPIView):
    permission_classes = [
        IsAuthenticated,
        IsMemberOfAssessmentOrganisation,
    ]

    def post(self, request, assessmentid):
        serializer = AssessmentReportInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        assessment = get_assessments_for_user(request.user).get(pk=assessmentid)
        organisation = assessment.organisation

        html = render_template(
            organisation.report.template,
            {
                "org": {
                    "name": organisation.name,
                    **organisation.report_vars,
                },
                "images": get_images(assessment),
                **serializer.data["context"],
            },
            serializer.data["graphs"],
        )
        pdf = render_to_pdf(html)

        report = Report(assessment=assessment)
        report.save()
        reproducibility_data = {
            **serializer.data["reproducibility_data"],
            "report_template": organisation.report.template,
        }
        reproducibility_data_file = ContentFile(json.dumps(reproducibility_data))
        report.reproducibility_data.save(
            "reproducibility-data.json", reproducibility_data_file
        )
        report.file.save("report.pdf", ContentFile(pdf))
        report.save()

        response = HttpResponse(status=303)
        response["Location"] = report.file.url
        return response


class PreviewAssessmentReport(APIView):
    permission_classes = [
        IsAuthenticated,
        IsMemberOfAssessmentOrganisation,
    ]

    def post(self, request, assessmentid):
        serializer = AssessmentReportInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        assessment = get_assessments_for_user(request.user).get(pk=assessmentid)
        organisation = assessment.organisation

        html = render_template(
            organisation.report.template,
            {
                "org": {
                    "name": organisation.name,
                    **organisation.report_vars,
                },
                "images": get_images(assessment),
                **serializer.data["context"],
            },
            serializer.data["graphs"],
        )
        response = HttpResponse(status=200, content_type="text/html")
        response.write(html)
        return response

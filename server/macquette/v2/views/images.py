from rest_framework import generics, permissions, status
from rest_framework.response import Response

from .. import models, serializers
from . import helpers


class UpdateDestroyImage(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    queryset = models.Image.objects
    serializer_class = serializers.ImageUpdateSerializer

    def delete(self, request, pk):
        image = self.get_object()

        allowed_assessments = helpers.get_assessments_for_user(request.user)
        if image.assessment_id not in allowed_assessments.values_list("id", flat=True):
            return Response(None, status.HTTP_403_FORBIDDEN)

        image.delete()

        return Response(None, status.HTTP_204_NO_CONTENT)

    def patch(self, request, pk):
        image = self.get_object()

        allowed_assessments = helpers.get_assessments_for_user(request.user)
        if image.assessment_id not in allowed_assessments.values_list("id", flat=True):
            return Response(None, status.HTTP_403_FORBIDDEN)

        serializer = serializers.ImageUpdateSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({"detail": serializer.errors}, status.HTTP_400_BAD_REQUEST)

        image.note = serializer.validated_data["note"]
        image.save()

        response = serializers.ImageSerializer(image).data
        return Response(response, status.HTTP_200_OK)

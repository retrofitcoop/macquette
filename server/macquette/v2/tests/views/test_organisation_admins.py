from rest_framework import status
from rest_framework.test import APITestCase

from macquette.users.tests.factories import UserFactory

from ..factories import OrganisationFactory
from .helpers import assert_error


class SetUpMixin:
    @classmethod
    def setUp(cls):
        cls.org_admin = UserFactory.create()
        cls.org_admin2 = UserFactory.create()
        cls.normal_member = UserFactory.create()
        cls.non_member = UserFactory.create()

        cls.org = OrganisationFactory.create()

        cls.org.members.add(cls.org_admin)
        cls.org.members.add(cls.org_admin2)
        cls.org.members.add(cls.normal_member)

        cls.org.admins.add(cls.org_admin)
        cls.org.admins.add(cls.org_admin2)


class TestCreateOrganisationAdmins(SetUpMixin, APITestCase):
    def test_can_promote_organisation_member_as_admin(self):
        self.client.force_authenticate(self.org_admin)

        response = self.client.post(
            f"/api/organisations/{self.org.id}/admins/{self.normal_member.id}/"
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert self.normal_member in self.org.admins.all()

    def test_returns_204_if_promoting_user_who_is_already_admin(self):
        self.client.force_authenticate(self.org_admin)

        response = self.client.post(
            f"/api/organisations/{self.org.id}/admins/{self.org_admin2.id}/"
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert self.org_admin2 in self.org.admins.all()

    def test_cannot_promote_user_who_isnt_member_of_org_as_admin(self):
        self.client.force_authenticate(self.org_admin)

        response = self.client.post(
            f"/api/organisations/{self.org.id}/admins/{self.non_member.id}/"
        )

        assert_error(
            response,
            status.HTTP_400_BAD_REQUEST,
            f"{self.non_member} is not a member of {self.org}",
        )
        assert self.non_member not in self.org.admins.all()


class TestDeleteOrganisationAdmins(SetUpMixin, APITestCase):
    def test_can_demote_admin_successfully(self):
        self.client.force_authenticate(self.org_admin)

        response = self.client.delete(
            f"/api/organisations/{self.org.id}/admins/{self.org_admin2.id}/"
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert self.org_admin2 not in self.org.admins.all()

    def test_returns_204_if_demoting_user_who_is_already_not_admin(self):
        self.client.force_authenticate(self.org_admin)

        response = self.client.delete(
            f"/api/organisations/{self.org.id}/admins/{self.normal_member.id}/"
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert self.normal_member not in self.org.admins.all()

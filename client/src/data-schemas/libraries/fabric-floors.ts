import { z } from 'zod';

import type { Library } from '.';
import {
  highLowCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

export const floor = z
  .object({
    description: z.string(),
    type: z
      .enum(['custom', 'solid (bs13370)', 'suspended', 'heated basement', 'exposed'])
      .default('custom'),
    uValue: z.number().nullable(),
    kValue: z.number(),
  })
  .merge(libraryItemCommonSchema);

const floorMeasure = floor
  .merge(measureCommonSchema)
  .merge(highLowCarbonAccountingSchema)
  .extend({
    cost_units: z.union([z.literal('ln m' as const), z.literal('sqm' as const)]),
  });

export const fabricFloors = makeLibrarySchema('floors', floor);
export const fabricFloorMeasures = makeLibrarySchema('floor_measures', floorMeasure);

export type Floor = z.infer<typeof floor>;
export type FloorMeasure = z.infer<typeof floorMeasure>;

export type FabricFloorsLibrary = z.infer<typeof fabricFloors>;
export function isFabricFloorsLibrary(library: Library): library is FabricFloorsLibrary {
  return library.type === 'floors';
}

export type FabricFloorMeasuresLibrary = z.infer<typeof fabricFloorMeasures>;
export function isFabricFloorMeasuresLibrary(
  library: Library,
): library is FabricFloorMeasuresLibrary {
  return library.type === 'floor_measures';
}

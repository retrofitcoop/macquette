import { z } from 'zod';

import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const item = z.object({ ventilation_rate: z.number() }).merge(libraryItemCommonSchema);

const measure = item.merge(measureCommonSchema).merge(basicCarbonAccountingSchema);

export type ExtractVent = z.infer<typeof item>;
export type ExtractVentMeasure = z.infer<typeof measure>;

export const extractVentilationPointsMeasures = makeLibrarySchema(
  'extract_ventilation_points',
  measure,
);
export type ExtractVentMeasuresLibrary = z.infer<typeof extractVentilationPointsMeasures>;
export function isExtractVentMeasuresLibrary(
  library: Library,
): library is ExtractVentMeasuresLibrary {
  return library.type === 'extract_ventilation_points';
}

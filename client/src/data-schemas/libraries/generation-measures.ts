import { z } from 'zod';

import type { Library } from '.';
import {
  highLowCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

export const generationMeasure = z
  .object({
    kWp: z.number(),
  })
  .merge(libraryItemCommonSchema)
  .merge(measureCommonSchema)
  .merge(highLowCarbonAccountingSchema)
  .extend({
    cost_units: z.union([z.literal('unit' as const), z.literal('kWp' as const)]),
  });

export const generationMeasures = makeLibrarySchema(
  'generation_measures',
  generationMeasure,
);

export type GenerationMeasure = z.infer<typeof generationMeasure>;
export type GenerationMeasuresLibrary = z.infer<typeof generationMeasures>;
export function isGenerationMeasuresLibrary(
  library: Library,
): library is GenerationMeasuresLibrary {
  return library.type === 'generation_measures';
}

import { z } from 'zod';
import type { Library } from '.';

import { stringyFloatSchema } from '../scenario/value-schemas';
import {
  highLowCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const item = z
  .object({
    category: z.enum([
      'Combi boilers',
      'Heat pumps',
      'Hot water only',
      'Room heaters',
      'System boilers',
      'Warm air systems',
    ]),
    combi_loss: z.enum([
      '0',
      'Instantaneous, without keep hot-facility',
      'Instantaneous, with keep-hot facility controlled by time clock',
      'Instantaneous, with keep-hot facility not controlled by time clock',
      'Storage combi boiler >= 55 litres',
      'Storage combi boiler < 55 litres',
    ]),
    sfp: z.number().nullable(),
    responsiveness: stringyFloatSchema,
    summer_efficiency: stringyFloatSchema,
    winter_efficiency: z.number(),
    central_heating_pump: stringyFloatSchema,
    primary_circuit_loss: z.enum(['Yes', 'No']),
    fans_and_supply_pumps: stringyFloatSchema,
  })
  .merge(libraryItemCommonSchema);

const measure = item.merge(measureCommonSchema).merge(highLowCarbonAccountingSchema);

export type HeatingSystem = z.infer<typeof item>;
export type HeatingSystemMeasure = z.infer<typeof measure>;

export const heatingSystems = makeLibrarySchema('heating_systems', item);
export type HeatingSystemsLibrary = z.infer<typeof heatingSystems>;
export function isHeatingSystemsLibrary(
  library: Library,
): library is HeatingSystemsLibrary {
  return library.type === 'heating_systems';
}

export const heatingSystemsMeasures = makeLibrarySchema(
  'heating_systems_measures',
  measure,
);
export type HeatingSystemMeasuresLibrary = z.infer<typeof heatingSystemsMeasures>;
export function isHeatingSystemMeasuresLibrary(
  library: Library,
): library is HeatingSystemMeasuresLibrary {
  return library.type === 'heating_systems_measures';
}

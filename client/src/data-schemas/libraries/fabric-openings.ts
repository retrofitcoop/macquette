import { z } from 'zod';

import type { Library } from '.';
import {
  highLowCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const basic = z
  .object({
    description: z.string(),
    uValue: z.number(),
    kValue: z.number(),
  })
  .merge(libraryItemCommonSchema);

const glazedOpening = basic.merge(
  z.object({
    type: z.enum(['door', 'roof light', 'window']),
    g: z.number(),
    ff: z.number(),
    gL: z.number(),
  }),
);
const unglazedOpening = basic.merge(
  z.object({
    type: z.literal('hatch'),
    g: z.null(),
    ff: z.null(),
    gL: z.null(),
  }),
);
const opening = z.union([glazedOpening, unglazedOpening]);

const openingMeasure = z.union([
  glazedOpening
    .merge(measureCommonSchema)
    .merge(highLowCarbonAccountingSchema)
    .extend({
      cost_units: z.enum(['sqm', 'unit']),
    }),
  unglazedOpening
    .merge(measureCommonSchema)
    .merge(highLowCarbonAccountingSchema)
    .extend({
      cost_units: z.enum(['sqm', 'unit']),
    }),
]);

export const fabricOpenings = makeLibrarySchema('openings', opening);
export const fabricOpeningMeasures = makeLibrarySchema(
  'opening_measures',
  openingMeasure,
);

export type Opening = z.infer<typeof opening>;
export type OpeningMeasure = z.infer<typeof openingMeasure>;

export type FabricOpeningsLibrary = z.infer<typeof fabricOpenings>;
export function isFabricOpeningsLibrary(
  library: Library,
): library is FabricOpeningsLibrary {
  return library.type === 'openings';
}

export type FabricOpeningMeasuresLibrary = z.infer<typeof fabricOpeningMeasures>;
export function isFabricOpeningMeasuresLibrary(
  library: Library,
): library is FabricOpeningMeasuresLibrary {
  return library.type === 'opening_measures';
}

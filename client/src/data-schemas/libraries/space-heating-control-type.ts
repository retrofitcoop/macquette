import { z } from 'zod';
import type { Library } from '.';

import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const measure = z
  .object({
    control_type: z.number(),
  })
  .merge(libraryItemCommonSchema)
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema);

export type HeatingControlMeasure = z.infer<typeof measure>;

export const spaceHeatingControlTypeMeasures = makeLibrarySchema(
  'space_heating_control_type',
  measure,
);
export type HeatingControlMeasureLibrary = z.infer<
  typeof spaceHeatingControlTypeMeasures
>;
export function isHeatingControlMeasureLibrary(library: Library) {
  return library.type === 'space_heating_control_type';
}

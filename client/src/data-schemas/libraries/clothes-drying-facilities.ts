import { z } from 'zod';

import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const measure = libraryItemCommonSchema
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema);

export const clothesDryingFacilitiesMeasures = makeLibrarySchema(
  'clothes_drying_facilities',
  measure,
);

export type ClothesDryingMeasure = z.infer<typeof measure>;
export type ClothesDryingMeasuresLibrary = z.infer<
  typeof clothesDryingFacilitiesMeasures
>;
export function isClothesDryingMeasuresLibrary(
  library: Library,
): library is ClothesDryingMeasuresLibrary {
  return library.type === 'clothes_drying_facilities';
}

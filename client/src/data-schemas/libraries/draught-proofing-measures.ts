import { z } from 'zod';

import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const measure = z
  .object({
    q50: z.number(),
  })
  .merge(libraryItemCommonSchema)
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema);

export const draughtProofingMeasures = makeLibrarySchema(
  'draught_proofing_measures',
  measure,
);

export type DraughtProofingMeasure = z.infer<typeof measure>;
export type DraughtProofingMeasuresLibrary = z.infer<typeof draughtProofingMeasures>;
export function isDraughtProofingMeasuresLibrary(
  library: Library,
): library is DraughtProofingMeasuresLibrary {
  return library.type === 'draught_proofing_measures';
}

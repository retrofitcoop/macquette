import { z } from 'zod';
import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const common = z.object({
  category: z.enum(['Cylinders with inmersion', 'Indirectly heated cylinders']),
  storage_volume: z.number(),
});

const manufacturerDeclaredLossFactor = z
  .object({
    declared_loss_factor_known: z.literal(true as const),
    manufacturer_loss_factor: z.number(),
    temperature_factor_a: z.number(),
  })
  .merge(common)
  .merge(libraryItemCommonSchema);

const undeclaredLossFactor = z
  .object({
    declared_loss_factor_known: z.literal(false as const),
    loss_factor_b: z.number(),
    volume_factor_b: z.number(),
    temperature_factor_b: z.number(),
  })
  .merge(common)
  .merge(libraryItemCommonSchema);

export const item = z.union([manufacturerDeclaredLossFactor, undeclaredLossFactor]);

export const measure = z.union([
  manufacturerDeclaredLossFactor
    .merge(measureCommonSchema)
    .merge(basicCarbonAccountingSchema),
  undeclaredLossFactor.merge(measureCommonSchema).merge(basicCarbonAccountingSchema),
]);

export type HotWaterStorage = z.infer<typeof item>;
export type HotWaterStorageMeasure = z.infer<typeof measure>;

export const storageType = makeLibrarySchema('storage_type', item);
export type HotWaterStorageLibrary = z.infer<typeof storageType>;
export function isHotWaterStorageLibrary(
  library: Library,
): library is HotWaterStorageLibrary {
  return library.type === 'storage_type';
}

export const storageTypeMeasures = makeLibrarySchema('storage_type_measures', measure);
export type HotWaterStorageMeasuresLibrary = z.infer<typeof storageTypeMeasures>;
export function isHotWaterStorageMeasuresLibrary(
  library: Library,
): library is HotWaterStorageMeasuresLibrary {
  return library.type === 'storage_type_measures';
}

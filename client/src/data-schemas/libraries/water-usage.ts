import { z } from 'zod';
import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const item = libraryItemCommonSchema;

const measure = item.merge(measureCommonSchema).merge(basicCarbonAccountingSchema);

export type WaterUsageItem = z.infer<typeof item>;
export type WaterUsageMeasure = z.infer<typeof measure>;

export const waterUsageMeasures = makeLibrarySchema('water_usage', measure);
export type WaterUsageMeasuresLibrary = z.infer<typeof waterUsageMeasures>;
export function isWaterUsageMeasuresLibrary(
  library: Library,
): library is WaterUsageMeasuresLibrary {
  return library.type === 'water_usage';
}

import { z } from 'zod';

import type { Library } from '.';
import { libraryItemCommonSchema, makeLibrarySchema } from './common';

const floorInsulationConductivityMaterialItem = libraryItemCommonSchema.extend({
  mechanism: z.literal('conductivity' as const),
  conductivity: z.number(),
  type: z.string().optional(),
  description: z.string(),
});
export type FloorInsulationConductivityMaterial = z.infer<
  typeof floorInsulationConductivityMaterialItem
>;
const floorInsulationResistanceMaterialItem = libraryItemCommonSchema.extend({
  mechanism: z.literal('resistance' as const),
  resistance: z.number(),
  type: z.string().optional(),
  description: z.string(),
});
export type FloorInsulationResistanceMaterial = z.infer<
  typeof floorInsulationResistanceMaterialItem
>;
export const floorInsulationMaterialItem = z.union([
  floorInsulationConductivityMaterialItem,
  floorInsulationResistanceMaterialItem,
]);
export type FloorInsulationMaterial = z.infer<typeof floorInsulationMaterialItem>;

export type FloorInsulationMaterialLibrary = z.infer<typeof floorInsulation>;
export const floorInsulation = makeLibrarySchema(
  'floor_insulation_materials',
  floorInsulationMaterialItem,
);
export function isFloorInsulationMaterialLibrary(
  l: Library,
): l is FloorInsulationMaterialLibrary {
  return l.type === 'floor_insulation_materials';
}

import { z } from 'zod';
import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const item = z
  .object({
    ventilation_rate: z.number(),
  })
  .merge(libraryItemCommonSchema);

const measure = item.merge(measureCommonSchema).merge(basicCarbonAccountingSchema);

export type VentOrFlue = z.infer<typeof item>;
export type VentOrFlueMeasure = z.infer<typeof measure>;

export const intentionalVentsAndFlues = makeLibrarySchema(
  'intentional_vents_and_flues',
  item,
);
export type VentOrFlueLibrary = z.infer<typeof intentionalVentsAndFlues>;
export function isVentOrFlueLibrary(library: Library): library is VentOrFlueLibrary {
  return library.type === 'intentional_vents_and_flues';
}

export const intentionalVentsAndFluesMeasures = makeLibrarySchema(
  'intentional_vents_and_flues_measures',
  measure,
);
export type VentOrFlueMeasuresLibrary = z.infer<typeof intentionalVentsAndFluesMeasures>;
export function isVentOrFlueMeasuresLibrary(
  library: Library,
): library is VentOrFlueMeasuresLibrary {
  return library.type === 'intentional_vents_and_flues_measures';
}

import { z } from 'zod';

import { mapValues, pickBy } from 'lodash';
import { Result } from '../../helpers/result';
import { stringyIntegerSchema } from '../scenario/value-schemas';

export const measureCommonSchema = z.object({
  associated_work: z.string(),
  benefits: z.string(),
  cost: z.number(),
  cost_units: z.string(),
  min_cost: stringyIntegerSchema
    .optional()
    .transform((val) => (val === '' || val === undefined ? 0 : val)),
  description: z.string(),
  source: z.string().optional().default(''),
  disruption: z.string(),
  key_risks: z.string(),
  maintenance: z.string(),
  lifetimeYears: z
    .number()
    .optional()
    .nullable()
    .transform((val) => val ?? null),
  notes: z.string(),
  performance: z.string(),
  who_by: z.string(),
});

export const basicCarbonAccountingSchema = z.object({
  carbonType: z.literal('basic').optional().default('basic'),
  carbonBaseUpfront: z.number().nullable().optional().default(null),
  carbonBaseBiogenic: z.number().nullable().optional().default(null),
  carbonPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonSource: z.string().optional().default(''),
});

export const highLowCarbonAccountingSchema = z.object({
  carbonType: z.literal('high/low').optional().default('high/low'),
  carbonHighBaseUpfront: z.number().nullable().optional().default(null),
  carbonHighBaseBiogenic: z.number().nullable().optional().default(null),
  carbonHighPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonHighPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonLowBaseUpfront: z.number().nullable().optional().default(null),
  carbonLowBaseBiogenic: z.number().nullable().optional().default(null),
  carbonLowPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonLowPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonSource: z.string().optional().default(''),
});

export const libraryItemCommonSchema = z.object({
  tag: z.string(),
  name: z.string(),
  source: z.string().optional().default(''),
});

export function makeLibrarySchema<Type extends string, ItemT>(
  type: Type,
  itemSchema: z.ZodType<ItemT, z.ZodTypeDef, unknown>,
) {
  return z.object({
    id: z.string(),
    name: z.string(),
    type: z.literal(type),
    data: z
      .record(
        z.union([
          itemSchema.transform((value) => ({
            type: 'known' as const,
            original: value,
          })),
          z.unknown().transform((value) => ({
            type: 'unknown' as const,
            original: value,
          })),
        ]),
      )
      .transform<Record<string | symbol, ItemT>>((data) =>
        mapValues(
          pickBy(data, (item): item is typeof item & { type: 'known' } => {
            if (item.type === 'known') {
              return true;
            } else {
              console.error('Encountered an invalid library item', {
                libraryType: type,
                libraryItem: JSON.stringify(item.original),
                zod: Result.zodParse(itemSchema, item.original).coalesce(),
              });
              return false;
            }
          }),
          ({ original }) => original,
        ),
      ),
  });
}

import { z } from 'zod';

type WithOriginUserData<T, U = never> =
  | { type: 'user provided'; value: T | U }
  | { type: 'overridden'; value: T | U; dbValue: T };

export type WithOrigin<T, U = never> =
  | { type: 'no data' }
  | { type: 'from database'; value: T }
  | WithOriginUserData<T, U>;

export function withOriginSchema<
  Value,
  NoValue,
  T extends z.ZodType<Value>,
  U extends z.ZodType<NoValue>,
>(valueSchema: T, noValueSchema: U) {
  return z.discriminatedUnion('type', [
    z.object({ type: z.literal('no data') }),
    z.object({
      type: z.literal('user provided'),
      value: z.union([valueSchema, noValueSchema]),
    }),
    z.object({ type: z.literal('from database'), value: valueSchema }),
    z.object({
      type: z.literal('overridden'),
      value: z.union([valueSchema, noValueSchema]),
      dbValue: valueSchema,
    }),
  ]);
}

export function hasUserData<T, U>(
  data: WithOrigin<T, U>,
): data is WithOriginUserData<T, U> {
  return data.type === 'user provided' || data.type === 'overridden';
}

export function getWithOriginValue<T, U>(data: WithOrigin<T, U>): T | U | undefined {
  return data.type === 'no data' ? undefined : data.value;
}

export function withUserData<T, U>(
  data: WithOrigin<T, U>,
  value: T | U,
): WithOrigin<T, U> {
  switch (data.type) {
    case 'no data':
      return { type: 'user provided', value };
    case 'user provided':
    case 'overridden':
      return { ...data, value };
    case 'from database':
      return { type: 'overridden', dbValue: data.value, value };
  }
}

export function withDatabaseData<T, U>(
  data: WithOrigin<T, U>,
  value: T,
): WithOrigin<T, U> {
  switch (data.type) {
    case 'no data':
      return { type: 'from database', value };
    case 'user provided':
      if (data.value === value) {
        return { type: 'from database', value: value };
      } else {
        return { type: 'overridden', value: data.value, dbValue: value };
      }
    case 'overridden':
      return { ...data, dbValue: value };
    case 'from database':
      return { ...data, value };
  }
}

import { z } from 'zod';

import { dateSchema } from './helpers/date';

export const statusSchema = z.enum([
  'Complete',
  'In progress',
  'For review',
  'Test',
  'Template',
  'Paused',
  'Abandoned',
]);

export const userSchema = z.object({
  id: z.string(),
  name: z.string(),
  email: z.string().optional(),
});
export type User = z.infer<typeof userSchema>;

export const organisationMetaSchema = z.object({
  id: z.string(),
  name: z.string(),
});
export type Organisation = z.infer<typeof organisationMetaSchema>;

export const userAccessSchema = z.array(
  z.object({
    roles: z.array(z.enum(['owner', 'org_admin', 'editor'])),
    id: z.string(),
    name: z.string(),
    email: z.string(),
  }),
);
export type UserAccess = z.output<typeof userAccessSchema>;

export const assessmentMetadataSchema = z.object({
  id: z.string(),
  name: z.string(),
  description: z.string(),
  status: statusSchema,
  createdAt: dateSchema,
  updatedAt: dateSchema,
  owner: userSchema,
  organisation: z.union([z.null(), organisationMetaSchema]),
});

export const createAssessmentSchema = assessmentMetadataSchema;
export const listAssessmentSchema = z.array(assessmentMetadataSchema);
export type AssessmentMetadata = z.infer<typeof assessmentMetadataSchema>;

export const organisationAssignmentSchema = z.object({
  access: userAccessSchema,
  organisation: organisationMetaSchema,
});
export type OrganisationAssignmentResponse = z.infer<typeof organisationAssignmentSchema>;

export const libraryTypes = [
  'appliances_and_cooking' as const,
  'clothes_drying_facilities' as const,
  'draught_proofing_measures' as const,
  'extract_ventilation_points' as const,
  'floor_insulation_materials' as const,
  'floor_measures' as const,
  'floors' as const,
  'generation_measures' as const,
  'heating_systems' as const,
  'heating_systems_measures' as const,
  'hot_water_control_type' as const,
  'intentional_vents_and_flues' as const,
  'intentional_vents_and_flues_measures' as const,
  'opening_measures' as const,
  'openings' as const,
  'pipework_insulation' as const,
  'space_heating_control_type' as const,
  'storage_type' as const,
  'storage_type_measures' as const,
  'ventilation_systems' as const,
  'ventilation_systems_measures' as const,
  'wall_measures' as const,
  'walls' as const,
  'water_usage' as const,
] as const;

export class UnknownLibraryTypeError extends Error {
  constructor(
    message: string,
    public type: string,
  ) {
    super(message);
  }
}

export const libraryMetadataSchema = z.object({
  id: z.string(),
  name: z.string(),
  type: z
    .enum(libraryTypes)
    .or(
      z
        .string()
        .transform((type) => new UnknownLibraryTypeError('unknown library', type)),
    ),
  created_at: dateSchema,
  updated_at: dateSchema,
  permissions: z.object({ can_write: z.boolean(), can_share: z.boolean() }),
  owner: z.discriminatedUnion('type', [
    z.object({ type: z.literal('global'), id: z.literal(null), name: z.string() }),
    z.object({ type: z.literal('personal') }).merge(userSchema),
    z.object({ type: z.literal('organisation') }).merge(organisationMetaSchema),
  ]),
  shared_with: z.array(z.object({ id: z.string(), name: z.string() })),
});
export type LibraryMetadata = z.infer<typeof libraryMetadataSchema>;

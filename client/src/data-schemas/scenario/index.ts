import { z } from 'zod';
import { solarHotWaterSchema } from './solar-hot-water';

import { withOriginSchema } from '../helpers/with-origin';
import { applianceCarbonCoop } from './appliance-carbon-coop';
import { fabric } from './fabric';
import { heatingSystems } from './heating-systems';
import { householdSchema } from './household';
import { measures } from './measures';

import { Region, RegionName } from '../../model/enums/region';
import {
  legacyBoolean,
  legacyString,
  nullableStringyFloat,
  numberWithNaN,
  stringyFloatSchema,
  stringyIntegerSchema,
} from './value-schemas';
import { waterHeating } from './water-heating';

const floors = z.array(
  z.object({
    area: z.nullable(stringyFloatSchema),
    height: z.nullable(stringyFloatSchema),
    name: legacyString,
  }),
);

const lacFuels = z.array(
  z.object({
    fuel: legacyString,
    fraction: numberWithNaN,
  }),
);
const LAC = z
  .object({
    L: stringyIntegerSchema,
    LLE: stringyIntegerSchema,
    reduced_heat_gains_lighting: legacyBoolean,
    energy_efficient_appliances: legacyBoolean,
    energy_efficient_cooking: legacyBoolean,
    fuels_lighting: lacFuels,
    fuels_appliances: lacFuels,
    fuels_cooking: lacFuels,
  })
  .partial();

export const modelBehaviourVersionSchema = z.union([
  z.literal('legacy'),
  z.literal(1),
  z.literal(2),
  z.literal(3),
  z.literal(4),
  z.literal(5),
  z.literal(6),
  z.literal(7),
  z.literal(8),
  z.literal(9),
  z.literal(10),
  z.literal(11),
  z.literal(12),
  z.literal(13),
  z.literal(14),
  z.literal(15),
]);
export type ModelBehaviourVersion = z.infer<typeof modelBehaviourVersionSchema>;

const regionNameOrNumber = z
  .union([z.number(), z.enum(Region.names)])
  .transform((val): RegionName => {
    switch (typeof val) {
      case 'number':
        return Region.fromIndex0(val).name;
      default:
        return val;
    }
  });

const unvalidatedHeatingPattern = z.array(
  z.object({ start: z.number().nullable(), end: z.number().nullable() }),
);

export const scenarioSchema = z
  .object({
    modelBehaviourVersion: modelBehaviourVersionSchema,
    created_from: legacyString.optional(),
    scenario_name: legacyString,
    scenario_description: legacyString,
    creation_hash: z.number().optional(),
    justCreated: z.boolean().optional(),
    floors,
    volumeReductionOverride: z.object({
      enabled: z.boolean(),
      value: z.number().nullable(),
    }),
    externalDesignTemperatureOverride: z.object({
      enabled: z.boolean(),
      value: z.number().nullable(),
    }),
    use_custom_occupancy: legacyBoolean,
    custom_occupancy: z.union([z.literal(null), z.number(), z.literal('')]),
    occupancy: z.number(),
    region: regionNameOrNumber,
    region_full: withOriginSchema(regionNameOrNumber, z.null()),
    fabric,
    water_heating: waterHeating,
    SHW: solarHotWaterSchema,
    use_SHW: legacyBoolean,
    locked: z.boolean(),
    fuels: z.record(
      z.object({
        category: z.enum([
          'Gas',
          'Solid fuel',
          'Generation',
          'generation',
          'Oil',
          'Electricity',
        ]),
        standingcharge: stringyFloatSchema,
        fuelcost: stringyFloatSchema,
        co2factor: stringyFloatSchema,
        primaryenergyfactor: stringyFloatSchema,
      }),
    ),
    LAC_calculation_type: z.enum(['SAP', 'carboncoop_SAPlighting']),
    LAC,
    ventilation: z
      .object({
        // Non-model inputs
        CDF: z.array(z.object({ id: z.number(), tag: z.string(), name: z.string() })),

        // Inputs
        IVF: z.array(
          z.object({
            id: stringyIntegerSchema,
            tag: z.string(),
            name: z.string(),
            source: z.string().optional().default(''),
            location: z.string().optional().default(''),
            ventilation_rate: stringyFloatSchema.nullable(),
            overrideVentilationRate: z.boolean().default(false),
            libraryItem: z
              .object({
                tag: z.string(),
                name: z.string(),
                source: z.string(),
                ventilation_rate: z.number(),
              })
              .optional(),
          }),
        ),
        air_permeability_test: legacyBoolean,
        air_permeability_value: stringyFloatSchema,
        dwelling_construction: z.enum(['timberframe', 'masonry']),
        suspended_wooden_floor: z.union([z.literal(0), z.enum(['sealed', 'unsealed'])]),
        percentage_draught_proofed: stringyFloatSchema,
        draught_lobby: legacyBoolean,
        number_of_sides_sheltered: z.number(),
        ventilation_type: z.enum(['NV', 'IE', 'MEV', 'PS', 'MVHR', 'MV', 'DEV']),
        EVP: z.array(
          z.object({
            id: stringyIntegerSchema,
            tag: z.string(),
            name: z.string(),
            source: legacyString.optional().default(''),
            location: legacyString.optional().default(''),
            ventilation_rate: stringyFloatSchema.nullable(),
            overrideVentilationRate: z.boolean().default(false),
            libraryItem: z
              .object({
                tag: z.string(),
                name: z.string(),
                source: z.string(),
                ventilation_rate: z.number(),
              })
              .optional(),
          }),
        ),
        system_air_change_rate: nullableStringyFloat,
        balanced_heat_recovery_efficiency: nullableStringyFloat,
        system_specific_fan_power: nullableStringyFloat,
        ventilation_name: z.string(),
        ventilation_tag: z.string(),
        libraryItem: z.object({
          tag: z.string(),
          name: z.string(),
          source: z.string(),
          ventilation_type: z.enum(['NV', 'IE', 'MEV', 'PS', 'MVHR', 'MV', 'DEV']),
          specific_fan_power: z.number().nullable(),
          system_air_change_rate: z.number().nullable(),
          balanced_heat_recovery_efficiency: z.number().nullable(),
        }),
        // Outputs
        // Only one of the structural_infiltration values is relevant,
        // depending on the value of air_permeability_test
        structural_infiltration_from_test: numberWithNaN,
        structural_infiltration: numberWithNaN,
      })
      .partial(),
    num_of_floors_override: z.number(), // Used only in ventilation
    FEE: numberWithNaN,
    total_cost: numberWithNaN,
    annualco2: numberWithNaN,
    totalWK: numberWithNaN,
    kwhdpp: numberWithNaN,
    kgco2perm2: numberWithNaN,
    primary_energy_use_m2: numberWithNaN,
    TFA: stringyFloatSchema,
    fuel_requirements: z.record(
      z
        .object({
          quantity: numberWithNaN,
        })
        .partial(),
    ),
    energy_requirements: z.record(
      z
        .object({
          quantity: numberWithNaN,
        })
        .partial(),
    ),
    temperature: z
      .object({
        target: z.number().nullable(),
        living_area: z.union([stringyFloatSchema, z.nan()]).nullable(),
      })
      .partial(),
    fuel_totals: z.record(
      z.object({
        name: legacyString,
        quantity: numberWithNaN,
        annualcost: numberWithNaN,
        annualco2: numberWithNaN,
      }),
    ),
    currentenergy: z
      .object({
        primaryenergy_annual_kwh: numberWithNaN,
        total_co2: numberWithNaN,
        total_cost: numberWithNaN,
        annual_net_cost: numberWithNaN,
        enduse_annual_kwh: stringyFloatSchema,
        use_by_fuel: z.record(
          z.object({
            annual_use: z.union([z.literal(''), z.number()]),
            annual_co2: numberWithNaN.optional(),
            primaryenergy: numberWithNaN.optional(),
            annualcost: numberWithNaN.optional(),
          }),
        ),

        // Not legacyBoolean because legacy model uses a "=== 1" check on this value
        onsite_generation: z.union([z.literal(1), z.literal(false)]),

        generation: z
          .object({
            annual_CO2: numberWithNaN,
            primaryenergy: numberWithNaN,
            annual_savings: numberWithNaN,
            annual_FIT_income: stringyFloatSchema,
            annual_generation: stringyFloatSchema,
            fraction_used_onsite: stringyFloatSchema,
          })
          .partial(),
      })
      .partial(),
    space_heating: z
      .object({
        annual_heating_demand_m2: numberWithNaN,
        heating_off_summer: legacyBoolean,
        heatingHours: z.object({
          weekday: unvalidatedHeatingPattern,
          weekend: unvalidatedHeatingPattern,
        }),
      })
      .partial(),
    space_heating_demand_m2: numberWithNaN,
    standardisedSHD: numberWithNaN,
    standardisedEUI: numberWithNaN,
    annual_useful_gains_kWh_m2: z
      .object({
        Solar: numberWithNaN,
        Internal: numberWithNaN,
        'Space heating': numberWithNaN,
      })
      .partial(),
    annual_losses_kWh_m2: z
      .object({
        fabric: numberWithNaN,
        ventilation: numberWithNaN,
        infiltration: numberWithNaN,
      })
      .partial(),
    heating_systems: heatingSystems,
    applianceCarbonCoop: applianceCarbonCoop,
    measures: measures,
    use_generation: legacyBoolean,
    model: z.unknown(),
    generation: z
      .object({
        solar_annual_kwh: nullableStringyFloat,
        solar_fraction_used_onsite: nullableStringyFloat,
        solar_payments: z.enum(['none', 'fit']),
        solar_FIT: nullableStringyFloat,
        solar_export_FIT: nullableStringyFloat,
        use_PV_calculator: legacyBoolean.nullable(),
        solarpv_kwp_installed: nullableStringyFloat,
        solarpv_orientation: stringyIntegerSchema.nullable(),
        solarpv_inclination: nullableStringyFloat,
        solarpv_overshading: z
          .union([z.literal(0.5), z.literal(0.65), z.literal(0.8), z.literal(1)])
          .nullable(),
        wind_annual_kwh: nullableStringyFloat,
        wind_fraction_used_onsite: nullableStringyFloat,
        wind_payments: z.enum(['none', 'fit']),
        wind_FIT: nullableStringyFloat,
        wind_export_FIT: nullableStringyFloat,
        hydro_annual_kwh: nullableStringyFloat,
        hydro_fraction_used_onsite: nullableStringyFloat,
        hydro_payments: z.enum(['none', 'fit']),
        hydro_FIT: nullableStringyFloat,
        hydro_export_FIT: nullableStringyFloat,
      })
      .partial(),
    altitude: stringyFloatSchema,
    altitude_full: withOriginSchema(z.number(), z.null()),
    household: householdSchema,
    gains_W: z
      .object({
        metabolic: z.array(numberWithNaN),
        losses: z.array(numberWithNaN),
        fans_and_pumps: z.array(numberWithNaN),
      })
      .partial(),
    fans_and_pumps: z.array(
      z.object({
        fuel: z.string(),
        fraction: z.number().nullable(),
      }),
    ),
  })
  .partial()
  .optional();

export type Scenario = z.infer<typeof scenarioSchema>;

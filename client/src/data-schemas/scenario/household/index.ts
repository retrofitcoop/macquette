import { z } from 'zod';
import { legacySchema } from './legacy';
import { migrateLegacyToV1 } from './migrate';

export const householdSchema = legacySchema.transform(migrateLegacyToV1);
export type Household = z.infer<typeof householdSchema>;

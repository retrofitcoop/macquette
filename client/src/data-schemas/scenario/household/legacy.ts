import { z } from 'zod';

import { withOriginSchema } from '../../helpers/with-origin';
import { legacyString, nullableStringyFloat } from '../value-schemas';

const yesNoSchema = z
  .union([
    z.literal('YES'),
    z.literal('NO'),
    z.literal('true'),
    z.literal('false'),
    z.literal(0),
    z.literal(1),
    z.boolean(),
    z.null(),
  ])
  .transform((val) => {
    if (val === 'YES' || val === 1 || val === 'true' || val === true) {
      return true;
    } else if (val === 'NO' || val === 0 || val === 'false' || val === false) {
      return false;
    } else {
      return val;
    }
  });

const yesNoNaSchema = z.enum(['YES', 'NO', 'NA']);

const areaOfOutstandingNaturalBeautySchema = z
  .enum([
    'none',
    'Arnside and Silverdale',
    'Blackdown Hills',
    'Cannock Chase',
    'Chichester Harbour',
    'Chilterns',
    'Cornwall',
    'Cotswolds',
    'Cranborne Chase and West Wiltshire Downs',
    'Dedham Vale',
    'Dorset',
    'East Devon',
    'Forest of Bowland',
    'High Weald',
    'Howardian Hills',
    'Isle of Wight',
    'Isles of Scilly',
    'Kent Downs',
    'Lincolnshire Wolds',
    'Malvern Hills',
    'Mendip Hills',
    'Nidderdale',
    'Norfolk Coast',
    'North Devon Coast',
    'North Pennines',
    'Northumberland Coast',
    'North Wessex Downs',
    'Quantock Hills',
    'Shropshire Hills',
    'Solway Coast',
    'South Devon',
    'Suffold & Essex Coast & Heaths',
    'Surrey Hills',
    'Tamar Valley',
    'Wye Valley',
    'Ynys Môn',
    'Bryniau Clwyd a Dyffryn Dyfrdwy',
    'Gŵyr',
    'Llŷn',
    'Dyffryn Gwy',
    'Antrim Coast and Glens',
    'Binevenagh',
    'Causeway Coast',
    'Lagan Valley',
    'Mourne Mountains',
    'Ring of Gullion',
    'Sperrins',
    'Strangford and Lecale',
  ])
  .nullable();

const temperatureComfortSchema = z
  .enum([
    'LOW',
    'MID',
    'HIGH',
    'DK',
    'NA',
    '',
    'too cold',
    'just right',
    'too hot',
    "don't know",
    'not applicable',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'LOW':
        return 'too cold';
      case 'MID':
        return 'just right';
      case 'HIGH':
        return 'too hot';
      case 'DK':
        return "don't know";
      case 'NA':
        return 'not applicable';
      case '':
        return null;
      default:
        return val;
    }
  });

const humidityComfortSchema = z
  .enum([
    'LOW',
    'MID',
    'HIGH',
    'DK',
    'NA',
    '',
    'too dry',
    'just right',
    'too stuffy',
    "don't know",
    'not applicable',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'LOW':
        return 'too dry';
      case 'MID':
        return 'just right';
      case 'HIGH':
        return 'too stuffy';
      case 'DK':
        return "don't know";
      case 'NA':
        return 'not applicable';
      case '':
        return null;
      default:
        return val;
    }
  });

const draughtsComfortSchema = z
  .enum([
    'LOW',
    'MID',
    'HIGH',
    'DK',
    'NA',
    '',
    'too draughty',
    'just right',
    'too still',
    "don't know",
    'not applicable',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'LOW':
        return 'too draughty';
      case 'MID':
        return 'just right';
      case 'HIGH':
        return 'too still';
      case 'DK':
        return "don't know";
      case 'NA':
        return 'not applicable';
      case '':
        return null;
      default:
        return val;
    }
  });

const daylightLevelSchema = z
  .enum([
    'LOW',
    'MID',
    'HIGH',
    'VAR',
    '',
    'too little',
    'just right',
    'too much',
    'varies',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'LOW':
        return 'too little';
      case 'MID':
        return 'just right';
      case 'HIGH':
        return 'too much';
      case 'VAR':
        return 'varies';
      case '':
        return null;
      default:
        return val;
    }
  });

const addressSchema = z.object({
  line1: z.string(),
  line2: z.string(),
  line3: z.string(),
  postTown: z.string(),
  postcode: z.string(),
  country: z.string(),
});
export type Address = z.infer<typeof addressSchema>;

const periodSchema = z.object({
  start: z.number().nullable(),
  end: z.number().nullable(),
});

const houseTypeSchema = z
  .enum([
    'FLAT_CONVERSION',
    'FLAT_PURPOSE',
    'MIDTERRACE',
    'SEMIDETACHED',
    'DETACHED',
    'mid-terrace',
    'semi-detached',
    'end terrace',
    'detached',
    'flat (conversion)',
    'flat (purpose built)',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'FLAT_CONVERSION':
        return 'flat (conversion)';
      case 'FLAT_PURPOSE':
        return 'flat (purpose built)';
      case 'MIDTERRACE':
        return 'mid-terrace';
      case 'SEMIDETACHED':
        return 'semi-detached';
      case 'DETACHED':
        return 'detached';
      default:
        return val;
    }
  });

const radonRiskSchema = z
  .union([z.enum(['LOW', '1-3', '3-5', '5-10', '10-30', '30']), z.literal(30)])
  .transform((val) => (val === 30 ? ('30' as const) : val));

const ageBandSchema = z
  .enum([
    'before-1900',
    '1900-1929',
    '1930-1949',
    '1950-1966',
    '1967-1975',
    '1976-1982',
    '1983-1990',
    '1991-1995',
    '1996-2002',
    '2003-2006',
    '2007-2011',
    '2012-onwards',
  ])
  .nullable();

const frostAttackRiskSchema = z.union([
  z
    .enum(['high risk', 'within a postcode with high risk', 'not at high risk'])
    .transform((value) => ({
      type: 'user provided' as const,
      value,
    })),
  withOriginSchema(
    z.enum(['high risk', 'within a postcode with high risk', 'not at high risk']),
    z.null(),
  ),
]);

const logisticsDisruptionSchema = z.union([
  z
    .enum([
      'no disruption',
      'very minimal',
      'having to redecorate some rooms',
      'complete strip out and redecoration throughout',
      'move out for a few days',
      'move out for a few weeks',
      'live elsewhere during the works',
    ])
    .nullable(),
  z
    .union([
      z.literal(0),
      z.literal(1),
      z.literal(2),
      z.literal(3),
      z.literal(4),
      z.literal('0'),
      z.literal('1'),
      z.literal('2'),
      z.literal('3'),
      z.literal('4'),
    ])
    .transform((val) => {
      switch (val) {
        case '0':
        case 0:
          return 'very minimal';
        case '1':
        case 1:
          return 'having to redecorate some rooms';
        case '2':
        case 2:
          return 'complete strip out and redecoration throughout';
        case '3':
        case 3:
          return 'move out for a few days';
        case '4':
        case 4:
          return 'live elsewhere during the works';
      }
    }),
]);

const occupancySchema = z
  .union([
    z.literal(0).transform(() => '0' as const),
    z.enum(['0', '1Y', '2Y', '5Y', '10Y', '20Y', '20+Y']),
    z.null(),
  ])
  .transform((value) => {
    switch (value) {
      case '0':
        return 'not yet moved in' as const;
      case '1Y':
        return 'less than 12 months' as const;
      case '2Y':
        return '1-2 years' as const;
      case '5Y':
        return '2-5 years' as const;
      case '10Y':
        return '5-10 years' as const;
      case '20Y':
        return '10-20 years' as const;
      case '20+Y':
        return 'more than 20 years' as const;
      case null:
        return null;
    }
  });

const daytimeOccupancySchema = z
  .enum(['most of the day', 'half the day', 'out all day'])
  .nullable();

const laundryFrequencySchema = z.enum([
  'most days',
  'once or twice a week',
  'most days in winter only',
  'once or twice a week in winter only',
  'once a month',
  'rarely',
]);

const laundryTumbleDrierSchema = z.enum(['no', 'yes - standard', 'yes - heat pump type']);

const laundryOptionsSchema = z.array(
  z.enum([
    'external line',
    'internal airing cupbard',
    'internal racks in living space',
    'internal heated racks in living space',
    'on radiators',
    'electric dryer (standard)',
    'electric dryer (condensing)',
  ]),
);

const heatingOffMonthsSchema = z.array(
  z.enum(['March', 'April', 'May', 'June', 'July', 'August', 'September', 'October']),
);

const expectedStaySchema = z.union([
  z.enum(['LONG', 'MED', 'SHORT']).transform((val): string => {
    switch (val) {
      case 'LONG':
        return 'long term (10 years plus)';
      case 'MED':
        return 'medium term (5–10 years)';
      case 'SHORT':
        return 'short term (less than 5 years)';
    }
  }),
  z.string(),
]);

const expectedStartSchema = z
  .enum(['3_MONTH', '6_MONTH', '24_MONTH', '5_YEAR'])
  .transform((val) => {
    switch (val) {
      case '3_MONTH':
        return '3 months' as const;
      case '6_MONTH':
        return '3–6 months' as const;
      case '24_MONTH':
        return '12–24 months' as const;
      case '5_YEAR':
        return '2–5 years' as const;
    }
  });

const priorityOptionSchema = z.enum([
  'reduce carbon emissions',
  'decarbonise heating (stop burning stuff)',
  'reduce fuel bills',
  'improve winter comfort',
  'improve indoor air quality',
  'resolve condensation, damp, and mould issues',
  'general modernisation, maintenance and repairs',
  'improve climate resilience (e.g. vs flooding)',
  'reduce summer overheating',
  'integrate energy efficiency with other building work',
  'future-proofing (e.g. for ageing)',
]);
const prioritiesSchema = z.array(priorityOptionSchema);

const logisticsPhasingSchema = z
  .enum(['ONEGO', 'PHASES', 'all in one go', 'in phases over time'])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'ONEGO':
        return 'all in one go';
      case 'PHASES':
        return 'in phases over time';
      default:
        return val;
    }
  });

const logisticsManagementSchema = z
  .enum([
    'fully self-managed',
    'largely self-managed with some targeted support from professionals and specialists and a main contractor to deal with site logistics',
    'employ a project manager and main contractor to deal with everything',
  ])
  .nullable();

const localPlanningAuthoritySchema = z
  .union([z.string(), withOriginSchema(z.string(), z.never())])
  .transform((val) =>
    typeof val === 'string' ? { type: 'user provided' as const, value: val } : val,
  );

const budgetFinancingSchema = z.array(
  z.enum(['savings', 'grants', 'mortgage borrowing', 'other borrowing']),
);

const budgetSchema = z
  .enum([
    'yes',
    "will work out a budget after understanding what's possible",
    'YES',
    'NO',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'YES':
        return 'yes';
      case 'NO':
        return "will work out a budget after understanding what's possible";
      default:
        return val;
    }
  });

const changeAppearanceSchema = z
  .enum(['keep the same', 'changing is ok', 'YES', 'NO'])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'YES':
        return 'keep the same';
      case 'NO':
        return 'changing is ok';
      default:
        return val;
    }
  });

const tenureSchema = z
  .enum([
    'OWNER',
    'RENT_PRIVATE',
    'RENT_SOCIAL',
    'COOP',
    'SHARED',
    'owner-occupied',
    'keep the same',
    'owner-occupied, with mortgage',
    'owner-occupied, no mortgage',
    'private rent',
    'social or affordable rent',
    'shared ownership',
    'co-op',
    'holiday let',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'OWNER':
        return 'owner-occupied';
      case 'RENT_PRIVATE':
        return 'private rent';
      case 'RENT_SOCIAL':
        return 'social or affordable rent';
      case 'COOP':
        return 'co-op';
      case 'SHARED':
        return 'shared ownership';
      default:
        return val;
    }
  });

export const subsidenceSchema = z
  .enum([
    'probable',
    'possible',
    'improbable',
    'highly likely',
    'likely',
    'highly unlikely',
  ])
  .nullable()
  .transform((val) => {
    switch (val) {
      case 'highly likely':
        return 'probable';
      case 'likely':
        return 'possible';
      case 'highly unlikely':
        return 'improbable';
      default:
        return val;
    }
  });

export const legacySchema = z
  .object({
    address_1: legacyString,
    address_2: legacyString,
    address_3: legacyString,
    address_country: z.string(),
    address_full: withOriginSchema(addressSchema, z.never()),
    address_la: legacyString,
    address_la_full: withOriginSchema(z.string(), z.never()),
    address_lsoa: legacyString,
    address_lsoa_full: withOriginSchema(z.string(), z.never()),
    address_postcode: legacyString,
    address_town: legacyString,
    aesthetics_external: changeAppearanceSchema,
    aesthetics_internal: changeAppearanceSchema,
    aesthetics_note: legacyString,
    appearanceDetailsToPreserve: z.string(),
    appearanceOutsideFront: changeAppearanceSchema,
    appearanceOutsideSideRear: changeAppearanceSchema,
    areaOfOutstandingNaturalBeauty: areaOfOutstandingNaturalBeautySchema,
    assessment_date: legacyString,
    assessor_name: z.string(),
    budgetExact: z.string(),
    buildingMaintenance: z.string(),
    budgetFinancing: budgetFinancingSchema,
    comfort_airquality_summer: humidityComfortSchema,
    comfort_airquality_winter: humidityComfortSchema,
    comfort_draughts_summer: draughtsComfortSchema,
    comfort_draughts_winter: draughtsComfortSchema,
    comfort_temperature_summer: temperatureComfortSchema,
    comfort_temperature_winter: temperatureComfortSchema,
    commentary_brief: legacyString,
    commentary_context: legacyString,
    commentary_decisions: legacyString,
    construct_note_drainage: z.string(),
    construct_note_floors: z.string(),
    construct_note_ingress: z.string(),
    construct_note_openings: z.string(),
    construct_note_roof: z.string(),
    construct_note_ventiliation: z.string(),
    construct_note_walls: z.string(),
    context_and_other_points: z.string(),
    controlNotes: z.string(),
    controlSummer: z.boolean().nullable(),
    controlWinter: z.boolean().nullable(),
    damp: legacyString,
    damp_note: z.string(),
    daylight: daylightLevelSchema,
    daylight_note: z.string(),
    daylight_problems: yesNoSchema,
    daytimeOccupancy: daytimeOccupancySchema,
    daytimeOccupancyNotes: z.string(),
    electricsCondition: z.string(),
    electricVehicles: yesNoSchema,
    electricVehiclesNotes: z.string(),
    expected_lifestyle_change: yesNoSchema,
    expected_lifestyle_change_note: z.string(),
    expected_other_works: yesNoSchema,
    expected_other_works_note: z.string(),
    expected_start: expectedStartSchema,
    expected_start_note: legacyString,
    expectedOtherWorksDesigns: z.string(),
    exposure: z.enum(['very severe', 'severe', 'moderate', 'sheltered']),
    externalAirPollution: yesNoSchema,
    externalNoisePollution: z.string(),
    flooding_groundwater: z.enum(['UNLIKELY', 'POSSIBLE', '']),
    flooding_history: yesNoSchema,
    flooding_note: z.string(),
    flooding_reservoirs: z.enum(['WITHIN', 'OUTWITH', '']),
    flooding_rivers_sea: z.enum(['HIGH', 'MED', 'LOW', 'VLOW', '']),
    flooding_surface_water: z.enum(['HIGH', 'MED', 'LOW', 'VLOW', '']),
    frostAttackRisk: frostAttackRiskSchema,
    fuel_burner: yesNoSchema,
    fuel_burner_note: z.string(),
    health: z.string(),
    healthConditions: yesNoSchema,
    heating_off_summer: yesNoSchema,
    heating_thermostat: legacyString,
    heating_unheated_habitable: legacyString,
    heating_weekday_off1_hours: z.union([z.string(), z.number()]),
    heating_weekday_off1_mins: z.union([z.string(), z.number()]),
    heating_weekday_off2_hours: z.union([z.string(), z.number()]),
    heating_weekday_off2_mins: z.union([z.string(), z.number()]),
    heating_weekday_off3_hours: z.union([z.string(), z.number()]),
    heating_weekday_off3_mins: z.union([z.string(), z.number()]),
    heating_weekday_on1_hours: z.union([z.string(), z.number()]),
    heating_weekday_on1_mins: z.union([z.string(), z.number()]),
    heating_weekday_on2_hours: z.union([z.string(), z.number()]),
    heating_weekday_on2_mins: z.union([z.string(), z.number()]),
    heating_weekday_on3_hours: z.union([z.string(), z.number()]),
    heating_weekday_on3_mins: z.union([z.string(), z.number()]),
    heating_weekend_off1_hours: z.union([z.string(), z.number()]),
    heating_weekend_off1_mins: z.union([z.string(), z.number()]),
    heating_weekend_off2_hours: z.union([z.string(), z.number()]),
    heating_weekend_off2_mins: z.union([z.string(), z.number()]),
    heating_weekend_off3_hours: z.union([z.string(), z.number()]),
    heating_weekend_off3_mins: z.union([z.string(), z.number()]),
    heating_weekend_on1_hours: z.union([z.string(), z.number()]),
    heating_weekend_on1_mins: z.union([z.string(), z.number()]),
    heating_weekend_on2_hours: z.union([z.string(), z.number()]),
    heating_weekend_on2_mins: z.union([z.string(), z.number()]),
    heating_weekend_on3_hours: z.union([z.string(), z.number()]),
    heating_weekend_on3_mins: z.union([z.string(), z.number()]),
    heatingBurnersAirSupply: yesNoSchema,
    heatingHoursWeekday: z.array(periodSchema),
    heatingHoursWeekend: z.array(periodSchema),
    heatingNotes: z.string(),
    heatingOffMonths: heatingOffMonthsSchema,
    heatingRoomHeaters: yesNoSchema,
    heatingRoomHeatersNotes: z.string(),
    heatNetworkInZone: yesNoSchema,
    heatNetworkNotes: z.string(),
    historic_age_band: ageBandSchema,
    historic_age_precise: legacyString,
    historic_conserved: yesNoSchema,
    historic_listed: z.enum(['NO', 'I', 'II', 'II*', 'LOCAL']).nullable(),
    historic_note: legacyString,
    hot_water_provided: z.string(),
    hotWaterBathsPerWeek: z.number().nullable(),
    hotWaterCylinder: yesNoSchema,
    hotWaterCylinderNotes: z.string(),
    hotWaterShowersPerWeek: z.number().nullable(),
    hotWaterUsageNotes: z.string(),
    house_nr_bedrooms: legacyString,
    house_type: houseTypeSchema,
    house_type_note: z.string(),
    householder_name: z.string(),
    interview_date: z.string(),
    worldHeritageSite: z.enum(['within whs', 'within buffer zone', 'no']).nullable(),
    nearAncientMonument: yesNoSchema,
    landRegistryLink: z.string(),
    latLong: z.tuple([z.number(), z.number()]).nullable(),
    laundry: z.string(),
    laundryFrequency: laundryFrequencySchema.nullable(),
    laundryOptions: laundryOptionsSchema,
    laundryTumbleDrier: laundryTumbleDrierSchema.nullable(),
    laundryTumbleDrierFrequency: laundryFrequencySchema.nullable(),
    local_planning_authority: localPlanningAuthoritySchema,
    location_density: z.enum(['urban', 'suburban', 'rural']),
    logistics_budget: budgetSchema,
    logistics_budget_note: z.string(),
    logistics_disruption: logisticsDisruptionSchema,
    logistics_disruption_note: z.string(),
    logistics_diy: yesNoSchema,
    logistics_diy_note: legacyString,
    logistics_packaging: logisticsPhasingSchema,
    logisticsManagement: logisticsManagementSchema,
    looked_up: z.boolean(),
    electricityDNO: z.string(),
    electricityLooped: yesNoSchema,
    electricityMPAN: z.string(),
    electricitySmartMeter: yesNoSchema,
    gasTransporter: z.string(),
    gasMPRN: z.string(),
    gasSmartMeter: yesNoSchema,
    mains_electricity: yesNoSchema,
    mains_gas: yesNoSchema,
    mains_sewer: yesNoSchema,
    mains_water: yesNoSchema,
    moveInDate: z.string(),
    moveInDateNotes: z.string(),
    noise_note: z.string(),
    noise_problems: yesNoSchema,
    occupancy_actual: occupancySchema,
    occupancy_expected: expectedStaySchema,
    occupancy_expected_note: z.string(),
    occupants_18to65: nullableStringyFloat,
    occupants_5to17: nullableStringyFloat,
    occupants_note: z.string(),
    occupants_over65: nullableStringyFloat,
    occupants_pets: legacyString,
    occupants_under5: nullableStringyFloat,
    overheating_note: z.string(),
    overheatingRisk: z.enum(['outside England', 'high risk', 'moderate risk']).nullable(),
    pets: yesNoSchema,
    previous_works: z.string(),
    previous_works_approvals: z.string(),
    priorities: prioritiesSchema,
    priority_airquality: legacyString,
    priority_carbon: legacyString,
    priority_comfort: legacyString,
    priority_health: legacyString,
    priority_modernisation: legacyString,
    priority_money: legacyString,
    priority_qualitative_note: legacyString,
    projectAims: z.string(),
    planningNotes: z.string(),
    radon_risk: radonRiskSchema,
    reading_humidity1: nullableStringyFloat,
    reading_humidity2: nullableStringyFloat,
    reading_temp1: nullableStringyFloat,
    reading_temp2: nullableStringyFloat,
    report_date: legacyString,
    report_signoff: z.string(),
    report_version: legacyString,
    report_reference: z.string(),
    rooms_favourite: z.string(),
    rooms_unloved: z.string(),
    roomUse: z.string(),
    space_heating_controls: z.string(),
    space_heating_provided: z.string(),
    structural_issues: z.string(),
    structural_issues_note: z.string(),
    structuralIssues: yesNoSchema,
    subsidence2030: subsidenceSchema,
    subsidence2050: subsidenceSchema,
    subsidence2070: subsidenceSchema,
    smokeControlZone: yesNoSchema,
    siteOfSpecialScientificInterest: z.string(),
    soilHeaveRisk: z.enum(['1', '2', '3', '4', '5']).nullable(),
    treeNotes: z.string(),
    tenure: tenureSchema,
    tenure_note: legacyString,
    thermal_comfort_note: z.string(),
    thermal_comfort_problems: yesNoSchema,
    uniquePropertyReferenceNumber: withOriginSchema(z.string(), z.never()),
    ventilation_adequate_paths: yesNoNaSchema,
    ventilation_gaps: yesNoNaSchema,
    ventilation_note: z.string(),
    ventilation_purge_vents: yesNoNaSchema,
    ventilation_suggestion: z.enum(['BAD', 'OK', 'GOOD']),
    ventilationSystem: z.string(),
  })
  .partial();

export type HouseholdLegacy = z.infer<typeof legacySchema>;

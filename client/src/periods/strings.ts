import { TimeOfDay } from './core';

type TimeComponents = { hour: number; minute: number };

export function parseTime(text: string): [TimeOfDay | null, string | null] {
  let output: TimeComponents | null = null;

  if (text.length === 0) {
    return [null, null];
  } else if (text.length === 1 || text.length === 2) {
    const re = /^\d+$/;
    if (text.match(re) !== null) {
      output = { hour: parseInt(text, 10), minute: 0 };
    }
  } else if (text.length <= 5) {
    const re = /^(\d\d?):(\d\d)$/;
    const result = text.match(re);
    if (result !== null) {
      output = {
        // SAFETY: result will have length 3 if the regex matches
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        hour: parseInt(result[1]!, 10),
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        minute: parseInt(result[2]!, 10),
      };
    }
  }

  if (output !== null) {
    if (output.hour < 0 || output.hour > 23) {
      return [null, 'Hour should be between 0 and 23'];
    } else if (output.minute < 0 || output.minute > 59) {
      return [null, 'Minute should be between 0 and 59'];
    } else {
      return [output.hour * 60 + output.minute, null];
    }
  }

  return [null, 'Not a valid time (hh:mm)'];
}

export function timeToComponents(time: TimeOfDay): TimeComponents {
  return {
    hour: Math.floor(time / 60),
    minute: time % 60,
  };
}

export function pad2(x: number) {
  return String(x).padStart(2, '0');
}

export function timeToString(time: TimeOfDay, padHours = true) {
  const { hour, minute } = timeToComponents(time);
  const hourStr = padHours ? pad2(hour) : String(hour);
  return `${hourStr}:${pad2(minute)}`;
}

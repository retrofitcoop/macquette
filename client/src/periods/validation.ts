import { Result } from '../helpers/result';
import { periodsOverlapping, TimeOfDay, TimePeriod } from './core';

export type UnvalidatedTimePeriod = {
  start: TimeOfDay | null;
  end: TimeOfDay | null;
};

export type TimePeriodValidationResult = Result<
  TimePeriod,
  { type: 'incomplete' } | { type: 'bad range' } | { type: 'overlap'; otherIdx: number }
>;

function validateStandalonePeriod({
  start,
  end,
}: UnvalidatedTimePeriod): TimePeriodValidationResult {
  if (start === null || end === null) {
    return Result.err({ type: 'incomplete' });
  }
  if (start < 0 || end >= 24 * 60) {
    return Result.err({ type: 'bad range' });
  }
  return Result.ok({ start, end });
}

function validateOverlap(
  period: TimePeriod,
  otherPeriods: UnvalidatedTimePeriod[],
): TimePeriodValidationResult {
  for (const [idx, { start, end }] of otherPeriods.entries()) {
    if (start === null || end === null) {
      continue;
    }
    if (periodsOverlapping(period, { start, end })) {
      return Result.err({ type: 'overlap', otherIdx: idx });
    }
  }
  return Result.ok(period);
}

export function validatePattern(
  pattern: UnvalidatedTimePeriod[],
): Array<[UnvalidatedTimePeriod, TimePeriodValidationResult]> {
  return pattern.map((period, index) => [
    period,
    validateStandalonePeriod(period).chain((p) =>
      validateOverlap(p, pattern.slice(0, index)),
    ),
  ]);
}

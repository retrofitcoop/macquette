export class LinkError extends Error {}
export class NodeNotFoundError extends Error {}

export class TreeNode<T> {
  public children: TreeNode<T>[] = [];

  constructor(
    public name: string,
    public data: T,
  ) {}

  addChild(node: TreeNode<T>) {
    this.children.push(node);
  }

  replaceChild(child: TreeNode<T>, newChildren: TreeNode<T>[]) {
    const index = this.children.indexOf(child);
    if (index === -1) {
      throw new NodeNotFoundError("Couldn't find node to replace with children");
    }

    // Remove the old child
    this.children.splice(index, 1);
    // Insert new children in same spot
    this.children.splice(index, 0, ...newChildren);
  }
}

/**
 * An n-ary tree implementation
 */
export class Tree<T> {
  constructor(public nodes: TreeNode<T>[]) {}

  link(childId: string, parentId: string): void {
    const child = this.nodes.find((node) => node.name === childId);
    if (child === undefined) {
      throw new NodeNotFoundError(`Node ${childId} not found while linking`);
    }

    const parent = this.nodes.find((node) => node.name === parentId);
    if (parent === undefined) {
      throw new LinkError(
        `Scenario chain is broken; ${childId} references missing parent ${parentId}`,
      );
    }

    parent.addChild(child);
  }

  removeNodeAndPromoteChildren(nodeId: string): void {
    const node = this.nodes.find((node) => node.name === nodeId);
    if (node === undefined) {
      throw new NodeNotFoundError(`Node ${nodeId} not found while removing`);
    }

    const parentNode = this.findParentNode(node);
    if (parentNode !== null) {
      parentNode.replaceChild(node, node.children);
    }

    const nodeIndex = this.nodes.findIndex((searchNode) => node === searchNode);
    this.nodes.splice(nodeIndex, 1);
  }

  findParentNode(node: TreeNode<T>): TreeNode<T> | null {
    return this.nodes.find((searchNode) => searchNode.children.includes(node)) ?? null;
  }

  lineage(nodeId: string): TreeNode<T>[] {
    const node = this.nodes.find((node) => node.name === nodeId);
    if (node === undefined) {
      throw new NodeNotFoundError(`Node ${nodeId} not found while removing`);
    }

    const lineage = [node];
    let currNode = node;

    for (;;) {
      const result = this.findParentNode(currNode);
      if (result !== null) {
        lineage.push(result);
        currNode = result;
      } else {
        break;
      }
    }

    return lineage;
  }

  linearise(): Record<string, T> {
    return Object.fromEntries(this.nodes.map((node) => [node.name, node.data]));
  }
}

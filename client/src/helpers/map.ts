export function groupBy<K, T>(
  arr: T[],
  grouper: (val: T, index: number) => K,
): Map<K, T[]> {
  const out = new Map<K, T[]>();
  for (const [index, value] of arr.entries()) {
    const group = grouper(value, index);
    if (!out.has(group)) {
      out.set(group, []);
    }
    // SAFETY: membership checked above
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    out.get(group)!.push(value);
  }
  return out;
}

export function mapValues<K, V, U>(
  map: Map<K, V>,
  mapper: (val: V, key: K) => U,
): Map<K, U> {
  const out = new Map<K, U>();
  for (const [key, value] of map) {
    out.set(key, mapper(value, key));
  }
  return out;
}

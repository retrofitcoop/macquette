import { z } from 'zod';
import { Result } from './result';

export function resultifyZodSchema<InT, OutT>(schema: z.Schema<OutT, z.ZodTypeDef, InT>) {
  return (input: unknown): Result<OutT, z.ZodError<InT>> => {
    const parseResult = schema.safeParse(input);
    if (parseResult.success) {
      return Result.ok(parseResult.data);
    } else {
      return Result.err(parseResult.error);
    }
  };
}

import { z } from 'zod';
import { ModelBehaviourVersion, scenarioSchema } from '../data-schemas/scenario';
import { cache } from '../helpers/cache-decorators';
import { Result } from '../helpers/result';
import { resultifyZodSchema } from '../helpers/zod-schema-result';
import { CombinedModules } from './combined-modules';
import { Region } from './enums/region';
import { ModelError } from './error';
import { extractInputFromLegacy } from './extractor';
import { CurrentEnergyInput } from './modules/current-energy';
import { FabricInput } from './modules/fabric';
import { FansPumpsElectricKeepHotInput } from './modules/fans-pumps-electric-keep-hot';
import { FloorsInput } from './modules/floors';
import { FuelsDict } from './modules/fuels';
import { GenerationInput } from './modules/generation';
import { GeographyInput } from './modules/geography';
import { HeatingHoursInput } from './modules/heating-hours';
import { HeatingSystemInput } from './modules/heating-systems';
import {
  setBlankLegacyOutputs,
  setDefaultLegacyInputs,
} from './modules/legacy-initialisation';
import { AppliancesCookingLoadCollectionsInput } from './modules/lighting-appliances-cooking/loads/load-collection';
import { AppliancesSAPInput } from './modules/lighting-appliances-cooking/sap/appliances';
import { CookingSAPInput } from './modules/lighting-appliances-cooking/sap/cooking';
import { LightingSAPInput } from './modules/lighting-appliances-cooking/sap/lighting';
import { MeanInternalTemperatureInput } from './modules/mean-internal-temperature';
import { OccupancyInput } from './modules/occupancy';
import { SolarHotWaterInput } from './modules/solar-hot-water';
import { SpaceHeatingInput } from './modules/space-heating';
import { VentilationInfiltrationCommonInput } from './modules/ventilation-infiltration/common-input';
import { InfiltrationInput } from './modules/ventilation-infiltration/infiltration';
import { VentilationInput } from './modules/ventilation-infiltration/ventilation';
import { WaterCommonInput } from './modules/water-common';
import { WaterHeatingInput } from './modules/water-heating';

export type Input = {
  modelBehaviourVersion: ModelBehaviourVersion;
  fuels: { fuels: FuelsDict };
  geography: GeographyInput;
  floors: FloorsInput;
  occupancy: OccupancyInput;
  region: Region;
  fabric: FabricInput;
  ventilationInfiltrationCommon: VentilationInfiltrationCommonInput;
  ventilation: VentilationInput;
  infiltration: InfiltrationInput;
  waterCommon: WaterCommonInput;
  solarHotWater: SolarHotWaterInput;
  lighting: LightingSAPInput;
  appliancesSap: AppliancesSAPInput;
  cookingSap: CookingSAPInput;
  appliancesCookingLoadCollections: AppliancesCookingLoadCollectionsInput;
  waterHeating: WaterHeatingInput;
  generation: GenerationInput;
  currentEnergy: CurrentEnergyInput;
  heatingSystems: HeatingSystemInput[];
  meanInternalTemperature: MeanInternalTemperatureInput;
  fansPumpsElectricKeepHot: FansPumpsElectricKeepHotInput;
  spaceHeating: SpaceHeatingInput;
  heatingHours: HeatingHoursInput;
};

export type ModelModePropertyName =
  | 'normal'
  | 'regulated'
  | 'standardisedHeatingPre2025'
  | 'standardisedHeating';
type IModel = {
  [K in ModelModePropertyName]: CombinedModules;
};
export class Model implements IModel {
  constructor(public input: Input) {}

  @cache
  get normal(): CombinedModules {
    return new CombinedModules(this.input, 'normal');
  }

  @cache
  get regulated(): CombinedModules {
    return new CombinedModules(this.input, 'regulated');
  }

  @cache
  get standardisedHeatingPre2025(): CombinedModules {
    return new CombinedModules(this.input, 'standardised heating (pre-2025)');
  }

  @cache
  get standardisedHeating(): CombinedModules {
    return new CombinedModules(this.input, 'standardised heating');
  }

  static fromLegacy(
    legacyData: unknown,
  ): Result<Model, z.ZodError<unknown> | ModelError> {
    return resultifyZodSchema(scenarioSchema)(legacyData)
      .chain(extractInputFromLegacy)
      .map((modelInputs) => new Model(modelInputs));
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-assignment,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    setDefaultLegacyInputs(data);
    setBlankLegacyOutputs(data);
    this.normal.mutateLegacyData(data);

    data.standardisedSHD =
      this.standardisedHeating.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea;
    data.standardisedEUI =
      this.standardisedHeating.fuelRequirements.totalEnergyAnnualPerArea;

    data.SAP = data.SAP ?? {};
    data.SAP.total_costSAP = this.regulated.fuelRequirements.totalCostAnnual;
    data.SAP.annualco2SAP = this.regulated.fuelRequirements.totalCarbonEmissionsAnnual;
    data.SAP.kgco2perm2SAP =
      this.regulated.fuelRequirements.totalCarbonEmissionsAnnual /
      this.regulated.floors.totalFloorArea;
    data.SAP.primary_energy_useSAP =
      this.regulated.fuelRequirements.totalPrimaryEnergyAnnual;
    data.SAP.primary_energy_use_m2SAP =
      this.regulated.fuelRequirements.totalPrimaryEnergyAnnual /
      this.regulated.floors.totalFloorArea;
  }
  /* eslint-enable */

  toJSON(): unknown {
    return null;
  }
}

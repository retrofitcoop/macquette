import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { mean, sum } from '../../../helpers/array-reducers';
import { cache } from '../../../helpers/cache-decorators';
import { Month } from '../../enums/month';
import { VentilationPoint } from './common-types';

type Unplanned = { type: 'unplanned/natural ventilation' };

type IntermittentExtract = {
  // We also include Positive Input Ventilation systems which supply air from loft space
  // as "intermittent extract", as per SAP 10.2 2.6.1(a) paragraph 2.
  type: 'intermittent extract';
  extractVentilationPoints: VentilationPoint[];
};
type MechanicalExtract = {
  // We also include Positive Input Ventilation systems which supply air directly from
  // outside under "mechanical extract", as per SAP 10.2 2.6.1(a) paragraph 3
  type: 'mechanical extract';
  systemAirChangeRate: number;
  systemSpecificFanPower: number; // W/(l/s)
};
type PassiveStack = {
  type: 'passive stack';
  extractVentilationPoints: VentilationPoint[];
};
type MechanicalVentilationWithHeatRecovery = {
  type: 'mechanical ventilation with heat recovery';
  efficiencyProportion: number; // in range [0, 1]
  systemAirChangeRate: number;
  systemSpecificFanPower: number;
};

// could be centralised or decentralised
type MechanicalVentilation = {
  type: 'mechanical ventilation';
  systemAirChangeRate: number;
  systemSpecificFanPower: number; // W/(l/s)
};

export type VentilationInput =
  | Unplanned
  | IntermittentExtract
  | MechanicalExtract
  | PassiveStack
  | MechanicalVentilation
  | MechanicalVentilationWithHeatRecovery;

export type VentilationDependencies = {
  floors: { totalVolume: number };
  ventilationInfiltrationCommon: {
    shelterFactor: number;
    windFactor: (m: Month) => number;
  };
};

export function extractVentilationInputFromLegacy(scenario: Scenario): VentilationInput {
  const { ventilation } = scenario ?? {};
  const extractVentilationPoints =
    ventilation?.EVP?.map(({ ventilation_rate }) => ({
      ventilationRate: coalesceEmptyString(ventilation_rate, 0) ?? 0,
    })) ?? [];
  const systemAirChangeRate =
    coalesceEmptyString(ventilation?.system_air_change_rate, 0) ?? 0.5;
  switch (ventilation?.ventilation_type) {
    case undefined:
    case 'NV': {
      return { type: 'unplanned/natural ventilation' };
    }
    case 'IE': {
      return { type: 'intermittent extract', extractVentilationPoints };
    }
    case 'DEV':
    case 'MEV': {
      return {
        type: 'mechanical extract',
        systemAirChangeRate,
        systemSpecificFanPower:
          coalesceEmptyString(ventilation?.system_specific_fan_power, 0) ?? 0,
      };
    }
    case 'PS': {
      return {
        type: 'passive stack',
        extractVentilationPoints,
      };
    }
    case 'MVHR': {
      return {
        type: 'mechanical ventilation with heat recovery',
        systemAirChangeRate,
        efficiencyProportion:
          (coalesceEmptyString(ventilation?.balanced_heat_recovery_efficiency, 0) ?? 65) /
          100,
        systemSpecificFanPower:
          coalesceEmptyString(ventilation?.system_specific_fan_power, 0) ?? 0,
      };
    }
    case 'MV': {
      return {
        type: 'mechanical ventilation',
        systemAirChangeRate,
        systemSpecificFanPower:
          coalesceEmptyString(ventilation?.system_specific_fan_power, 0) ?? 0,
      };
    }
  }
}

export class Ventilation {
  constructor(
    private input: VentilationInput,
    private dependencies: VentilationDependencies,
  ) {}

  // in ACH
  @cache
  get rawEVPAirChanges(): number {
    switch (this.input.type) {
      case 'intermittent extract':
      case 'passive stack': {
        const perPointRates = this.input.extractVentilationPoints.map(
          (v) => v.ventilationRate,
        );
        if (this.dependencies.floors.totalVolume === 0) {
          return 0;
        }
        return sum(perPointRates) / this.dependencies.floors.totalVolume;
      }
      case 'unplanned/natural ventilation':
      case 'mechanical ventilation':
      case 'mechanical ventilation with heat recovery':
      case 'mechanical extract': {
        return 0;
      }
    }
  }

  adjustedEVPAirChanges(month: Month): number {
    return (
      this.dependencies.ventilationInfiltrationCommon.shelterFactor *
      this.dependencies.ventilationInfiltrationCommon.windFactor(month) *
      this.rawEVPAirChanges
    );
  }

  get systemAirChanges(): number {
    switch (this.input.type) {
      case 'unplanned/natural ventilation':
      case 'intermittent extract':
      case 'passive stack':
        /*
                    According to SAP 9, if the infiltration rate ≥ 1, then the
                    combined infiltration and ventilation rate should be set to
                    the calculated adjusted infiltration rate; otherwise it
                    should be 0.5 + [infiltration rate squared × 0.5]. (This is
                    expressed as "if (22b)m ≥ 1, then (24d)m = (22b)m,
                    otherwise (24d)m = 0.5 + [(22b)m² × 0.5]".)

                    However, this leads to an underestimation of heat losses
                    from infiltration in some cases, and in any case our model
                    divorces ventilation and infiltration, so we ignore the
                    special case and always use the "otherwise" formula.

                    See https://github.com/emoncms/MyHomeEnergyPlanner/issues/407.
                */
        return 0;
      case 'mechanical extract':
        /*
                    Similar to above, SAP 9 says if the infiltration rate is less than
                    0.5, then the combined infiltration and ventilation rate
                    should be set to the system air change rate of the
                    ventilation system; otherwise it should be the calculated
                    adjusted infiltration rate plus 0.5 × the system air change
                    rate. (This is expressed as "if (22b)m < 0.5, then
                    (24c) = (23b); otherwise (24c) = (22b)m + 0.5 × (23b)".)

                    Again, to avoid underestimating heat losses from
                    infiltration, we ignore the special case.

                    See https://github.com/emoncms/MyHomeEnergyPlanner/issues/407.
                */
        return 0.5 * this.input.systemAirChangeRate;
      case 'mechanical ventilation':
        return this.input.systemAirChangeRate;
      case 'mechanical ventilation with heat recovery':
        return this.input.systemAirChangeRate * (1 - this.input.efficiencyProportion);
    }
  }

  airChangesPerHour(month: Month): number {
    return this.adjustedEVPAirChanges(month) + this.systemAirChanges;
  }

  heatLossMonthly(month: Month): number {
    return this.airChangesPerHour(month) * 0.33 * this.dependencies.floors.totalVolume;
  }

  get heatLossAverage(): number {
    return mean(Month.all.map((m) => this.heatLossMonthly(m) ?? 0));
  }

  @cache
  get fanHeatGain(): number {
    // SAP tables 4h and 5a
    switch (this.input.type) {
      case 'unplanned/natural ventilation':
      case 'intermittent extract':
      case 'passive stack':
      case 'mechanical ventilation with heat recovery':
        return 0;
      case 'mechanical ventilation':
        return (
          2.5 *
          this.input.systemSpecificFanPower *
          0.06 *
          this.dependencies.floors.totalVolume
        );
      case 'mechanical extract':
        return (
          2.5 *
          this.input.systemSpecificFanPower *
          0.12 *
          this.dependencies.floors.totalVolume
        );
    }
  }

  // kWh
  get annualEnergyFans(): number {
    switch (this.input.type) {
      case 'unplanned/natural ventilation':
        return 0;
      case 'intermittent extract':
      case 'passive stack':
        /*
          There are several things to note here:

          1. We group PIV systems from loft space under "intermittent extract"
          2. SAP says PIV systems should have at least 2 fans entered by the assessor for
             a total of at least 20m³/h ventilation rate
          3. SAP says PIV systems from loft space should have 0 energy usage (SAP 10.2
             2.6.1 (a) paragraph 2)
          4. SAP does not include energy usage from other intermittent extract fans
             anywhere

          We actually disagree with (3) and (4), so we add 28kWh/year per Extract
          Ventilation Point (BREDEM).
         */
        return sum(
          this.input.extractVentilationPoints.map((extractVentilationPoint) =>
            // ventilationRate === 0 is used in non-baseline scenarios to signify an EVP
            // that has been removed
            extractVentilationPoint.ventilationRate > 0 ? 28 : 0,
          ),
        );
      case 'mechanical extract': {
        const inUseFactor = 2.5; // SAP 9 Table 4h (p214)
        return (
          inUseFactor *
          this.input.systemSpecificFanPower *
          1.22 *
          this.dependencies.floors.totalVolume
        );
      }
      case 'mechanical ventilation':
      case 'mechanical ventilation with heat recovery': {
        const inUseFactor = 2.5; // SAP 9 Table 4h (p214)
        return (
          inUseFactor *
          this.input.systemSpecificFanPower *
          2.44 *
          this.input.systemAirChangeRate *
          this.dependencies.floors.totalVolume
        );
      }
    }
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-assignment,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    data.ventilation = data.ventilation ?? {};
    const { ventilation } = data;
    ventilation.EVP_air_changes = this.rawEVPAirChanges;
    ventilation.adjusted_EVP_air_changes = Month.all.map((m) =>
      this.adjustedEVPAirChanges(m),
    );
    ventilation.average_ventilation_WK = this.heatLossAverage;
    const ventilationHeatLossArray = Month.all.map((m) => this.heatLossMonthly(m));
    ventilation.ventilation_WK = ventilationHeatLossArray;
    data.losses_WK.ventilation = ventilationHeatLossArray;
  }
  /* eslint-enable */
}

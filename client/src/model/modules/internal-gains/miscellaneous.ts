import { z } from 'zod';
import { scenarioSchema } from '../../../data-schemas/scenario';
import { Month } from '../../enums/month';

export type MiscellaneousInternalGainsDependencies = {
  occupancy: { occupancy: number };
};

export class MiscellaneousInternalGains {
  constructor(
    _input: null,
    private dependencies: MiscellaneousInternalGainsDependencies,
  ) {}

  get metabolicHeatGainPower(): number {
    // SAP Table 5
    // We always use Column A ("Typical")
    return 60 * this.dependencies.occupancy.occupancy;
  }

  get miscellaneousHeatLossPower(): number {
    // SAP counts this loss as a negative gain, the reason being that losses in SAP are
    // W/K, whereas gains are W, and this is a figure in W.
    return -40 * this.dependencies.occupancy.occupancy;
  }

  get heatGain(): number {
    return this.metabolicHeatGainPower + this.miscellaneousHeatLossPower;
  }

  mutateLegacyData(data: z.input<typeof scenarioSchema>) {
    const gains_W = data?.gains_W;
    if (gains_W === undefined) return;
    gains_W.metabolic = Month.all.map(() => this.metabolicHeatGainPower);
    gains_W.losses = Month.all.map(() => this.miscellaneousHeatLossPower);
  }
}

import { z } from 'zod';
import { Result } from '../../helpers/result';

const modeSchema = z.enum([
  'normal',
  'regulated',
  'standardised heating (pre-2025)',
  'standardised heating',
]);

export type Mode = z.output<typeof modeSchema>;
export function validateMode(
  mode: unknown,
): Result<Mode, z.ZodError<z.input<typeof modeSchema>>> {
  return Result.zodParse(modeSchema, mode);
}

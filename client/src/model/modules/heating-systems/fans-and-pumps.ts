import type { LegacyHeatingSystem } from '.';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { TypeOf, t } from '../../../data-schemas/visitable-types';
import { Month } from '../../enums/month';
import { ModelError } from '../../error';
import { ModelBehaviourFlags } from '../behaviour-version';
import { combiLossParameters, extractCombiLossParametersFromLegacy } from './combi-loss';

const common = {
  pumpPowerKWhPerYear: t.number(),
  combiLoss: combiLossParameters,
};

export const heatingSystemFansAndPumpsParameters = t.discriminatedUnion('type', [
  t.struct({
    type: t.literal('warm air system'),
    ...common,
    specificFanPower: t.nullable(t.number()), // W/(l/s) aka J/l; null means unspecified or unknown
    annualEnergyFansPumpsOverride: t.nullable(t.number()),
  }),
  t.struct({
    type: t.literal('central heating system with pump inside'),
    annualEnergyFansPumps: t.number(),
    ...common,
  }),
  t.struct({
    type: t.literal('other'),
    annualEnergyFansPumps: t.number(),
    ...common,
  }),
]);

export type HeatingSystemFansAndPumpsParameters = TypeOf<
  typeof heatingSystemFansAndPumpsParameters
>;

export function extractHeatingSystemFansAndPumpsParameters(
  legacySystem: LegacyHeatingSystem,
  Vc: number | null,
): HeatingSystemFansAndPumpsParameters {
  const common = {
    pumpPowerKWhPerYear: coalesceEmptyString(legacySystem.central_heating_pump, 0) ?? 0,
    combiLoss: extractCombiLossParametersFromLegacy(legacySystem, Vc),
  };
  if (legacySystem.central_heating_pump_inside ?? false) {
    return {
      type: 'central heating system with pump inside',
      ...common,
      annualEnergyFansPumps: legacySystem.fans_and_supply_pumps ?? 0,
    };
  } else if (legacySystem.category === 'Warm air systems') {
    return {
      type: 'warm air system' as const,
      ...common,
      specificFanPower: typeof legacySystem.sfp === 'number' ? legacySystem.sfp : null,
      annualEnergyFansPumpsOverride: legacySystem.fans_and_supply_pumps ?? null,
    };
  } else {
    return {
      type: 'other',
      ...common,
      annualEnergyFansPumps: legacySystem.fans_and_supply_pumps ?? 0,
    };
  }
}

export type HeatingSystemFansAndPumpsDependencies = {
  floors: { totalVolume: number };
  modelBehaviourFlags: {
    heatingSystems: Pick<ModelBehaviourFlags['heatingSystems'], 'fansAndPumps'>;
  };
};

export class HeatingSystemFansAndPumpsCalculator {
  private flags: ModelBehaviourFlags['heatingSystems']['fansAndPumps'];
  constructor(
    private input: HeatingSystemFansAndPumpsParameters,
    private dependencies: HeatingSystemFansAndPumpsDependencies,
  ) {
    this.flags = dependencies.modelBehaviourFlags.heatingSystems.fansAndPumps;
  }

  get specificFanPower(): number {
    if (this.input.type !== 'warm air system') {
      throw new ModelError(
        'input should be warm air system when using specific fan power',
      );
    }
    if (this.input.specificFanPower === null) {
      if (this.flags.fixUndefinedSpecificFanPowerInWarmAirSystems) {
        return 1.5;
      } else {
        return 0;
      }
    } else {
      return this.input.specificFanPower;
    }
  }

  heatGain(month: Month): number {
    switch (this.input.type) {
      case 'warm air system': {
        if (this.flags.warmAirSystemsZeroGainInSummer && month.season === 'summer') {
          return 0;
        }
        if (this.flags.fixHeatGainsForWarmAirSystems) {
          return this.specificFanPower * 0.04 * this.dependencies.floors.totalVolume;
        } else {
          return 0;
        }
      }
      case 'central heating system with pump inside':
        return (this.input.pumpPowerKWhPerYear * 1000) / (24 * 365); // to Watts
      case 'other':
        return 0;
    }
  }

  // kWh
  get annualEnergyFansPumps(): number {
    let out = 0;
    out += this.input.pumpPowerKWhPerYear;
    if (this.input.type === 'warm air system') {
      if (this.input.annualEnergyFansPumpsOverride !== null) {
        out += this.input.annualEnergyFansPumpsOverride;
      } else if (
        this.dependencies.modelBehaviourFlags.heatingSystems.fansAndPumps
          .allowFansPumpsEnergyToUseSpecificFanPower
      ) {
        // non-legacy mode: use specific fan power
        out += 0.4 * this.specificFanPower * this.dependencies.floors.totalVolume;
      } else {
        // legacy mode: ignore system
      }
    } else {
      out += this.input.annualEnergyFansPumps;
    }
    if (
      this.input.combiLoss?.type === 'instantaneous' &&
      this.input.combiLoss.keepHotFacility !== null
    ) {
      if (this.input.combiLoss.keepHotFacility.controlledByTimeClock) {
        out += 600;
      } else {
        out += 900;
      }
    }
    return out;
  }
}

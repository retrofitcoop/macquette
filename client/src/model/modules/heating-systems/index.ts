import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { cache } from '../../../helpers/cache-decorators';
import { isNotNull } from '../../../helpers/null-checking';
import { Fuels } from '../fuels';
import { validateFuelNames } from '../fuels/validate-fuel-fractions';
import {
  extractHeatingSystemFansAndPumpsParameters,
  HeatingSystemFansAndPumpsCalculator,
  HeatingSystemFansAndPumpsDependencies,
  HeatingSystemFansAndPumpsParameters,
} from './fans-and-pumps';
import type { HeatingSystemFuelParameters } from './fuel-requirements';
import {
  extractHeatingSystemSpaceParameters,
  HeatingSystemSpaceParameters,
} from './space-heating';
import {
  extractHeatingSystemWaterParameters,
  HeatingSystemWaterCalculator,
  HeatingSystemWaterDependencies,
  HeatingSystemWaterParameters,
} from './water';

export type HeatingSystemInput = {
  waterHeating: null | HeatingSystemWaterParameters;
  spaceHeating: null | HeatingSystemSpaceParameters;
  fansAndPumps: HeatingSystemFansAndPumpsParameters;
  fuel: HeatingSystemFuelParameters;
  id: string | number;
};

export type HeatingSystem = {
  waterHeating: null | HeatingSystemWaterCalculator;
  spaceHeating: null | HeatingSystemSpaceParameters;
  fansAndPumps: HeatingSystemFansAndPumpsCalculator;
  fuel: HeatingSystemFuelParameters;
  id: string | number;
};

export function extractHeatingSystemsInputFromLegacy(
  scenario: Scenario,
): HeatingSystemInput[] {
  return (scenario?.heating_systems ?? []).map((legacySystem) => ({
    waterHeating: extractHeatingSystemWaterParameters(legacySystem, scenario),
    fansAndPumps: extractHeatingSystemFansAndPumpsParameters(
      legacySystem,
      scenario?.water_heating?.Vc ?? null,
    ),
    spaceHeating: extractHeatingSystemSpaceParameters(legacySystem),
    fuel: {
      summerEfficiency:
        (coalesceEmptyString(legacySystem.summer_efficiency, 100) ?? 100) / 100,
      winterEfficiency:
        (coalesceEmptyString(legacySystem.winter_efficiency, 100) ?? 100) / 100,
      name: legacySystem.fuel,
    },
    id: legacySystem.id ?? '',
  }));
}

export type LegacyHeatingSystem = Exclude<
  Exclude<Scenario, undefined>['heating_systems'],
  undefined
>[0];

export type HeatingSystemsDependencies = HeatingSystemWaterDependencies &
  HeatingSystemFansAndPumpsDependencies & { fuels: Pick<Fuels, 'names'> };

export class HeatingSystems {
  constructor(
    private input: HeatingSystemInput[],
    private dependencies: HeatingSystemsDependencies,
  ) {}

  @cache
  get all(): HeatingSystem[] {
    validateFuelNames(
      this.input.map((inputSystem) => inputSystem.fuel.name),
      this.dependencies.fuels,
      'heating systems',
    );
    return this.input.map((inputSystem) => ({
      waterHeating:
        inputSystem.waterHeating === null
          ? null
          : new HeatingSystemWaterCalculator(inputSystem.waterHeating, this.dependencies),
      spaceHeating: inputSystem.spaceHeating === null ? null : inputSystem.spaceHeating,
      fansAndPumps: new HeatingSystemFansAndPumpsCalculator(
        inputSystem.fansAndPumps,
        this.dependencies,
      ),
      fuel: inputSystem.fuel,
      id: inputSystem.id,
    }));
  }

  /** A view of the water heating components of the heating systems */
  @cache
  get waterHeatingSystems(): HeatingSystemWaterCalculator[] {
    return this.all.map((system) => system.waterHeating).filter(isNotNull);
  }

  /** A view of the space heating components of the heating systems */
  @cache
  get spaceHeatingSystems(): HeatingSystemSpaceParameters[] {
    return this.all.map((system) => system.spaceHeating).filter(isNotNull);
  }

  /** A view of the fans and pumps energy components of the heating systems */
  @cache
  get fansAndPumps(): HeatingSystemFansAndPumpsCalculator[] {
    return this.all.map((system) => system.fansAndPumps);
  }
}

import { sumBy } from 'lodash';
import type { HeatingSystem } from '.';
import { sum } from '../../../helpers/array-reducers';
import { cache } from '../../../helpers/cache-decorators';
import { groupBy, mapValues } from '../../../helpers/map';
import { Month } from '../../enums/month';
import { ModelBehaviourFlags } from '../behaviour-version';
import type { FuelDemandByFuel } from '../fuel-requirements';
import { FuelName, Fuels } from '../fuels';
import type { SpaceHeating } from '../space-heating';
import type { WaterHeating } from '../water-heating';

export type HeatingSystemsFuelRequirementsDependencies = HeatingSystemFuelDependencies & {
  fuels: Pick<Fuels, 'fuels'>;
  heatingSystems: {
    all: Array<HeatingSystem>;
  };
};

type SpaceHeatingSystemFuel = HeatingSystemFuel & {
  baseSystem: {
    spaceHeating: NonNullable<HeatingSystemFuel['baseSystem']['spaceHeating']>;
  };
};
type WaterHeatingSystemFuel = HeatingSystemFuel & {
  baseSystem: {
    waterHeating: NonNullable<HeatingSystemFuel['baseSystem']['waterHeating']>;
  };
};

export class HeatingSystemsFuelRequirements {
  constructor(
    private _input: null,
    private dependencies: HeatingSystemsFuelRequirementsDependencies,
  ) {}

  get systems(): HeatingSystemFuel[] {
    return this.dependencies.heatingSystems.all.map(
      (system) => new HeatingSystemFuel(system, this.dependencies),
    );
  }

  @cache get spaceHeatingSystems(): SpaceHeatingSystemFuel[] {
    return this.systems.filter(
      (system): system is SpaceHeatingSystemFuel =>
        system.baseSystem.spaceHeating !== null,
    );
  }

  @cache get waterHeatingSystems(): WaterHeatingSystemFuel[] {
    return this.systems.filter(
      (system): system is WaterHeatingSystemFuel =>
        system.baseSystem.waterHeating !== null,
    );
  }

  get waterHeatingFuelDemandByFuelAnnual(): FuelDemandByFuel {
    return mapValues(
      groupBy(
        this.systems.filter((system) => system.baseSystem.waterHeating !== null),
        (system) => system.baseSystem.fuel.name,
      ),
      (systems) => {
        const energyDemand = sumBy(
          systems,
          (system) => system.waterHeatingEnergyDemandAnnual,
        );
        const fuelInput = sumBy(systems, (system) => system.waterHeatingFuelInputAnnual);
        const fractionSum = sumBy(
          systems,
          (system) => system.baseSystem.waterHeating?.input.fractionWaterHeating ?? 0,
        );
        return { energyDemand, fuelInput, relativeFraction: fractionSum };
      },
    );
  }

  get waterHeatingFuelInputAnnual(): number {
    return sum(
      this.systems
        .filter((system) => system.baseSystem.waterHeating !== null)
        .map((system) => system.waterHeatingFuelInputAnnual),
    );
  }

  get spaceHeatingFuelDemandByFuelAnnual(): FuelDemandByFuel {
    return mapValues(
      groupBy(
        this.systems.filter((system) => system.baseSystem.spaceHeating !== null),
        (system) => system.baseSystem.fuel.name,
      ),
      (systems) => {
        const energyDemand = sumBy(
          systems,
          (system) => system.spaceHeatingEnergyDemandAnnual,
        );
        const fuelInput = sumBy(systems, (system) => system.spaceHeatingFuelInputAnnual);
        const fractionSum = sumBy(
          systems,
          (system) => system.baseSystem.spaceHeating?.fraction ?? 0,
        );
        return { energyDemand, fuelInput, relativeFraction: fractionSum };
      },
    );
  }

  get spaceHeatingFuelInputAnnual(): number {
    return sum(
      this.systems
        .filter((system) => system.baseSystem.spaceHeating !== null)
        .map((system) => system.spaceHeatingFuelInputAnnual),
    );
  }

  /* eslint-disable @typescript-eslint/no-explicit-any,
      @typescript-eslint/no-unsafe-assignment,
      @typescript-eslint/no-unsafe-member-access,
      @typescript-eslint/no-unsafe-call */
  mutateLegacyData(data: any) {
    if (data === undefined) return;
    for (const system of this.systems) {
      const legacySystem = data.heating_systems.find(
        (s: any) => s.id === system.baseSystem.id,
      );
      if (legacySystem === undefined) {
        console.error('Could not match system ID', system.baseSystem.id);
      } else {
        legacySystem.efficiency = system.waterHeatingEfficiency;
      }
    }
    data.fuel_requirements = data.fuel_requirements ?? {};
    data.fuel_requirements.waterheating = {
      quantity: this.waterHeatingFuelInputAnnual,
      list: Array.from(this.waterHeatingFuelDemandByFuelAnnual).map(
        ([name, { energyDemand, fuelInput, relativeFraction }]) => ({
          fuel: name,
          demand: energyDemand,
          fuel_input: fuelInput,
          fraction: relativeFraction,
        }),
      ),
    };
    data.fuel_requirements.space_heating = {
      quantity: this.spaceHeatingFuelInputAnnual,
      list: Array.from(this.spaceHeatingFuelDemandByFuelAnnual).map(
        ([name, { energyDemand, fuelInput, relativeFraction }]) => ({
          fuel: name,
          demand: energyDemand,
          fuel_input: fuelInput,
          fraction: relativeFraction,
        }),
      ),
    };
  }
  /* eslint-enable */
}

export type HeatingSystemFuelParameters = {
  summerEfficiency: number;
  winterEfficiency: number;
  name: FuelName;
};

export type HeatingSystemFuelDependencies = {
  waterHeating: Pick<WaterHeating, 'heatOutputAnnual' | 'heatOutputMonthly'>;
  spaceHeating: Pick<
    SpaceHeating,
    'spaceHeatingDemandEnergyAnnual' | 'spaceHeatingDemandEnergy'
  >;
  modelBehaviourFlags: {
    fuelRequirements: Pick<ModelBehaviourFlags['fuelRequirements'], 'heatingSystems'>;
  };
};

export class HeatingSystemFuel {
  constructor(
    public baseSystem: HeatingSystem,
    private dependencies: HeatingSystemFuelDependencies,
  ) {}

  get spaceHeatingEfficiency(): number {
    if (this.baseSystem.spaceHeating === null) return 0;
    return this.baseSystem.fuel.winterEfficiency;
  }

  get waterHeatingEfficiency(): number {
    /* In SAP this is monthly, but only because PCDB gives monthly values. We use an
     * annual average in libraries, so we don't make it monthly.
     *
     * Notes on this algorithm: It is implemented as according to SAP 9 paragraph 9.2.1. In
     * SAP 10 this is moved to Appendix D, section D2.1, but the equation (D1) contains
     * errors. We assume it is intended to match the equation in SAP 9.
     */
    if (this.baseSystem.waterHeating === null) return 0;
    if (this.baseSystem.spaceHeating === null) {
      return this.baseSystem.fuel.summerEfficiency;
    } else {
      const energySpace =
        this.dependencies.spaceHeating.spaceHeatingDemandEnergyAnnual *
        this.baseSystem.spaceHeating.fraction;
      const energyWater =
        this.dependencies.waterHeating.heatOutputAnnual *
        this.baseSystem.waterHeating.input.fractionWaterHeating;
      let summerEfficiency = this.baseSystem.fuel.summerEfficiency;
      let winterEfficiency = this.baseSystem.fuel.winterEfficiency;
      if (
        this.dependencies.modelBehaviourFlags.fuelRequirements.heatingSystems
          .useSummerEfficiencyIfHigherThanWinter &&
        summerEfficiency > winterEfficiency
      ) {
        return summerEfficiency;
      }
      if (
        !this.dependencies.modelBehaviourFlags.fuelRequirements.heatingSystems
          .fixSwappedSeasonalEfficiencies
      ) {
        const temp = summerEfficiency;
        summerEfficiency = winterEfficiency;
        winterEfficiency = temp;
      }
      const out =
        (energySpace + energyWater) /
        (energySpace / winterEfficiency + energyWater / summerEfficiency);
      if (Number.isNaN(out)) return summerEfficiency;
      else return out;
    }
  }

  // kWh
  waterHeatingEnergyDemand(month: Month): number {
    if (this.baseSystem.waterHeating === null) return 0;
    return (
      this.baseSystem.waterHeating.input.fractionWaterHeating *
      this.dependencies.waterHeating.heatOutputMonthly(month)
    );
  }

  get waterHeatingEnergyDemandAnnual(): number {
    return sum(Month.all.map((m) => this.waterHeatingEnergyDemand(m)));
  }

  // kWh-fuel
  waterHeatingFuelInput(month: Month): number {
    if (this.waterHeatingEnergyDemandAnnual === 0 && this.waterHeatingEfficiency === 0) {
      return 0;
    }
    return this.waterHeatingEnergyDemand(month) / this.waterHeatingEfficiency;
  }

  get waterHeatingFuelInputAnnual(): number {
    return sum(Month.all.map((m) => this.waterHeatingFuelInput(m)));
  }

  // kWh
  spaceHeatingEnergyDemand(month: Month): number {
    if (this.baseSystem.spaceHeating === null) return 0;
    return (
      this.baseSystem.spaceHeating.fraction *
      this.dependencies.spaceHeating.spaceHeatingDemandEnergy(month)
    );
  }

  // kWh-fuel
  spaceHeatingFuelInput(month: Month): number {
    const energyDemand = this.spaceHeatingEnergyDemand(month);
    if (energyDemand === 0 && this.spaceHeatingEfficiency === 0) {
      return 0;
    }
    return energyDemand / this.spaceHeatingEfficiency;
  }

  // kWh
  get spaceHeatingEnergyDemandAnnual(): number {
    return sum(Month.all.map((m) => this.spaceHeatingEnergyDemand(m)));
  }

  // kWh-fuel
  get spaceHeatingFuelInputAnnual(): number {
    return sum(Month.all.map((m) => this.spaceHeatingFuelInput(m)));
  }
}

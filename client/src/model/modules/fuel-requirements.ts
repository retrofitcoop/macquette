import { mapValues } from 'lodash';
import { sum } from '../../helpers/array-reducers';
import { cache } from '../../helpers/cache-decorators';
import { ModelError } from '../error';
import { ModelBehaviourFlags } from './behaviour-version';
import { FansPumpsElectricKeepHot } from './fans-pumps-electric-keep-hot';
import { Floors } from './floors';
import { FuelName, Fuels } from './fuels';
import { Generation } from './generation';
import { HeatingSystemsFuelRequirements } from './heating-systems/fuel-requirements';
import { AppliancesCookingLoadCollections } from './lighting-appliances-cooking/loads/load-collection';
import { AppliancesSAP } from './lighting-appliances-cooking/sap/appliances';
import { CookingSAP } from './lighting-appliances-cooking/sap/cooking';
import { LightingSAP } from './lighting-appliances-cooking/sap/lighting';
import { Mode } from './mode';
import { Occupancy } from './occupancy';

export type FuelDemandParameters = {
  energyDemand: number;
  fuelInput: number;
  relativeFraction: number; // Relative to the fuel's purpose, e.g. water heating fuel fractions sum to 1
};

export type FuelDemandByFuel = Map<FuelName, FuelDemandParameters>;

export type FuelRequirementsDependencies = {
  fuels: Pick<Fuels, 'fuels' | 'generation'>;
  fansPumpsElectricKeepHot: Pick<FansPumpsElectricKeepHot, 'fuelDemandByFuelAnnual'>;
  heatingSystemsFuelRequirements: Pick<
    HeatingSystemsFuelRequirements,
    'waterHeatingFuelDemandByFuelAnnual' | 'spaceHeatingFuelDemandByFuelAnnual'
  >;
  appliancesCookingLoadCollections: {
    cooking: Pick<AppliancesCookingLoadCollections['cooking'], 'fuelDemandByFuelAnnual'>;
    appliances: Pick<
      AppliancesCookingLoadCollections['appliances'],
      'fuelDemandByFuelAnnual'
    >;
  };
  appliancesSap: Pick<AppliancesSAP, 'fuelDemandByFuelAnnual'>;
  cookingSap: Pick<CookingSAP, 'fuelDemandByFuelAnnual'>;
  lighting: Pick<LightingSAP, 'fuelDemandByFuelAnnual'>;
  generation: Pick<
    Generation,
    | 'energyAnnual'
    | 'energyUsedOnsiteAnnual'
    | 'carbonEmissionsAnnual'
    | 'primaryEnergyAnnual'
    | 'incomeAnnual'
  >;
  floors: Pick<Floors, 'totalFloorArea'>;
  occupancy: Pick<Occupancy, 'occupancy'>;
  modelBehaviourFlags: {
    fuelRequirements: Pick<
      ModelBehaviourFlags['fuelRequirements'],
      | 'zeroQuantityFuelsDoNotIncurStandingCharge'
      | 'propagateInfiniteFuelInputInRegulatedMode'
    >;
  };
  mode: Mode;
};

export type FuelUsage = {
  quantity: number; // in fuel units
  cost: number; // £
  primaryEnergy: number; // kWh
  carbonEmissions: number; // kg CO_2
};

export class FuelRequirements {
  constructor(
    _input: null,
    private dependencies: FuelRequirementsDependencies,
  ) {}

  @cache
  get fuelDemandsByFuelByModule() {
    let appliancesCookingMixin: {
      appliances: FuelDemandByFuel;
      cooking: FuelDemandByFuel;
      appliancesSap: FuelDemandByFuel;
      cookingSap: FuelDemandByFuel;
    };
    switch (this.dependencies.mode) {
      case 'normal':
      case 'standardised heating':
      case 'standardised heating (pre-2025)':
        appliancesCookingMixin = {
          appliances:
            this.dependencies.appliancesCookingLoadCollections.appliances
              .fuelDemandByFuelAnnual,
          cooking:
            this.dependencies.appliancesCookingLoadCollections.cooking
              .fuelDemandByFuelAnnual,
          appliancesSap: this.dependencies.appliancesSap.fuelDemandByFuelAnnual,
          cookingSap: this.dependencies.cookingSap.fuelDemandByFuelAnnual,
        };
        break;
      case 'regulated':
        appliancesCookingMixin = {
          appliances: new Map(),
          cooking: new Map(),
          appliancesSap: new Map(),
          cookingSap: new Map(),
        };
        break;
    }
    return {
      fansPumpsElectricKeepHot:
        this.dependencies.fansPumpsElectricKeepHot.fuelDemandByFuelAnnual,
      waterHeating:
        this.dependencies.heatingSystemsFuelRequirements
          .waterHeatingFuelDemandByFuelAnnual,
      spaceHeating:
        this.dependencies.heatingSystemsFuelRequirements
          .spaceHeatingFuelDemandByFuelAnnual,
      ...appliancesCookingMixin,
      lighting: this.dependencies.lighting.fuelDemandByFuelAnnual,
    };
  }

  @cache
  get totalsByFuelAnnual(): Map<FuelName, FuelUsage> {
    const out = new Map();
    for (const [name, fuel] of Object.entries(this.dependencies.fuels.fuels)) {
      const moduleFuelInputs = Object.values(this.fuelDemandsByFuelByModule).map(
        (demands) => demands.get(name)?.fuelInput,
      );
      const quantity = sum(
        moduleFuelInputs.map((fuelInput) => {
          if (fuelInput === undefined) return 0;
          const { propagateInfiniteFuelInputInRegulatedMode } =
            this.dependencies.modelBehaviourFlags.fuelRequirements;
          if (
            !propagateInfiniteFuelInputInRegulatedMode &&
            this.dependencies.mode === 'regulated' &&
            !Number.isFinite(fuelInput)
          ) {
            return 0;
          }
          return fuelInput;
        }),
      );
      const { zeroQuantityFuelsDoNotIncurStandingCharge } =
        this.dependencies.modelBehaviourFlags.fuelRequirements;
      const sensibleCaseInclude =
        zeroQuantityFuelsDoNotIncurStandingCharge && quantity !== 0;
      const legacyCaseInclude =
        !zeroQuantityFuelsDoNotIncurStandingCharge &&
        moduleFuelInputs.some((q) => q !== undefined);
      if (sensibleCaseInclude || legacyCaseInclude) {
        out.set(name, {
          quantity,
          cost: (quantity * fuel.unitPrice) / 100 + fuel.standingCharge,
          carbonEmissions: quantity * fuel.carbonEmissionsFactor,
          primaryEnergy: quantity * fuel.primaryEnergyFactor,
        });
      }
    }
    out.set(Fuels.GENERATION, {
      quantity: -this.dependencies.generation.energyAnnual,
      cost:
        (-this.dependencies.generation.energyUsedOnsiteAnnual *
          this.dependencies.fuels.generation.unitPrice) /
        100,
      carbonEmissions: -this.dependencies.generation.carbonEmissionsAnnual,
      primaryEnergy: -this.dependencies.generation.primaryEnergyAnnual,
    });
    return out;
  }

  @cache
  get fuelInputByFuelCategoryAnnual() {
    const result = { electricity: 0, gas: 0, oil: 0, 'solid fuel': 0 };

    for (const [fuelName, fuelUsage] of this.totalsByFuelAnnual.entries()) {
      const fuel = this.dependencies.fuels.fuels[fuelName];
      if (fuel === undefined) {
        throw new ModelError(
          `unreachable (fuel ${fuelName} did not have a matching entry in fuels table)`,
        );
      } else if (fuel.category === 'generation') {
        continue;
      }

      result[fuel.category] += fuelUsage.quantity;
    }

    return result;
  }

  get carbonEmissionsByModuleAnnual() {
    return mapValues(this.fuelDemandsByFuelByModule, (fuelDemandByFuel) => {
      let total = 0;
      for (const [fuelName, { fuelInput }] of fuelDemandByFuel) {
        const fuel = this.dependencies.fuels.fuels[fuelName];
        if (fuel === undefined) {
          throw new ModelError(
            `unreachable (fuel ${fuelName} did not have a matching entry in fuels table)`,
          );
        }

        total += fuelInput * fuel.carbonEmissionsFactor;
      }
      return total;
    });
  }

  get fuelInputByModuleAnnual() {
    const fuelDemandsByFuel = this.fuelDemandsByFuelByModule;
    return mapValues(fuelDemandsByFuel, (fuelDemandByFuel) => {
      return sum(Array.from(fuelDemandByFuel.values()).map(({ fuelInput }) => fuelInput));
    });
  }

  @cache
  get fuelInputByModulePerAreaAnnual() {
    return mapValues(
      this.fuelInputByModuleAnnual,
      (energyDemand) => energyDemand / this.dependencies.floors.totalFloorArea,
    );
  }

  get totalCostAnnual(): number {
    return sum(Array.from(this.totalsByFuelAnnual.values()).map(({ cost }) => cost));
  }

  get totalNetCostAnnual(): number {
    return this.totalCostAnnual - this.dependencies.generation.incomeAnnual;
  }

  get totalCarbonEmissionsAnnual(): number {
    return sum(
      Array.from(this.totalsByFuelAnnual.values()).map(
        ({ carbonEmissions }) => carbonEmissions,
      ),
    );
  }

  get totalCarbonEmissionsAnnualPerArea(): number {
    return this.totalCarbonEmissionsAnnual / this.dependencies.floors.totalFloorArea;
  }

  get totalEnergyAnnual(): number {
    return sum(
      Array.from(this.totalsByFuelAnnual)
        .filter(([fuelName]) => fuelName !== Fuels.GENERATION)
        .map(([, { quantity }]) => quantity),
    );
  }

  get energyPerPersonPerDayAverage(): number {
    return this.totalEnergyAnnual / 365 / this.dependencies.occupancy.occupancy;
  }

  get totalEnergyAnnualPerArea(): number {
    return this.totalEnergyAnnual / this.dependencies.floors.totalFloorArea;
  }

  get totalPrimaryEnergyAnnual(): number {
    return sum(
      Array.from(this.totalsByFuelAnnual.values()).map(
        ({ primaryEnergy }) => primaryEnergy,
      ),
    );
  }

  get primaryEnergyPerPersonPerDayAverage(): number {
    return this.totalPrimaryEnergyAnnual / 365 / this.dependencies.occupancy.occupancy;
  }

  get totalPrimaryEnergyAnnualPerArea(): number {
    return this.totalPrimaryEnergyAnnual / this.dependencies.floors.totalFloorArea;
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    if (data === undefined) return;
    data.fuel_totals = Object.fromEntries(
      Array.from(this.totalsByFuelAnnual).map(
        ([name, { quantity, cost, carbonEmissions, primaryEnergy }]) =>
          [
            name,
            {
              name,
              quantity,
              annualcost: cost,
              annualco2: carbonEmissions,
              primaryenergy: primaryEnergy,
            },
          ] as const,
      ),
    );
    data.total_cost = this.totalCostAnnual;
    data.net_cost = this.totalNetCostAnnual;
    data.annualco2 = this.totalCarbonEmissionsAnnual;
    data.kgco2perm2 = this.totalCarbonEmissionsAnnualPerArea;
    data.primary_energy_use = this.totalPrimaryEnergyAnnual;
    data.primary_energy_use_m2 = this.totalPrimaryEnergyAnnualPerArea;
    data.energy_use = this.totalEnergyAnnual;
    data.kwhdpp = this.energyPerPersonPerDayAverage;
    data.primarykwhdpp = this.primaryEnergyPerPersonPerDayAverage;
  }
  /* eslint-enable */
}

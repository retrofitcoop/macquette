import { Scenario } from '../../data-schemas/scenario';
import { coalesceEmptyString } from '../../data-schemas/scenario/value-schemas';
import { distance } from '../../helpers/distance';
import type { WeatherStation } from '../datasets/weather-stations';
import { weatherStations } from '../datasets/weather-stations';

export type GeographyInput = {
  elevation: number | null;
  latLong: [number, number] | null;
  override: number | null;
};

export function extractGeographyInputFromLegacy(scenario: Scenario): GeographyInput {
  let override = null;
  if (
    scenario?.externalDesignTemperatureOverride !== undefined &&
    scenario?.externalDesignTemperatureOverride.enabled === true
  ) {
    override = scenario.externalDesignTemperatureOverride.value;
  }

  return {
    elevation: coalesceEmptyString(scenario?.altitude, null) ?? null,
    latLong: scenario?.household?.latLong ?? null,
    override,
  };
}

export class Geography {
  readonly latLong: GeographyInput['latLong'];
  readonly elevation: GeographyInput['elevation'];
  readonly override: GeographyInput['override'];

  constructor(readonly input: GeographyInput) {
    this.latLong = input.latLong;
    this.elevation = input.elevation;
    this.override = input.override;
  }

  /** Find closest weather station to the location using Vincenty distance */
  get nearestWeatherStation(): WeatherStation | null {
    const latLong = this.latLong;
    if (latLong === null) {
      return null;
    }

    const withDistances = weatherStations.map((station) => {
      return {
        ...station,
        distance: distance(latLong, [station.lat, station.long]),
      };
    });
    withDistances.sort((a, b) => a.distance - b.distance);
    const closest = withDistances[0];
    if (closest !== undefined) {
      return closest;
    } else {
      return null;
    }
  }

  /** Calculate external design temp as per CIBSE A */
  get cibseExternalDesignTemperature(): number {
    const station = this.nearestWeatherStation;
    if (station === null || this.elevation === null) {
      return -4;
    } else {
      const metresAboveStation = Math.max(0, this.elevation - station.altitude);
      // -0.6deg per complete 100m of altitude increase
      return station.minTemp + Math.floor(metresAboveStation / 100) * -0.6;
    }
  }

  get externalDesignTemperature(): number {
    if (this.override !== null) {
      return this.override;
    } else {
      return this.cibseExternalDesignTemperature;
    }
  }
}

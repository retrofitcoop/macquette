import { range } from 'lodash';
import { electricityCarbonIntensityByYear } from '../datasets/electricity-carbon-intensity';
import { Fuels } from './fuels';

type CarbonDependencies = {
  fuels: Pick<Fuels, 'fuels'>;
  fuelRequirements: {
    totalsByFuelAnnual: Map<
      string,
      {
        quantity: number;
      }
    >;
  };
};

type CumulativeEmissionsByYear = Array<{ year: number; cumulativeEmissions: number }>;

export class Carbon {
  constructor(
    _input: null,
    private dependencies: CarbonDependencies,
  ) {}

  // kgCO₂e
  emissionsByYear(year: number): number {
    let out = 0;
    for (const [name, { quantity }] of this.dependencies.fuelRequirements
      .totalsByFuelAnnual) {
      const fuel = this.dependencies.fuels.fuels[name];
      if (fuel === undefined) {
        console.warn(`fuel ${name} couldn't be found`);
        continue;
      }
      if (fuel.category === 'electricity') {
        const gramsPerKilogram = 1000;
        out += (quantity * electricityCarbonIntensityByYear(year)) / gramsPerKilogram;
      } else if (fuel.category === 'generation') {
        // This doesn't represent usage, skip
      } else {
        out += quantity * fuel.carbonEmissionsFactor;
      }
    }
    return out;
  }

  // kgCO₂e
  cumulativeEmissionsByYear(
    firstYear: number,
    lastYear: number,
  ): CumulativeEmissionsByYear {
    const out: CumulativeEmissionsByYear = [];
    let cumulativeTotal = 0;
    for (const year of range(firstYear, lastYear + 1)) {
      cumulativeTotal += this.emissionsByYear(year);
      out.push({ year, cumulativeEmissions: cumulativeTotal });
    }
    return out;
  }
}

import { ModelBehaviourVersion } from '../../data-schemas/scenario';
import { safeMerge } from '../../helpers/safe-merge';

export type ModelBehaviourFlags = {
  appliancesCookingLoadCollections: {
    treatMonthlyGainAsPower: boolean; // Severe bug
    convertGainsToWatts: boolean; // Severe bug
    useFuelInputForFuelFraction: boolean; // Minor bug
    useWeightedMonthsForEnergyDemand: boolean; // Minor improvement
  };
  appliancesSap: {
    applyEfficiencyReductionForEnergyMonthly: boolean; // Insignificant
  };
  generation: {
    includeAllSystemsInPrimaryEnergyTotal: boolean; // Minor bug
  };
  currentEnergy: {
    countSavingsCorrectlyInUsage: boolean; // Minor bug
    calculateSavingsIncorporatingOnsiteUse: boolean; // Minor improvement
  };
  heatingSystems: {
    fansAndPumps: {
      fixUndefinedSpecificFanPowerInWarmAirSystems: boolean; // Minor bug
      warmAirSystemsZeroGainInSummer: boolean; // SAP 9 vs 10
      fixHeatGainsForWarmAirSystems: boolean; // ~5% error
      allowFansPumpsEnergyToUseSpecificFanPower: boolean; // Insignificant
    };
    waterHeating: {
      fixLossesForStorageCombiOfUnknownSize: boolean; // Insignificant
    };
  };
  meanInternalTemperature: {
    utilisationFactor: {
      preserveGammaPrecision: boolean; // Common but ~10e-9% error
      etaIs1IfGammaIsLessThanOrEqualTo0: boolean; // Rare but ~20% error
    };
    fixRestOfDwellingTemperatureCalculation: boolean; // Rare but ~+/-30% error
    treatDifferentHeatingControlsCorrectly: boolean; // Totally insignificant in test data
    normaliseHeatingFractionsForTemperatureAdjustment: boolean; // Very rare and ~-1% error
    whenNoHeatingSystems: {
      correctRestOfDwellingTemperature: boolean; // ~20% error
      correctRestOfDwellingUtilisationFactor: boolean; // Insignificant
      useSAPTemperatureAdjustment: boolean; // ~-5% error
    };
    includeSolarGains: boolean;
    disallowPre2025StandardisedHeatingMode: boolean;
  };
  spaceHeating: {
    utilisationFactor: {
      preserveGammaPrecision: boolean; // Common but ~10e-9% error
    };
    skipLegacyCoolingCalculation: boolean; // Unknown error due to selection bias
    clampNegativeTemperatureDifference: boolean; // Insignificant if no space cooling
  };
  fuelRequirements: {
    heatingSystems: {
      fixSwappedSeasonalEfficiencies: boolean; // Insignificant for SHD
      useSummerEfficiencyIfHigherThanWinter: boolean; // Insignificant for SHD
    };
    zeroQuantityFuelsDoNotIncurStandingCharge: boolean;
    propagateInfiniteFuelInputInRegulatedMode: boolean;
  };
  waterHeating: {
    applyEtudeGainsReductions: boolean;
  };
  floors: {
    applyVolumeReduction: boolean;
  };
  heatingHours: {
    fixHoursOffCalculation: boolean;
  };
  fansPumpsElectricKeepHot: {
    useDefaultFuelIfFuelFractionsIsEmpty: boolean;
  };
};

export function constructModelBehaviourFlags(
  version: ModelBehaviourVersion,
): ModelBehaviourFlags {
  const legacyFlags: ModelBehaviourFlags = {
    appliancesCookingLoadCollections: {
      treatMonthlyGainAsPower: false,
      convertGainsToWatts: false,
      useFuelInputForFuelFraction: false,
      useWeightedMonthsForEnergyDemand: false,
    },
    appliancesSap: {
      applyEfficiencyReductionForEnergyMonthly: false,
    },
    generation: {
      includeAllSystemsInPrimaryEnergyTotal: false,
    },
    currentEnergy: {
      countSavingsCorrectlyInUsage: false,
      calculateSavingsIncorporatingOnsiteUse: false,
    },
    heatingSystems: {
      fansAndPumps: {
        fixUndefinedSpecificFanPowerInWarmAirSystems: false,
        warmAirSystemsZeroGainInSummer: false,
        allowFansPumpsEnergyToUseSpecificFanPower: false,
        fixHeatGainsForWarmAirSystems: false,
      },
      waterHeating: {
        fixLossesForStorageCombiOfUnknownSize: false,
      },
    },
    meanInternalTemperature: {
      utilisationFactor: {
        preserveGammaPrecision: false,
        etaIs1IfGammaIsLessThanOrEqualTo0: false,
      },
      fixRestOfDwellingTemperatureCalculation: false,
      treatDifferentHeatingControlsCorrectly: false,
      normaliseHeatingFractionsForTemperatureAdjustment: false,
      whenNoHeatingSystems: {
        correctRestOfDwellingTemperature: false,
        correctRestOfDwellingUtilisationFactor: false,
        useSAPTemperatureAdjustment: false,
      },
      includeSolarGains: true, // This was a bug introduced in v5
      disallowPre2025StandardisedHeatingMode: false,
    },
    spaceHeating: {
      utilisationFactor: {
        preserveGammaPrecision: false,
      },
      skipLegacyCoolingCalculation: false,
      clampNegativeTemperatureDifference: false,
    },
    fuelRequirements: {
      heatingSystems: {
        fixSwappedSeasonalEfficiencies: false,
        useSummerEfficiencyIfHigherThanWinter: false,
      },
      zeroQuantityFuelsDoNotIncurStandingCharge: false,
      propagateInfiniteFuelInputInRegulatedMode: false,
    },
    waterHeating: {
      applyEtudeGainsReductions: false,
    },
    floors: {
      applyVolumeReduction: false,
    },
    heatingHours: {
      fixHoursOffCalculation: false,
    },
    fansPumpsElectricKeepHot: {
      useDefaultFuelIfFuelFractionsIsEmpty: false,
    },
  };
  const v1Flags = safeMerge(legacyFlags, {
    appliancesCookingLoadCollections: {
      treatMonthlyGainAsPower: true,
      convertGainsToWatts: true,
      useFuelInputForFuelFraction: true,
      useWeightedMonthsForEnergyDemand: true,
    },
  });
  const v2Flags = safeMerge(v1Flags, {
    generation: {
      includeAllSystemsInPrimaryEnergyTotal: true,
    },
  });
  const v3Flags = safeMerge(v2Flags, {
    currentEnergy: {
      countSavingsCorrectlyInUsage: true,
      calculateSavingsIncorporatingOnsiteUse: true,
    },
  });
  const v4Flags = safeMerge(v3Flags, {
    heatingSystems: {
      fansAndPumps: {
        fixUndefinedSpecificFanPowerInWarmAirSystems: true,
        warmAirSystemsZeroGainInSummer: true,
        fixHeatGainsForWarmAirSystems: true,
      },
    },
  });
  const v5Flags = safeMerge(v4Flags, {
    meanInternalTemperature: {
      utilisationFactor: {
        preserveGammaPrecision: true,
        etaIs1IfGammaIsLessThanOrEqualTo0: true,
      },
      fixRestOfDwellingTemperatureCalculation: true,
      treatDifferentHeatingControlsCorrectly: true,
      normaliseHeatingFractionsForTemperatureAdjustment: true,
      whenNoHeatingSystems: {
        correctRestOfDwellingTemperature: true,
        correctRestOfDwellingUtilisationFactor: true,
        useSAPTemperatureAdjustment: true,
      },
      includeSolarGains: false,
    },
  });
  const v6Flags = safeMerge(v5Flags, {
    meanInternalTemperature: {
      includeSolarGains: true,
    },
  });
  const v7Flags = safeMerge(v6Flags, {
    meanInternalTemperature: {
      includeSolarGains: false, // Regression
    },
    heatingSystems: {
      fansAndPumps: {
        allowFansPumpsEnergyToUseSpecificFanPower: true,
      },
      waterHeating: {
        fixLossesForStorageCombiOfUnknownSize: true,
      },
    },
  });
  const v8Flags = safeMerge(v7Flags, {
    meanInternalTemperature: {
      includeSolarGains: true,
      utilisationFactor: {
        etaIs1IfGammaIsLessThanOrEqualTo0: true,
      },
    },
  });
  const v9Flags = safeMerge(v8Flags, {
    spaceHeating: {
      utilisationFactor: {
        preserveGammaPrecision: true,
      },
      skipLegacyCoolingCalculation: true,
      clampNegativeTemperatureDifference: true,
    },
  });
  const v10Flags = safeMerge(v9Flags, {
    fuelRequirements: {
      heatingSystems: {
        fixSwappedSeasonalEfficiencies: true,
        useSummerEfficiencyIfHigherThanWinter: true,
      },
    },
  });
  const v11Flags = safeMerge(v10Flags, {
    appliancesSap: {
      applyEfficiencyReductionForEnergyMonthly: true,
    },
    fuelRequirements: {
      zeroQuantityFuelsDoNotIncurStandingCharge: true,
      propagateInfiniteFuelInputInRegulatedMode: true,
    },
  });
  const v12Flags = safeMerge(v11Flags, {
    meanInternalTemperature: {
      disallowPre2025StandardisedHeatingMode: true,
    },
    waterHeating: {
      applyEtudeGainsReductions: true,
    },
  });
  const v13Flags = safeMerge(v12Flags, {
    floors: {
      applyVolumeReduction: true,
    },
  });
  const v14Flags = safeMerge(v13Flags, {
    heatingHours: {
      fixHoursOffCalculation: true,
    },
  });
  const v15Flags = safeMerge(v14Flags, {
    fansPumpsElectricKeepHot: {
      useDefaultFuelIfFuelFractionsIsEmpty: true,
    },
  });
  switch (version) {
    case 'legacy':
      return legacyFlags;
    case 1:
      return v1Flags;
    case 2:
      return v2Flags;
    case 3:
      return v3Flags;
    case 4:
      return v4Flags;
    case 5:
      return v5Flags;
    case 6:
      return v6Flags;
    case 7:
      return v7Flags;
    case 8:
      return v8Flags;
    case 9:
      return v9Flags;
    case 10:
      return v10Flags;
    case 11:
      return v11Flags;
    case 12:
      return v12Flags;
    case 13:
      return v13Flags;
    case 14:
      return v14Flags;
    case 15:
      return v15Flags;
  }
}

export type Weekly<T> = { weekday: T; weekend: T };

export function averageWeekly(val: Weekly<number>) {
  return (5 * val.weekday + 2 * val.weekend) / 7;
}

export function mapWeekly<U, V>(val: Weekly<U>, fn: (u: U) => V): Weekly<V> {
  return {
    weekday: fn(val.weekday),
    weekend: fn(val.weekend),
  };
}

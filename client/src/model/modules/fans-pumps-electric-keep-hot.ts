import { cloneDeep, times } from 'lodash';
import { Scenario } from '../../data-schemas/scenario';
import { sum } from '../../helpers/array-reducers';
import { cache } from '../../helpers/cache-decorators';
import { isIndexable } from '../../helpers/is-indexable';
import { ModelBehaviourFlags } from './behaviour-version';
import type { FuelDemandByFuel, FuelDemandParameters } from './fuel-requirements';
import { FuelName, Fuels, FuelsDict } from './fuels';
import { extractFuelFractionsFromLegacy } from './fuels/extract-fuel-fractions-from-legacy';
import { validateFuelNames } from './fuels/validate-fuel-fractions';
import { SolarHotWater } from './solar-hot-water';
import { Ventilation } from './ventilation-infiltration/ventilation';
import { WaterCommon } from './water-common';

export type FansPumpsElectricKeepHotDependencies = {
  heatingSystems: {
    fansAndPumps: Array<{ annualEnergyFansPumps: number }>;
  };
  ventilation: Pick<Ventilation, 'annualEnergyFans'>;
  waterCommon: Pick<WaterCommon, 'solarHotWater'>;
  solarHotWater: Pick<SolarHotWater, 'annualEnergyPumps'>;
  fuels: Pick<Fuels, 'names'>;
  modelBehaviourFlags: {
    fansPumpsElectricKeepHot: Pick<
      ModelBehaviourFlags['fansPumpsElectricKeepHot'],
      'useDefaultFuelIfFuelFractionsIsEmpty'
    >;
  };
};

export type FansPumpsElectricKeepHotInput = {
  fuelFractions: FuelsDict<number>;
};

export function extractFansPumpsElectricKeepHotInputFromLegacy(
  scenario: Scenario,
): FansPumpsElectricKeepHotInput {
  return {
    fuelFractions: extractFuelFractionsFromLegacy(scenario?.fans_and_pumps),
  };
}

export class FansPumpsElectricKeepHot {
  constructor(
    public input: FansPumpsElectricKeepHotInput,
    private dependencies: FansPumpsElectricKeepHotDependencies,
  ) {
    validateFuelNames(input.fuelFractions, dependencies.fuels, this.constructor.name);
  }

  // kWh
  @cache
  get annualEnergy(): number {
    return (
      sum(
        this.dependencies.heatingSystems.fansAndPumps.map((s) => s.annualEnergyFansPumps),
      ) +
      this.dependencies.ventilation.annualEnergyFans +
      this.dependencies.solarHotWater.annualEnergyPumps
    );
  }

  @cache
  get fuelDemandByFuelAnnual(): FuelDemandByFuel {
    let fuelFractionsEntries = Object.entries(this.input.fuelFractions);
    if (
      this.dependencies.modelBehaviourFlags.fansPumpsElectricKeepHot
        .useDefaultFuelIfFuelFractionsIsEmpty &&
      fuelFractionsEntries.length === 0
    ) {
      fuelFractionsEntries = [[Fuels.STANDARD_TARIFF, 1]];
    }
    return new Map(
      fuelFractionsEntries.map(
        ([fuelName, fraction]): [FuelName, FuelDemandParameters] => [
          fuelName,
          {
            energyDemand: this.annualEnergy * fraction,
            fuelInput: this.annualEnergy * fraction,
            relativeFraction: fraction,
          },
        ],
      ),
    );
  }

  get totalFuelDemandAnnual(): number {
    const fuelNames = Object.keys(this.input.fuelFractions);
    if (fuelNames.length === 0) {
      return 0;
    } else {
      // assumes:
      // - fuel fractions sum to 1
      // - efficiency is always 1 (which is valid because fans and pumps are always
      //   electrical)
      return this.annualEnergy;
    }
  }

  /* eslint-disable */
  mutateLegacyData(legacyData_: unknown) {
    if (!isIndexable(legacyData_)) return;
    const legacyData = legacyData_ as any;
    for (const legacyFanPumpFuelFraction of legacyData.fans_and_pumps ?? []) {
      const modelFuelDemand = this.fuelDemandByFuelAnnual.get(
        legacyFanPumpFuelFraction.fuel,
      );
      legacyFanPumpFuelFraction.demand = modelFuelDemand?.energyDemand ?? 0;
      legacyFanPumpFuelFraction.fuel_input = modelFuelDemand?.fuelInput ?? 0;
    }
    legacyData.energy_requirements = legacyData.energy_requirements ?? {};
    legacyData.energy_requirements.fans_and_pumps = {
      name: 'Fans and pumps',
      quantity: this.annualEnergy,
      monthly: times(12, () => this.annualEnergy / 12),
    };
    legacyData.fuel_requirements = legacyData.fuel_requirements ?? {};
    legacyData.fuel_requirements.fans_and_pumps =
      legacyData.fuel_requirements.fans_and_pumps ?? {};
    legacyData.fuel_requirements.fans_and_pumps.quantity = this.totalFuelDemandAnnual;
    legacyData.fuel_requirements.fans_and_pumps.list = cloneDeep(
      legacyData.fans_and_pumps,
    );
  }
  /* eslint-enable */
}

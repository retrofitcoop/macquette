import { cacheMonth } from '../../helpers/cache-decorators';
import { isIndexable } from '../../helpers/is-indexable';
import { Month } from '../enums/month';
import { Fabric } from './fabric';
import { FansAndPumpsGains } from './internal-gains/fans-and-pumps';
import { MiscellaneousInternalGains } from './internal-gains/miscellaneous';
import { AppliancesCookingLoadCollections } from './lighting-appliances-cooking/loads/load-collection';
import { AppliancesSAP } from './lighting-appliances-cooking/sap/appliances';
import { CookingSAP } from './lighting-appliances-cooking/sap/cooking';
import { LightingSAP } from './lighting-appliances-cooking/sap/lighting';
import { WaterHeating } from './water-heating';

export type HeatGainDependencies = {
  fansAndPumpsGains: Pick<FansAndPumpsGains, 'heatGain'>;
  miscellaneousInternalGains: Pick<MiscellaneousInternalGains, 'heatGain'>;
  lighting: Pick<LightingSAP, 'heatGain'>;
  appliancesSap: Pick<AppliancesSAP, 'heatGain'>;
  cookingSap: Pick<CookingSAP, 'heatGain'>;
  appliancesCookingLoadCollections: Pick<AppliancesCookingLoadCollections, 'heatGain'>;
  waterHeating: Pick<WaterHeating, 'heatGain'>;
  fabric: Pick<Fabric, 'heatGainSolar'>;
};

export class HeatGain {
  constructor(
    _input: null,
    private dependencies: HeatGainDependencies,
  ) {}

  @cacheMonth
  heatGain(month: Month): number {
    return this.heatGainInternal(month) + this.heatGainSolar(month);
  }

  @cacheMonth
  heatGainInternal(month: Month): number {
    return (
      this.dependencies.fansAndPumpsGains.heatGain(month) +
      this.dependencies.miscellaneousInternalGains.heatGain +
      this.dependencies.lighting.heatGain(month) +
      this.dependencies.appliancesSap.heatGain(month) +
      this.dependencies.cookingSap.heatGain +
      this.dependencies.appliancesCookingLoadCollections.heatGain(month) +
      this.dependencies.waterHeating.heatGain(month)
    );
  }

  @cacheMonth
  heatGainSolar(month: Month) {
    return this.dependencies.fabric.heatGainSolar(month);
  }

  mutateLegacyData(data: unknown): void {
    if (!isIndexable(data)) return;
    data['total_internal_gains'] = Month.all.map((m) => this.heatGainInternal(m));
    data['total_solar_gains'] = Month.all.map((m) => this.heatGainSolar(m));
    data['total_internal_and_solar_gains'] = Month.all.map((m) => this.heatGain(m));
  }
}

import _ from 'lodash';
import { FuelsDict } from '../fuels';

type LegacyFuelFractions = Array<{ fuel: string; fraction: number | null }>;

export function extractFuelFractionsFromLegacy(
  legacyFuelFractions: LegacyFuelFractions | undefined,
): FuelsDict<number> {
  return _.chain(legacyFuelFractions ?? [])
    .groupBy(({ fuel }) => fuel)
    .mapValues((fuels) => fuels.map(({ fraction }) => fraction ?? 0))
    .mapValues((fractions) => _.sum(fractions))
    .value();
}

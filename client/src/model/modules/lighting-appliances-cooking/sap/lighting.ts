import { Scenario } from '../../../../data-schemas/scenario';
import { sum } from '../../../../helpers/array-reducers';
import { cache } from '../../../../helpers/cache-decorators';
import { isTruthy } from '../../../../helpers/is-truthy';
import { Month } from '../../../enums/month';
import type { FuelDemandByFuel, FuelDemandParameters } from '../../fuel-requirements';
import { FuelName, Fuels, FuelsDict } from '../../fuels';
import { extractFuelFractionsFromLegacy } from '../../fuels/extract-fuel-fractions-from-legacy';
import { validateFuelNames } from '../../fuels/validate-fuel-fractions';

export type LightingSAPInput = {
  outlets: {
    total: number;
    lowEnergy: number;
  };
  reducedHeatGains: boolean;
  fuelFractions: FuelsDict<number>;
};

export function extractLightingSAPInputFromLegacy(scenario: Scenario): LightingSAPInput {
  const { LAC } = scenario ?? {};
  // undefined => 1; empty string => 0... obviously
  function interpretLightOutletLegacyValue(val: number | '' | undefined) {
    return val === '' ? 0 : val === undefined ? 1 : val;
  }
  return {
    outlets: {
      total: interpretLightOutletLegacyValue(LAC?.L),
      lowEnergy: interpretLightOutletLegacyValue(LAC?.LLE),
    },
    reducedHeatGains: LAC?.reduced_heat_gains_lighting ?? false,
    fuelFractions: extractFuelFractionsFromLegacy(LAC?.fuels_lighting),
  };
}

export type LightingSAPDependencies = {
  fuels: { names: string[] };
  floors: { totalFloorArea: number };
  occupancy: { occupancy: number };
  fabric: { naturalLight: number };
};

export class LightingSAP {
  constructor(
    private input: LightingSAPInput,
    private dependencies: LightingSAPDependencies,
  ) {
    validateFuelNames(input.fuelFractions, dependencies.fuels, this.constructor.name);
  }

  // aka E_B
  get baselineEnergyAnnual() {
    return (
      59.73 *
      Math.pow(
        this.dependencies.floors.totalFloorArea * this.dependencies.occupancy.occupancy,
        0.4714,
      )
    );
  }

  // aka C_1
  get lowEnergyCorrectionFactor(): number | null {
    const { lowEnergy, total } = this.input.outlets;
    if (total === 0) {
      return null;
    } else {
      if (lowEnergy > total) {
        console.warn(
          'Number of low energy light bulbs was greater than number of total light bulbs',
        );
      }
      return Math.max(0, 1 - 0.5 * (lowEnergy / total));
    }
  }

  // aka C_2
  get daylightingCorrectionFactor(): number {
    const { naturalLight } = this.dependencies.fabric;
    if (naturalLight <= 0.095) {
      return 52.2 * naturalLight * naturalLight - 9.94 * naturalLight + 1.433;
    } else {
      return 0.96;
    }
  }

  // aka E_L (initial value)
  @cache
  get initialEnergyAnnual(): number {
    return (
      this.baselineEnergyAnnual *
      (this.lowEnergyCorrectionFactor ?? 0) *
      this.daylightingCorrectionFactor
    );
  }

  // aka E_L,m
  energyMonthly(month: Month): number {
    const adjustment = 1 + 0.5 * Math.cos((2 * Math.PI * (month.index1 - 0.2)) / 12);
    const weight = month.days / 365;
    return this.initialEnergyAnnual * adjustment * weight;
  }

  // aka E_L (final value)
  get energyAnnual(): number {
    return sum(Month.all.map((m) => this.energyMonthly(m)));
  }

  // aka G_{L,m}
  heatGain(month: Month): number {
    const reducedGainFactor = this.input.reducedHeatGains ? 0.4 : 1.0;
    return (
      (reducedGainFactor * this.energyMonthly(month) * 0.85 * 1000) / (24 * month.days)
    );
  }

  get fuelDemandByFuelAnnual(): FuelDemandByFuel {
    if (this.energyAnnual === 0) return new Map();
    let fuelFractionsEntries = Object.entries(this.input.fuelFractions);
    if (fuelFractionsEntries.length === 0) {
      fuelFractionsEntries = [[Fuels.STANDARD_TARIFF, 1]];
    }
    return new Map(
      fuelFractionsEntries.map(
        ([fuelName, fraction]): [FuelName, FuelDemandParameters] => [
          fuelName,
          {
            energyDemand: this.energyAnnual * fraction,
            fuelInput: this.energyAnnual * fraction,
            relativeFraction: fraction,
          },
        ],
      ),
    );
  }

  get totalFuelRequirement(): number {
    return sum(
      Array.from(this.fuelDemandByFuelAnnual.values()).map(({ fuelInput }) => fuelInput),
    );
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-argument,
       @typescript-eslint/no-unsafe-assignment,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    data.LAC = data.LAC ?? {};
    const { LAC } = data;
    LAC.LLE = this.input.outlets.lowEnergy;
    LAC.L = this.input.outlets.total;
    LAC.reduced_heat_gains_lighting = this.input.reducedHeatGains;
    LAC.EB = this.baselineEnergyAnnual;

    // This if-statement is ugly because it reads from the legacy data
    // object. Fortunately, it does not seem essential for any actual
    // subsequent behaviour, so all the real functionality is still
    // captured in the other methods of this class.

    if (!isTruthy(LAC.fuels_lighting)) {
      LAC.fuels_lighting = [{ fuel: Fuels.STANDARD_TARIFF, fraction: 1 }];
    }

    if (this.input.outlets.total !== 0) {
      LAC.C1 = this.lowEnergyCorrectionFactor;
      LAC.C2 = this.daylightingCorrectionFactor;
      LAC.EL = this.initialEnergyAnnual;
      data.energy_requirements = data.energy_requirements ?? {};
      if (this.energyAnnual > 0) {
        data.energy_requirements.lighting = {
          name: 'Lighting',
          quantity: this.energyAnnual,
          monthly: Month.all.map((m) => this.energyMonthly(m)),
        };
        data.gains_W['Lighting'] = Month.all.map((m) => this.heatGain(m));
        const legacyFuelsLighting = Array.from(this.fuelDemandByFuelAnnual).map(
          ([fuelName, { energyDemand, fuelInput, relativeFraction }]) => ({
            fuel: fuelName,
            demand: energyDemand,
            fuel_input: fuelInput,
            fraction: relativeFraction,
          }),
        );
        LAC.fuels_lighting = legacyFuelsLighting;
        data.fuel_requirements.lighting.list = legacyFuelsLighting;
        data.fuel_requirements.lighting.quantity = this.totalFuelRequirement;
      }
    }
  }
  /* eslint-enable */
}

import { Scenario } from '../../../../data-schemas/scenario';
import { sum } from '../../../../helpers/array-reducers';
import { isTruthy } from '../../../../helpers/is-truthy';
import { Month } from '../../../enums/month';
import type { FuelDemandByFuel, FuelDemandParameters } from '../../fuel-requirements';
import { FuelName, Fuels, FuelsDict } from '../../fuels';
import { extractFuelFractionsFromLegacy } from '../../fuels/extract-fuel-fractions-from-legacy';
import { validateFuelNames } from '../../fuels/validate-fuel-fractions';

export type CookingSAPInput = {
  enabled: boolean;
  energyEfficient: boolean;
  fuelFractions: FuelsDict<number>;
};

export function extractCookingSAPInputFromLegacy(scenario: Scenario): CookingSAPInput {
  const enabled = scenario?.LAC_calculation_type !== 'carboncoop_SAPlighting';
  const { LAC } = scenario ?? {};
  return {
    enabled,
    energyEfficient: LAC?.energy_efficient_cooking ?? false,
    fuelFractions: extractFuelFractionsFromLegacy(LAC?.fuels_cooking),
  };
}

export type CookingSAPDependencies = {
  floors: {
    totalFloorArea: number;
  };
  occupancy: {
    occupancy: number;
  };
  fuels: {
    names: string[];
    standardTariff: { carbonEmissionsFactor: number };
  };
};

export class CookingSAP {
  constructor(
    public input: CookingSAPInput,
    private dependencies: CookingSAPDependencies,
  ) {
    validateFuelNames(input.fuelFractions, dependencies.fuels, this.constructor.name);
  }

  get heatGain(): number {
    if (!this.input.enabled) return 0;
    if (this.input.energyEfficient) {
      return 23 + 5 * this.dependencies.occupancy.occupancy;
    } else {
      return 35 + 7 * this.dependencies.occupancy.occupancy;
    }
  }

  // kgCO_2/m_2/year
  get emissionsAnnualPerArea(): number {
    if (!this.input.enabled) return 0;
    if (this.dependencies.floors.totalFloorArea === 0) return 0;
    return (
      (119 + 24 * this.dependencies.occupancy.occupancy) /
      this.dependencies.floors.totalFloorArea
    );
  }

  // Estimated from emissions
  get energyAnnual(): number {
    if (!this.input.enabled) return 0;
    const out =
      (this.emissionsAnnualPerArea * this.dependencies.floors.totalFloorArea) /
      this.dependencies.fuels.standardTariff.carbonEmissionsFactor;
    if (Number.isNaN(out)) return 0;
    else return out;
  }

  get energyMonthly(): number {
    return this.energyAnnual / Month.all.length;
  }

  get fuelDemandByFuelAnnual(): FuelDemandByFuel {
    if (!this.input.enabled) return new Map();
    let fuelFractionsEntries = Object.entries(this.input.fuelFractions);
    if (fuelFractionsEntries.length === 0) {
      fuelFractionsEntries = [[Fuels.STANDARD_TARIFF, 1]];
    }
    return new Map(
      fuelFractionsEntries.map(
        ([fuelName, fraction]): [FuelName, FuelDemandParameters] => [
          fuelName,
          {
            energyDemand: this.energyAnnual * fraction,
            fuelInput: this.energyAnnual * fraction,
            relativeFraction: fraction,
          },
        ],
      ),
    );
  }

  get totalFuelRequirement(): number {
    return sum(
      Array.from(this.fuelDemandByFuelAnnual.values()).map(({ fuelInput }) => fuelInput),
    );
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-argument,
       @typescript-eslint/no-unsafe-assignment,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    data.LAC = data.LAC ?? {};
    const { LAC } = data;
    LAC.energy_efficient_cooking = this.input.energyEfficient;
    LAC.EC = this.energyAnnual;
    LAC.EC_monthly = this.energyMonthly;

    // This is totally wrong -- GC should be the gain (in Watts) from
    // cooking, but it is actually being set to the annual energy demand
    // (in kWh/year) for cooking. Fortunately, nothing seems to use it.
    LAC.GC = this.energyAnnual;

    if (!isTruthy(data.LAC.fuels_cooking)) {
      data.LAC.fuels_cooking = [{ fuel: Fuels.STANDARD_TARIFF, fraction: 1 }];
    }

    if (this.heatGain > 0) {
      data.gains_W['Cooking'] = new Array(Month.all.length).fill(this.heatGain);
      data.energy_requirements.cooking = {
        name: 'Cooking',
        quantity: this.energyAnnual,
        monthly: this.energyMonthly,
      };
      const fuelsLegacyArray = Array.from(this.fuelDemandByFuelAnnual).map(
        ([fuelName, { energyDemand, fuelInput, relativeFraction }]) => ({
          fuel: fuelName,
          demand: energyDemand,
          fuel_input: fuelInput,
          fraction: relativeFraction,
        }),
      );
      LAC.fuels_cooking = fuelsLegacyArray;
      data.fuel_requirements.cooking.list = fuelsLegacyArray;
      data.fuel_requirements.cooking.quantity = this.totalFuelRequirement;
    }
  }
  /* eslint-enable */
}

import { Scenario } from '../../data-schemas/scenario';
import { Region } from '../enums/region';

export function extractRegionFromLegacy(scenario: Scenario) {
  const { region: name } = scenario ?? {};
  if (name === undefined) {
    return new Region('UK average');
  }
  return new Region(name);
}

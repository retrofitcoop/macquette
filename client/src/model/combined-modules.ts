import type { Input } from './model';
import { AirChangeRate } from './modules/air-change-rate';
import {
  constructModelBehaviourFlags,
  ModelBehaviourFlags,
} from './modules/behaviour-version';
import { Carbon } from './modules/carbon';
import { CurrentEnergy } from './modules/current-energy';
import { Fabric } from './modules/fabric';
import { FansPumpsElectricKeepHot } from './modules/fans-pumps-electric-keep-hot';
import { Floors } from './modules/floors';
import { FuelRequirements } from './modules/fuel-requirements';
import { Fuels } from './modules/fuels';
import { Generation } from './modules/generation';
import { Geography } from './modules/geography';
import { HeatGain } from './modules/heat-gain';
import { HeatLoss } from './modules/heat-loss';
import { HeatingHours } from './modules/heating-hours';
import { HeatingSystems } from './modules/heating-systems';
import { HeatingSystemsFuelRequirements } from './modules/heating-systems/fuel-requirements';
import { FansAndPumpsGains } from './modules/internal-gains/fans-and-pumps';
import { MiscellaneousInternalGains } from './modules/internal-gains/miscellaneous';
import { AppliancesCookingLoadCollections } from './modules/lighting-appliances-cooking/loads/load-collection';
import { AppliancesSAP } from './modules/lighting-appliances-cooking/sap/appliances';
import { CookingSAP } from './modules/lighting-appliances-cooking/sap/cooking';
import { LightingSAP } from './modules/lighting-appliances-cooking/sap/lighting';
import { MeanInternalTemperature } from './modules/mean-internal-temperature';
import { Mode } from './modules/mode';
import { Occupancy } from './modules/occupancy';
import { SolarHotWater } from './modules/solar-hot-water';
import { SpaceHeating } from './modules/space-heating';
import { VentilationInfiltrationCommon } from './modules/ventilation-infiltration/common-input';
import { Infiltration } from './modules/ventilation-infiltration/infiltration';
import { Ventilation } from './modules/ventilation-infiltration/ventilation';
import { WaterCommon } from './modules/water-common';
import { WaterHeating } from './modules/water-heating';

function warnForInputInvariantViolations(input: Input) {
  if (
    input.appliancesCookingLoadCollections.loads.length !== 0 &&
    (input.appliancesSap.enabled || input.cookingSap.enabled)
  ) {
    console.error(
      'Appliances and cooking "load collections" module is enabled, but one of the AppliancesSAP or CookingSAP is also enabled. Appliances and cooking energy may be double-counted.',
    );
  }
}

export class CombinedModules {
  modelBehaviourFlags: ModelBehaviourFlags;
  geography: Geography;
  floors: Floors;
  occupancy: Occupancy;
  fabric: Fabric;
  ventilationInfiltrationCommon: VentilationInfiltrationCommon;
  ventilation: Ventilation;
  infiltration: Infiltration;
  airChangeRate: AirChangeRate;
  heatLoss: HeatLoss;
  waterCommon: WaterCommon;
  solarHotWater: SolarHotWater;
  lighting: LightingSAP;
  appliancesSap: AppliancesSAP;
  cookingSap: CookingSAP;
  appliancesCookingLoadCollections: AppliancesCookingLoadCollections;
  waterHeating: WaterHeating;
  generation: Generation;
  currentEnergy: CurrentEnergy;
  fuels: Fuels;
  miscellaneousInternalGains: MiscellaneousInternalGains;
  fansAndPumpsGains: FansAndPumpsGains;
  meanInternalTemperature: MeanInternalTemperature;
  fansPumpsElectricKeepHot: FansPumpsElectricKeepHot;
  heatGain: HeatGain;
  spaceHeating: SpaceHeating;
  heatingSystemsFuelRequirements: HeatingSystemsFuelRequirements;
  fuelRequirements: FuelRequirements;
  carbon: Carbon;
  heatingHours: HeatingHours;

  constructor(input: Input, mode: Mode) {
    warnForInputInvariantViolations(input);
    const { region } = input;
    this.modelBehaviourFlags = constructModelBehaviourFlags(input.modelBehaviourVersion);
    this.fuels = new Fuels(input.fuels);
    this.geography = new Geography(input.geography);
    this.floors = new Floors(input.floors, {
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.occupancy = new Occupancy(input.occupancy, { floors: this.floors });
    this.fabric = new Fabric(input.fabric, { region, floors: this.floors });
    this.ventilationInfiltrationCommon = new VentilationInfiltrationCommon(
      input.ventilationInfiltrationCommon,
      { region },
    );
    this.ventilation = new Ventilation(input.ventilation, {
      floors: this.floors,
      ventilationInfiltrationCommon: this.ventilationInfiltrationCommon,
    });
    this.infiltration = new Infiltration(input.infiltration, {
      fabric: this.fabric,
      floors: this.floors,
      ventilationInfiltrationCommon: this.ventilationInfiltrationCommon,
    });
    this.airChangeRate = new AirChangeRate(null, {
      ventilation: this.ventilation,
      infiltration: this.infiltration,
    });
    this.heatLoss = new HeatLoss(null, {
      geography: this.geography,
      floors: this.floors,
      fabric: this.fabric,
      ventilation: this.ventilation,
      infiltration: this.infiltration,
    });
    this.waterCommon = new WaterCommon(input.waterCommon, {
      occupancy: this.occupancy,
    });
    this.solarHotWater = new SolarHotWater(input.solarHotWater, {
      region,
      waterCommon: this.waterCommon,
    });
    this.lighting = new LightingSAP(input.lighting, {
      fuels: this.fuels,
      floors: this.floors,
      fabric: this.fabric,
      occupancy: this.occupancy,
    });
    this.appliancesSap = new AppliancesSAP(input.appliancesSap, {
      fuels: this.fuels,
      floors: this.floors,
      occupancy: this.occupancy,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.cookingSap = new CookingSAP(input.cookingSap, {
      fuels: this.fuels,
      floors: this.floors,
      occupancy: this.occupancy,
    });
    this.appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
      input.appliancesCookingLoadCollections,
      { fuels: this.fuels, modelBehaviourFlags: this.modelBehaviourFlags },
    );
    const heatingSystems = new HeatingSystems(input.heatingSystems, {
      floors: this.floors,
      waterCommon: this.waterCommon,
      fuels: this.fuels,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.waterHeating = new WaterHeating(input.waterHeating, {
      heatingSystems,
      waterCommon: this.waterCommon,
      solarHotWater: this.solarHotWater,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.generation = new Generation(input.generation, {
      modelBehaviourFlags: this.modelBehaviourFlags,
      region,
      fuels: this.fuels,
    });
    this.currentEnergy = new CurrentEnergy(input.currentEnergy, {
      fuels: this.fuels,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.miscellaneousInternalGains = new MiscellaneousInternalGains(null, {
      occupancy: this.occupancy,
    });
    this.fansAndPumpsGains = new FansAndPumpsGains(null, {
      ventilation: this.ventilation,
      heatingSystems,
    });
    this.heatGain = new HeatGain(null, {
      fansAndPumpsGains: this.fansAndPumpsGains,
      miscellaneousInternalGains: this.miscellaneousInternalGains,
      lighting: this.lighting,
      appliancesSap: this.appliancesSap,
      cookingSap: this.cookingSap,
      appliancesCookingLoadCollections: this.appliancesCookingLoadCollections,
      waterHeating: this.waterHeating,
      fabric: this.fabric,
    });
    this.heatingHours = new HeatingHours(input.heatingHours, {
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.meanInternalTemperature = new MeanInternalTemperature(
      input.meanInternalTemperature,
      {
        floors: this.floors,
        fabric: this.fabric,
        heatGain: this.heatGain,
        heatLoss: this.heatLoss,
        region,
        heatingSystems,
        heatingHours: this.heatingHours,
        mode,
        modelBehaviourFlags: this.modelBehaviourFlags,
      },
    );
    this.fansPumpsElectricKeepHot = new FansPumpsElectricKeepHot(
      input.fansPumpsElectricKeepHot,
      {
        waterCommon: this.waterCommon,
        solarHotWater: this.solarHotWater,
        ventilation: this.ventilation,
        heatingSystems,
        fuels: this.fuels,
        modelBehaviourFlags: this.modelBehaviourFlags,
      },
    );
    this.spaceHeating = new SpaceHeating(input.spaceHeating, {
      meanInternalTemperature: this.meanInternalTemperature,
      fabric: this.fabric,
      heatGain: this.heatGain,
      heatLoss: this.heatLoss,
      region,
      floors: this.floors,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.heatingSystemsFuelRequirements = new HeatingSystemsFuelRequirements(null, {
      heatingSystems,
      waterHeating: this.waterHeating,
      spaceHeating: this.spaceHeating,
      fuels: this.fuels,
      modelBehaviourFlags: this.modelBehaviourFlags,
    });
    this.fuelRequirements = new FuelRequirements(null, {
      fuels: this.fuels,
      fansPumpsElectricKeepHot: this.fansPumpsElectricKeepHot,
      heatingSystemsFuelRequirements: this.heatingSystemsFuelRequirements,
      appliancesCookingLoadCollections: this.appliancesCookingLoadCollections,
      appliancesSap: this.appliancesSap,
      cookingSap: this.cookingSap,
      lighting: this.lighting,
      generation: this.generation,
      floors: this.floors,
      occupancy: this.occupancy,
      modelBehaviourFlags: this.modelBehaviourFlags,
      mode,
    });
    this.carbon = new Carbon(null, {
      fuels: this.fuels,
      fuelRequirements: this.fuelRequirements,
    });
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-argument,
    */
  mutateLegacyData(data: any) {
    const mutatorModules = [
      this.floors,
      this.occupancy,
      this.fabric,
      this.ventilationInfiltrationCommon,
      this.ventilation,
      this.infiltration,
      this.airChangeRate,
      this.heatLoss,
      this.waterCommon,
      this.solarHotWater,
      this.lighting,
      this.appliancesSap,
      this.cookingSap,
      this.appliancesCookingLoadCollections,
      this.waterHeating,
      this.generation,
      this.currentEnergy,
      this.miscellaneousInternalGains,
      this.fansAndPumpsGains,
      this.meanInternalTemperature,
      this.fansPumpsElectricKeepHot,
      this.heatGain,
      this.spaceHeating,
      this.heatingSystemsFuelRequirements,
      this.fuelRequirements,
    ];
    for (const mod of Object.values(mutatorModules)) {
      mod.mutateLegacyData(data);
    }
  }
  /* eslint-enable */
}

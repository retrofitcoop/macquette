import { Scenario } from '../data-schemas/scenario';
import { Result } from '../helpers/result';
import { ModelError } from './error';
import type { Input } from './model';
import { extractCurrentEnergyInputFromLegacy } from './modules/current-energy';
import { extractFabricInputFromLegacy } from './modules/fabric';
import { extractFansPumpsElectricKeepHotInputFromLegacy } from './modules/fans-pumps-electric-keep-hot';
import { extractFloorsInputFromLegacy } from './modules/floors';
import { extractFuelsInputFromLegacy } from './modules/fuels';
import { extractGenerationInputFromLegacy } from './modules/generation';
import { extractGeographyInputFromLegacy } from './modules/geography';
import { extractHeatingHoursInputFromLegacy } from './modules/heating-hours';
import { extractHeatingSystemsInputFromLegacy } from './modules/heating-systems';
import { extractAppliancesCookingLoadCollectionsInputFromLegacy } from './modules/lighting-appliances-cooking/loads/load-collection';
import { extractAppliancesSAPInputFromLegacy } from './modules/lighting-appliances-cooking/sap/appliances';
import { extractCookingSAPInputFromLegacy } from './modules/lighting-appliances-cooking/sap/cooking';
import { extractLightingSAPInputFromLegacy } from './modules/lighting-appliances-cooking/sap/lighting';
import { extractMeanInternalTemperatureInputFromLegacy } from './modules/mean-internal-temperature/extract-from-legacy';
import { extractOccupancyInputFromLegacy } from './modules/occupancy';
import { extractRegionFromLegacy } from './modules/region';
import { extractSolarHotWaterInputFromScenario } from './modules/solar-hot-water';
import { extractSpaceHeatingInputFromLegacy } from './modules/space-heating';
import { extractVentilationInfiltrationCommonInputFromLegacy } from './modules/ventilation-infiltration/common-input';
import { extractInfiltrationInputFromLegacy } from './modules/ventilation-infiltration/infiltration';
import { extractVentilationInputFromLegacy } from './modules/ventilation-infiltration/ventilation';
import { extractWaterCommonInputFromLegacy } from './modules/water-common';
import { extractWaterHeatingInputFromLegacy } from './modules/water-heating';

export function extractInputFromLegacy(scenario: Scenario): Result<Input, ModelError> {
  try {
    return Result.ok({
      modelBehaviourVersion: scenario?.modelBehaviourVersion ?? 'legacy',
      fuels: extractFuelsInputFromLegacy(scenario),
      geography: extractGeographyInputFromLegacy(scenario),
      floors: extractFloorsInputFromLegacy(scenario),
      occupancy: extractOccupancyInputFromLegacy(scenario),
      region: extractRegionFromLegacy(scenario),
      fabric: extractFabricInputFromLegacy(scenario),
      ventilationInfiltrationCommon:
        extractVentilationInfiltrationCommonInputFromLegacy(scenario),
      ventilation: extractVentilationInputFromLegacy(scenario),
      infiltration: extractInfiltrationInputFromLegacy(scenario),
      waterCommon: extractWaterCommonInputFromLegacy(scenario),
      solarHotWater: extractSolarHotWaterInputFromScenario(scenario),
      lighting: extractLightingSAPInputFromLegacy(scenario),
      appliancesSap: extractAppliancesSAPInputFromLegacy(scenario),
      cookingSap: extractCookingSAPInputFromLegacy(scenario),
      appliancesCookingLoadCollections:
        extractAppliancesCookingLoadCollectionsInputFromLegacy(scenario),
      waterHeating: extractWaterHeatingInputFromLegacy(scenario),
      generation: extractGenerationInputFromLegacy(scenario),
      currentEnergy: extractCurrentEnergyInputFromLegacy(scenario),
      heatingSystems: extractHeatingSystemsInputFromLegacy(scenario),
      meanInternalTemperature: extractMeanInternalTemperatureInputFromLegacy(scenario),
      fansPumpsElectricKeepHot: extractFansPumpsElectricKeepHotInputFromLegacy(scenario),
      spaceHeating: extractSpaceHeatingInputFromLegacy(scenario),
      heatingHours: extractHeatingHoursInputFromLegacy(scenario),
    });
  } catch (err) {
    if (err instanceof ModelError) {
      return Result.err(err);
    }
    throw err;
  }
}

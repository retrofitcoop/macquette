import { clamp } from 'lodash';
import { ModelError } from '../error';

/*
 * FES 2024 data workbook
 * Sheet ES1
 * Pathway = Counterfactual
 * Variable = "CO2 Intensity of Generation excluding BECCS (gCO2/kWh)"
 *
 * https://www.neso.energy/publications/future-energy-scenarios-fes/fes-documents
 */

const electricityCarbonIntensity = [
  { year: 2023, intensity: 133.35 },
  { year: 2024, intensity: 171.49 },
  { year: 2025, intensity: 165.3 },
  { year: 2026, intensity: 164.02 },
  { year: 2027, intensity: 167.43 },
  { year: 2028, intensity: 159.08 },
  { year: 2029, intensity: 137.68 },
  { year: 2030, intensity: 134.35 },
  { year: 2031, intensity: 117.94 },
  { year: 2032, intensity: 95.69 },
  { year: 2033, intensity: 93.32 },
  { year: 2034, intensity: 84.6 },
  { year: 2035, intensity: 68.73 },
  { year: 2036, intensity: 62.11 },
  { year: 2037, intensity: 65.63 },
  { year: 2038, intensity: 62.83 },
  { year: 2039, intensity: 58.52 },
  { year: 2040, intensity: 55.42 },
  { year: 2041, intensity: 54.63 },
  { year: 2042, intensity: 53.79 },
  { year: 2043, intensity: 49.87 },
  { year: 2044, intensity: 48.99 },
  { year: 2045, intensity: 47.8 },
  { year: 2046, intensity: 44.68 },
  { year: 2047, intensity: 42.55 },
  { year: 2048, intensity: 41.9 },
  { year: 2049, intensity: 37.74 },
  { year: 2050, intensity: 38.24 },
];
const minYear = Math.min(...electricityCarbonIntensity.map(({ year }) => year));
const maxYear = Math.max(...electricityCarbonIntensity.map(({ year }) => year));

/**
 * FES-modelled electricity carbon intensity by year in gCO₂e/kWh.
 */
export function electricityCarbonIntensityByYear(year: number): number {
  year = clamp(year, minYear, maxYear);
  const result = electricityCarbonIntensity.find((row) => row.year === year);
  if (result === undefined) {
    throw new ModelError('invalid year', { year });
  } else {
    return result.intensity;
  }
}

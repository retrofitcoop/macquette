import { HTTPClient } from '../api/http';
import { SaveManager } from '../api/save-manager';
import { StaticFileResolver } from '../api/static-file-resolver';
import { emulateJsonRoundTrip } from '../helpers/emulate-json-round-trip';
import { calcRun as calcRunAny } from '../model/calc-run';
import { datasets } from '../model/datasets/legacy';
import { Month } from '../model/enums/month';
import {
  selectHeatingControlMeasure,
  selectHeatingSystemMeasure,
} from '../ui/input-components/library-picker/legacy';

import { SingleModuleShim } from '../ui/module-management/shim';
import { comparisonModule } from '../ui/modules/comparison';
import { heatingModuleBottom } from '../ui/modules/heating/bottom';
import { heatingModuleTop } from '../ui/modules/heating/top';
import { heatingTimesModule } from '../ui/modules/questionnaire/heating-times-module';
import { PageManager } from '../ui/page-manager';
import { questionnaireAnswerChoices } from '../ui/questionnaire-answer-choices';

declare global {
  interface Window {
    Macquette?: Record<string, unknown>;
  }
}

const calcRun: unknown = calcRunAny;

window.Macquette = {
  datasets,
  calcRun,
  uiModuleShims: {
    heatingTimes: new SingleModuleShim(heatingTimesModule),
    heatingTop: new SingleModuleShim(heatingModuleTop),
    heatingBottom: new SingleModuleShim(heatingModuleBottom),
    comparison: new SingleModuleShim(comparisonModule),
  },
  HTTPClient,
  StaticFileResolver,
  emulateJsonRoundTrip,
  PageManager,
  SaveManager,
  questionnaire: questionnaireAnswerChoices,
  selectHeatingSystemMeasure,
  selectHeatingControlMeasure,
  Month,
};

import { HTTPClient } from '../api/http';
import { isIndexable } from '../helpers/is-indexable';
import { DEFAULT_ROUTE, Route, parseRoute } from '../ui/routes';

/* eslint-disable
    @typescript-eslint/consistent-type-assertions,
*/
export function externals() {
  if (!isIndexable(window)) {
    throw new Error('not running in browser');
  }
  const update: unknown = window['update'];
  if (typeof update !== 'function') {
    throw new Error('window.update was not a function');
  }
  const project: unknown = window['p'];
  if (!isIndexable(project)) {
    throw new Error('window.p is not a Record');
  }
  const appName: unknown = window['appName'];
  if (typeof appName !== 'string') {
    throw new Error('window.appName is not a string');
  }
  const userId: unknown = window['userId'];
  if (typeof userId !== 'string') {
    throw new Error('window.userId is not a string');
  }
  return {
    project,
    update,
    appName,
    userId,

    // SAFETY: window.libraries is set in the legacy library helper from
    // this API function.
    libraries: window['libraries'] as
      | (ReturnType<HTTPClient['listLibraries']> extends Promise<infer T> ? T : unknown)
      | undefined,
  };
}

export type Externals = ReturnType<typeof externals>;

export function getCurrentRoute(): Route {
  return (
    parseRoute(window.location.hash)
      .mapErr((e) => {
        console.warn(e);
        return DEFAULT_ROUTE;
      })
      .coalesce() ?? DEFAULT_ROUTE
  );
}

import type { Result } from '../../helpers/result';
import type { Externals } from '../../shims/typed-globals';
import { AppContext } from './app-context';

export type Dispatcher<Action> = (action: Action) => void;

export type BasicProps<State, Action> = {
  state: State;
  dispatch: Dispatcher<Action>;
};

export type ReducerComponent<
  State,
  Action,
  Effect,
  Props extends BasicProps<State, Action> = BasicProps<State, Action>,
> = {
  name: string;
  initialState: (instanceKey: string | null) => State;
  reducer: (state: State, action: Action) => [State, Array<Effect>?];
  component: React.FC<Props>;
  effector: (effect: Effect, dispatch: Dispatcher<Action>) => Promise<void>;
};

export type UiModule<State, Action, Effect> = ReducerComponent<
  State,
  Action,
  Effect,
  BasicProps<State, Action>
> & {
  shims: {
    extractUpdateAction: (
      context: AppContext,
      instanceKey: string | null,
      changed: { inputs: boolean; outputs: boolean },
    ) => Result<Action[], Error>;
    mutateLegacyData: (
      externals: Pick<Externals, 'project'>,
      context: Pick<AppContext, 'scenarioId'>,
      state: State,
      instanceKey: string | null,
    ) => void;
  };
};

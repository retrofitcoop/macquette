const standalonePageNames = [
  'questionnaire',
  'householdquestionnaire',
  'commentary',
  'currentenergy',
  'imagegallery',
  'compare',
  'report',
  'scopeofworks',
  'export',
  'fuelsmanager',
  'sandbox',
  'address-search',
  'project',
] as const;

const scenarioPageNames = [
  'dimensions-and-occupancy',
  'ventilation',
  'elements',
  'LAC',
  'heating',
  'energy-use',
  'generation',
  'solarhotwater',
  'worksheets',
  'sap-worksheet',
] as const;

export type StandalonePageName = (typeof standalonePageNames)[number];
export type ScenarioPageName = (typeof scenarioPageNames)[number];

export function isStandalonePage(x: string): x is StandalonePageName {
  const names: readonly string[] = standalonePageNames;
  return names.includes(x);
}

export function isScenarioPage(x: string): x is ScenarioPageName {
  const names: readonly string[] = scenarioPageNames;
  return names.includes(x);
}

export const pageTitles: Record<StandalonePageName | ScenarioPageName, string> = {
  'address-search': 'Address Search',
  'dimensions-and-occupancy': 'Dimensions & Occupancy',
  'energy-use': 'Energy Use',
  commentary: 'Commentary',
  compare: 'Compare Scenarios',
  currentenergy: 'Current Energy Use',
  elements: 'Fabric',
  export: 'Import/Export',
  fuelsmanager: 'Fuels Manager',
  generation: 'Generation',
  heating: 'Heating',
  householdquestionnaire: 'Household Questionnaire',
  imagegallery: 'Image Gallery',
  LAC: 'Lighting, Appliances & Cooking',
  project: 'Project',
  questionnaire: 'Questionnaire',
  report: 'Generate Report',
  sandbox: 'Sandbox',
  scopeofworks: 'Scope of Works',
  solarhotwater: 'Solar Hot Water Heating',
  ventilation: 'Ventilation & Infiltration',
  worksheets: 'SAP worksheets',
  'sap-worksheet': 'SAP Worksheet (New)',
};

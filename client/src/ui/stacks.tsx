import React, { ReactNode } from 'react';

type Base = { children: ReactNode };
type VStackTypes =
  | Base
  | (Base & { xsmall: boolean })
  | (Base & { small: boolean })
  | (Base & { mid: boolean })
  | (Base & { large: boolean });

export function VStack({ children, ...rest }: VStackTypes) {
  let flexGap: string;

  if ('xsmall' in rest) flexGap = 'gap-3';
  else if ('small' in rest) flexGap = 'gap-7';
  else if ('mid' in rest) flexGap = 'gap-15';
  else flexGap = 'gap-30';

  return <section className={`d-flex flex-v ${flexGap}`}>{children}</section>;
}

export function HStack({ children, ...rest }: VStackTypes) {
  let flexGap: string;

  if ('xsmall' in rest) flexGap = 'gap-3';
  else if ('mid' in rest) flexGap = 'gap-15';
  else if ('large' in rest) flexGap = 'gap-30';
  else flexGap = 'gap-7';

  return <section className={`d-flex ${flexGap}`}>{children}</section>;
}

import React from 'react';
import { z } from 'zod';

import { resultSchema } from '../../data-schemas/helpers/result';
import { Scenario } from '../../data-schemas/scenario';
import { getScenarioMeasures, totalCostOfMeasures } from '../../measures';
import * as targets from '../../model/datasets/targets';
import { Model } from '../../model/model';
import { House } from './house';
import { NumberOutput } from './numeric';
import { TargetBar } from './target-bar';

export type GraphicsInput = {
  measuresCost: number | null;
  targetBarData: {
    spaceHeatingDemand: [number | null, number | null];
    peakHeatLoad: number | null;
    energyUseIntensity: [number | null, number | null];
  };
  houseData: {
    floor: number | null;
    ventilation: number | null;
    infiltration: number | null;
    windows: number | null;
    walls: number | null;
    roof: number | null;
    thermalbridge: number | null;
  };
};

export function getGraphicsInput(scenarioId: string, scenario: Scenario): GraphicsInput {
  let model: Model | null = null;

  if (scenario?.model !== undefined) {
    const modelR = resultSchema(z.instanceof(Model), z.unknown()).safeParse(
      scenario.model,
    );
    if (modelR.success === true) {
      model = modelR.data.mapErr(() => null).coalesce();
    }
  }

  return {
    measuresCost:
      scenarioId !== 'master' ? totalCostOfMeasures(getScenarioMeasures(scenario)) : null,
    targetBarData: {
      spaceHeatingDemand: [
        scenario?.space_heating_demand_m2 ?? null,
        scenario?.standardisedSHD ?? null,
      ],
      peakHeatLoad: (model?.normal.heatLoss.peakHeatLoad ?? 0) / 1000,
      energyUseIntensity: [
        model?.normal.fuelRequirements.totalEnergyAnnualPerArea ?? null,
        model?.standardisedHeating.fuelRequirements.totalEnergyAnnualPerArea ?? null,
      ],
    },
    houseData: {
      floor: model?.normal.fabric.heatLossTotals.floor ?? null,
      windows: model?.normal.fabric.heatLossTotals.windowLike ?? null,
      walls:
        model === null
          ? null
          : model.normal.fabric.heatLossTotals.externalWall +
            model.normal.fabric.heatLossTotals.partyWall,
      roof: model?.normal.fabric.heatLossTotals.roof ?? null,
      ventilation: model?.normal.ventilation.heatLossAverage ?? null,
      infiltration: model?.normal.infiltration.heatLossAverage ?? null,
      thermalbridge: model?.normal.fabric.thermalBridgingHeatLoss ?? null,
    },
  };
}

export function Graphics({ input }: { input: GraphicsInput }) {
  return (
    <div className="d-flex align-items-center justify-content-between pb-30">
      <div style={{ width: '50%' }}>
        <House {...input.houseData} />
        {input.measuresCost !== null && (
          <div>
            Measures cost: £
            <NumberOutput value={input.measuresCost} dp={0} />
          </div>
        )}
      </div>

      <div className="d-flex flex-v gap-15">
        <TargetBar
          name="Space heating demand"
          width={425}
          value={input.targetBarData.spaceHeatingDemand}
          units="kWh/m²"
          targets={targets.spaceHeatingDemand}
        />

        <TargetBar
          name="Energy use intensity"
          width={425}
          value={input.targetBarData.energyUseIntensity}
          units="kWh/m²"
          targets={targets.energyUseIntensity}
        />

        <TargetBar
          name="Peak heat load"
          width={425}
          value={[input.targetBarData.peakHeatLoad]}
          dp={1}
          units="kW"
          targets={targets.peakHeatLoad}
        />
      </div>
    </div>
  );
}

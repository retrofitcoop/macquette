import React from 'react';

export function Spinner({
  className = '',
  visibility = true,
}: {
  className?: string;
  visibility?: boolean;
}) {
  return (
    <div
      className={`${className} spinner`}
      style={{ visibility: visibility ? 'visible' : 'hidden' }}
      {...(visibility
        ? {
            role: 'alert',
            'aria-live': 'polite',
            'aria-relevant': 'additions removals',
            'aria-valuetext': 'loading',
          }
        : {})}
    />
  );
}

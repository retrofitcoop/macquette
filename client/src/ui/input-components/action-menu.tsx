import React from 'react';
import type { MenuItemProps, MenuProps, PopoverProps } from 'react-aria-components';
import {
  Button as AdobeButton,
  Menu as AdobeMenu,
  MenuItem as AdobeMenuItem,
  Popover as AdobePopover,
} from 'react-aria-components';
import { DownCaretSmall } from '../icons';
export { MenuTrigger } from 'react-aria-components';

export function Popover(props: PopoverProps) {
  return <AdobePopover offset={0} {...props} />;
}

export function Menu<T extends object>(props: MenuProps<T>) {
  return <AdobeMenu<T> className="dropdown-menu" {...props} />;
}

export function MenuItem(props: MenuItemProps) {
  // We supply key=id to avoid this issue:
  // https://github.com/adobe/react-spectrum/issues/5176
  return <AdobeMenuItem className="dropdown-menu-item" key={props.id} {...props} />;
}

export function ActionsButton({ isDisabled }: { isDisabled?: boolean }) {
  return (
    <AdobeButton
      className="btn d-flex align-items-center gap-7"
      isDisabled={isDisabled ?? false}
    >
      <span>Actions</span>
      <DownCaretSmall />
    </AdobeButton>
  );
}

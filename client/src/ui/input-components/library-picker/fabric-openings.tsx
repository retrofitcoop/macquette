import { pickBy } from 'lodash';
import React from 'react';

import type {
  Opening,
  OpeningMeasure,
} from '../../../data-schemas/libraries/fabric-openings';
import {
  isFabricOpeningMeasuresLibrary,
  isFabricOpeningsLibrary,
} from '../../../data-schemas/libraries/fabric-openings';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { calcMeasureQtyAndCost } from '../../../measures';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectOpeningParams = {
  onSelect: (item: Opening) => void;
  onClose: () => void;
  type: Opening['type'] | null;
  currentItemTag?: string | null;
};

export function SelectOpening({
  onSelect,
  onClose,
  type = null,
  currentItemTag = null,
}: SelectOpeningParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const libraryList = libraries.data.filter(isFabricOpeningsLibrary).map((library) => ({
    ...library,
    data: pickBy(library.data, (item) => type === null || item.type === type),
  }));
  if (!isNonEmpty(libraryList)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No opening libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Opening"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={libraryList}
      searchText={(opening) => opening.name}
      tableColumns={[
        { title: 'U', type: 'number', value: (opening) => opening.uValue.toString() },
        { title: 'k', type: 'number', value: (opening) => opening.kValue.toString() },
      ]}
      getFullItemData={(opening: Opening) =>
        opening.description === ''
          ? []
          : [
              {
                title: 'Description',
                value: nl2br(opening.description),
              },
            ]
      }
    />
  );
}

type SelectOpeningMeasureParams = {
  onSelect: (item: OpeningMeasure) => void;
  onClose: () => void;
  type: Opening['type'] | null;
  showCost: boolean;
  areaSqm?: number | null;
  currentItemTag: string | null;
};

export function SelectOpeningMeasure({
  onSelect,
  onClose,
  showCost,
  type,
  areaSqm = null,
  currentItemTag,
}: SelectOpeningMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data
    .filter(isFabricOpeningMeasuresLibrary)
    .map((library) => ({
      ...library,
      data: pickBy(library.data, (item) => type === null || item.type === type),
    }));
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No opening measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Opening measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(opening) => opening.name}
      tableColumns={[
        { title: 'U', type: 'number', value: (opening) => opening.uValue.toString() },
        ...(showCost
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (opening: OpeningMeasure) =>
                  opening.cost_units === 'sqm' && areaSqm !== null ? (
                    <NumberOutput
                      dp={0}
                      value={
                        calcMeasureQtyAndCost({
                          area: areaSqm ?? 0,
                          costUnits: opening.cost_units,
                          costPerUnit: opening.cost,
                          baseCost: opening.min_cost,
                          isExternalWallInsulation: false,
                        })[1]
                      }
                    />
                  ) : (
                    '?'
                  ),
              },
            ]
          : []),
      ]}
      getFullItemData={(opening: OpeningMeasure) => [
        ...(opening.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(opening.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${opening.min_cost} + £${opening.cost} per ${opening.cost_units}`,
        },
        { title: 'Associated work', value: opening.associated_work },
        { title: 'Key risks', value: opening.key_risks },
        { title: 'Notes', value: nl2br(opening.notes) },
      ]}
    />
  );
}

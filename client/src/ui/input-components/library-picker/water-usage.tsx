import React from 'react';

import {
  WaterUsageItem,
  WaterUsageMeasure,
  isWaterUsageMeasuresLibrary,
} from '../../../data-schemas/libraries/water-usage';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectWaterUsageParams = {
  onSelect: (item: WaterUsageItem) => void;
  onClose: () => void;
};

export function SelectWaterUsageItem({ onSelect, onClose }: SelectWaterUsageParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isWaterUsageMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No water usage libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="water usage"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[]}
      getFullItemData={() => []}
    />
  );
}

type SelectWaterUsageMeasureParams = {
  onSelect: (item: WaterUsageMeasure) => void;
  onClose: () => void;
};

export function SelectWaterUsageMeasure({
  onSelect,
  onClose,
}: SelectWaterUsageMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isWaterUsageMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No water usage libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="water usage measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: '£',
          type: 'number' as const,
          value: (system: WaterUsageMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: WaterUsageMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

import React from 'react';

import {
  HeatingSystem,
  HeatingSystemMeasure,
  isHeatingSystemMeasuresLibrary,
  isHeatingSystemsLibrary,
} from '../../../data-schemas/libraries/heating-systems';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { calcMeasureQtyAndCost } from '../../../measures';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectHeatingSystemParams = {
  onSelect: (item: HeatingSystem) => void;
  onClose: () => void;
  currentItemTag?: string | null;
};

export function SelectHeatingSystem({
  onSelect,
  onClose,
  currentItemTag = null,
}: SelectHeatingSystemParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const libraryList = libraries.data.filter(isHeatingSystemsLibrary);
  if (!isNonEmpty(libraryList)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No heating system libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Heating system"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={libraryList}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'Category',
          type: 'text',
          value: (system) => system.category.toString(),
        },
      ]}
      getFullItemData={() => []}
    />
  );
}

type SelectHeatingSystemMeasureParams = {
  onSelect: (item: HeatingSystemMeasure) => void;
  onClose: () => void;
  showCost: boolean;
  areaSqm?: number | null;
  currentItemTag: string | null;
};

export function SelectHeatingSystemMeasure({
  onSelect,
  onClose,
  showCost,
  areaSqm = null,
  currentItemTag,
}: SelectHeatingSystemMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isHeatingSystemMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No heating system measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Heating system measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        ...(showCost
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (system: HeatingSystemMeasure) =>
                  system.cost_units === 'sqm' && areaSqm !== null ? (
                    <NumberOutput
                      dp={0}
                      value={
                        calcMeasureQtyAndCost({
                          area: areaSqm ?? 0,
                          costUnits: system.cost_units,
                          costPerUnit: system.cost,
                          baseCost: system.min_cost,
                          isExternalWallInsulation: false,
                        })[1]
                      }
                    />
                  ) : (
                    '?'
                  ),
              },
            ]
          : []),
      ]}
      getFullItemData={(system: HeatingSystemMeasure) => [
        ...(system.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(system.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${system.min_cost} + £${system.cost} per ${system.cost_units}`,
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

import React from 'react';

import {
  VentOrFlue,
  VentOrFlueMeasure,
  isVentOrFlueLibrary,
  isVentOrFlueMeasuresLibrary,
} from '../../../data-schemas/libraries/intentional-vents-and-flues';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectVentOrFlueParams = {
  onSelect: (item: VentOrFlue) => void;
  onClose: () => void;
};

export function SelectVentOrFlue({ onSelect, onClose }: SelectVentOrFlueParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isVentOrFlueLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No vents and flues libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="vent or flue"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'Ventilation rate',
          type: 'number' as const,
          value: (system: VentOrFlue) => (
            <NumberOutput
              value={coalesceEmptyString(system.ventilation_rate, null)}
              dp={0}
              unit="m³/h"
            />
          ),
        },
      ]}
      getFullItemData={() => []}
    />
  );
}

type SelectVentOrFlueMeasureParams = {
  onSelect: (item: VentOrFlueMeasure) => void;
  onClose: () => void;
};

export function SelectVentOrFlueMeasure({
  onSelect,
  onClose,
}: SelectVentOrFlueMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isVentOrFlueMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No vents and flues libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="vent or flue"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'Ventilation rate',
          type: 'number' as const,
          value: (system: VentOrFlueMeasure) => (
            <NumberOutput
              value={coalesceEmptyString(system.ventilation_rate, null)}
              dp={0}
              unit="m³/h"
            />
          ),
        },
        {
          title: '£',
          type: 'number' as const,
          value: (system: VentOrFlueMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: VentOrFlueMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

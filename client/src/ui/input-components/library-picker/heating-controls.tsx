import React from 'react';

import {
  HeatingControlMeasure,
  isHeatingControlMeasureLibrary,
} from '../../../data-schemas/libraries/space-heating-control-type';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectHeatingControlMeasureParams = {
  onSelect: (item: HeatingControlMeasure) => void;
  onClose: () => void;
  currentItemTag: string | null;
};

export function SelectHeatingControlMeasure({
  onSelect,
  onClose,
  currentItemTag,
}: SelectHeatingControlMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isHeatingControlMeasureLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No heating control measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Heating control measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: '£',
          type: 'number' as const,
          value: (system: HeatingControlMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: HeatingControlMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

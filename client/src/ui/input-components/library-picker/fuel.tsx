import React from 'react';

import { SelectLibraryItem } from './generic';

export type Fuel = {
  tag: string;
  name: string;
  category: string;
};

type SelectFuelProps = {
  fuels: Record<string, Fuel>;
  onSelect: (item: Fuel) => void;
  onClose: () => void;
};

export function SelectFuel({ fuels, onSelect, onClose }: SelectFuelProps) {
  const fuelList = Object.entries(fuels);
  fuelList.sort(([, a], [, b]) => {
    if (a.category === b.category) {
      return 0;
    } else {
      if (b.category < a.category) {
        return 1;
      } else if (b.category === a.category) {
        return 0;
      } else {
        return -1;
      }
    }
  });

  const builtinFuelLibrary = {
    id: 'builtin-dataset-fuels',
    name: 'Fuel list',
    type: 'fuels',
    data: Object.fromEntries(fuelList),
  };

  return (
    <SelectLibraryItem
      title="fuel"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={[builtinFuelLibrary]}
      searchText={(fuel) => `${fuel.name} ${fuel.category}`}
      tableColumns={[{ title: 'Category', type: 'text', value: (fuel) => fuel.category }]}
      hideTags
      getFullItemData={() => []}
    />
  );
}

import React from 'react';

import type { Floor, FloorMeasure } from '../../../data-schemas/libraries/fabric-floors';
import {
  isFabricFloorMeasuresLibrary,
  isFabricFloorsLibrary,
} from '../../../data-schemas/libraries/fabric-floors';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { calcMeasureQtyAndCost } from '../../../measures';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectFloorParams = {
  onSelect: (item: Floor) => void;
  onClose: () => void;
  currentItemTag?: string | null;
};

export function SelectFloor({
  onSelect,
  onClose,
  currentItemTag = null,
}: SelectFloorParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const libraryList = libraries.data.filter(isFabricFloorsLibrary);
  if (!isNonEmpty(libraryList)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No floor libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Floor"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={libraryList}
      searchText={(floor) => floor.name}
      tableColumns={[
        {
          title: 'U',
          type: 'number',
          value: (floor) => (floor.uValue ?? '-').toString(),
        },
        { title: 'k', type: 'number', value: (floor) => floor.kValue.toString() },
      ]}
      getFullItemData={(floor: Floor) =>
        floor.description === ''
          ? []
          : [
              {
                title: 'Description',
                value: nl2br(floor.description),
              },
            ]
      }
    />
  );
}

type SelectFloorMeasureParams = {
  onSelect: (item: FloorMeasure) => void;
  onClose: () => void;
  showCost: boolean;
  areaSqm?: number | null;
  perimeter?: number | null;
  currentItemTag: string | null;
};

export function SelectFloorMeasure({
  onSelect,
  onClose,
  showCost,
  areaSqm = null,
  perimeter = null,
  currentItemTag,
}: SelectFloorMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isFabricFloorMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No floor measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Floor measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(floor) => floor.name}
      tableColumns={[
        {
          title: 'U',
          type: 'number',
          value: (floor) => (floor.uValue ?? '-').toString(),
        },
        ...(showCost
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (floor: FloorMeasure) =>
                  (floor.cost_units === 'sqm' && areaSqm !== null) ||
                  (floor.cost_units === 'ln m' && perimeter !== null) ? (
                    <NumberOutput
                      dp={0}
                      value={
                        calcMeasureQtyAndCost({
                          area: areaSqm ?? 0,
                          perimeter: perimeter ?? 0,
                          costUnits: floor.cost_units,
                          costPerUnit: floor.cost,
                          baseCost: floor.min_cost,
                          isExternalWallInsulation: false,
                        })[1]
                      }
                    />
                  ) : (
                    '?'
                  ),
              },
            ]
          : []),
      ]}
      getFullItemData={(floor: FloorMeasure) => [
        ...(floor.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(floor.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${floor.min_cost} + £${floor.cost} per ${floor.cost_units}`,
        },
        { title: 'Associated work', value: floor.associated_work },
        { title: 'Key risks', value: floor.key_risks },
        { title: 'Notes', value: nl2br(floor.notes) },
      ]}
    />
  );
}

import { pickBy } from 'lodash';
import type { ReactElement } from 'react';
import React, { ReactNode, useEffect, useReducer, useState } from 'react';

import { Library } from '../../../data-schemas/libraries';
import { NonEmptyArray } from '../../../helpers/non-empty-array';
import { isNotNull } from '../../../helpers/null-checking';
import { externals } from '../../../shims/typed-globals';
import { Select } from '../../input-components/select';
import { Modal, ModalBody, ModalHeader } from '../../output-components/modal';

type LibraryOf<T> = {
  id: string;
  name: string;
  type: string;
  data: Record<string, T>;
};

type MinimalLibraryItem = {
  tag: string;
  name: string;
};

type SelectLibraryItemProps<T> = {
  title: string;
  libraries: NonEmptyArray<LibraryOf<T>>;
  onSelect: (item: T) => void;
  onClose: () => void;
  hideTags?: boolean;
  tableColumns: {
    title: string;
    type: 'text' | 'number';
    value: (item: T) => ReactNode;
  }[];
  getFullItemData: (item: T) => {
    title: string;
    value: ReactNode | ReactNode[];
  }[];
  searchText: (item: T) => string;
  currentItemTag: string | null;
};

type LibraryItemProps<T> = Omit<
  SelectLibraryItemProps<T>,
  'searchText' | 'libraries' | 'title' | 'onClose'
> & {
  libraryItem: T;
};

function LibraryItem<T extends MinimalLibraryItem>({
  onSelect,
  hideTags = false,
  tableColumns,
  getFullItemData,
  currentItemTag,
  libraryItem,
}: LibraryItemProps<T>): JSX.Element {
  const fullItemData = getFullItemData(libraryItem);
  const canBeRevealed = fullItemData.length > 0;
  const [isRevealed, setRevealed] = useState(false);
  function toggleRevealed() {
    return setRevealed(!isRevealed && canBeRevealed);
  }

  const classNames = [
    currentItemTag === libraryItem.tag ? 'bg-pale-green' : null,
    canBeRevealed === true ? 'clickable clickable-hover' : null,
  ].filter(isNotNull);

  return (
    <>
      <tr
        className={classNames.join(' ')}
        onClick={() => canBeRevealed && toggleRevealed()}
      >
        <td className="pr-7">
          {canBeRevealed && (
            <svg
              viewBox="0 0 1000 1000"
              width="1em"
              height="1em"
              xmlns="http://www.w3.org/2000/svg"
            >
              {isRevealed ? (
                <polygon points="75,254.6 925,254.6 500,990.7" style={{ fill: '#000' }} />
              ) : (
                <polygon points="254.6,75 254.6,925 990.7,500" style={{ fill: '#000' }} />
              )}
            </svg>
          )}
        </td>
        {hideTags !== true && <td className="pr-15 text-nowrap">{libraryItem.tag}</td>}
        <td className="pr-15">{libraryItem.name}</td>

        {tableColumns.map(({ title, type, value }) => (
          <td
            key={title}
            className={`pr-15 text-tabular-nums ${
              type === 'number' ? 'align-right' : ''
            }`}
          >
            {value(libraryItem)}
          </td>
        ))}

        <td>
          {currentItemTag === libraryItem.tag ? (
            <div className="py-7">Current</div>
          ) : (
            <button
              className="btn"
              onClick={() => {
                onSelect(libraryItem);
              }}
            >
              Select
            </button>
          )}
        </td>
      </tr>
      {isRevealed && (
        <tr>
          <td colSpan={6}>
            <table
              cellPadding={7}
              style={{
                border: '4px solid var(--grey-700)',
              }}
            >
              <tbody>
                {fullItemData.map(({ title, value }) => (
                  <tr key={title}>
                    <th
                      className="pr-15"
                      style={{
                        verticalAlign: 'top',
                        textAlign: 'left',
                      }}
                    >
                      {title}
                    </th>
                    <td>{value}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </td>
        </tr>
      )}
    </>
  );
}

/**
 * Allow the user to select a library item from a library.
 *
 * If the user aborts, onClose is called; if the user selects something, onSelect is.
 */
export function SelectLibraryItem<T extends MinimalLibraryItem>({
  title,
  libraries,
  onSelect,
  onClose,
  hideTags = false,
  tableColumns,
  getFullItemData,
  currentItemTag,
  searchText,
}: SelectLibraryItemProps<T>): ReactElement {
  const [selectedLibrary, selectLibraryById] = useReducer(
    (current: LibraryOf<T>, newId: string): LibraryOf<T> => {
      const newLibrary = libraries.find((lib) => lib.id === newId);
      if (newLibrary !== undefined) return newLibrary;
      else return current;
    },
    libraries[0],
  );
  const [filterText, setFilterText] = useState('');

  const filteredLibraryItems = Object.values(
    pickBy(
      selectedLibrary.data,
      (element, tag) =>
        tag.toLowerCase().includes(filterText.toLowerCase()) ||
        searchText(element).toLowerCase().includes(filterText.toLowerCase()),
    ),
  );

  return (
    <Modal onClose={onClose} headerId="modal-header">
      <ModalHeader onClose={onClose} title={`Select ${title}`}>
        <div className="d-flex">
          <div>
            <label htmlFor="field_library_filter" className="small-caps">
              Filter
            </label>

            <input
              id="field_library_filter"
              className="mb-0"
              type="text"
              onChange={(evt) => setFilterText(evt.target.value)}
              value={filterText}
              autoFocus={true}
            />
          </div>

          <div className="ml-30">
            <label htmlFor="field_library_select" className="small-caps">
              From library
            </label>
            {libraries.length > 1 ? (
              <Select
                id="field_library_select"
                options={libraries.map((lib) => ({
                  value: lib.id,
                  display: lib.name,
                }))}
                value={selectedLibrary.id}
                onChange={selectLibraryById}
              />
            ) : (
              <div
                style={{
                  height: 30,
                  verticalAlign: 'middle',
                  display: 'table-cell',
                }}
              >
                {selectedLibrary.name}
              </div>
            )}
          </div>
        </div>
      </ModalHeader>
      <ModalBody className="table-td-inherit">
        {filteredLibraryItems.length > 0 ? (
          <table style={{ width: '100%' }}>
            <thead>
              <tr>
                <td style={{ width: 0 }}></td>
                {hideTags !== true && (
                  <th align="left" style={{ width: 0 }}>
                    Tag
                  </th>
                )}
                <th align="left">Name</th>
                {tableColumns.map(({ title }) => (
                  <th key={title} align="left" style={{ width: 0 }}>
                    {title}
                  </th>
                ))}
                <th style={{ width: 0 }}></th>
              </tr>
            </thead>
            <tbody>
              {filteredLibraryItems.map((libraryItem) => (
                <LibraryItem
                  key={libraryItem.tag}
                  {...{
                    currentItemTag,
                    libraryItem,
                    hideTags,
                    tableColumns,
                    onSelect,
                    getFullItemData,
                  }}
                />
              ))}
            </tbody>
          </table>
        ) : (
          <p>Nothing matching this filter</p>
        )}
      </ModalBody>
    </Modal>
  );
}

export function useLibraries() {
  const libraries: Library[] | undefined = externals().libraries;
  const [librariesAvailable, setLibrariesAvailable] = useState(libraries !== undefined);
  useEffect(() => {
    if (!librariesAvailable) {
      const timer = setInterval(() => {
        if (externals().libraries !== undefined) {
          setLibrariesAvailable(true);
          clearInterval(timer);
        }
      }, 1000);
      return () => clearInterval(timer);
    } else {
      return () => null;
    }
  }, [librariesAvailable]);
  if (librariesAvailable === false) {
    return {
      type: 'waiting' as const,
      modal: (onClose: () => void) => (
        <Modal onClose={onClose} headerId="modal-header">
          <ModalHeader onClose={onClose} title={`Loading...`} />
          <ModalBody>
            <p>Libraries still loading...</p>
          </ModalBody>
        </Modal>
      ),
    };
  }
  if (libraries === undefined) {
    throw new Error('temporally unreachable');
  }
  return { type: 'libraries' as const, data: libraries };
}

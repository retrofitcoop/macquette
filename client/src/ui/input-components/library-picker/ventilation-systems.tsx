import React from 'react';

import {
  VentilationSystem,
  VentilationSystemMeasure,
  isVentilationSystemMeasuresLibrary,
  isVentilationSystemsLibrary,
} from '../../../data-schemas/libraries/ventilation-system';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { calcMeasureQtyAndCost } from '../../../measures';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectVentilationSystemParams = {
  onSelect: (item: VentilationSystem) => void;
  onClose: () => void;
  currentItemTag?: string | null;
};

export function SelectVentilationSystem({
  onSelect,
  onClose,
  currentItemTag = null,
}: SelectVentilationSystemParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const libraryList = libraries.data.filter(isVentilationSystemsLibrary);
  if (!isNonEmpty(libraryList)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No ventilation system libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Ventilation System"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={libraryList}
      searchText={(system) => system.name}
      tableColumns={[]}
      getFullItemData={() => []}
    />
  );
}

type SelectVentilationSystemMeasureParams = {
  onSelect: (item: VentilationSystemMeasure) => void;
  onClose: () => void;
  showCost: boolean;
  areaSqm?: number | null;
  currentItemTag: string | null;
};

export function SelectVentilationSystemMeasure({
  onSelect,
  onClose,
  showCost,
  areaSqm = null,
  currentItemTag,
}: SelectVentilationSystemMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isVentilationSystemMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No ventilation system measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Ventilation System Measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        ...(showCost
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (system: VentilationSystemMeasure) =>
                  system.cost_units === 'sqm' && areaSqm !== null ? (
                    <NumberOutput
                      dp={0}
                      value={
                        calcMeasureQtyAndCost({
                          area: areaSqm ?? 0,
                          costUnits: system.cost_units,
                          costPerUnit: system.cost,
                          baseCost: system.min_cost,
                          isExternalWallInsulation: false,
                        })[1]
                      }
                    />
                  ) : system.cost_units === 'unit' ? (
                    <NumberOutput
                      dp={0}
                      value={
                        calcMeasureQtyAndCost({
                          costUnits: system.cost_units,
                          costPerUnit: system.cost,
                          baseCost: system.min_cost,
                        })[1]
                      }
                    />
                  ) : (
                    '?'
                  ),
              },
            ]
          : []),
      ]}
      getFullItemData={(system: VentilationSystemMeasure) => [
        ...(system.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(system.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${system.min_cost} + £${system.cost} per ${system.cost_units}`,
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

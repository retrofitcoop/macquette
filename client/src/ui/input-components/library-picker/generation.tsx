import React from 'react';

import {
  GenerationMeasure,
  isGenerationMeasuresLibrary,
} from '../../../data-schemas/libraries/generation-measures';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { calcMeasureQtyAndCost } from '../../../measures';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectGenerationMeasureParams = {
  onSelect: (item: GenerationMeasure) => void;
  onClose: () => void;
  currentItemTag: string | null;
};

export function SelectGenerationMeasure({
  onSelect,
  onClose,
  currentItemTag,
}: SelectGenerationMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isGenerationMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No generation measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Generation measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'kWp',
          type: 'number' as const,
          value: (system: GenerationMeasure) => (
            <NumberOutput dp={1} value={system.kWp} />
          ),
        },
        {
          title: '£',
          type: 'number' as const,
          value: (system: GenerationMeasure) => (
            <NumberOutput
              dp={0}
              value={
                calcMeasureQtyAndCost({
                  kWp: system.kWp,
                  costUnits: system.cost_units,
                  costPerUnit: system.cost,
                  baseCost: system.min_cost,
                })[1]
              }
            />
          ),
        },
      ]}
      getFullItemData={(system: GenerationMeasure) => [
        ...(system.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(system.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${system.min_cost} + £${system.cost} per ${system.cost_units}`,
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}

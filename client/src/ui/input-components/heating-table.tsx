import React, { useState } from 'react';

import { cloneDeep } from 'lodash';
import { sum } from '../../helpers/array-reducers';
import { periodLength, TimeOfDay } from '../../periods/core';
import { parseTime, timeToString } from '../../periods/strings';
import { UnvalidatedTimePeriod, validatePattern } from '../../periods/validation';

function ErrorDisplay({ error }: { error: string | null | undefined }) {
  if (error === null || error === undefined) {
    return null;
  } else {
    return <div className="text-error">{error}</div>;
  }
}

function nullableTimeToString(time: TimeOfDay | null): string {
  if (time === null) {
    return '';
  } else {
    return timeToString(time);
  }
}

function TimeInput({
  value,
  onChange: externalOnChange,
}: {
  value: TimeOfDay | null;
  onChange: (time: TimeOfDay | null) => void;
}) {
  const [previousOuterState, setPreviousOuterState] = useState(value);
  const [innerState, setInnerState] = useState(nullableTimeToString(value));
  const [error, setError] = useState<string | null>(null);

  if (previousOuterState !== value) {
    setPreviousOuterState(value);
    setInnerState(nullableTimeToString(value));
  }

  const style: React.CSSProperties = {
    width: '6ch',
    ...(error !== null ? { borderColor: 'red' } : {}),
  };

  return (
    <>
      <input
        type="text"
        style={style}
        value={innerState}
        onChange={(evt) => setInnerState(evt.target.value)}
        onBlur={() => {
          const [time, error] = parseTime(innerState);
          if (error === null) {
            setInnerState(nullableTimeToString(time));
          }
          if (time !== value) {
            setError(error);
            externalOnChange(error === null ? time : null);
          }
        }}
        className="mb-0"
      />
      <ErrorDisplay error={error} />
    </>
  );
}

export type State = UnvalidatedTimePeriod[];
export type Action = {
  type: 'update period';
  idx: number;
  part: 'start' | 'end';
  time: TimeOfDay | null;
};

export function reducer(state: State, action: Action): State {
  const newState = cloneDeep(state);
  const period = newState[action.idx];
  if (period === undefined) {
    return state;
  }
  period[action.part] = action.time;
  if (newState.every(({ start, end }) => (start === null) === (end === null))) {
    newState.sort((a, b) => {
      if (a.start === null || b.start === null) return 0;
      return a.start - b.start;
    });
  }
  return newState;
}

export function HeatingTable({
  label,
  periods,
  onChange,
}: {
  label?: string;
  periods: State;
  onChange: (newPeriods: State) => void;
}) {
  function updatePeriod(idx: number, part: 'start' | 'end') {
    return function (val: TimeOfDay | null) {
      const action: Action = {
        type: 'update period',
        idx,
        part,
        time: val,
      };
      const newPeriods = reducer(periods, action);
      onChange(newPeriods);
    };
  }

  const validatedPattern = validatePattern(periods);
  const totalOn = sum(
    validatedPattern.map(([_, vr]) =>
      vr
        .mapOk((p) => periodLength(p))
        .mapErr(() => 0)
        .coalesce(),
    ),
  );

  return (
    <div>
      <table
        className="table table--vertical-middle text-tabular-nums"
        style={{ width: 'auto' }}
      >
        <thead style={{ backgroundColor: 'var(--grey-800)' }}>
          <tr>
            <th>{label}</th>
            <th>On</th>
            <th>Off</th>
            <th>Hours on</th>
          </tr>
        </thead>
        <tbody>
          {validatedPattern.map(([period, validationR], idx) => {
            const duration = validationR
              .mapOk((p) => periodLength(p))
              .mapErr(() => null)
              .coalesce();
            return (
              <tr key={idx}>
                <td>Period {idx + 1}</td>
                <td>
                  <TimeInput value={period.start} onChange={updatePeriod(idx, 'start')} />
                  <ErrorDisplay
                    error={validationR
                      .mapOk(() => null)
                      .mapErr((e) =>
                        e.type === 'overlap'
                          ? `Overlaps with period ${e.otherIdx + 1}`
                          : null,
                      )
                      .coalesce()}
                  />
                </td>
                <td>
                  <TimeInput value={period.end} onChange={updatePeriod(idx, 'end')} />
                  <ErrorDisplay
                    error={validationR
                      .mapOk(() => null)
                      .mapErr((e) =>
                        e.type === 'bad range' ? 'Bad start or end time' : null,
                      )
                      .coalesce()}
                  />
                </td>
                <td className="text-right">
                  {duration !== null ? `${timeToString(duration, false)}` : '-'}
                </td>
              </tr>
            );
          })}
        </tbody>
        <tfoot style={{ backgroundColor: 'var(--grey-800)' }}>
          <tr>
            <td></td>
            <td></td>
            <td className="text-right">Total</td>
            <td className="text-right">{`${timeToString(totalOn, false)}`}</td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
}

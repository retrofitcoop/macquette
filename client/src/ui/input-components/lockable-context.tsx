import React from 'react';

type LockableContextInterface = {
  locked: boolean;
};

export const LockableContext = React.createContext<LockableContextInterface>({
  locked: false,
});

import { omit } from 'lodash';
import React from 'react';
import type { PropsOf } from '../../helpers/props-of';
import type { Icon } from '../icons';
import { LockableContext } from './lockable-context';

type BasicButtonProps = {
  className?: string;
  tooltip?: string | undefined;
} & ({ icon: Icon; title: string } | { title: string } | { icon: Icon });

export type ButtonProps = Omit<
  PropsOf<'button'>,
  keyof BasicButtonProps | 'icon' | 'title'
> &
  BasicButtonProps;

export function Button({ className = '', tooltip = undefined, ...props }: ButtonProps) {
  const { locked } = React.useContext(LockableContext);

  const disabled = props.disabled ?? locked ?? false;

  return (
    <button
      className={`btn d-iflex align-items-center gap-7 ${className}`}
      title={tooltip}
      disabled={disabled}
      {...omit(props, ['icon', 'title', 'disabled'])}
    >
      {'icon' in props && React.createElement(props.icon)}
      {'title' in props && props.title}
    </button>
  );
}

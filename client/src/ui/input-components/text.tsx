import React from 'react';

import { PropsOf } from '../../helpers/props-of';
import { Shadow } from '../../helpers/shadow-object-type';
import { LockableContext } from './lockable-context';
import { useStateTracker } from './use-state-tracker';

type BasicTextInputProps = {
  value: string;
  onChange: (value: string) => void;
  error?: string | undefined;
};

export type TextInputProps = Shadow<PropsOf<'input'>, BasicTextInputProps>;

export function TextInput({
  value,
  onChange,
  error,
  style: styleProp,
  ...passthroughProps
}: TextInputProps) {
  const { locked } = React.useContext(LockableContext);

  const [innerState, setInnerState] = useStateTracker<string>(value);
  const style: React.CSSProperties = {
    ...(styleProp ?? {}),
    ...(error === undefined ? {} : { borderColor: 'red' }),
  };

  const { disabled: disabledProp, ...rest } = passthroughProps;
  const disabled = disabledProp ?? locked ?? false;

  return (
    <input
      type="text"
      value={innerState}
      onChange={(evt) => setInnerState(evt.target.value)}
      onBlur={() => {
        if (innerState !== value) {
          onChange(innerState);
        }
      }}
      style={style}
      title={error}
      disabled={disabled}
      {...rest}
    />
  );
}

import React, { Dispatch, ReactElement, ReactNode } from 'react';
import { assertNever } from '../../helpers/assert-never';
import { Result } from '../../helpers/result';
import { safeIsArray } from '../../helpers/safe-is-array';
import { Month } from '../../model/enums/month';
import { Model, ModelModePropertyName } from '../../model/model';
import { Select } from '../input-components/select';
import { UiModule } from '../module-management/module-type';
import { NumberOutput } from '../output-components/numeric';

type State = { model: Model | null; mode: ModelModePropertyName };
type Action =
  | { type: 'set model'; model: Model | null }
  | { type: 'set model mode'; mode: ModelModePropertyName };

/* SAFETY: As long as T[M] refers to a single property of T, the signature of this
 * function matches its behaviour */
/* eslint-disable @typescript-eslint/consistent-type-assertions,
    @typescript-eslint/no-unsafe-function-type */
function bindMethod<M extends string | symbol, T extends { [K in M]: Function }>(
  obj: T | undefined,
  methodName: M,
): null | T[M] {
  if (obj === undefined) return null;
  return obj[methodName].bind(obj) as T[M];
}
/* eslint-enable */

function SAPWorksheet({
  state: { model, mode },
  dispatch,
}: {
  state: State;
  dispatch: Dispatch<Action>;
}) {
  const haveStandardisedHeatingPre2025 = !(
    model?.normal.modelBehaviourFlags.meanInternalTemperature
      .disallowPre2025StandardisedHeatingMode ?? true
  );
  return (
    <section className="sap-worksheet">
      <section>
        <h3>SAP Worksheet</h3>
        <p>
          This section summarizes scenario calculations, following the format of the SAP
          Worksheet.
        </p>
        <p>
          This is a tool for helping to improve the performance of existing buildings. It
          is based on SAP 9 and 10, but in some cases different assumptions have been made
          and different calculation methods used. As a result, the results shown here may
          differ from a standard SAP assessment, and not all SAP Worksheet cells appear.
          Additionally, we mostly omit cells used for calculation inputs.
        </p>
        <p>
          In SAP, some values are expressed monthly. These are denoted (00)ₘ, and can be
          aggregated either by taking their weighted mean (x̄) or by summation (Σ).
        </p>
        <h4>Model mode</h4>
        <p>This tool operates in one of several modes:</p>
        <dl>
          <dt>Normal mode</dt>
          <dd>Uses all user inputs without overrides.</dd>
          <dt>Regulated mode</dt>
          <dd>
            Provides output figures for scenarios covering only energy uses regulated by
            the UK Building Regulations by setting fuel consumption for appliances and
            cooking to 0. Heat gains from appliances and cooking are still counted for
            calculating the space heating demand. This mode changes values from cell (255)
            onwards.
          </dd>
          <dt>Standardised heating mode</dt>
          <dd>
            Disregards the heating pattern and the effect of temperature reduction outside
            the living area, by setting the overall mean internal temperature to 20°C
            constantly. This mode changes values from cell (93)ₘ onwards.
          </dd>
          {haveStandardisedHeatingPre2025 && (
            <>
              <dt>Standardised heating (pre-2025) mode</dt>
              <dd>
                Assume a heating pattern of 7 and 8 hours off on weekdays, and 8 hours off
                on weekends, at a set point of 21°C, which was the default behaviour of
                the standardised heating mode for assessments created before 2025. This
                mode changes values from cell (85) onwards.
              </dd>
            </>
          )}
        </dl>
      </section>
      <section className="mode-selector">
        <label htmlFor="mode-selector">Model mode</label>
        <Select
          value={mode}
          id="mode-selector"
          options={[
            { value: 'normal', display: 'Normal' },
            { value: 'regulated', display: 'Regulated' },
            ...(haveStandardisedHeatingPre2025
              ? [
                  {
                    value: 'standardisedHeatingPre2025' as const,
                    display: 'Standardised heating (pre-2025)',
                  },
                ]
              : []),
            { value: 'standardisedHeating', display: 'Standardised heating' },
          ]}
          onChange={(mode) => dispatch({ type: 'set model mode', mode })}
          style={{ marginTop: '10px' }}
        />
      </section>
      <section>
        <h4>1. Overall dwelling dimensions</h4>
        <p>
          Unlike SAP, we allow a reduction percentage to be specified for volume to
          account for internal volume being different from external volume (e.g. internal
          floors and walls, voids).
        </p>
        <OutputTable>
          <MultiRow
            elements={model?.[mode].floors.floors ?? []}
            row={(floor) => (
              <SimpleRow
                name={`Volume: ${floor.spec.name}`}
                sapCell="3n"
                unit="m³"
                value={floor.volume}
              />
            )}
          />
          <SimpleRow
            name="Total Floor Area (TFA)"
            sapCell="4"
            unit="m²"
            value={model?.[mode].floors.totalFloorArea}
          />
          <SimpleRow
            name="Gross volume"
            unit="m³"
            value={model?.[mode].floors.grossVolume}
          />
          <SimpleRow
            name="Volume reduction"
            unit="%"
            value={model === null ? null : model[mode].floors.volumeReduction * 100}
          />
          <SimpleRow
            name="Dwelling Volume (Net volume)"
            sapCell="5"
            unit="m³"
            value={model?.[mode].floors.totalVolume}
          />
        </OutputTable>
      </section>
      <section>
        <h4>2. Ventilation rate</h4>
        <p>
          We diverge from SAP in that we treat infiltration (structural infiltration and
          infiltration from vents and flues) separately from ventilation (extract
          ventilation points and/or ventilation systems such as MVHR). This means that we
          have split this section up accordingly, and reordered some cells.
        </p>
        <OutputTable>
          <SimpleRow
            name="Shelter factor"
            sapCell="20"
            unit="ratio"
            value={model?.[mode].ventilationInfiltrationCommon.shelterFactor}
          />
          <MonthlyRow
            name="Wind factor"
            sapCell="22a"
            unit="ratio"
            value={bindMethod(model?.[mode].ventilationInfiltrationCommon, 'windFactor')}
            reduction="mean"
          />
        </OutputTable>
        <h5>2a. Infiltration</h5>
        <p>
          Structural infiltration (either from fabric elements or from pressure test),
          plus air change due to intentional vents and flues, multiplied by the shelter
          and wind factors.
        </p>
        <OutputTable>
          <SimpleRow
            name="Infiltration due to intentional vents and flues"
            sapCell="8"
            unit="ACH"
            value={model?.[mode].infiltration.intentionalVentsFluesAirChangesPerHour}
          />
          <SimpleRow
            name="Number of storeys"
            sapCell="9"
            unit="#"
            value={
              model?.[mode].infiltration.input.estimateFrom.type === 'fabric elements'
                ? model[mode].infiltration.effectiveNumberOfFloors
                : null
            }
          />
          <SimpleRow
            name="Air permeability value AP₅₀"
            sapCell="17"
            unit="m³/h/m²"
            value={
              model?.[mode].infiltration.input.estimateFrom.type === 'pressure test'
                ? model[mode].infiltration.input.estimateFrom.airPermeability
                : null
            }
          />
          <SimpleRow
            name="Base infiltration rate"
            sapCell="18"
            unit="ACH"
            value={model?.[mode].infiltration.rawAirChangesPerHour}
          />
          <SimpleRow
            name="Infiltration incorporating shelter factor"
            sapCell="21"
            unit="ACH"
            value={model?.[mode].infiltration.withShelterFactorAirChangesPerHour}
          />
          <MonthlyRow
            name="Infiltration incorporating shelter factor and wind factor"
            sapCell="22b"
            reduction="mean"
            unit="ACH"
            value={bindMethod(model?.[mode].infiltration, 'airChangesPerHour')}
          />
        </OutputTable>
        <h5>2b. Ventilation</h5>
        <p>
          Ventilation rate is taken from the ventilation system if present, otherwise by
          summing the air change rates of extract ventilation points and applying the
          shelter and wind factors.
        </p>
        <OutputTable>
          <SimpleRow
            name="Ventilation from extract ventilation points"
            unit="ACH"
            value={model?.[mode].ventilation.rawEVPAirChanges}
          />
          <MonthlyRow
            name="EVP ventilation incorporating shelter factor and wind factor"
            unit="ACH"
            reduction="mean"
            value={bindMethod(model?.[mode].ventilation, 'adjustedEVPAirChanges')}
          />
          <SimpleRow
            name="System ventilation rate"
            unit="ACH"
            value={model?.[mode].ventilation.systemAirChanges}
          />
          <MonthlyRow
            name="Ventilation rate"
            unit="ACH"
            reduction="mean"
            value={bindMethod(model?.[mode].ventilation, 'airChangesPerHour')}
          />
        </OutputTable>
        <h5>2c. Combined</h5>
        <OutputTable>
          <MonthlyRow
            name="Effective air change rate"
            sapCell="25"
            reduction="mean"
            unit="ACH"
            value={bindMethod(model?.[mode].airChangeRate, 'totalAirChangeRate')}
          />
        </OutputTable>
      </section>
      <section>
        <h4>3. Heat losses and heat loss parameter</h4>
        <p>
          This section corresponds with the Fabric page, and the data for cells 26-30 can
          be found there.
        </p>
        <OutputTable>
          <SimpleRow
            name="Total area of external elements"
            sapCell="31"
            unit="m²"
            value={model?.[mode].fabric.externalArea}
          />
          <SimpleRow
            name="Party walls"
            sapCell="32"
            unit="m²"
            value={model?.[mode].fabric.areaTotals.partyWall}
          />
          <SimpleRow
            name="Fabric heat loss"
            sapCell="33"
            unit="W/K"
            value={model?.[mode].fabric.fabricElementsHeatLoss}
          />
          <SimpleRow
            name="Heat capacity"
            sapCell="34"
            unit="kJ/K"
            value={model?.[mode].fabric.fabricElementsThermalCapacity}
          />
          <SimpleRow
            name="Thermal mass parameter"
            sapCell="35"
            unit="kJ/K/m²"
            value={model?.[mode].fabric.thermalMassParameter}
          />
          <SimpleRow
            name="Thermal bridges"
            sapCell="36"
            unit="W/K"
            value={model?.[mode].fabric.thermalBridgingHeatLoss}
          />
          <SimpleRow
            name="Total fabric heat loss"
            sapCell="37"
            unit="W/K"
            value={model?.[mode].fabric.heatLoss}
          />
          <MonthlyRow
            name="Infiltration heat loss"
            reduction="mean"
            unit="W/K"
            value={bindMethod(model?.[mode].infiltration, 'heatLossMonthly')}
          />
          <MonthlyRow
            name="Ventilation heat loss"
            reduction="mean"
            unit="W/K"
            value={bindMethod(model?.[mode].ventilation, 'heatLossMonthly')}
          />
          <MonthlyRow
            name="Ventilation heat loss (including infiltration)"
            sapCell="38"
            reduction="mean"
            unit="W/K"
            value={bindMethod(model?.[mode].heatLoss, 'sapVentilationHeatLossMonthly')}
          />
          <MonthlyRow
            name="Heat transfer coefficient"
            sapCell="39"
            reduction="mean"
            unit="W/K"
            value={bindMethod(model?.[mode].heatLoss, 'heatLossMonthly')}
          />
          <MonthlyRow
            name="Heat loss parameter (HLP)"
            sapCell="40"
            reduction="mean"
            unit="W/K/m²"
            value={bindMethod(model?.[mode].heatLoss, 'heatLossParameter')}
          />
        </OutputTable>
      </section>
      <section>
        <h4>4. Water heating energy requirement</h4>
        <p>
          SAP 9 calculates <em>(44) Hot water usage</em> by a simple formula on the
          occupancy, whereas SAP 10 models shower and bath usage and the effect of mixer
          taps etc. We use the SAP 9 model.
        </p>
        <OutputTable>
          <SimpleRow
            name="Occupancy"
            sapCell="42"
            unit="#"
            value={model?.[mode].occupancy.occupancy}
          />
          <MonthlyRow
            name="Hot water usage"
            sapCell="44"
            reduction="mean"
            unit="l/day"
            value={bindMethod(
              model?.[mode].waterCommon,
              'dailyHotWaterUsageLitresByMonth',
            )}
          />
          <MonthlyRow
            name="Energy content of hot water used"
            sapCell="45"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterCommon, 'hotWaterEnergyContentByMonth')}
          />
          <MonthlyRow
            name="Distribution loss"
            sapCell="46"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'distributionLossMonthly')}
          />
          <SimpleRow
            name="Storage loss"
            sapCell="55"
            unit="kWh/day"
            value={model?.[mode].waterHeating.energyLossFromWaterStorageDaily}
          />
          <MonthlyRow
            name="Primary circuit loss"
            sapCell="59"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'primaryCircuitLossMonthly')}
          />
          <MonthlyRow
            name="Combi loss"
            sapCell="61"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'combiLossMonthly')}
          />
          <MonthlyRow
            name="Total heat required for water heating"
            sapCell="62"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'totalHeatRequiredMonthly')}
          />
          <MonthlyRow
            name="Solar DHW input"
            sapCell="63c"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].solarHotWater, 'solarInputMonthly')}
          />
          <MonthlyRow
            name="Output from water heater"
            sapCell="64"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'heatOutputMonthly')}
          />
          <MonthlyRow
            name="Heat gain from water heating"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].waterHeating, 'heatGain')}
          />
          <MonthlyRow
            name="Heat gain from water heating"
            sapCell="65"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].waterHeating, 'heatGainKWhPerMonth')}
          />
        </OutputTable>
      </section>
      <section>
        <h4>5. Internal gains</h4>
        <p>
          We allow the user to diverge from SAP for modelling gains from Appliances and
          Cooking. The PPR calculation means doing basic accounting of actual appliances
          and taking the total average input power to be equal to their contribution to
          internal gains.
        </p>
        <p>
          In SAP, gains are modelled as constant (average) power (W), whereas losses are
          modelled as power per unit temperature difference (W/K). However, certain losses
          (e.g. evaporation) are unaffected by the temperature difference, and are
          therefore considered as negative gains; see cell (71).
        </p>
        <OutputTable>
          <SimpleRow
            name="Metabolic gains"
            sapCell="66"
            unit="W"
            value={model?.[mode].miscellaneousInternalGains.metabolicHeatGainPower}
          />
          <MonthlyRow
            name="Lighting gains"
            sapCell="67"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].lighting, 'heatGain')}
          />
          <MonthlyRow
            name="Appliances gains (SAP 9 calculation)"
            sapCell="68"
            reduction="mean"
            unit="W"
            value={
              model?.[mode].appliancesSap.input.enabled === true
                ? bindMethod(model?.[mode].appliancesSap, 'heatGain')
                : null
            }
          />
          <SimpleRow
            name="Cooking gains (SAP 9 calculation)"
            sapCell="69"
            unit="W"
            value={
              model?.[mode].cookingSap.input.enabled === true
                ? model?.[mode].cookingSap.heatGain
                : null
            }
          />
          <MonthlyRow
            name="Appliances gains (PPR calculation)"
            sapCell="68"
            reduction="mean"
            unit="W"
            value={
              model?.[mode].appliancesCookingLoadCollections.input.enabled === true
                ? bindMethod(
                    model?.[mode].appliancesCookingLoadCollections.appliances,
                    'heatGain',
                  )
                : null
            }
          />
          <MonthlyRow
            name="Cooking gains (PPR calculation)"
            sapCell="69"
            reduction="mean"
            unit="W"
            value={
              model?.[mode].appliancesCookingLoadCollections.input.enabled === true
                ? bindMethod(
                    model?.[mode].appliancesCookingLoadCollections.cooking,
                    'heatGain',
                  )
                : null
            }
          />
          <MonthlyRow
            name="Pumps and fans gains"
            sapCell="70"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].fansAndPumpsGains, 'heatGain')}
          />
          <SimpleRow
            name="Losses (constant power)"
            sapCell="71"
            unit="W"
            value={model?.[mode].miscellaneousInternalGains.miscellaneousHeatLossPower}
          />
          <MonthlyRow
            name="Water heating gains"
            sapCell="72"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].waterHeating, 'heatGain')}
          />
          <MonthlyRow
            name="Total internal gains"
            sapCell="73"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].heatGain, 'heatGainInternal')}
          />
        </OutputTable>
      </section>
      <section>
        <h4>6. Solar gains</h4>
        <p>Cells 74-82 correspond to values found on the Fabric page.</p>
        <OutputTable>
          <MonthlyRow
            name="Solar gains"
            sapCell="83"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].heatGain, 'heatGainSolar')}
          />
          <MonthlyRow
            name="Total gains – internal and solar"
            sapCell="84"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].heatGain, 'heatGain')}
          />
        </OutputTable>
      </section>
      <section>
        <h4>7. Mean internal temperature (in heating season)</h4>
        <p>
          In SAP, mean internal temperature is calculated first by assuming that heating
          demand is met entirely for the specified heating-on periods, and then the
          internal temperature decays over the subsequent heating-off period. The
          parameters of the decay are determined by the thermal mass and the balance of
          gains and losses.
        </p>
        <p>
          Mean temperatures are determined for weekdays vs weekends and for the living
          area vs the rest of dwelling, and a final mean is taken, weighted by the
          weekday-weekend ratio 5:2 and the living area fraction.
        </p>
        <p>
          Finally, a blanket adjustment can be applied depending on the heating system.
        </p>
        <p>
          Note that for months in which the modelled mean internal temperature is less
          than the external temperature, the formula for calculating the heat gain
          utilisation factor (SAP Table 9) breaks down. In these cases the utilisation
          factor is taken to be 1.
        </p>
        <OutputTable>
          <MonthlyRow
            name="Temperature when heated (living area)"
            sapCell="85"
            unit="°C"
            reduction="mean"
            value={(m) =>
              model?.[mode].meanInternalTemperature.wholeDwellingCalculators(m).weekday
                .livingAreaCalculator.temperatureWhenHeated
            }
          />
          <MonthlyRow
            name="Utilisation factor for gains for living area"
            sapCell="86"
            reduction="mean"
            unit="ratio"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'initialUtilisationFactorLivingArea',
            )}
          />
          <MonthlyRow
            name="Mean internal temperature in living area"
            sapCell="87"
            reduction="mean"
            unit="°C"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'meanInternalTemperatureLivingArea',
            )}
          />
          <MonthlyRow
            name="Temperature during heating periods in rest of dwelling"
            sapCell="88"
            reduction="mean"
            unit="°C"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'temperatureWhenHeatedRestOfDwelling',
            )}
          />
          <MonthlyRow
            name="Utilisation factor for gains for rest of dwelling"
            sapCell="89"
            unit="ratio"
            reduction="mean"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'initialUtilisationFactorRestOfDwelling',
            )}
          />
          <MonthlyRow
            name="Mean internal temperature in rest of dwelling"
            sapCell="90"
            reduction="mean"
            unit="°C"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'meanInternalTemperatureRestOfDwelling',
            )}
          />
          <SimpleRow
            name="Living area fraction"
            sapCell="90"
            unit="ratio"
            value={model?.[mode].meanInternalTemperature.livingAreaFraction}
          />
          <MonthlyRow
            name="Mean internal temperature (for the whole dwelling) (before adjustment)"
            sapCell="92"
            reduction="mean"
            unit="°C"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'meanInternalTemperatureOverallUnadjusted',
            )}
          />
          <SimpleRow
            name="Mean internal temperature adjustment"
            unit="°C"
            value={model?.[mode].meanInternalTemperature.temperatureAdjustment}
          />
          <MonthlyRow
            name="Mean internal temperature overall"
            sapCell="93"
            reduction="mean"
            unit="°C"
            value={bindMethod(
              model?.[mode].meanInternalTemperature,
              'meanInternalTemperatureOverall',
            )}
          />
        </OutputTable>
      </section>
      <section>
        <h4>8. Space heating requirement</h4>
        <p>
          Once the mean internal temperature is calculated, we calculate the amount of
          space heating power output actually required to maintain it.
        </p>
        <p>
          If the user has selected that heating is off in summer, the space heating
          requirement for the summer months is forced to 0, even if the model would
          otherwise deem that space heating would be required in summer to maintain the
          mean internal temperature. An undesirable effect of this is that for the summer
          months (and for the aggregate annual figures), the calculated losses do not
          balance against the calculated gains + space heating energy.
        </p>
        <OutputTable flexible>
          <MonthlyRow
            name="Utilisation factor for gains"
            sapCell="94"
            reduction="mean"
            unit="ratio"
            value={bindMethod(model?.[mode].spaceHeating, 'heatGainUtilisationFactor')}
          />
          <MonthlyRow
            name="Useful gains"
            sapCell="95"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].spaceHeating, 'usefulHeatGain')}
          />
          <MonthlyRow
            name="Monthly average external temperature"
            sapCell="96"
            reduction="mean"
            unit="°C"
            value={bindMethod(model?.[mode].spaceHeating, 'externalTemperature')}
          />
          <MonthlyRow
            name="Heat loss rate for mean internal temperature"
            sapCell="97"
            reduction="mean"
            unit="W"
            value={bindMethod(model?.[mode].spaceHeating, 'heatLossPower')}
          />
          <MonthlyRow
            name="Space heating requirement (power)"
            unit="W"
            reduction="mean"
            value={bindMethod(model?.[mode].spaceHeating, 'spaceHeatingDemand')}
          />
          <MonthlyRow
            name="Space heating requirement for each month"
            sapCell="98"
            reduction="sum"
            unit="kWh"
            value={bindMethod(model?.[mode].spaceHeating, 'spaceHeatingDemandEnergy')}
          />
          <SimpleRow
            name="Space heating requirement"
            sapCell="99"
            unit="kWh/m²"
            value={model?.[mode].spaceHeating.spaceHeatingDemandAnnualEnergyPerArea}
          />
        </OutputTable>
      </section>
      <section>
        <h4>9. Energy requirements</h4>
        <p>
          This section calculates energy demand for each fuel-using component, and for
          heating systems (which may have an efficiency other than 100%), it calculates
          the required amount of fuel input in kWh-fuel.
        </p>
        <OutputTable flexible>
          <SimpleRow
            name="Fraction of space heat from main system(s)"
            sapCell="202"
            unit="ratio"
            value={
              model?.[mode].meanInternalTemperature.fractionOfSpaceHeatingFromMainSystems
            }
          />
          <SimpleRow
            name="Fraction of main heating from main system 2"
            sapCell="203"
            unit="ratio"
            value={
              model?.[mode].meanInternalTemperature
                .fractionOfMainHeatingFromSecondMainSystem
            }
          />
          <SimpleRow
            name="Fraction of total space heat from main system 1"
            sapCell="204"
            unit="ratio"
            value={model?.[mode].meanInternalTemperature.heatingSystems.main?.fraction}
          />
          <SimpleRow
            name="Fraction of total space heat from main system 2"
            sapCell="205"
            unit="ratio"
            value={
              model?.[mode].meanInternalTemperature.heatingSystems.secondMain?.fraction
            }
          />
          <MultiRow
            elements={
              model?.[mode].heatingSystemsFuelRequirements.spaceHeatingSystems ?? []
            }
            row={(system) => (
              <SimpleRow
                key={system.baseSystem.id}
                name={`Space heating system efficiency (ID ${system.baseSystem.id}, ${system.baseSystem.spaceHeating.type})`}
                sapCell={((str: typeof system.baseSystem.spaceHeating.type) => {
                  switch (str) {
                    case 'main':
                      return '206';
                    case 'second main (whole house)':
                      return '207';
                    case 'second main (part of house)':
                      return '207';
                    case 'supplementary':
                      return '208';
                  }
                })(system.baseSystem.spaceHeating.type)}
                unit="%"
                value={system.spaceHeatingEfficiency * 100}
              />
            )}
          />
          <MultiRow
            elements={
              model?.[mode].heatingSystemsFuelRequirements.spaceHeatingSystems ?? []
            }
            row={(system) => (
              <MonthlyRow
                name={`Space heating fuel (ID ${system.baseSystem.id}, ${system.baseSystem.spaceHeating.type})`}
                key={system.baseSystem.id}
                sapCell={((str: typeof system.baseSystem.spaceHeating.type) => {
                  switch (str) {
                    case 'main':
                      return '211';
                    case 'second main (whole house)':
                      return '213';
                    case 'second main (part of house)':
                      return '213';
                    case 'supplementary':
                      return '215';
                  }
                })(system.baseSystem.spaceHeating.type)}
                unit="kWh-fuel"
                reduction="sum"
                value={(m: Month) => system.spaceHeatingFuelInput(m)}
              />
            )}
          />
          <MultiRow
            elements={
              model?.[mode].heatingSystemsFuelRequirements.waterHeatingSystems ?? []
            }
            row={(system) => (
              <SimpleRow
                name={`Efficiency of water heating system (ID ${system.baseSystem.id})`}
                key={system.baseSystem.id}
                sapCell="217"
                unit="%"
                value={system.waterHeatingEfficiency}
              />
            )}
          />
          <MultiRow
            elements={
              model?.[mode].heatingSystemsFuelRequirements.waterHeatingSystems ?? []
            }
            row={(system) => (
              <MonthlyRow
                name={`Fuel for water heating (ID ${system.baseSystem.id})`}
                key={system.baseSystem.id}
                sapCell="219"
                unit="kWh-fuel"
                reduction="sum"
                value={(m: Month) => system.waterHeatingFuelInput(m)}
              />
            )}
          />
          <SimpleRow
            name="Total electricity for fans, pumps and electric keep-hot"
            sapCell="231"
            unit="kWh"
            value={model?.[mode].fansPumpsElectricKeepHot.annualEnergy}
          />
          <MonthlyRow
            name="Electricity for lighting"
            sapCell="232"
            unit="kWh"
            reduction="sum"
            value={bindMethod(model?.[mode].lighting, 'energyMonthly')}
          />
          <SimpleRow
            name="Electricity generated by PVs (on-site use)"
            sapCell="233a"
            unit="kWh"
            value={model?.[mode].generation.solar?.energyUsedOnsiteAnnual}
          />
          <SimpleRow
            name="Electricity generated by wind turbines (on-site use)"
            sapCell="234a"
            unit="kWh"
            value={model?.[mode].generation.wind?.energyUsedOnsiteAnnual}
          />
          <SimpleRow
            name="Electricity generated by hydro-electric (on-site use)"
            sapCell="235a"
            unit="kWh"
            value={model?.[mode].generation.hydro?.energyUsedOnsiteAnnual}
          />
          <SimpleRow
            name="Electricity generated by PVs (exported)"
            sapCell="233b"
            unit="kWh"
            value={model?.[mode].generation.solar?.energyExportedAnnual}
          />
          <SimpleRow
            name="Electricity generated by wind turbines (exported)"
            sapCell="234b"
            unit="kWh"
            value={model?.[mode].generation.wind?.energyExportedAnnual}
          />
          <SimpleRow
            name="Electricity generated by hydro-electric (exported)"
            sapCell="235b"
            unit="kWh"
            value={model?.[mode].generation.hydro?.energyExportedAnnual}
          />
        </OutputTable>
      </section>
      <section>
        <h4>10. Fuel costs</h4>
        <p>
          Unlike SAP, we do not calculate costs broken down by heating system or module
          (such as lighting); rather we sum all their fuel input requirements (in
          kWh-fuel) and calculate the cost of the total per fuel. As such, most of the
          cells in this section are skipped, and the data according to our model can be
          found on the Energy Use page.
        </p>
        <OutputTable>
          <MultiRow
            elements={[
              ...(model?.[mode].fuelRequirements.totalsByFuelAnnual.entries() ?? []),
            ]}
            row={([name, usage]) => (
              <SimpleRow
                name={`Total fuel cost: ${name}`}
                key={name}
                unit="£"
                value={usage.cost}
              />
            )}
          />
          <SimpleRow
            name="Total energy cost"
            sapCell="255"
            unit="£"
            value={model?.[mode].fuelRequirements.totalNetCostAnnual}
          />
        </OutputTable>
      </section>
      <section>
        <h4>11. SAP rating</h4>
        <p>This is not a SAP tool, so it does not provide a SAP rating.</p>
      </section>
      <section>
        <h4>12. CO₂ emissions</h4>
        <p>
          Similarly to section 10, the equivalent data to this section can be found on the
          Energy Use page.
        </p>
        <OutputTable>
          <MultiRow
            elements={[
              ...(model?.[mode].fuelRequirements.totalsByFuelAnnual.entries() ?? []),
            ]}
            row={([name, usage]) => (
              <SimpleRow
                name={`Total fuel carbon emissions: ${name}`}
                key={name}
                unit="kgCO₂e"
                value={usage.carbonEmissions}
              />
            )}
          />
          <SimpleRow
            name="Total CO₂"
            sapCell="272"
            unit="kgCO₂e"
            value={model?.[mode].fuelRequirements.totalCarbonEmissionsAnnual}
          />
          <SimpleRow
            name="Dwelling CO₂ emission rate"
            sapCell="273"
            unit="kgCO₂e/m²"
            value={model?.[mode].fuelRequirements.totalCarbonEmissionsAnnualPerArea}
          />
        </OutputTable>
      </section>
      <section>
        <h4>13. Primary energy</h4>
        <p>
          Similarly to section 10, the equivalent data to this section can be found on the
          Energy Use page.
        </p>
        <OutputTable>
          <MultiRow
            elements={[
              ...(model?.[mode].fuelRequirements.totalsByFuelAnnual.entries() ?? []),
            ]}
            row={([name, usage]) => (
              <SimpleRow
                name={`Total fuel primary energy: ${name}`}
                key={name}
                unit="kWh"
                value={usage.primaryEnergy}
              />
            )}
          />
          <SimpleRow
            name="Total primary energy"
            sapCell="286"
            unit="kWh"
            value={model?.[mode].fuelRequirements.totalPrimaryEnergyAnnual}
          />
          <SimpleRow
            name="Dwelling PE rate"
            sapCell="287"
            unit="kWh"
            value={model?.[mode].fuelRequirements.totalPrimaryEnergyAnnualPerArea}
          />
        </OutputTable>
      </section>
    </section>
  );
}

function OutputTable({
  children,
  flexible: flexible_,
}: {
  children:
    | InstantiatedSimpleRow
    | InstantiatedMonthlyRow
    | InstantiatedMultiRow
    | Array<InstantiatedSimpleRow | InstantiatedMonthlyRow | InstantiatedMultiRow>;
  flexible?: true;
}) {
  const children_ = safeIsArray(children) ? children : [children];
  const flexible = flexible_ ?? false;
  return (
    <table className={['sap-worksheet-output', flexible ? 'flexible' : ''].join(' ')}>
      <thead>
        <tr>
          <th className="sap-cell"></th>
          <th></th>
          <th className="unit">Unit</th>
          <th className="annual">Value</th>
          {...Month.all.map((m) => (
            <th key={m.name} className="monthly">
              {m.name.slice(0, 3)}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>{children_}</tbody>
    </table>
  );
}

type InstantiatedSimpleRow = ReactElement<
  Parameters<typeof SimpleRow>[0],
  typeof SimpleRow
>;
function SimpleRow({
  sapCell,
  name,
  value,
  unit,
}: {
  sapCell?: string;
  name: string;
  value: number | null | undefined;
  unit: string;
}) {
  return (
    <tr className="simple">
      <td className="sap-cell">{sapCell !== undefined && `(${sapCell})`}</td>
      <td>{name}</td>
      <td className="unit">{unit}</td>
      <td className="numeric annual">
        <NumberOutput value={value} />
      </td>
      {Month.all.map((m) => (
        <td key={m.name} className="numeric monthly">
          -
        </td>
      ))}
    </tr>
  );
}

type Instantiated<T> = T extends (props: infer P) => ReactNode
  ? ReactElement<P, T>
  : never;
type InstantiatedMonthlyRow = Instantiated<typeof MonthlyRow>;
function MonthlyRow({
  name,
  sapCell,
  value,
  unit,
  reduction,
}: {
  name: string;
  sapCell?: string;
  value: ((m: Month) => number | null | undefined) | null | undefined;
  unit: string;
  reduction: 'mean' | 'sum';
}) {
  let reduced: number | null;
  let reductionDisplay: ReactNode;
  if (value === null || value === undefined) {
    reduced = null;
    reductionDisplay = '';
  } else {
    switch (reduction) {
      case 'mean':
        reduced = Month.all.reduce((total: number | null, m: Month) => {
          const v = value(m);
          if (total === null || v === null || v === undefined) {
            return null;
          } else {
            return total + (v * m.days) / 365.0;
          }
        }, 0);
        reductionDisplay = <>x̄&nbsp;</>;
        break;
      case 'sum':
        reduced = Month.all.reduce((total: number | null, m: Month) => {
          const v = value(m);
          if (total === null || v === null || v === undefined) {
            return null;
          } else {
            return total + v;
          }
        }, 0);
        reductionDisplay = <>Σ&nbsp;</>;
        break;
    }
  }
  return (
    <tr>
      <td className="sap-cell">{sapCell !== undefined && <>({sapCell})ₘ</>}</td>
      <td>{name}</td>
      <td className="unit">{unit}</td>
      <td className="numeric annual">
        {reductionDisplay}
        <NumberOutput value={reduced} />
      </td>
      {Month.all.map((m) => (
        <td key={m.name} className="numeric monthly">
          <NumberOutput value={(value ?? (() => null))(m)} />
        </td>
      ))}
    </tr>
  );
}

type InstantiatedMultiRow = Instantiated<typeof MultiRow>;
function MultiRow<T>({
  row,
  elements,
}: {
  row: (element: T) => InstantiatedSimpleRow | InstantiatedMonthlyRow;
  elements: T[];
}) {
  return <>{...elements.map((element) => row(element))}</>;
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'set model':
      return { ...state, model: action.model };
    case 'set model mode':
      return { ...state, mode: action.mode };
  }
}

export const sapWorksheetModule: UiModule<State, Action, never> = {
  name: 'sap worksheet',
  component: SAPWorksheet,
  initialState: () => ({
    model: null,
    mode: 'normal',
  }),
  reducer: (state, action) => [reducer(state, action)],
  effector: assertNever,
  shims: {
    extractUpdateAction: (context) =>
      Result.ok([
        {
          type: 'set model',
          model: context.currentModel.mapErr((e) => console.error(e) ?? null).coalesce(),
        },
      ]),
    mutateLegacyData: () => undefined,
  },
};

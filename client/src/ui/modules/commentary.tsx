import React from 'react';
import { z } from 'zod';
import { projectSchema } from '../../data-schemas/project';

import { assertNever } from '../../helpers/assert-never';
import { Result } from '../../helpers/result';
import type { DeepPartial } from '../../helpers/safe-merge';
import { safeMerge } from '../../helpers/safe-merge';
import { TextInput } from '../input-components/text';
import { Textarea } from '../input-components/textarea';
import type { Dispatcher, UiModule } from '../module-management/module-type';
import type { GraphicsInput } from '../output-components/graphics';
import { Graphics, getGraphicsInput } from '../output-components/graphics';

type State = {
  brief: string;
  context: string;
  decisions: string;
  scenarios: Record<
    string,
    {
      number: number;
      name: string;
      description: string;
      graphicsInput: GraphicsInput;
    }
  >;
  baselineGraphics: GraphicsInput | null;
};

type Action =
  | { type: 'update state'; state: DeepPartial<State> }
  | { type: 'external data update'; state: State };

function Commentary({ state, dispatch }: { state: State; dispatch: Dispatcher<Action> }) {
  return (
    <section>
      <div className="mb-30">
        <label htmlFor="brief">
          <b>Initial project brief and scope</b>:
        </label>
        <Textarea
          id="brief"
          rows={6}
          style={{ width: '38rem' }}
          value={state.brief}
          onChange={(val) => dispatch({ type: 'update state', state: { brief: val } })}
        />
        <p className="text-italic">
          You should note here your understanding of the project brief and scope.
          <br />
          It should include aims and priorities - how big the project is, etc.
        </p>
      </div>

      <div className="mb-30">
        <label htmlFor="context">
          <b>Current context + logic of scenarios</b>:
        </label>

        <Textarea
          id="context"
          rows={6}
          style={{ width: '38rem' }}
          value={state.context}
          onChange={(val) => dispatch({ type: 'update state', state: { context: val } })}
        />
      </div>

      <h3>Baseline</h3>
      {state.baselineGraphics !== null && <Graphics input={state.baselineGraphics} />}

      {Object.entries(state.scenarios).map(
        ([id, { name, number, description, graphicsInput }]) => (
          <section key={`commentary_${id}`} className="mb-30 line-top">
            <h3 className="mt-0">
              Scenario {number}: {name}
            </h3>

            <Graphics input={graphicsInput} />

            <label htmlFor={`name_${id}`}>
              <b>Name:</b>
            </label>
            <TextInput
              id={`name_${id}`}
              value={name}
              onChange={(val) =>
                dispatch({
                  type: 'update state',
                  state: { scenarios: { [id]: { name: val } } },
                })
              }
            />

            <label htmlFor={`description_${id}`}>
              <b>Description:</b>
            </label>
            <Textarea
              id={`description_${id}`}
              rows={6}
              style={{ width: '38rem' }}
              value={description}
              onChange={(val) =>
                dispatch({
                  type: 'update state',
                  state: { scenarios: { [id]: { description: val } } },
                })
              }
            />
          </section>
        ),
      )}

      <div className="mb-30">
        <label htmlFor="decisions">
          <b>
            Key decisions to be made / risks and constraints / areas for further
            investigation and development
          </b>
          :
        </label>

        <Textarea
          id="decisions"
          rows={6}
          style={{ width: '38rem' }}
          value={state.decisions}
          onChange={(val) =>
            dispatch({ type: 'update state', state: { decisions: val } })
          }
        />
      </div>
    </section>
  );
}

export const commentaryModule: UiModule<State, Action, never> = {
  name: 'commentary',
  component: Commentary,
  initialState: () => {
    return {
      brief: '',
      context: '',
      decisions: '',
      scenarios: {},
      baselineGraphics: null,
    };
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'update state':
        return [safeMerge(state, action.state)];
      case 'external data update':
        return [action.state];
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ project }) => {
      const scenarios: State['scenarios'] = Object.fromEntries(
        Object.entries(project.data)
          .filter(([scenarioId]) => scenarioId !== 'master')
          .map(([scenarioId, data]) => [
            scenarioId,
            {
              number: parseInt(scenarioId.charAt(8), 10),
              name: data?.scenario_name ?? '',
              description: data?.scenario_description ?? '',
              graphicsInput: getGraphicsInput(scenarioId, data),
            },
          ]),
      );

      const baselineGraphics = getGraphicsInput('master', project.data['master']);
      if (baselineGraphics === undefined) {
        return Result.err(new Error('No baseline in project'));
      }

      return Result.ok([
        {
          type: 'external data update',
          state: {
            brief: project.data['master']?.household?.commentary.brief ?? '',
            context: project.data['master']?.household?.commentary.context ?? '',
            decisions: project.data['master']?.household?.commentary.decisions ?? '',
            baselineGraphics,
            scenarios,
          },
        },
      ]);
    },
    mutateLegacyData: ({ project: projectRaw }, _context, state) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;

      project.data['master'] = project.data['master'] ?? {};
      project.data['master'].household = project.data['master'].household ?? {};
      project.data['master'].household.commentary_brief = state.brief;
      project.data['master'].household.commentary_context = state.context;
      project.data['master'].household.commentary_decisions = state.decisions;

      for (const scenarioId of Object.keys(project.data)) {
        const stateData = state.scenarios[scenarioId];
        if (stateData === undefined) {
          continue;
        }

        const data = project.data[scenarioId] ?? {};
        data.scenario_name = stateData.name;
        data.scenario_description = stateData.description;
        project.data[scenarioId] = data;
      }
    },
  },
};

import React from 'react';

import { max, omit, sortBy } from 'lodash';
import { z } from 'zod';
import type {
  VentOrFlue,
  VentOrFlueMeasure,
} from '../../../data-schemas/libraries/intentional-vents-and-flues';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { DeleteIcon, EditIcon, HammerIcon, PlusIcon, X } from '../../icons';
import { Button } from '../../input-components/button';
import {
  SelectVentOrFlue,
  SelectVentOrFlueMeasure,
} from '../../input-components/library-picker/vents-and-flues';
import { NumberInput } from '../../input-components/number';
import { TextInput } from '../../input-components/text';
import { NumberOutput } from '../../output-components/numeric';
import { colourForStatus } from '../fabric/components/measure-status';

export type VentsFluesAction =
  | { type: 'vf/external update'; state: Pick<VentsFluesState, 'items'> }
  | { type: 'vf/show modal'; modal: VentsFluesState['modal'] }
  | {
      type: 'vf/add';
      item: VentOrFlue;
    }
  | {
      type: 'vf/modify';
      id: number;
      update: Partial<Item['inputs']>;
    }
  | {
      type: 'vf/apply measure';
      ids: number[] | null;
      measure: VentOrFlueMeasure;
    }
  | { type: 'vf/delete'; id: number };

export type VentsFluesState = {
  modal:
    | {
        type: 'picker';
      }
    | {
        type: 'measure picker';
        replaces: number | null;
      }
    | null;
  items: Item[];
};

export const initialVentsFluesState: VentsFluesState = { modal: null, items: [] };

type BaseItem = {
  type: 'item';
  id: number;
  inputs: {
    location: string;
    overrideVentilationRate: boolean;
    ventilationRate: number | null;
  };
  libraryItem: VentOrFlue;
};
type MeasureItem = {
  type: 'measure';
  id: number;
  inputs: {
    location: string;
    overrideVentilationRate: boolean;
    ventilationRate: number | null;
  };
  libraryItem: VentOrFlueMeasure;
};
type Item = BaseItem | MeasureItem;

export function ventsFluesReducer(
  state: VentsFluesState,
  action: VentsFluesAction,
): VentsFluesState {
  switch (action.type) {
    case 'vf/external update':
      return { ...state, ...action.state };
    case 'vf/show modal':
      return { modal: action.modal, items: state.items };
    case 'vf/add': {
      const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
      state.items.push({
        type: 'item',
        id: nextId,
        inputs: {
          location: '',
          overrideVentilationRate: false,
          ventilationRate: null,
        },
        libraryItem: action.item,
      });
      state.modal = null;
      return state;
    }
    case 'vf/modify': {
      for (const item of state.items) {
        if (item.id !== action.id) {
          continue;
        }
        item.inputs = {
          ...item.inputs,
          ...action.update,
        };
      }
      return state;
    }
    case 'vf/apply measure': {
      if (action.ids !== null) {
        const ids = action.ids;
        return {
          items: state.items.map((item) =>
            ids.includes(item.id)
              ? {
                  type: 'measure',
                  id: item.id,
                  inputs: {
                    location: item.inputs.location,
                    overrideVentilationRate: false,
                    ventilationRate: null,
                  },
                  libraryItem: action.measure,
                }
              : item,
          ),
          modal: null,
        };
      } else {
        const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
        state.items.push({
          type: 'measure',
          id: nextId,
          inputs: {
            location: '',
            overrideVentilationRate: false,
            ventilationRate: action.measure.ventilation_rate,
          },
          libraryItem: action.measure,
        });
        state.modal = null;
      }
      return state;
    }
    case 'vf/delete':
      return {
        modal: state.modal,
        items: state.items.filter((facility) => facility.id !== action.id),
      };
  }
}

export function ventsFluesMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: VentsFluesState,
): void {
  scenario.ventilation = scenario.ventilation ?? {};
  scenario.ventilation.IVF = state.items.map((item) => ({
    id: item.id,
    tag: item.libraryItem.tag,
    name: item.libraryItem.name,
    source: item.libraryItem.source,
    overrideVentilationRate: item.inputs.overrideVentilationRate,
    ventilation_rate: item.inputs.overrideVentilationRate
      ? (item.inputs.ventilationRate ?? '')
      : item.libraryItem.ventilation_rate,
    location: item.inputs.location,
    libraryItem: item.libraryItem,
  }));

  scenario.measures = scenario.measures ?? {};
  scenario.measures.ventilation = scenario.measures.ventilation ?? {};
  scenario.measures.ventilation.intentional_vents_and_flues_measures = Object.fromEntries(
    state.items
      .filter(
        (item): item is Extract<Item, { type: 'measure' }> => item.type === 'measure',
      )
      .map((item) => [
        item.id,
        {
          measure: {
            id: item.id,
            location: item.inputs.location,
            ...item.libraryItem,
            quantity: 1,
            cost_total: item.libraryItem.cost,
          },
        },
      ]),
  );
}

export function ventsFluesExtractor(scenario: Scenario): VentsFluesAction[] {
  const basic: Item[] =
    scenario?.ventilation?.IVF?.map((item) => {
      if (item.id === '') {
        throw new Error('id was not a number');
      }
      return {
        type: 'item' as const,
        id: item.id,
        inputs: {
          location: item.location,
          overrideVentilationRate: item.overrideVentilationRate,
          ventilationRate: coalesceEmptyString(item.ventilation_rate, 0),
        },
        libraryItem: item.libraryItem ?? {
          tag: item.tag,
          name: item.name,
          source: item.source,
          ventilation_rate: coalesceEmptyString(item.ventilation_rate, 0) ?? 0,
        },
      } satisfies BaseItem;
    }) ?? [];

  type BulkyMeasureItem = Omit<MeasureItem, 'inputs' | 'id'> & { id: number[] };

  const measures: BulkyMeasureItem[] = Object.entries(
    scenario?.measures?.ventilation?.intentional_vents_and_flues_measures ?? {},
  ).map(([id, item]) => {
    const parsedId = parseInt(id, 10);
    const base = basic.find((base) => base.id === parsedId);
    const isBulkMeasure =
      'original_elements' in item && item.original_elements !== undefined;
    if (base === undefined && !isBulkMeasure) {
      throw new Error(
        'vent/flue measure has no corresponding base item and is not bulk measure',
      );
    }
    return {
      type: 'measure' as const,
      id: isBulkMeasure
        ? Object.keys(item.original_elements ?? {}).map((id) => parseInt(id, 10))
        : [parsedId],
      libraryItem: {
        ...omit(item.measure, ['ventilation_rate', 'cost_total', 'quantity']),
        ventilation_rate: coalesceEmptyString(item.measure.ventilation_rate, 0),
      },
    };
  });

  for (const item of basic) {
    const measure = measures.find((measure) => measure.id.includes(item.id));
    if (measure === undefined) continue;

    item.type = 'measure';
    item.libraryItem = measure.libraryItem;
  }

  return [
    {
      type: 'vf/external update',
      state: { items: sortBy(basic, ['id']) },
    },
  ];
}

export function VentsFlues({
  state,
  dispatch,
  isBaseline,
}: {
  state: VentsFluesState;
  dispatch: (a: VentsFluesAction) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="mb-45">
      <h4 className="ma-0 mb-15">Vents and flues</h4>

      {state.items.length > 0 && (
        <table className="table mt-15 table--vertical-middle" style={{ width: 'auto' }}>
          <thead>
            <tr style={{ backgroundColor: 'var(--grey-800)' }}>
              <th>Tag</th>
              <th>Name</th>
              <th>Location</th>
              <th className="text-nowrap">Ventilation rate</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {state.items.map(({ id, type, inputs, libraryItem }) => (
              <tr key={id}>
                <td className="text-nowrap">
                  {libraryItem.tag}
                  {type === 'measure' && (
                    <span
                      className="circle ml-7"
                      title="Measure applied"
                      style={{
                        backgroundColor: `var(${colourForStatus['new measure']})`,
                      }}
                    />
                  )}
                </td>
                <td>{libraryItem.name}</td>
                <td>
                  <TextInput
                    value={inputs.location}
                    onChange={(location) =>
                      dispatch({ type: 'vf/modify', id, update: { location } })
                    }
                    className="mb-0"
                  />
                </td>
                <td className="text-right text-tabular-nums">
                  {inputs.overrideVentilationRate ? (
                    <span className="d-flex gap-7 justify-content-end">
                      <NumberInput
                        className="mb-0"
                        value={inputs.ventilationRate}
                        style={{ width: '2rem' }}
                        onChange={(ventilationRate) =>
                          dispatch({ type: 'vf/modify', id, update: { ventilationRate } })
                        }
                        unit="m³/h"
                      />
                      <Button
                        icon={X}
                        onClick={() =>
                          dispatch({
                            type: 'vf/modify',
                            id,
                            update: { overrideVentilationRate: false },
                          })
                        }
                      />
                    </span>
                  ) : (
                    <span className="d-flex gap-7 justify-content-end">
                      <NumberOutput value={libraryItem.ventilation_rate} unit="m³/h" />
                      <Button
                        icon={EditIcon}
                        onClick={() =>
                          dispatch({
                            type: 'vf/modify',
                            id,
                            update: { overrideVentilationRate: true },
                          })
                        }
                      />
                    </span>
                  )}
                </td>
                <td className="text-nowrap">
                  <div className="d-flex gap-7 justify-content-end">
                    {!isBaseline && (
                      <Button
                        icon={HammerIcon}
                        title={type === 'measure' ? 'Replace measure' : 'Apply measure'}
                        onClick={() =>
                          dispatch({
                            type: 'vf/show modal',
                            modal: { type: 'measure picker', replaces: id },
                          })
                        }
                      />
                    )}
                    <Button
                      icon={DeleteIcon}
                      onClick={() => dispatch({ type: 'vf/delete', id })}
                    />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}

      {state.modal?.type === 'picker' && (
        <SelectVentOrFlue
          onSelect={(item) => dispatch({ type: 'vf/add', item })}
          onClose={() => dispatch({ type: 'vf/show modal', modal: null })}
        />
      )}

      {state.modal?.type === 'measure picker' && (
        <SelectVentOrFlueMeasure
          onSelect={(measure) => {
            if (state.modal?.type !== 'measure picker') {
              return;
            }
            dispatch({
              type: 'vf/apply measure',
              ids: state.modal.replaces === null ? null : [state.modal.replaces],
              measure,
            });
          }}
          onClose={() => dispatch({ type: 'vf/show modal', modal: null })}
        />
      )}

      {isBaseline ? (
        <Button
          title="New vent or flue"
          icon={PlusIcon}
          onClick={() => dispatch({ type: 'vf/show modal', modal: { type: 'picker' } })}
        />
      ) : (
        <Button
          title="New vent or flue measure"
          icon={HammerIcon}
          onClick={() =>
            dispatch({
              type: 'vf/show modal',
              modal: { type: 'measure picker', replaces: null },
            })
          }
        />
      )}
    </section>
  );
}

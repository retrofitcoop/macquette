import React from 'react';

import { max, omit, sortBy } from 'lodash';
import { z } from 'zod';
import { ClothesDryingMeasure } from '../../../data-schemas/libraries/clothes-drying-facilities';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { DeleteIcon, HammerIcon, PlusIcon } from '../../icons';
import { Button } from '../../input-components/button';
import { SelectClothesDryingFacility } from '../../input-components/library-picker/clothes-drying';
import { colourForStatus } from '../fabric/components/measure-status';
export type ClothesAction =
  | { type: 'clothes/external update'; state: Pick<ClothesState, 'items'> }
  | { type: 'clothes/show modal'; modal: ClothesState['modal'] }
  | {
      type: 'clothes/add';
      item: ClothesDryingMeasure;
      as: 'measure' | 'item';
    }
  | { type: 'clothes/delete'; id: number };

export type ClothesState = {
  modal: 'picker' | 'measure picker' | null;
  items: ClothesDryingFacility[];
};

export const initialClothesState: ClothesState = { modal: null, items: [] };

export type ClothesDryingFacility =
  | {
      type: 'item';
      id: number;
      data: {
        tag: string;
        name: string;
      };
    }
  | {
      type: 'measure';
      id: number;
      data: ClothesDryingMeasure;
    };

export function clothesReducer(state: ClothesState, action: ClothesAction): ClothesState {
  switch (action.type) {
    case 'clothes/external update':
      return { ...state, ...action.state };
    case 'clothes/show modal':
      return { modal: action.modal, items: state.items };
    case 'clothes/add': {
      const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
      if (action.as === 'measure') {
        state.items.push({
          type: 'measure',
          id: nextId,
          data: action.item,
        });
      } else {
        state.items.push({
          type: 'item',
          id: nextId,
          data: { tag: action.item.tag, name: action.item.name },
        });
      }
      state.modal = null;
      return state;
    }
    case 'clothes/delete':
      return {
        modal: state.modal,
        items: state.items.filter((facility) => facility.id !== action.id),
      };
  }
}

export function clothesMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: ClothesState,
): void {
  scenario.ventilation = scenario.ventilation ?? {};
  scenario.ventilation.CDF = state.items.map((item) => ({
    id: item.id,
    tag: item.data.tag,
    name: item.data.name,
  }));

  scenario.measures = scenario.measures ?? {};
  scenario.measures.ventilation = scenario.measures.ventilation ?? {};
  scenario.measures.ventilation.clothes_drying_facilities = Object.fromEntries(
    state.items
      .filter(
        (item): item is Extract<ClothesDryingFacility, { type: 'measure' }> =>
          item.type === 'measure',
      )
      .map((item) => [
        item.id,
        {
          measure: {
            id: item.id,
            ...item.data,
            quantity: 1,
            cost_total: item.data.cost,
          },
        },
      ]),
  );
}

export function clothesExtractor(scenario: Scenario): ClothesAction[] {
  const basic: ClothesDryingFacility[] =
    scenario?.ventilation?.CDF?.map((item) => ({
      type: 'item' as const,
      id: item.id,
      data: {
        tag: item.tag,
        name: item.name,
      },
    })) ?? [];

  const measures: ClothesDryingFacility[] = Object.values(
    scenario?.measures?.ventilation?.clothes_drying_facilities ?? {},
  ).map((item) => {
    return {
      type: 'measure' as const,
      id: item.measure.id,
      data: omit(item.measure, ['id']),
    } satisfies ClothesDryingFacility;
  });

  // Combine
  const measureIds = measures.map((measure) => measure.id);
  for (const item of basic) {
    if (!measureIds.includes(item.id)) {
      measures.push(item);
    }
  }

  return [
    {
      type: 'clothes/external update',
      state: { items: sortBy(measures, ['id']) },
    },
  ];
}

export function ClothesDryingFacilities({
  state,
  dispatch,
  isBaseline,
}: {
  state: ClothesState;
  dispatch: (a: ClothesAction) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="line-top mb-45">
      <h3 className="ma-0 mb-15">Clothes drying facilities</h3>
      {state.items.length > 0 && (
        <table className="table mt-15 table--vertical-middle" style={{ width: 'auto' }}>
          <thead>
            <tr style={{ backgroundColor: 'var(--grey-800)' }}>
              <th>Tag</th>
              <th>Name</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {state.items.map(({ id, type, data }) => (
              <tr key={id}>
                <td>
                  {data.tag}
                  {type === 'measure' && (
                    <span
                      className="circle ml-7"
                      title="Measure applied"
                      style={{
                        backgroundColor: `var(${colourForStatus['new measure']})`,
                      }}
                    />
                  )}
                </td>
                <td>{data.name}</td>
                <td>
                  <Button
                    title="Delete"
                    icon={DeleteIcon}
                    onClick={() => dispatch({ type: 'clothes/delete', id })}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}

      {state.modal === 'picker' && (
        <SelectClothesDryingFacility
          forMeasure={false}
          onSelect={(item) => dispatch({ type: 'clothes/add', as: 'item', item })}
          onClose={() => dispatch({ type: 'clothes/show modal', modal: null })}
        />
      )}

      {state.modal === 'measure picker' && (
        <SelectClothesDryingFacility
          forMeasure={true}
          onSelect={(item) => dispatch({ type: 'clothes/add', as: 'measure', item })}
          onClose={() => dispatch({ type: 'clothes/show modal', modal: null })}
        />
      )}

      {isBaseline ? (
        <Button
          title="New clothes drying facility"
          icon={PlusIcon}
          onClick={() => dispatch({ type: 'clothes/show modal', modal: 'picker' })}
        />
      ) : (
        <Button
          title="New clothes drying facility measure"
          icon={HammerIcon}
          onClick={() =>
            dispatch({ type: 'clothes/show modal', modal: 'measure picker' })
          }
        />
      )}
    </section>
  );
}

import React from 'react';

import { z } from 'zod';
import { DraughtProofingMeasure } from '../../../data-schemas/libraries/draught-proofing-measures';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { DeepPartial, safeMerge } from '../../../helpers/safe-merge';
import { Model } from '../../../model/model';
import { EditIcon, HammerIcon, X } from '../../icons';
import { Button } from '../../input-components/button';
import { CheckboxInput } from '../../input-components/checkbox';
import { FormGrid } from '../../input-components/forms';
import { SelectDraughtProofingMeasure } from '../../input-components/library-picker/draughtproofing';
import { NumberInput } from '../../input-components/number';
import { Select } from '../../input-components/select';
import { NumberOutput } from '../../output-components/numeric';

export type StructuralInfiltrationAction =
  | {
      type: 'si/external update';
      state: Pick<StructuralInfiltrationState, 'model' | 'inputs'>;
    }
  | { type: 'si/show modal'; modal: StructuralInfiltrationState['modal'] }
  | {
      type: 'si/modify';
      update: DeepPartial<StructuralInfiltrationState['inputs']>;
    }
  | {
      type: 'si/apply measure';
      measure: DraughtProofingMeasure;
    };

export type StructuralInfiltrationState = {
  inputs: {
    sidesSheltered: number | null;
    estimateFrom:
      | {
          type: 'fabric elements';
          numberOfFloorsOverride: boolean;
          numberOfFloorsOverrideValue: number | null;
          walls: 'timber' | 'masonry' | null;
          floor: 'suspended sealed' | 'suspended unsealed' | 'solid' | null;
          draughtProofedPercentage: number | null;
          draughtLobby: boolean;
        }
      | {
          type: 'pressure test';
          airPermeability: number | null;
        };
    measure: DraughtProofingMeasure | null;
  };
  model: Model | null;
  modal: { type: 'measure picker' } | null;
};

export const initialStructuralInfiltrationState: StructuralInfiltrationState = {
  inputs: {
    sidesSheltered: null,
    estimateFrom: {
      type: 'pressure test',
      airPermeability: null,
    },
    measure: null,
  },
  model: null,
  modal: null,
};

export function structuralInfiltrationReducer(
  state: StructuralInfiltrationState,
  action: StructuralInfiltrationAction,
): StructuralInfiltrationState {
  switch (action.type) {
    case 'si/external update':
      return { ...state, ...action.state };
    case 'si/show modal':
      return { ...state, modal: action.modal };
    case 'si/modify':
      return { ...state, inputs: safeMerge(state.inputs, action.update) };
    case 'si/apply measure': {
      let airPermeability = action.measure.q50;
      if (airPermeability < 0) {
        if (state.inputs.estimateFrom.type === 'pressure test') {
          airPermeability += state.inputs.estimateFrom.airPermeability ?? 0;
        }
      }
      return {
        ...state,
        inputs: {
          ...state.inputs,
          estimateFrom: { type: 'pressure test', airPermeability },
          measure: action.measure,
        },
        modal: null,
      };
    }
  }
}

export function structuralInfiltrationMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: StructuralInfiltrationState,
): void {
  scenario.ventilation ??= {};
  scenario.ventilation.number_of_sides_sheltered =
    state.inputs.sidesSheltered ?? undefined;

  if (state.inputs.estimateFrom.type === 'fabric elements') {
    switch (state.inputs.estimateFrom.walls) {
      case null:
        scenario.ventilation.dwelling_construction = undefined;
        break;
      case 'timber':
        scenario.ventilation.dwelling_construction = 'timberframe';
        break;
      case 'masonry':
        scenario.ventilation.dwelling_construction = 'masonry';
        break;
    }

    switch (state.inputs.estimateFrom.floor) {
      case null:
        scenario.ventilation.suspended_wooden_floor = undefined;
        break;
      case 'solid':
        scenario.ventilation.suspended_wooden_floor = 0;
        break;
      case 'suspended sealed':
        scenario.ventilation.suspended_wooden_floor = 'sealed';
        break;
      case 'suspended unsealed':
        scenario.ventilation.suspended_wooden_floor = 'unsealed';
        break;
    }

    scenario.ventilation.air_permeability_test = false;
    scenario.ventilation.percentage_draught_proofed =
      state.inputs.estimateFrom.draughtProofedPercentage ?? undefined;
    scenario.ventilation.draught_lobby = state.inputs.estimateFrom.draughtLobby;

    if (state.inputs.estimateFrom.numberOfFloorsOverride) {
      scenario.num_of_floors_override =
        state.inputs.estimateFrom.numberOfFloorsOverrideValue ?? undefined;
    } else {
      delete scenario.num_of_floors_override;
    }
  } else {
    scenario.ventilation.air_permeability_test = true;
    scenario.ventilation.air_permeability_value =
      state.inputs.estimateFrom.airPermeability ?? undefined;
  }

  if (state.inputs.measure !== null) {
    scenario.measures ??= {};
    scenario.measures.ventilation ??= {};
    scenario.measures.ventilation.draught_proofing_measures = {
      measure: {
        ...state.inputs.measure,
        quantity: 1,
        cost_total: state.inputs.measure.cost,
      },
    };
  } else {
    delete scenario.measures?.ventilation?.draught_proofing_measures;
  }
}

export function structuralInfiltrationExtractor(
  scenario: Scenario,
  model: Model | null,
): StructuralInfiltrationAction[] {
  const { ventilation, measures } = scenario ?? {};
  let estimateFrom: StructuralInfiltrationState['inputs']['estimateFrom'];
  if (ventilation?.air_permeability_test === true) {
    estimateFrom = {
      type: 'pressure test',
      airPermeability:
        coalesceEmptyString(ventilation?.air_permeability_value, null) ?? null,
    };
  } else {
    const { dwelling_construction, suspended_wooden_floor } = ventilation ?? {};
    function interpretWalls(legacyValue: typeof dwelling_construction) {
      switch (legacyValue) {
        case undefined:
          return null;
        case 'timberframe':
          return 'timber';
        case 'masonry':
          return 'masonry';
      }
    }
    function interpretFloor(legacyValue: typeof suspended_wooden_floor) {
      switch (legacyValue) {
        case undefined:
          return null;
        case 0:
          return 'solid';
        case 'sealed':
          return 'suspended sealed';
        case 'unsealed':
          return 'suspended unsealed';
      }
    }
    estimateFrom = {
      type: 'fabric elements',
      numberOfFloorsOverride:
        scenario !== undefined ? 'num_of_floors_override' in scenario : false,
      numberOfFloorsOverrideValue: scenario?.num_of_floors_override ?? null,
      walls: interpretWalls(dwelling_construction),
      floor: interpretFloor(suspended_wooden_floor),
      draughtProofedPercentage: coalesceEmptyString(
        ventilation?.percentage_draught_proofed ?? null,
        null,
      ),
      draughtLobby: ventilation?.draught_lobby ?? false,
    };
  }

  const measure = measures?.ventilation?.draught_proofing_measures?.measure ?? null;
  const typedMeasure: DraughtProofingMeasure | null =
    measure === null
      ? null
      : {
          ...measure,
          q50: coalesceEmptyString(measure?.q50 ?? 0, 0),
        };

  return [
    {
      type: 'si/external update',
      state: {
        model,
        inputs: {
          sidesSheltered: ventilation?.number_of_sides_sheltered ?? null,
          estimateFrom,
          measure: typedMeasure,
        },
      },
    },
  ];
}

export function StructuralInfiltration({
  state,
  dispatch,
  isBaseline,
}: {
  state: StructuralInfiltrationState;
  dispatch: (a: StructuralInfiltrationAction) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="mb-45">
      <h4 className="ma-0 mb-15">Structural infiltration</h4>

      <FormGrid>
        <span>Structural infiltration</span>
        <span>
          <NumberOutput
            value={state.model?.normal.infiltration?.withShelterFactorAirChangesPerHour}
            unit="ACH"
          />
        </span>

        <label htmlFor="sheltered">Number of sides sheltered</label>
        <span>
          <NumberInput
            id="sheltered"
            value={state.inputs.sidesSheltered}
            onChange={(sidesSheltered) =>
              dispatch({
                type: 'si/modify',
                update: { sidesSheltered },
              })
            }
          />
        </span>

        <label htmlFor="source">Infiltration rate from</label>
        <span>
          {state.inputs.measure !== null ? (
            `Measure (${state.inputs.measure.tag}: ${state.inputs.measure.name})`
          ) : (
            <Select
              id="source"
              className="input--auto-width"
              value={state.inputs.estimateFrom.type}
              options={[
                { value: 'fabric elements', display: 'SAP calculation' },
                { value: 'pressure test', display: 'Airtightness test' },
              ]}
              onChange={(type) =>
                dispatch({
                  type: 'si/modify',
                  update: { estimateFrom: { type } },
                })
              }
            />
          )}
        </span>

        {state.inputs.estimateFrom.type === 'pressure test' &&
          (state.inputs.measure !== null ? (
            <>
              <span>AP50</span>
              <NumberOutput
                value={state.inputs.estimateFrom.airPermeability}
                unit="m³/hour/m² envelope area"
              />
            </>
          ) : (
            <>
              <label htmlFor="ap50">AP50</label>
              <NumberInput
                id="ap50"
                value={state.inputs.estimateFrom.airPermeability}
                unit="m³/hour/m² envelope area"
                onChange={(airPermeability) =>
                  dispatch({
                    type: 'si/modify',
                    update: { estimateFrom: { airPermeability } },
                  })
                }
              />
            </>
          ))}

        {state.inputs.estimateFrom.type === 'fabric elements' &&
          (state.inputs.estimateFrom.numberOfFloorsOverride ? (
            <>
              <label htmlFor="storeys">Number of storeys</label>
              <span className="d-flex gap-7">
                <NumberInput
                  id="storeys"
                  className="mb-0"
                  value={state.inputs.estimateFrom.numberOfFloorsOverrideValue}
                  style={{ width: '2rem' }}
                  onChange={(numberOfFloorsOverrideValue) =>
                    dispatch({
                      type: 'si/modify',
                      update: { estimateFrom: { numberOfFloorsOverrideValue } },
                    })
                  }
                />
                <Button
                  icon={X}
                  title={`Use assumed number (${
                    state.model?.normal.floors.numberOfFloors ?? '-'
                  }) instead`}
                  onClick={() =>
                    dispatch({
                      type: 'si/modify',
                      update: { estimateFrom: { numberOfFloorsOverride: false } },
                    })
                  }
                />
              </span>
            </>
          ) : (
            <>
              <span>Number of storeys</span>
              <span className="d-flex gap-7">
                <NumberOutput value={state.model?.normal.floors.numberOfFloors} />
                <Button
                  icon={EditIcon}
                  title="Override"
                  onClick={() =>
                    dispatch({
                      type: 'si/modify',
                      update: { estimateFrom: { numberOfFloorsOverride: true } },
                    })
                  }
                />
              </span>
            </>
          ))}

        {state.inputs.estimateFrom.type === 'fabric elements' && (
          <>
            <label htmlFor="wall-construction">Wall construction</label>
            <span>
              <Select
                id="wall-construction"
                className="input--auto-width"
                value={state.inputs.estimateFrom.walls}
                options={[
                  { value: 'timber', display: 'timber frame (+0.2)' },
                  { value: 'masonry', display: 'masonry (+0.35)' },
                ]}
                onChange={(walls) =>
                  dispatch({
                    type: 'si/modify',
                    update: { estimateFrom: { walls } },
                  })
                }
              />
            </span>

            <label htmlFor="floor-construction">Floor construction</label>
            <span>
              <Select
                id="floor-construction"
                className="input--auto-width"
                value={state.inputs.estimateFrom.floor}
                options={[
                  {
                    value: 'suspended unsealed',
                    display: 'suspended unsealed wooden floor (+0.2)',
                  },
                  {
                    value: 'suspended sealed',
                    display: 'suspended sealed wooden floor (+0.1)',
                  },
                  { value: 'solid', display: 'solid floor (+0)' },
                ]}
                onChange={(floor) =>
                  dispatch({
                    type: 'si/modify',
                    update: { estimateFrom: { floor } },
                  })
                }
              />
            </span>

            <label htmlFor="draughtproofed">Windows and doors</label>
            <NumberInput
              id="draughtproofed"
              value={state.inputs.estimateFrom.draughtProofedPercentage}
              unit="% draughtpoofed"
              error={
                state.inputs.estimateFrom.draughtProofedPercentage !== null &&
                (state.inputs.estimateFrom.draughtProofedPercentage < 0 ||
                  state.inputs.estimateFrom.draughtProofedPercentage > 100)
                  ? 'should be between 0 and 100'
                  : undefined
              }
              onChange={(draughtProofedPercentage) =>
                dispatch({
                  type: 'si/modify',
                  update: { estimateFrom: { draughtProofedPercentage } },
                })
              }
            />

            <label htmlFor="draught-lobby">Has draught lobby?</label>
            <CheckboxInput
              id="draught-lobby"
              value={state.inputs.estimateFrom.draughtLobby}
              onChange={(draughtLobby) =>
                dispatch({
                  type: 'si/modify',
                  update: { estimateFrom: { draughtLobby } },
                })
              }
            />

            <span>Equivalent AP50</span>
            <NumberOutput
              value={state.model?.normal.infiltration?.airPermeability ?? null}
              unit="m³/hour/m² envelope area"
            />
          </>
        )}
      </FormGrid>

      {!isBaseline && (
        <Button
          title={`${
            state.inputs.measure === null ? 'Apply' : 'Replace'
          } draughtproofing measure`}
          icon={HammerIcon}
          onClick={() =>
            dispatch({ type: 'si/show modal', modal: { type: 'measure picker' } })
          }
        />
      )}

      {state.modal?.type === 'measure picker' && (
        <SelectDraughtProofingMeasure
          onSelect={(measure) => dispatch({ type: 'si/apply measure', measure })}
          onClose={() => dispatch({ type: 'si/show modal', modal: null })}
        />
      )}
    </section>
  );
}

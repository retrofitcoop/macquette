import React from 'react';

import { z } from 'zod';
import {
  VentilationSystem,
  VentilationSystemMeasure,
} from '../../../data-schemas/libraries/ventilation-system';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { safeMerge } from '../../../helpers/safe-merge';
import { calcMeasureQtyAndCost } from '../../../measures';
import { Model } from '../../../model/model';
import { HammerIcon } from '../../icons';
import { Button } from '../../input-components/button';
import { FormGrid } from '../../input-components/forms';
import {
  SelectVentilationSystem,
  SelectVentilationSystemMeasure,
} from '../../input-components/library-picker/ventilation-systems';
import { NumberInput } from '../../input-components/number';
import { colourForStatus } from '../fabric/components/measure-status';

export type VentilationSystemAction =
  | {
      type: 'sys/external update';
      state: Pick<VentilationSystemState, 'system' | 'model'>;
    }
  | { type: 'sys/show modal'; modal: VentilationSystemState['modal'] }
  | {
      type: 'sys/replace';
      libraryItem: VentilationSystem;
    }
  | {
      type: 'sys/modify';
      update: Partial<System['inputs']>;
    }
  | {
      type: 'sys/apply measure';
      measure: VentilationSystemMeasure;
    };

export type System =
  | {
      type: 'base';
      libraryItem: VentilationSystem;
      inputs: {
        airChangeRate: number | null;
        specificFanPower: number | null;
        heatRecoveryEfficiency: number | null;
      };
    }
  | {
      type: 'measure';
      libraryItem: VentilationSystemMeasure;
      inputs: {
        airChangeRate: number | null;
        specificFanPower: number | null;
        heatRecoveryEfficiency: number | null;
      };
    };

export type VentilationSystemState = {
  modal: { type: 'picker' } | { type: 'measure picker' } | null;
  model: Model | null;
  system: System;
};

export const initialVentilationSystemState: VentilationSystemState = {
  modal: null,
  model: null,
  system: {
    type: 'base',
    libraryItem: {
      tag: '',
      name: '',
      source: '',
      ventilation_type: 'NV',
      specific_fan_power: null,
      system_air_change_rate: null,
      balanced_heat_recovery_efficiency: null,
    },
    inputs: {
      airChangeRate: null,
      specificFanPower: null,
      heatRecoveryEfficiency: null,
    },
  },
};

export function ventilationSystemReducer(
  state: VentilationSystemState,
  action: VentilationSystemAction,
): VentilationSystemState {
  switch (action.type) {
    case 'sys/external update':
      return { ...state, ...action.state };

    case 'sys/show modal':
      return { ...state, modal: action.modal };

    case 'sys/modify':
      return {
        ...state,
        system: {
          ...state.system,
          inputs: safeMerge(state.system.inputs, action.update),
        },
      };

    case 'sys/replace':
      return {
        ...state,
        modal: null,
        system: {
          type: 'base',
          libraryItem: action.libraryItem,
          inputs: {
            airChangeRate: action.libraryItem.system_air_change_rate,
            specificFanPower: action.libraryItem.specific_fan_power,
            heatRecoveryEfficiency: action.libraryItem.balanced_heat_recovery_efficiency,
          },
        },
      };

    case 'sys/apply measure':
      return {
        ...state,
        modal: null,
        system: {
          type: 'measure',
          libraryItem: action.measure,
          inputs: {
            airChangeRate: action.measure.system_air_change_rate,
            specificFanPower: action.measure.specific_fan_power,
            heatRecoveryEfficiency: action.measure.balanced_heat_recovery_efficiency,
          },
        },
      };
  }
}

export function ventilationSystemMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: VentilationSystemState,
): void {
  scenario ??= {};
  scenario.ventilation ??= {};

  scenario.ventilation.libraryItem = state.system.libraryItem;
  scenario.ventilation.ventilation_name = state.system.libraryItem.name;
  scenario.ventilation.ventilation_tag = state.system.libraryItem.tag;
  scenario.ventilation.ventilation_type = state.system.libraryItem.ventilation_type;

  scenario.ventilation.system_specific_fan_power = state.system.inputs.specificFanPower;
  scenario.ventilation.system_air_change_rate = state.system.inputs.airChangeRate;
  scenario.ventilation.balanced_heat_recovery_efficiency =
    state.system.inputs.heatRecoveryEfficiency;

  scenario.measures ??= {};
  scenario.measures.ventilation ??= {};

  if (state.system.type !== 'measure') {
    delete scenario.measures.ventilation.ventilation_systems_measures;
  } else {
    const [quantity, costTotal] = calcMeasureQtyAndCost({
      costUnits: state.system.libraryItem.cost_units,
      baseCost: state.system.libraryItem.min_cost,
      costPerUnit: state.system.libraryItem.cost,
      area: state.model?.normal.floors.totalFloorArea ?? 0,
      isExternalWallInsulation: false,
    });
    scenario.measures.ventilation.ventilation_systems_measures = {
      measure: {
        ...state.system.libraryItem,
        quantity,
        cost_total: costTotal,
      },
    };
  }
}

export function ventilationSystemExtractor(
  scenario: Scenario,
  model: Model | null,
): VentilationSystemAction[] {
  let libraryItem: VentilationSystem | null = scenario?.ventilation?.libraryItem ?? null;
  if (libraryItem === null) {
    libraryItem = {
      name: scenario?.ventilation?.ventilation_name ?? '',
      tag: scenario?.ventilation?.ventilation_tag ?? '',
      source: '',
      ventilation_type: scenario?.ventilation?.ventilation_type ?? 'NV',
      specific_fan_power:
        coalesceEmptyString(scenario?.ventilation?.system_specific_fan_power, null) ??
        null,
      system_air_change_rate:
        coalesceEmptyString(scenario?.ventilation?.system_air_change_rate, null) ?? null,
      balanced_heat_recovery_efficiency:
        coalesceEmptyString(
          scenario?.ventilation?.balanced_heat_recovery_efficiency,
          null,
        ) ?? null,
    };
  }

  const measure =
    scenario?.measures?.ventilation?.ventilation_systems_measures?.measure ?? null;
  if (measure === null) {
    return [
      {
        type: 'sys/external update',
        state: {
          model,
          system: {
            type: 'base',
            libraryItem,
            inputs: {
              airChangeRate: libraryItem.system_air_change_rate,
              specificFanPower: libraryItem.specific_fan_power,
              heatRecoveryEfficiency: libraryItem.balanced_heat_recovery_efficiency,
            },
          },
        },
      },
    ];
  } else {
    if (measure.cost_units !== 'unit' && measure.cost_units !== 'sqm') {
      console.error('Ventilation system measure had bad cost units');
      return [];
    } else {
      const measureTyped: VentilationSystemMeasure = {
        ...measure,
        cost_units: measure.cost_units,
      };
      return [
        {
          type: 'sys/external update',
          state: {
            model,
            system: {
              type: 'measure',
              libraryItem: measureTyped,
              inputs: {
                airChangeRate: libraryItem.system_air_change_rate,
                specificFanPower: libraryItem.specific_fan_power,
                heatRecoveryEfficiency: libraryItem.balanced_heat_recovery_efficiency,
              },
            },
          },
        },
      ];
    }
  }
}

export function VentilationSystem({
  state,
  dispatch,
  isBaseline,
}: {
  state: VentilationSystemState;
  dispatch: (a: VentilationSystemAction) => void;
  isBaseline: boolean;
}) {
  return (
    <div className="mb-30">
      <FormGrid>
        <span>System</span>
        <span>
          <span className="mr-7">
            {state.system.type === 'measure' && (
              <span
                className="circle mr-7"
                title="Measure applied"
                style={{ backgroundColor: `var(${colourForStatus['new measure']})` }}
              />
            )}
            {state.system.libraryItem.tag}: {state.system.libraryItem.name}
          </span>
          {isBaseline ? (
            <Button
              title="Replace system"
              onClick={() =>
                dispatch({ type: 'sys/show modal', modal: { type: 'picker' } })
              }
            />
          ) : (
            <Button
              title={
                state.system.type === 'measure' ? 'Replace measure' : 'Apply measure'
              }
              icon={HammerIcon}
              onClick={() =>
                dispatch({
                  type: 'sys/show modal',
                  modal: { type: 'measure picker' },
                })
              }
            />
          )}
        </span>

        {['NV', 'IE', 'PS'].includes(state.system.libraryItem.ventilation_type) ===
          false && (
          <>
            <label htmlFor="airChangeRate">Air change rate</label>
            <span>
              <NumberInput
                id="airChangeRate"
                value={state.system.inputs.airChangeRate}
                unit="ACH"
                onChange={(airChangeRate) =>
                  dispatch({
                    type: 'sys/modify',
                    update: { airChangeRate },
                  })
                }
              />
            </span>

            <label htmlFor="specificFanPower">Specific fan power</label>
            <span>
              <NumberInput
                id="specificFanPower"
                value={state.system.inputs.specificFanPower}
                unit="W/(litre.sec)"
                onChange={(specificFanPower) =>
                  dispatch({
                    type: 'sys/modify',
                    update: { specificFanPower },
                  })
                }
              />
            </span>
          </>
        )}

        {state.system.libraryItem.ventilation_type === 'MVHR' && (
          <>
            <label htmlFor="heatRecoveryEfficiency">Heat recovery efficiency</label>
            <span>
              <NumberInput
                id="heatRecoveryEfficiency"
                value={state.system.inputs.heatRecoveryEfficiency}
                unit="%"
                onChange={(heatRecoveryEfficiency) =>
                  dispatch({
                    type: 'sys/modify',
                    update: { heatRecoveryEfficiency },
                  })
                }
              />
            </span>
          </>
        )}
      </FormGrid>

      {state.modal?.type === 'picker' && (
        <SelectVentilationSystem
          onSelect={(libraryItem) => {
            dispatch({ type: 'sys/replace', libraryItem });
          }}
          onClose={() => dispatch({ type: 'sys/show modal', modal: null })}
        />
      )}

      {state.modal?.type === 'measure picker' && (
        <SelectVentilationSystemMeasure
          onSelect={(measure) => {
            dispatch({ type: 'sys/apply measure', measure: measure });
          }}
          showCost={true}
          areaSqm={state.model?.normal.floors.totalFloorArea ?? null}
          currentItemTag={state.system.libraryItem.tag}
          onClose={() => dispatch({ type: 'sys/show modal', modal: null })}
        />
      )}
    </div>
  );
}

import React, { useId } from 'react';
import { CheckboxInput } from '../../input-components/checkbox';
import { NumberInput } from '../../input-components/number';
import { RadioGroup } from '../../input-components/radio-group';
import { TextInput } from '../../input-components/text';
import { VStack } from '../../stacks';

type MultipleChoiceProps<T> = {
  label: string;
  choices: Array<T>;
  value: T | null;
  onChange: (val: T) => void;
};

export function MultipleChoice<T extends string>({
  label,
  choices,
  value,
  onChange,
}: MultipleChoiceProps<T>) {
  const labelId = useId();
  const options = choices.map((choice) => ({ value: choice, display: choice }));

  return (
    <VStack small>
      <span id={labelId}>{label}</span>

      <RadioGroup
        options={options}
        onChange={onChange}
        value={value}
        radioClasses={['mr-7', 'big-checkbox', 'flex-noshrink']}
        labelClasses={['mb-0']}
        ariaLabelledBy={labelId}
      />
    </VStack>
  );
}

type MultipleCheckboxProps<T> = {
  label: string;
  choices: Array<T>;
  value: T[];
  onChange: (val: T[]) => void;
};

export function MultipleCheckbox<T extends string>({
  label,
  choices,
  value,
  onChange,
}: MultipleCheckboxProps<T>) {
  const labelId = useId();

  return (
    <VStack small>
      <span>{label}</span>

      {choices.map((choice) => (
        <label
          className="radio d-flex mb-0 pl-0"
          htmlFor={`${labelId}-${choice}`}
          key={choice}
        >
          <CheckboxInput
            className="mr-7 big-checkbox"
            style={{ flexShrink: 0 }}
            id={`${labelId}-${choice}`}
            // SAFETY: data[id] is always Array[string] and so `includes` does not take
            // `never`.`
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            value={value.includes(choice) ?? false}
            onChange={(updateValue) => {
              onChange(
                updateValue === true
                  ? [...value, choice]
                  : value.filter((item) => item !== choice),
              );
            }}
          />
          {choice}
        </label>
      ))}
    </VStack>
  );
}

type YesNoFieldProps = {
  value: boolean | null;
  onChange: (b: boolean) => void;
  label: string;
  prompt?: string;
};

export function YesNoField({ label, value, onChange, prompt }: YesNoFieldProps) {
  const id = useId();

  return (
    <VStack small>
      <span>{label}</span>
      <label htmlFor={`${id}-yes`} className="mb-0">
        <input
          type="radio"
          className="mr-7 big-checkbox"
          id={`${id}-yes`}
          name={id}
          checked={value === true}
          onChange={() => onChange(true)}
        />
        yes
      </label>

      <label htmlFor={`${id}-no`} className="mb-0">
        <input
          type="radio"
          className="mr-7 big-checkbox"
          id={`${id}-no`}
          name={id}
          checked={value === false}
          onChange={() => onChange(false)}
        />
        no
      </label>

      {prompt !== undefined && <div className="text-italic">{prompt}</div>}
    </VStack>
  );
}

type NumberFieldProps = {
  value: number | null;
  onChange: (b: number | null) => void;
  label: string;
};

export function NumberField({ value, onChange, label }: NumberFieldProps) {
  const id = useId();

  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <NumberInput
        id={id}
        className="mb-0"
        style={{ width: '4ch' }}
        value={value}
        onChange={onChange}
      />
    </div>
  );
}

type TextFieldProps = {
  value: string | null;
  onChange: (val: string) => void;
  label: string;
  prompt?: string;
};

export function TextField({ label, value, onChange, prompt }: TextFieldProps) {
  const id = useId();

  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <TextInput id={id} className="mb-0" value={value ?? ''} onChange={onChange} />
      {prompt !== undefined && <div className="text-italic">{prompt}</div>}
    </div>
  );
}

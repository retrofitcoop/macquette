import React from 'react';
import { z } from 'zod';

import { projectSchema } from '../../../data-schemas/project';
import { assertNever } from '../../../helpers/assert-never';
import { Result } from '../../../helpers/result';
import { UnvalidatedTimePeriod } from '../../../periods/validation';
import { HeatingTable } from '../../input-components/heating-table';
import type { UiModule } from '../../module-management/module-type';

type State = {
  weekday: UnvalidatedTimePeriod[];
  weekend: UnvalidatedTimePeriod[];
};
type Action =
  | { type: 'update weekday'; weekday: UnvalidatedTimePeriod[] }
  | {
      type: 'update weekend';
      weekend: UnvalidatedTimePeriod[];
    };

export const heatingTimesModule: UiModule<State, Action, never> = {
  name: 'sandbox',
  component: function HeatingTimes({ state, dispatch }) {
    return (
      <div>
        <p>
          Your normal heating hours on a <b>weekday</b> in winter are:
        </p>
        <HeatingTable
          periods={state.weekday}
          onChange={(newPeriods) =>
            dispatch({ type: 'update weekday', weekday: newPeriods })
          }
        />

        <p>
          Your normal heating hours on a <b>weekend</b> in winter are:
        </p>
        <HeatingTable
          periods={state.weekend}
          onChange={(newPeriods) =>
            dispatch({ type: 'update weekend', weekend: newPeriods })
          }
        />
      </div>
    );
  },
  initialState: () => {
    return {
      weekday: [
        { start: null, end: null },
        { start: null, end: null },
        { start: null, end: null },
      ],
      weekend: [
        { start: null, end: null },
        { start: null, end: null },
        { start: null, end: null },
      ],
    };
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'update weekday': {
        return [{ ...state, weekday: action.weekday }];
      }
      case 'update weekend': {
        return [{ ...state, weekend: action.weekend }];
      }
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ project }) => {
      const baselineScenario = project.data['master'];
      const household = baselineScenario?.household;

      if (household === undefined) {
        return Result.ok([]);
      } else {
        return Result.ok([
          {
            type: 'update weekday',
            weekday: household.heatingHoursWeekday,
          },
          {
            type: 'update weekend',
            weekend: household.heatingHoursWeekend,
          },
        ]);
      }
    },
    mutateLegacyData: ({ project: projectRaw }, _, state) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      const data = project.data['master'];
      if (data === undefined) {
        console.error('Could not mutate legacy data as baseline scenario not found');
        return;
      }
      if (data.household === undefined) {
        data.household = {};
      }
      data.household.heatingHoursWeekday = state.weekday;
      data.household.heatingHoursWeekend = state.weekend;
    },
  },
};

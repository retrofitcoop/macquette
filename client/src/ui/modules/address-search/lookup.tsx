import React, { Dispatch, useReducer } from 'react';
import { HTTPClient } from '../../../api/http';
import type { AddressSuggestion, ResolvedAddress } from '../../../data-schemas/address';
import { TextInput } from '../../input-components/text';

type FetchStatus = 'at rest' | 'in progress' | 'error';

type SearchStage = {
  stage: 'enter search text';
  searchText: string;
  suggestionFetchStatus: FetchStatus;
};
type ChooseStage = {
  stage: 'choosing suggestion';
  searchText: string;
  suggestions: AddressSuggestion[];
  selected: AddressSuggestion | null;
  suggestionFetchStatus: FetchStatus;
  resolveFetchStatus: FetchStatus;
};
type ResolvedStage = { stage: 'address resolved' };

type State = SearchStage | ChooseStage | ResolvedStage;
function initialState(initiallyResolved: boolean): State {
  return initiallyResolved
    ? { stage: 'address resolved' }
    : {
        stage: 'enter search text' as const,
        searchText: '',
        suggestionFetchStatus: 'at rest' as const,
      };
}

function SearchTextSection({
  state,
  dispatch,
}: {
  state: SearchStage | ChooseStage;
  dispatch: Dispatch<Action>;
}) {
  return (
    <>
      <label htmlFor="address_search">
        Find address by postcode or first line of address
      </label>
      <TextInput
        id="address_search"
        value={state.searchText}
        onChange={(value) => dispatch({ type: 'new search text', value })}
        onKeyDown={(evt) => {
          // SAFETY: We know this is an <input>.
          // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
          const target = evt.target as HTMLInputElement;
          const searchText = target.value;
          if (evt.key === 'Enter' && searchText !== '') {
            dispatch({ type: 'new search text', value: searchText });
            findAddressSuggestions(dispatch, searchText).catch(console.error);
          }
        }}
        style={{ width: '20ch' }}
        className="mb-0"
      />
      <button
        className="btn btn-primary ml-7"
        onClick={() => {
          if (state.searchText === '') {
            alert('Please enter a postcode or the first line of an address.');
          } else {
            findAddressSuggestions(dispatch, state.searchText).catch(console.error);
          }
        }}
      >
        Lookup
      </button>
      {state.suggestionFetchStatus === 'in progress' && (
        <span className="spinner ml-7"></span>
      )}
      {state.suggestionFetchStatus === 'error' && (
        <span className="ml-7">Error fetching suggestions; please try again</span>
      )}
    </>
  );
}

function SuggestionsSection({
  state,
  dispatch,
  onResolve,
}: {
  state: ChooseStage;
  dispatch: Dispatch<Action>;
  onResolve: (address: ResolvedAddress) => void;
}) {
  if (state.suggestions.length === 0) {
    return (
      <span className="alert alert-warning pa-7 px-15">
        No results, plese try a different query
      </span>
    );
  }

  return (
    <div>
      <div className="mb-7">Select address:</div>
      <div className="addressoptions mb-7">
        {state.suggestions.map((suggestion) => (
          // ACCESSIBILITY: The div can be safely ignored because
          // it's just a big click area for the radio button.
          // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions
          <div
            key={suggestion.id}
            className="addressoptions-option"
            onClick={() => {
              dispatch({
                type: 'select address suggestion',
                address: suggestion,
              });
            }}
          >
            <input
              type="radio"
              className="ma-0"
              id={`addr-result-${suggestion.id}`}
              name="addr-result"
              checked={suggestion.id === state.selected?.id}
              onChange={() => {
                dispatch({
                  type: 'select address suggestion',
                  address: suggestion,
                });
              }}
            />{' '}
            <label className="d-ib ma-0" htmlFor={`addr-result-${suggestion.id}`}>
              {suggestion.suggestion}
            </label>
          </div>
        ))}
      </div>
      <button
        disabled={state.selected === null}
        className="btn btn-primary"
        onClick={() => {
          if (state.selected !== null)
            resolveSelectedSuggestion(dispatch, onResolve, state.selected.id).catch(
              console.error,
            );
        }}
      >
        Select
      </button>
      {state.resolveFetchStatus === 'in progress' && (
        <span className="spinner ml-7"></span>
      )}
      {state.resolveFetchStatus === 'error' && (
        <span className="ml-7">Error getting full data; please try again</span>
      )}
    </div>
  );
}

function NewSearchSection({ dispatch }: { dispatch: Dispatch<Action> }) {
  return (
    <button className="btn mb-15" onClick={() => dispatch({ type: 'new search' })}>
      Search for new address
    </button>
  );
}

type Action =
  | { type: 'new search text'; value: string }
  | { type: 'find address suggestions' }
  | { type: 'error during suggestion fetching' }
  | { type: 'use address suggestions'; results: AddressSuggestion[] }
  | { type: 'select address suggestion'; address: AddressSuggestion }
  | { type: 'resolve selected suggestion' }
  | { type: 'finish address resolution' }
  | { type: 'error during address resolution' }
  | { type: 'new search' };

async function findAddressSuggestions(dispatch: Dispatch<Action>, lookupText: string) {
  dispatch({ type: 'find address suggestions' });
  const apiClient = new HTTPClient();
  const result = await apiClient.suggestAddresses(lookupText);
  if (result.isOk()) {
    dispatch({
      type: 'use address suggestions',
      results: result.coalesce(),
    });
  } else {
    console.error(result.unwrapErr());
    dispatch({ type: 'error during suggestion fetching' });
  }
}

async function resolveSelectedSuggestion(
  dispatch: Dispatch<Action>,
  onResolve: (address: ResolvedAddress) => void,
  addressId: string,
) {
  dispatch({ type: 'resolve selected suggestion' });
  const apiClient = new HTTPClient();
  const result = await apiClient.resolveAddress(addressId);
  if (result.isOk()) {
    onResolve(result.coalesce());
    dispatch({ type: 'finish address resolution' });
  } else {
    console.error(result.unwrapErr());
    dispatch({ type: 'error during address resolution' });
  }
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'new search text': {
      if (state.stage === 'address resolved') {
        return state;
      } else {
        return {
          ...state,
          searchText: action.value,
        };
      }
    }
    case 'find address suggestions': {
      if (state.stage === 'address resolved') {
        return state;
      } else {
        return {
          ...state,
          suggestionFetchStatus: 'in progress',
        };
      }
    }
    case 'error during suggestion fetching': {
      if (state.stage === 'address resolved') {
        return state;
      } else {
        return {
          ...state,
          suggestionFetchStatus: 'error',
        };
      }
    }
    case 'use address suggestions': {
      if (state.stage === 'address resolved') {
        return state;
      } else {
        return {
          ...state,
          stage: 'choosing suggestion',
          searchText: state.searchText,
          suggestions: action.results,
          selected: null,
          suggestionFetchStatus: 'at rest',
          resolveFetchStatus: 'at rest',
        };
      }
    }
    case 'select address suggestion': {
      if (state.stage !== 'choosing suggestion') {
        return state;
      } else {
        return {
          ...state,
          selected: action.address,
        };
      }
    }
    case 'resolve selected suggestion': {
      if (state.stage === 'choosing suggestion' && state.selected !== null) {
        return {
          ...state,
          resolveFetchStatus: 'in progress',
        };
      } else {
        return state;
      }
    }
    case 'error during address resolution': {
      if (state.stage !== 'choosing suggestion') {
        return state;
      } else {
        return {
          ...state,
          resolveFetchStatus: 'error',
        };
      }
    }
    case 'new search': {
      return initialState(false);
    }
    case 'finish address resolution': {
      return { stage: 'address resolved' };
    }
  }
}

export function LookupSection({
  initiallyResolved,
  onResolve,
}: {
  initiallyResolved: boolean;
  onResolve: (address: ResolvedAddress) => void;
}) {
  const [lookupState, dispatchLookupState] = useReducer(
    reducer,
    initialState(initiallyResolved),
  );
  return (
    <section className="line-top mb-30">
      <h3 className="mt-0">Automatic search</h3>

      {lookupState.stage === 'address resolved' ? (
        <NewSearchSection dispatch={dispatchLookupState} />
      ) : (
        <SearchTextSection state={lookupState} dispatch={dispatchLookupState} />
      )}
      {lookupState.stage === 'choosing suggestion' && (
        <SuggestionsSection
          state={lookupState}
          dispatch={dispatchLookupState}
          onResolve={onResolve}
        />
      )}
    </section>
  );
}

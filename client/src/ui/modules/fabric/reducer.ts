import {
  FloorMeasure,
  Floor as LibraryFloor,
} from '../../../data-schemas/libraries/fabric-floors';
import {
  Opening as LibraryOpening,
  OpeningMeasure,
} from '../../../data-schemas/libraries/fabric-openings';
import {
  WallLike as LibraryWallLike,
  WallLikeMeasure,
} from '../../../data-schemas/libraries/fabric-walls';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { DeepPartial, safeMerge } from '../../../helpers/safe-merge';
import { calcMeasureQtyAndCost } from '../../../measures';
import { initialPerFloorTypeSpec } from './components/u-value-calculator/state';
import type {
  AppliedFloorMeasure,
  AppliedOpeningMeasure,
  AppliedWallMeasure,
  Floor,
  Opening,
  State,
  WallLike,
} from './state';

export type Action =
  | {
      type: 'external data update';
      state: Partial<State>;
    }
  | {
      type: 'fabric/set thermal mass parameter';
      value: State['thermalMassParameter'];
    }
  | {
      type: 'fabric/set thermal bridging override';
      value: State['thermalBridgingOverride'];
    }
  | {
      type: 'fabric/show modal';
      value: State['modal'];
    }
  | {
      type: 'fabric/set floor input';
      id: string | number;
      value: Partial<Floor['inputs']>;
    }
  | {
      type: 'fabric/set custom floor u-value';
      id: string | number;
      uValue: number | null;
    }
  | {
      type: 'fabric/add floor';
      item: LibraryFloor;
    }
  | {
      type: 'fabric/replace floor';
      id: string | number;
      item: LibraryFloor;
    }
  | {
      type: 'fabric/apply floor measures';
      ids: (string | number)[];
      item: FloorMeasure;
    }
  | {
      type: 'fabric/merge wall input';
      id: string | number;
      value: DeepPartial<WallLike['inputs']>;
    }
  | {
      type: 'fabric/add wall';
      item: LibraryWallLike;
    }
  | {
      type: 'fabric/replace wall';
      id: string | number;
      item: LibraryWallLike;
    }
  | {
      type: 'fabric/apply wall measures';
      ids: (string | number)[];
      item: WallLikeMeasure;
    }
  | {
      type: 'fabric/merge opening input';
      id: string | number;
      value: DeepPartial<Opening['inputs']>;
    }
  | {
      type: 'fabric/add opening';
      item: LibraryOpening;
    }
  | {
      type: 'fabric/replace opening';
      id: string | number;
      item: LibraryOpening;
    }
  | {
      type: 'fabric/apply opening measures';
      ids: (string | number)[];
      item: OpeningMeasure;
    }
  | {
      type: 'fabric/delete';
      id: string | number;
    }
  | {
      type: 'fabric/revert measure';
      id: string | number;
    };

function wallWithMeasuresQtyAndCost(
  wall: Omit<AppliedWallMeasure, 'outputs'>,
): AppliedWallMeasure {
  const [costQuantity, costTotal] = calcMeasureQtyAndCost({
    area: wall.inputs.area[wall.inputs.area.type].area ?? 0,
    costUnits: wall.libraryItem.cost_units,
    costPerUnit: coalesceEmptyString(wall.libraryItem.cost, 0),
    baseCost: coalesceEmptyString(wall.libraryItem.min_cost, 0) ?? 0,
    isExternalWallInsulation: wall.libraryItem.isExternalWallInsulation,
  });

  return {
    ...wall,
    outputs: {
      costQuantity,
      costTotal,
    },
  };
}

function floorWithMeasuresQtyAndCost(
  floor: Omit<AppliedFloorMeasure, 'outputs'>,
): AppliedFloorMeasure {
  const [costQuantity, costTotal] = calcMeasureQtyAndCost({
    area: floor.inputs.area ?? 0,
    perimeter: floor.inputs.exposedPerimeter ?? 0,
    costUnits: floor.libraryItem.cost_units,
    costPerUnit: coalesceEmptyString(floor.libraryItem.cost, 0),
    baseCost: coalesceEmptyString(floor.libraryItem.min_cost, 0) ?? 0,
    isExternalWallInsulation: false,
  });

  return {
    ...floor,
    outputs: {
      costQuantity,
      costTotal,
    },
  };
}

function openingWithMeasuresQtyAndCost(
  opening: Omit<AppliedOpeningMeasure, 'outputs'>,
): AppliedOpeningMeasure {
  const [costQuantity, costTotal] = calcMeasureQtyAndCost({
    area: opening.inputs.area[opening.inputs.area.type].area ?? 0,
    costUnits: opening.libraryItem.cost_units,
    costPerUnit: coalesceEmptyString(opening.libraryItem.cost, 0),
    baseCost: coalesceEmptyString(opening.libraryItem.min_cost, 0) ?? 0,
    isExternalWallInsulation: false,
  });

  return {
    ...opening,
    outputs: {
      costQuantity,
      costTotal,
    },
  };
}

/**
 * Remove a fabric element from a bulk measure.
 *
 * Note that we leave empty bulk measures in place because the mutator function deals
 * with removing them from the global state.  At some point this function should do that
 * itself.
 */
function removeFromBulkMeasure(state: State, ids: (string | number)[]) {
  for (const measure of state.bulkMeasures) {
    measure.appliesTo = measure.appliesTo.filter((id) =>
      ids.includes(id) ? false : true,
    );
  }
}

export function reducer(state: State, action: Action): [State] {
  switch (action.type) {
    case 'external data update': {
      return [
        {
          ...state,
          ...action.state,
        },
      ];
    }
    case 'fabric/set thermal mass parameter': {
      state.thermalMassParameter = action.value;
      return [state];
    }
    case 'fabric/set thermal bridging override': {
      state.thermalBridgingOverride = action.value;
      return [state];
    }
    case 'fabric/merge wall input': {
      if (state.modal !== null) {
        state.justInserted = null;
      }
      state.walls = state.walls.map((wall) => {
        if (wall.id !== action.id) {
          return wall;
        } else {
          const result = {
            ...wall,
            inputs: safeMerge(wall.inputs, action.value),
          };

          const dimensions = result.inputs.area.dimensions;
          if (dimensions.length !== null && dimensions.height !== null) {
            dimensions.area = dimensions.length * dimensions.height;
          } else {
            dimensions.area = null;
          }

          if (result.type === 'measure') {
            return wallWithMeasuresQtyAndCost(result);
          } else {
            return result;
          }
        }
      });
      return [state];
    }
    case 'fabric/set floor input': {
      if (state.modal !== null) {
        state.justInserted = null;
      }
      state.floors = state.floors.map((floor) => {
        if (floor.id !== action.id) {
          return floor;
        } else {
          const result = {
            ...floor,
            inputs: {
              ...floor.inputs,
              ...action.value,
            },
          };

          if (result.type === 'measure') {
            return floorWithMeasuresQtyAndCost(result);
          } else {
            return result;
          }
        }
      });
      return [state];
    }
    case 'fabric/set custom floor u-value': {
      if (state.modal !== null) {
        state.justInserted = null;
      }
      state.floors = state.floors.map((floor) => {
        if (floor.id !== action.id) {
          return floor;
        } else {
          return {
            ...floor,
            inputs: {
              ...floor.inputs,
              perFloorTypeSpec: {
                ...floor.inputs.perFloorTypeSpec,
                custom: { uValue: action.uValue },
              },
            },
          };
        }
      });
      return [state];
    }
    case 'fabric/merge opening input': {
      if (state.modal !== null) {
        state.justInserted = null;
      }
      state.openings = state.openings.map((opening) => {
        if (opening.id !== action.id) {
          return opening;
        } else {
          const result = {
            ...opening,
            inputs: safeMerge(opening.inputs, action.value),
          };

          const dimensions = result.inputs.area.dimensions;
          if (dimensions.length !== null && dimensions.height !== null) {
            dimensions.area = dimensions.length * dimensions.height;
          } else {
            dimensions.area = null;
          }

          if (result.type === 'measure') {
            return openingWithMeasuresQtyAndCost(result);
          } else {
            return result;
          }
        }
      });
      return [state];
    }
    case 'fabric/add wall': {
      let areaInputType: 'dimensions' | 'specific' = 'dimensions';
      const prevElementOfType = state.walls.filter(
        (wall) => wall.libraryItem.type === action.item.type,
      );
      if (prevElementOfType.length > 0) {
        // SAFETY: length is checked
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        areaInputType = prevElementOfType[prevElementOfType.length - 1]!.inputs.area.type;
      }

      const toInsert: WallLike = {
        id: state.maxId + 1,
        type: 'element',
        elementType: 'wall',
        inputs: {
          location: '',
          area: {
            type: areaInputType,
            dimensions: { area: null, length: null, height: null },
            specific: { area: null },
          },
        },
        libraryItem: action.item,
        modelElement: null,
        revertTo: null,
      };
      state.walls.push(toInsert);
      state.maxId += 1;
      state.justInserted = toInsert.id;
      state.modal = null;
      return [state];
    }
    case 'fabric/add floor': {
      const toInsert: Floor = {
        id: state.maxId + 1,
        type: 'element',
        elementType: 'floor',
        inputs: {
          location: '',
          area: null,
          perFloorTypeSpec: {
            ...initialPerFloorTypeSpec,
            custom: {
              uValue: action.item.uValue,
            },
          },
          exposedPerimeter: null,
          selectedFloorType: action.item.type,
        },
        libraryItem: action.item,
        modelElement: null,
        revertTo: null,
      };
      state.floors.push(toInsert);
      state.maxId += 1;
      state.justInserted = toInsert.id;
      state.modal = null;
      return [state];
    }
    case 'fabric/add opening': {
      let areaInputType: Opening['inputs']['area']['type'] = 'dimensions';
      let overshadingInputType: Opening['inputs']['overshading'] = null;
      let orientationInputType: Opening['inputs']['orientation'] = null;

      const prevElementOfType = state.openings.filter(
        (opening) => opening.libraryItem.type === action.item.type,
      );
      if (prevElementOfType.length > 0) {
        // SAFETY: length is checked
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const prevElement = prevElementOfType[prevElementOfType.length - 1]!;

        areaInputType = prevElement.inputs.area.type;
        overshadingInputType = prevElement.inputs.overshading;
        orientationInputType = prevElement.inputs.orientation;
      }

      const toInsert: Opening = {
        id: state.maxId + 1,
        type: 'element',
        elementType: 'opening',
        inputs: {
          location: '',
          area: {
            type: areaInputType,
            dimensions: { area: null, length: null, height: null },
            specific: { area: null },
          },
          orientation: orientationInputType,
          overshading: overshadingInputType,
          subtractFrom: null,
        },
        libraryItem: action.item,
        modelElement: null,
        revertTo: null,
      };
      state.openings.push(toInsert);
      state.maxId += 1;
      state.justInserted = toInsert.id;
      state.modal = null;
      return [state];
    }
    case 'fabric/delete': {
      state.walls = state.walls.filter((wall) => wall.id !== action.id);
      state.floors = state.floors.filter((floor) => floor.id !== action.id);
      state.openings = state.openings.filter((opening) => opening.id !== action.id);
      removeFromBulkMeasure(state, [action.id]);
      return [state];
    }
    case 'fabric/show modal': {
      state.modal = action.value;
      if (state.modal !== null) {
        state.justInserted = null;
      }
      return [state];
    }
    case 'fabric/replace wall': {
      state.walls = state.walls.map((wall) =>
        wall.id === action.id && wall.type === 'element'
          ? { ...wall, libraryItem: action.item }
          : wall,
      );
      state.modal = null;
      return [state];
    }
    case 'fabric/replace floor': {
      state.floors = state.floors.map((floor) =>
        floor.id === action.id && floor.type === 'element'
          ? { ...floor, libraryItem: action.item }
          : floor,
      );
      state.modal = null;
      return [state];
    }
    case 'fabric/replace opening': {
      state.openings = state.openings.map((opening) =>
        opening.id === action.id && opening.type === 'element'
          ? { ...opening, libraryItem: action.item }
          : opening,
      );
      state.modal = null;
      return [state];
    }
    case 'fabric/apply wall measures': {
      state.walls = state.walls.map((wall) => {
        if (!action.ids.includes(wall.id)) {
          return wall;
        } else {
          return wallWithMeasuresQtyAndCost({
            ...wall,
            type: 'measure',
            libraryItem: {
              ...action.item,
              cost: coalesceEmptyString(action.item.cost, 0),
            },
          });
        }
      });

      // Remove any entries previously in bulk measures & create a new bulk
      // measure if required
      removeFromBulkMeasure(state, action.ids);
      if (action.ids.length > 1) {
        state.bulkMeasures.push({
          id: state.maxId + 1,
          appliesTo: action.ids,
        });
      }

      state.modal = null;

      return [state];
    }
    case 'fabric/apply floor measures': {
      state.floors = state.floors.map((floor) => {
        if (!action.ids.includes(floor.id)) {
          return floor;
        } else {
          return floorWithMeasuresQtyAndCost({
            ...floor,
            type: 'measure',
            libraryItem: {
              ...action.item,
              cost: action.item.cost,
            },
            inputs: {
              ...floor.inputs,
              perFloorTypeSpec: {
                ...floor.inputs.perFloorTypeSpec,
                custom: {
                  uValue: action.item.uValue,
                },
              },
              selectedFloorType: 'custom',
            },
          });
        }
      });

      // Remove any entries previously in bulk measures & create a new bulk
      // measure if required
      removeFromBulkMeasure(state, action.ids);
      if (action.ids.length > 1) {
        state.bulkMeasures.push({
          id: state.maxId + 1,
          appliesTo: action.ids,
        });
      }

      state.modal = null;

      return [state];
    }
    case 'fabric/apply opening measures': {
      state.openings = state.openings.map((opening) => {
        if (!action.ids.includes(opening.id)) {
          return opening;
        } else {
          return openingWithMeasuresQtyAndCost({
            ...opening,
            type: 'measure',
            libraryItem: {
              ...action.item,
              cost: coalesceEmptyString(action.item.cost, 0),
            },
          });
        }
      });

      // Remove any entries previously in bulk measures & create a new bulk
      // measure if required
      removeFromBulkMeasure(state, action.ids);
      if (action.ids.length > 1) {
        state.bulkMeasures.push({
          id: state.maxId + 1,
          appliesTo: action.ids,
        });
      }

      state.modal = null;

      return [state];
    }

    case 'fabric/revert measure': {
      for (const fabricElement of [...state.walls, ...state.floors, ...state.openings]) {
        if (fabricElement.id !== action.id) {
          continue;
        }

        if (fabricElement.revertTo === null) {
          throw new Error("Couldn't revert: nothing to revert to");
        }

        fabricElement.type = 'element';
        fabricElement.libraryItem = fabricElement.revertTo.libraryItem;
        removeFromBulkMeasure(state, [action.id]);
        return [state];
      }

      throw new Error("Couldn't revert: bad ID");
    }
  }
}

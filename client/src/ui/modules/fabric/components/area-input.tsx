import React, { useId } from 'react';
import { DeepPartial } from '../../../../helpers/safe-merge';
import { NumberOutput } from '../../../output-components/numeric';
import { AreaSpec } from '../state';
import { MyNumericInput, MyRadioGroup } from './lockable-inputs';

type AreaInputProps = {
  area: AreaSpec;
  onChange: (area: DeepPartial<AreaSpec>) => void;
  dimension1?: string;
  dimension2?: string;
};

export function AreaInput({
  area,
  onChange,
  dimension1 = 'Length',
  dimension2 = 'Height',
}: AreaInputProps) {
  const id = useId();
  return (
    <>
      <div className="d-flex mr-30" style={{ flexDirection: 'column', alignSelf: 'end' }}>
        <MyRadioGroup<'dimensions' | 'specific'>
          options={[
            {
              value: 'dimensions',
              display: `${dimension1[0] ?? 'W'} x ${dimension2[0] ?? 'H'}`,
            },
            { value: 'specific', display: 'Area' },
          ]}
          labelClasses={['mb-0']}
          radioClasses={['mr-3']}
          value={area.type}
          onChange={(type) => onChange({ type })}
        />
      </div>

      {area.type === 'dimensions' && (
        <div className="d-flex align-items-center mr-15">
          <div>
            <label className="d-i small-caps" htmlFor={`${id}-length`}>
              {dimension1}
            </label>{' '}
            <span className="text-units">m</span>
            <br />
            <MyNumericInput
              id={`${id}-length`}
              value={area.dimensions.length}
              onChange={(length) => onChange({ dimensions: { length } })}
            />
          </div>

          <span
            style={{
              fontSize: '1.5rem',
              margin: '0 0.2rem',
            }}
          >
            <br />⨯
          </span>

          <div>
            <label className="d-i small-caps" htmlFor={`${id}-height`}>
              {dimension2}
            </label>{' '}
            <span className="text-units">m</span>
            <br />
            <MyNumericInput
              id={`${id}-height`}
              value={area.dimensions.height}
              onChange={(height) => onChange({ dimensions: { height } })}
            />
          </div>
        </div>
      )}

      <div className="d-flex align-items-center mr-15">
        {area.type === 'dimensions' && (
          <span
            style={{
              fontSize: '1.5rem',
              margin: '0 0.2rem',
            }}
          >
            <br />=
          </span>
        )}

        {area.type === 'specific' ? (
          <div className="mr-15">
            <label className="d-i small-caps" htmlFor={`${id}-area`}>
              Area
            </label>{' '}
            <span className="text-units">m²</span>
            <br />
            <MyNumericInput
              id={`${id}-area`}
              value={area.specific.area}
              onChange={(area) => onChange({ specific: { area } })}
            />
          </div>
        ) : (
          <div>
            <label className="d-i small-caps" htmlFor={`${id}-area`}>
              Area
            </label>
            <br />
            <NumberOutput value={area[area.type].area} unit="m²" />
          </div>
        )}
      </div>
    </>
  );
}

import React, { useState } from 'react';

import { Floor, FloorMeasure } from '../../../../data-schemas/libraries/fabric-floors';
import {
  Opening,
  OpeningMeasure,
} from '../../../../data-schemas/libraries/fabric-openings';
import {
  WallLike,
  WallLikeMeasure,
} from '../../../../data-schemas/libraries/fabric-walls';

import { CheckboxInput } from '../../../input-components/checkbox';
import {
  SelectFloor,
  SelectFloorMeasure,
  SelectOpening,
  SelectOpeningMeasure,
  SelectWallLike,
  SelectWallLikeMeasure,
} from '../../../input-components/library-picker';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../../output-components/modal';
import type { Action } from '../reducer';
import type { State } from '../state';
type Dispatcher = (action: Action) => void;

type SelectBulkElementsParams = {
  onClose: () => void;
  onSelect: (ids: (string | number)[]) => void;
  measureToApply: { tag: string; name: string };
  items: {
    id: string | number;
    libraryItem: { tag: string; name: string };
    inputs: { location: string | null };
  }[];
};

function SelectBulkElements({
  onClose,
  onSelect,
  measureToApply,
  items,
}: SelectBulkElementsParams): JSX.Element {
  const [selected, setSelected] = useState<(string | number)[]>([]);
  function toggleSelected(id: string | number) {
    if (selected.includes(id)) {
      setSelected(selected.filter((element) => element !== id));
    } else {
      setSelected([...selected, id]);
    }
  }

  return (
    <Modal onClose={onClose} headerId="modal-header">
      <ModalHeader title="Select elements" onClose={onClose}>
        <div>
          Applying <b>{measureToApply.tag}</b>: {measureToApply.name}
        </div>
      </ModalHeader>
      <ModalBody>
        <table className="table">
          <thead>
            <tr>
              <th>
                <CheckboxInput
                  value={items.length === selected.length}
                  onChange={(checked) => {
                    if (checked) setSelected(items.map((item) => item.id));
                    else setSelected([]);
                  }}
                />
              </th>
              <th>Tag</th>
              <th>Name</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item, idx) => (
              <tr
                key={idx}
                onClick={() => toggleSelected(item.id)}
                className="clickable clickable-hover"
              >
                <td>
                  <CheckboxInput
                    value={selected.includes(item.id)}
                    onChange={() => toggleSelected(item.id)}
                  />
                </td>
                <td className="text-nowrap" style={{ width: 0 }}>
                  {item.libraryItem.tag}
                </td>
                <td
                  title={item.libraryItem.name}
                  style={{
                    maxWidth: '240px',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                  }}
                >
                  {item.libraryItem.name}
                </td>
                <td style={{ minWidth: '100px' }}>{item.inputs.location}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        <button className="btn btn-primary" onClick={() => onSelect(selected)}>
          Apply
        </button>
      </ModalFooter>
    </Modal>
  );
}

type ModalsParams = {
  state: State;
  dispatch: Dispatcher;
};

export function Modals({ state, dispatch }: ModalsParams): JSX.Element | null {
  const modal = state.modal;
  function closeModal() {
    dispatch({
      type: 'fabric/show modal',
      value: null,
    });
  }

  if (modal === null) {
    return null;
  }

  switch (modal.type) {
    case 'add wall':
      return (
        <SelectWallLike
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: WallLike) => {
            dispatch({
              type: 'fabric/add wall',
              item,
            });
          }}
        />
      );
    case 'add floor':
      return (
        <SelectFloor
          onClose={closeModal}
          onSelect={(item: Floor) => {
            dispatch({
              type: 'fabric/add floor',
              item,
            });
          }}
        />
      );
    case 'add opening':
      return (
        <SelectOpening
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: Opening) => {
            dispatch({
              type: 'fabric/add opening',
              item,
            });
          }}
        />
      );
    case 'replace wall': {
      const currentWall = state.walls.find((wall) => wall.id === modal.id);
      return (
        <SelectWallLike
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: WallLike) => {
            dispatch({
              type: 'fabric/replace wall',
              id: modal.id,
              item,
            });
          }}
          currentItemTag={currentWall?.libraryItem.tag ?? null}
        />
      );
    }
    case 'replace floor': {
      const currentFloor = state.floors.find((floor) => floor.id === modal.id);
      return (
        <SelectFloor
          onClose={closeModal}
          onSelect={(item: Floor) => {
            dispatch({
              type: 'fabric/replace floor',
              id: modal.id,
              item,
            });
          }}
          currentItemTag={currentFloor?.libraryItem.tag ?? null}
        />
      );
    }
    case 'replace opening': {
      const currentOpening = state.openings.find((opening) => opening.id === modal.id);
      return (
        <SelectOpening
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: Opening) => {
            dispatch({
              type: 'fabric/replace opening',
              id: modal.id,
              item,
            });
          }}
          currentItemTag={currentOpening?.libraryItem.tag ?? null}
        />
      );
    }
    case 'apply wall measure': {
      const currentWall = state.walls.find((wall) => wall.id === modal.id);
      return (
        <SelectWallLikeMeasure
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: WallLikeMeasure) => {
            dispatch({
              type: 'fabric/apply wall measures',
              ids: [modal.id],
              item,
            });
          }}
          currentItemTag={currentWall?.libraryItem.tag ?? null}
          areaSqm={
            currentWall !== undefined
              ? currentWall.inputs.area[currentWall.inputs.area.type].area
              : null
          }
        />
      );
    }
    case 'apply floor measure': {
      const currentFloor = state.floors.find((floor) => floor.id === modal.id);
      return (
        <SelectFloorMeasure
          onClose={closeModal}
          onSelect={(item: FloorMeasure) => {
            dispatch({
              type: 'fabric/apply floor measures',
              ids: [modal.id],
              item,
            });
          }}
          currentItemTag={currentFloor?.libraryItem.tag ?? null}
          showCost={true}
          areaSqm={currentFloor?.inputs?.area ?? null}
          perimeter={currentFloor?.inputs?.exposedPerimeter ?? null}
        />
      );
    }
    case 'apply opening measure': {
      const currentOpening = state.openings.find((opening) => opening.id === modal.id);
      return (
        <SelectOpeningMeasure
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: OpeningMeasure) => {
            dispatch({
              type: 'fabric/apply opening measures',
              ids: [modal.id],
              item,
            });
          }}
          currentItemTag={currentOpening?.libraryItem.tag ?? null}
          areaSqm={
            currentOpening !== undefined
              ? currentOpening.inputs.area[currentOpening.inputs.area.type].area
              : null
          }
          showCost={true}
        />
      );
    }
    case 'select wall bulk measure':
      // If we reversed the order of bulk measure selection, i.e. choose the fabric
      // elements to apply to and then the measure, we could provide the areaSqm.
      return (
        <SelectWallLikeMeasure
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: WallLikeMeasure) => {
            dispatch({
              type: 'fabric/show modal',
              value: {
                type: 'select wall bulk measure elements',
                measure: item,
              },
            });
          }}
          currentItemTag={null}
          areaSqm={null}
        />
      );
    case 'select floor bulk measure':
      // If we reversed the order of bulk measure selection, i.e. choose the fabric
      // elements to apply to and then the measure, we could provide the areaSqm.
      return (
        <SelectFloorMeasure
          onClose={closeModal}
          onSelect={(item: FloorMeasure) => {
            dispatch({
              type: 'fabric/show modal',
              value: {
                type: 'select floor bulk measure elements',
                measure: item,
              },
            });
          }}
          currentItemTag={null}
          showCost={false}
        />
      );
    case 'select opening bulk measure':
      // If we reversed the order of bulk measure selection, i.e. choose the fabric
      // elements to apply to and then the measure, we could provide the areaSqm.
      return (
        <SelectOpeningMeasure
          type={modal.elementType}
          onClose={closeModal}
          onSelect={(item: OpeningMeasure) => {
            dispatch({
              type: 'fabric/show modal',
              value: {
                type: 'select opening bulk measure elements',
                measure: item,
              },
            });
          }}
          currentItemTag={null}
          showCost={false}
        />
      );
    case 'select wall bulk measure elements':
      return (
        <SelectBulkElements
          onClose={closeModal}
          onSelect={(ids) =>
            dispatch({
              type: 'fabric/apply wall measures',
              item: modal.measure,
              ids,
            })
          }
          measureToApply={modal.measure}
          items={state.walls.filter(
            (wall) => wall.libraryItem.type === modal.measure.type,
          )}
        />
      );
    case 'select floor bulk measure elements':
      return (
        <SelectBulkElements
          onClose={closeModal}
          onSelect={(ids) =>
            dispatch({
              type: 'fabric/apply floor measures',
              item: modal.measure,
              ids,
            })
          }
          measureToApply={modal.measure}
          items={state.floors}
        />
      );
    case 'select opening bulk measure elements':
      return (
        <SelectBulkElements
          onClose={closeModal}
          onSelect={(ids) =>
            dispatch({
              type: 'fabric/apply opening measures',
              item: modal.measure,
              ids,
            })
          }
          measureToApply={modal.measure}
          items={state.openings.filter(
            (opening) => opening.libraryItem.type === modal.measure.type,
          )}
        />
      );
  }
}

export function showModal(dispatch: Dispatcher, value: State['modal']) {
  return () =>
    dispatch({
      type: 'fabric/show modal',
      value,
    });
}

import React, { useState } from 'react';

import { FloorType } from '../../../../data-schemas/scenario/fabric/floor-u-value';
import { DeleteIcon, HammerIcon, PlusIcon, UndoIcon } from '../../../icons';
import { NumberInput } from '../../../input-components/number';
import { Select } from '../../../input-components/select';
import { NumberOutput } from '../../../output-components/numeric';
import { Action } from '../reducer';
import { Floor, State } from '../state';
import { Button, MyTextInput } from './lockable-inputs';
import { colourForStatus, measureStatus } from './measure-status';
import { showModal } from './modals';
import { FUVC } from './u-value-calculator';
import { floorTypeDisplay } from './u-value-calculator/shared-components/floor-type-display';

type Dispatcher = (action: Action) => void;
type SubProps = {
  state: State;
  dispatch: Dispatcher;
};

function FloorRow({
  floor,
  dispatch,
  state,
}: {
  floor: Floor;
  dispatch: Dispatcher;
  state: State;
}) {
  const [showLegacySolidType] = useState(floor.inputs.selectedFloorType === 'solid');

  const status = measureStatus(floor);

  return (
    <div
      className="card"
      style={{
        borderLeft: `10px solid var(${colourForStatus[status]})`,
      }}
    >
      <div className="d-flex justify-content-between align-items-center mb-15">
        <div>
          <span>
            {floor.inputs.area === 0 && (
              <span className="label label-important mr-7">Deleted</span>
            )}
            <b>{floor.libraryItem.name}</b>
          </span>
          <div className="d-flex">
            <span>{floor.libraryItem.tag}</span>
            <span className="ml-15">
              U value: <NumberOutput value={floor.libraryItem.uValue} unit="W/K·m²" />
            </span>
            <span className="ml-15">
              k value: <NumberOutput value={floor.libraryItem.kValue} unit="kJ/K·m²" />
            </span>
            {floor.type === 'measure' && (
              <span className="ml-15">
                Measure cost: £
                <NumberOutput value={floor.outputs.costTotal} dp={0} />
              </span>
            )}
          </div>
        </div>

        <div>
          {state.currentScenarioIsBaseline ? (
            <Button
              title="Delete"
              icon={DeleteIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/delete',
                  id: floor.id,
                })
              }
            />
          ) : floor.type === 'measure' && floor.revertTo !== null ? (
            <Button
              title="Revert"
              icon={UndoIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/revert measure',
                  id: floor.id,
                })
              }
            />
          ) : null}

          {state.currentScenarioIsBaseline ? (
            <Button
              title="Replace"
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'replace floor',
                id: floor.id,
              })}
            />
          ) : (
            <Button
              title={floor.type === 'measure' ? 'Replace measure' : 'Apply measure'}
              icon={HammerIcon}
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'apply floor measure',
                id: floor.id,
              })}
            />
          )}
        </div>
      </div>

      <div className="d-flex flex-wrap">
        <div className="mr-30">
          <label className="small-caps d-i" htmlFor={`e${floor.id}-location`}>
            Location
          </label>
          <br />
          <MyTextInput
            id={`e${floor.id}-location`}
            value={floor.inputs.location}
            onChange={(location) =>
              dispatch({
                type: 'fabric/set floor input',
                id: floor.id,
                value: { location },
              })
            }
            className={floor.id === state.justInserted ? 'just-inserted' : ''}
          />
        </div>

        <div className="mr-30">
          <label className="d-i small-caps" htmlFor={`e${floor.id}-area`}>
            Area
          </label>{' '}
          <span className="text-units">m²</span>
          <br />
          <NumberInput
            id={`e${floor.id}-area`}
            value={floor.inputs.area}
            onChange={(area) =>
              dispatch({
                type: 'fabric/set floor input',
                id: floor.id,
                value: { area: area ?? 0 },
              })
            }
          />
        </div>

        <div className="mr-30">
          <label className="d-i small-caps" htmlFor={`e${floor.id}-exposed-perimeter`}>
            Exposed perimeter
          </label>{' '}
          <span className="text-units">m</span>
          <br />
          <NumberInput
            id={`e${floor.id}-exposed-perimeter`}
            value={floor.inputs.exposedPerimeter}
            onChange={(exposedPerimeter) =>
              dispatch({
                type: 'fabric/set floor input',
                id: floor.id,
                value: { exposedPerimeter: exposedPerimeter ?? 0 },
              })
            }
          />
        </div>

        <div className="mr-30">
          <label className="d-i small-caps" htmlFor={`e${floor.id}-type`}>
            Type
          </label>
          <br />
          <Select
            id={`e${floor.id}-type`}
            className="input--auto-width"
            value={floor.inputs.selectedFloorType}
            onChange={(selectedFloorType) => {
              dispatch({
                type: 'fabric/set floor input',
                id: floor.id,
                value: {
                  selectedFloorType,
                },
              });
            }}
            options={(
              [
                'solid (bs13370)',
                ...(showLegacySolidType || floor.inputs.selectedFloorType === 'solid'
                  ? ['solid' as const]
                  : []),
                'suspended',
                'exposed',
                'heated basement',
                'custom',
              ] as const
            ).map((floorType: FloorType) => ({
              value: floorType,
              display: floorTypeDisplay(floorType),
            }))}
          />
        </div>

        {floor.inputs.selectedFloorType === 'custom' && (
          <div className="mr-30">
            <label className="d-i small-caps" htmlFor={`e${floor.id}-uvalue`}>
              U value
            </label>
            <br />
            <NumberInput
              id={`e${floor.id}-uvalue`}
              value={floor.inputs.perFloorTypeSpec.custom.uValue}
              onChange={(value) =>
                dispatch({
                  type: 'fabric/set custom floor u-value',
                  id: floor.id,
                  uValue: value,
                })
              }
              unit="W/K.m²"
            />
          </div>
        )}

        <div className="mr-30">
          <span className="small-caps">Heat loss</span> <br />
          <NumberOutput value={floor.modelElement?.heatLoss ?? 0} unit="W/K" />
        </div>
      </div>

      {floor.inputs.selectedFloorType !== 'custom' && (
        <FUVC
          value={floor.inputs.perFloorTypeSpec}
          onChange={(perFloorTypeSpec) => {
            dispatch({
              type: 'fabric/set floor input',
              id: floor.id,
              value: { perFloorTypeSpec },
            });
          }}
          modelElement={floor.modelElement}
          selectedFloorType={floor.inputs.selectedFloorType}
          suppressNonFiniteNumberErrors={
            // Hack: sometimes an assessor will want to remove an element in a
            // non-baseline scenario (e.g., to model replacing a floor). We do
            // not yet support this, so as a workaround we tell them to set the
            // area to 0. Therefore we must suppress non-finite number warnings
            // in non-baseline scenarios where the area is set to 0.
            !state.currentScenarioIsBaseline && floor.inputs.area === 0
          }
        />
      )}
    </div>
  );
}

export function Floors({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Floors</h3>

      <div className="rounded-cards mb-15">
        {state.floors.map((floor) => (
          <FloorRow floor={floor} key={floor.id} state={state} dispatch={dispatch} />
        ))}
      </div>

      <Button
        title="New floor"
        icon={PlusIcon}
        onClick={showModal(dispatch, { type: 'add floor' })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, { type: 'select floor bulk measure' })}
          className={'ml-15'}
          disabled={state.floors.length === 0}
        />
      )}
    </section>
  );
}

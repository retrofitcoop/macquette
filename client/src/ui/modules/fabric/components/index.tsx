import React, { useLayoutEffect } from 'react';

import type { Action } from './../reducer';
import type { State } from './../state';
import { Floors } from './floors';
import { PageContext } from './lockable-inputs';
import { Modals } from './modals';
import { Doors, Hatches, RoofLights, Windows } from './openings';
import { ThermalBridging } from './thermal-bridging';
import { ThermalMassParameter } from './thermal-mass-parameter';
import { Totals } from './totals';
import { PartyWalls, RoofsAndLofts, Walls } from './walls';

type Dispatcher = (action: Action) => void;

type PageProps = {
  state: State;
  dispatch: Dispatcher;
};

export function Fabric({ state, dispatch }: PageProps) {
  const locked = state.locked;

  useLayoutEffect(() => {
    const elem: HTMLElement | null = document.querySelector('.just-inserted');
    if (elem !== null) {
      elem.focus();
    }
  }, [state.justInserted]);

  return (
    <PageContext.Provider value={{ locked }}>
      <ThermalMassParameter state={state} dispatch={dispatch} />
      <Walls state={state} dispatch={dispatch} />
      <PartyWalls state={state} dispatch={dispatch} />
      <RoofsAndLofts state={state} dispatch={dispatch} />
      <Floors state={state} dispatch={dispatch} />
      <Hatches state={state} dispatch={dispatch} />
      <Doors state={state} dispatch={dispatch} />
      <Windows state={state} dispatch={dispatch} />
      <RoofLights state={state} dispatch={dispatch} />
      <Modals state={state} dispatch={dispatch} />
      <ThermalBridging state={state} dispatch={dispatch} />
      <Totals state={state} />
    </PageContext.Provider>
  );
}

import React from 'react';

import { DeepPartial } from '../../../../helpers/safe-merge';
import { WindowLike } from '../../../../model/modules/fabric/element-types';
import { DeleteIcon, HammerIcon, PlusIcon, UndoIcon } from '../../../icons';
import { RadioGroup } from '../../../input-components/radio-group';
import { Select } from '../../../input-components/select';
import { NumberOutput } from '../../../output-components/numeric';
import { Action } from '../reducer';
import { Opening, State } from '../state';
import { Button, MyNumericInput, MyTextInput } from './lockable-inputs';
import { colourForStatus, measureStatus } from './measure-status';
import { showModal } from './modals';

type Dispatcher = (action: Action) => void;
type SubProps = {
  state: State;
  dispatch: Dispatcher;
};

function OpeningRow({
  opening,
  dispatch,
  state,
}: {
  opening: Opening;
  dispatch: Dispatcher;
  state: State;
}) {
  const status = measureStatus(opening);

  const isDeleted =
    (opening.inputs.area.type === 'dimensions' &&
      opening.inputs.area.dimensions.length === 0 &&
      opening.inputs.area.dimensions.height === 0) ||
    (opening.inputs.area.type === 'specific' && opening.inputs.area.specific.area === 0);

  const subtractFromList = [
    { value: 'no', display: '(none)' },
    ...state.walls.map((wall) => ({
      value: wall.id.toString(),
      display: wall.inputs.location,
    })),
  ];

  function mergeInput(input: DeepPartial<Opening['inputs']>) {
    dispatch({
      type: 'fabric/merge opening input',
      id: opening.id,
      value: input,
    });
  }

  return (
    <div
      className="card"
      style={{
        borderLeft: `10px solid var(${colourForStatus[status]})`,
      }}
    >
      <div className="d-flex justify-content-between align-items-center mb-15">
        <div>
          <span>
            {isDeleted && <span className="label label-important mr-7">Deleted</span>}
            <b>{opening.libraryItem.name}</b>
          </span>
          <div className="d-flex">
            <span>{opening.libraryItem.tag}</span>
            <span className="ml-15">
              U value: <NumberOutput value={opening.libraryItem.uValue} unit="W/K·m²" />
            </span>
            <span className="ml-15">
              k value: <NumberOutput value={opening.libraryItem.kValue} unit="kJ/K·m²" />
            </span>
            {opening.type === 'measure' && (
              <span className="ml-15">
                Measure cost: £
                <NumberOutput value={opening.outputs.costTotal} dp={0} />
              </span>
            )}
          </div>
        </div>

        <div>
          {state.currentScenarioIsBaseline ? (
            <Button
              title="Delete"
              icon={DeleteIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/delete',
                  id: opening.id,
                })
              }
            />
          ) : opening.type === 'measure' && opening.revertTo !== null ? (
            <Button
              title="Revert"
              icon={UndoIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/revert measure',
                  id: opening.id,
                })
              }
            />
          ) : null}

          {state.currentScenarioIsBaseline ? (
            <Button
              title="Replace"
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'replace opening',
                id: opening.id,
                elementType: opening.libraryItem.type,
              })}
            />
          ) : (
            <Button
              title={opening.type === 'measure' ? 'Replace measure' : 'Apply measure'}
              icon={HammerIcon}
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'apply opening measure',
                id: opening.id,
                elementType: opening.libraryItem.type,
              })}
            />
          )}
        </div>
      </div>

      <div className="d-flex flex-wrap gap-15">
        <div className="d-flex flex-v gap-7">
          <div>
            <label className="small-caps d-i" htmlFor={`e${opening.id}-location`}>
              Location
            </label>
            <br />
            <MyTextInput
              id={`e${opening.id}-location`}
              value={opening.inputs.location}
              onChange={(location) => mergeInput({ location })}
              className={opening.id === state.justInserted ? 'just-inserted' : ''}
            />
          </div>

          <div>
            <label className="small-caps d-i" htmlFor={`e${opening.id}-subtractFrom`}>
              Subtract from
            </label>
            <br />
            <Select<string>
              id={`e${opening.id}-subtractFrom`}
              options={subtractFromList}
              value={opening.inputs.subtractFrom?.toString() ?? null}
              onChange={(subtractFrom) => mergeInput({ subtractFrom })}
            />
          </div>
        </div>

        <div className="d-flex flex-v">
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
          <label className="d-flex hover-gray-bg mb-0" style={{ padding: '5px 10px' }}>
            <input
              type="radio"
              className="mr-7"
              checked={opening.inputs.area.type === 'dimensions'}
              onChange={() => mergeInput({ area: { type: 'dimensions' } })}
            />
            <div className="d-flex align-items-center">
              <div>
                <label className="d-i small-caps" htmlFor={`${opening.id}-length`}>
                  Width
                </label>{' '}
                <span className="text-units">m</span>
                <br />
                <MyNumericInput
                  id={`${opening.id}-length`}
                  value={
                    opening.inputs.area.type === 'dimensions'
                      ? opening.inputs.area.dimensions.length
                      : null
                  }
                  onChange={(length) => mergeInput({ area: { dimensions: { length } } })}
                  disabled={opening.inputs.area.type !== 'dimensions'}
                />
              </div>

              <span
                style={{
                  fontSize: '1.5rem',
                  margin: '0 0.2rem',
                }}
              >
                <br />⨯
              </span>

              <div>
                <label className="d-i small-caps" htmlFor={`${opening.id}-height`}>
                  Height
                </label>{' '}
                <span className="text-units">m</span>
                <br />
                <MyNumericInput
                  id={`${opening.id}-height`}
                  value={
                    opening.inputs.area.type === 'dimensions'
                      ? opening.inputs.area.dimensions.height
                      : null
                  }
                  onChange={(height) => mergeInput({ area: { dimensions: { height } } })}
                  disabled={opening.inputs.area.type !== 'dimensions'}
                />
              </div>
            </div>
          </label>

          <label className="d-flex hover-gray-bg mb-0" style={{ padding: '5px 10px' }}>
            <input
              type="radio"
              className="mr-7"
              checked={opening.inputs.area.type === 'specific'}
              onChange={() => mergeInput({ area: { type: 'specific' } })}
            />

            <div>
              <label className="d-i small-caps" htmlFor={`${opening.id}-area`}>
                Area
              </label>{' '}
              <span className="text-units">m²</span>
              <br />
              {opening.inputs.area.type === 'specific' ? (
                <MyNumericInput
                  id={`${opening.id}-area`}
                  value={opening.inputs.area.specific.area}
                  onChange={(area) => mergeInput({ area: { specific: { area } } })}
                />
              ) : (
                <NumberOutput
                  value={opening.inputs.area[opening.inputs.area.type].area}
                  unit="m²"
                />
              )}
            </div>
          </label>
        </div>

        {opening.modelElement instanceof WindowLike && (
          <>
            <div className="d-flex flex-v">
              <div>
                <span className="small-caps">Orientation</span>
                <RadioGroup
                  options={[
                    { value: 'North', display: 'North' },
                    { value: 'NE/NW', display: 'NE/NW' },
                    { value: 'East/West', display: 'East/West' },
                    { value: 'SE/SW', display: 'SE/SW' },
                    { value: 'South', display: 'South' },
                  ]}
                  labelClasses={['mb-0']}
                  radioClasses={['mr-3']}
                  value={opening.inputs.orientation}
                  onChange={(orientation) =>
                    dispatch({
                      type: 'fabric/merge opening input',
                      id: opening.id,
                      value: { orientation },
                    })
                  }
                />
              </div>
            </div>

            <div>
              <span className="small-caps">% overshading</span>
              <table className="table-unstyled">
                <tbody>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`overshading-${opening.id}`}
                        id={`overshading-${opening.id}-0`}
                        checked={opening.inputs.overshading === '>80%'}
                        onChange={() =>
                          dispatch({
                            type: 'fabric/merge opening input',
                            id: opening.id,
                            value: { overshading: '>80%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i" htmlFor={`overshading-${opening.id}-0`}>
                        &gt; 80
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Heavy</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`overshading-${opening.id}`}
                        id={`overshading-${opening.id}-1`}
                        checked={opening.inputs.overshading === '60-80%'}
                        onChange={() =>
                          dispatch({
                            type: 'fabric/merge opening input',
                            id: opening.id,
                            value: { overshading: '60-80%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label
                        className="d-i text-nowrap"
                        htmlFor={`overshading-${opening.id}-1`}
                      >
                        60–80
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Above average</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`overshading-${opening.id}`}
                        id={`overshading-${opening.id}-2`}
                        checked={opening.inputs.overshading === '20-60%'}
                        onChange={() =>
                          dispatch({
                            type: 'fabric/merge opening input',
                            id: opening.id,
                            value: { overshading: '20-60%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label
                        className="d-i text-nowrap"
                        htmlFor={`overshading-${opening.id}-2`}
                      >
                        20–60
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Average/unknown</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`overshading-${opening.id}`}
                        id={`overshading-${opening.id}-3`}
                        checked={opening.inputs.overshading === '<20%'}
                        onChange={() =>
                          dispatch({
                            type: 'fabric/merge opening input',
                            id: opening.id,
                            value: { overshading: '<20%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i" htmlFor={`overshading-${opening.id}-3`}>
                        &lt; 20
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Very little</small>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </>
        )}
        <div className="d-flex flex-v gap-7">
          {opening.modelElement instanceof WindowLike && (
            <div>
              <span className="small-caps">Solar gain</span> <br />
              <NumberOutput value={opening.modelElement.meanSolarGain} unit="W" />
            </div>
          )}

          <div>
            <span className="small-caps">Heat loss</span> <br />
            <NumberOutput value={opening.modelElement?.heatLoss} unit="W/K" />
          </div>
        </div>
      </div>
    </div>
  );
}

export function Hatches({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Hatches</h3>

      <div className="rounded-cards mb-15">
        {state.openings
          .filter((opening) => opening.libraryItem.type === 'hatch')
          .map((opening) => (
            <OpeningRow
              opening={opening}
              key={opening.id}
              state={state}
              dispatch={dispatch}
            />
          ))}
      </div>

      <Button
        title="New hatch"
        icon={PlusIcon}
        onClick={showModal(dispatch, { type: 'add opening', elementType: 'hatch' })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select opening bulk measure',
            elementType: 'hatch',
          })}
          className={'ml-15'}
          disabled={state.openings.length === 0}
        />
      )}
    </section>
  );
}

export function Doors({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Doors</h3>

      <div className="rounded-cards mb-15">
        {state.openings
          .filter((opening) => opening.libraryItem.type === 'door')
          .map((opening) => (
            <OpeningRow
              opening={opening}
              key={opening.id}
              state={state}
              dispatch={dispatch}
            />
          ))}
      </div>

      <Button
        title="New door"
        icon={PlusIcon}
        onClick={showModal(dispatch, { type: 'add opening', elementType: 'door' })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select opening bulk measure',
            elementType: 'door',
          })}
          className={'ml-15'}
          disabled={state.openings.length === 0}
        />
      )}
    </section>
  );
}

export function Windows({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Windows</h3>

      <div className="rounded-cards mb-15">
        {state.openings
          .filter((opening) => opening.libraryItem.type === 'window')
          .map((opening) => (
            <OpeningRow
              opening={opening}
              key={opening.id}
              state={state}
              dispatch={dispatch}
            />
          ))}
      </div>

      <Button
        title="New window"
        icon={PlusIcon}
        onClick={showModal(dispatch, { type: 'add opening', elementType: 'window' })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select opening bulk measure',
            elementType: 'window',
          })}
          className={'ml-15'}
          disabled={state.openings.length === 0}
        />
      )}
    </section>
  );
}

export function RoofLights({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Roof lights</h3>
      <p className="text-italic mb-15">
        HRP applies the same solar access factor to roof lights as to other windows. This
        is a deviation from SAP2012 (p.26 note 2), where solar factors for roof lights are
        always 1, independent of the overshading.
      </p>

      <div className="rounded-cards mb-15">
        {state.openings
          .filter((opening) => opening.libraryItem.type === 'roof light')
          .map((opening) => (
            <OpeningRow
              opening={opening}
              key={opening.id}
              state={state}
              dispatch={dispatch}
            />
          ))}
      </div>

      <Button
        title="New roof light"
        icon={PlusIcon}
        onClick={showModal(dispatch, { type: 'add opening', elementType: 'roof light' })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select opening bulk measure',
            elementType: 'roof light',
          })}
          className={'ml-15'}
          disabled={state.openings.length === 0}
        />
      )}
    </section>
  );
}

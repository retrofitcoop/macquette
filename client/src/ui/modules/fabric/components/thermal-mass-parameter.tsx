import React from 'react';

import { Action } from '../reducer';
import type { State } from './../state';
import { MyRadioGroup } from './lockable-inputs';

type Dispatcher = (action: Action) => void;
type SubProps = {
  state: State;
  dispatch: Dispatcher;
};

export function ThermalMassParameter({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Thermal Mass Parameter (TMP)</h3>

      <p>
        The thermal mass parameter (TMP) gives an indication of how quickly a structure
        will absorb and release heat. You can choose to ignore the k value of individual
        elements and use a global TMP value instead.
      </p>

      <p>
        In multi-layered constructions - e.g. timber frame with external masonry leaf -
        the construction of the inner leaf is the more important factor.
      </p>

      <p>Use:</p>

      <MyRadioGroup
        options={[
          {
            value: 'no override',
            display: 'the k value of individual elements',
          },
          {
            value: 'low',
            display: 'timber or steel frame with lightweight infill (low TMP)',
          },
          {
            value: 'medium',
            display:
              'masonry walls with timber floors or other mixed structure (medium TMP)',
          },
          {
            value: 'high',
            display: 'masonry walls and solid floors (high TMP)',
          },
        ]}
        labelClasses={['mb-7']}
        radioClasses={['mr-7', 'big-checkbox']}
        value={state.thermalMassParameter}
        onChange={(value) =>
          dispatch({
            type: 'fabric/set thermal mass parameter',
            value,
          })
        }
      />
    </section>
  );
}

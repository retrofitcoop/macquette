import { z } from 'zod';
import { projectSchema } from '../../../data-schemas/project';
import {
  Fabric,
  FabricElementInput,
  FabricMeasureInput,
  OpeningMeasure,
} from '../../../data-schemas/scenario/fabric';
import { sum } from '../../../helpers/array-reducers';
import { Orientation } from '../../../model/enums/orientation';
import { Overshading } from '../../../model/enums/overshading';
import { thermalMassParameterTable } from './extractor';
import type {
  AppliedFloorMeasure,
  AppliedOpeningMeasure,
  AppliedWallMeasure,
  Floor,
  Opening,
  State,
  WallLike,
} from './state';

function wallLikeTypeToLegacy(type: 'external wall' | 'party wall' | 'loft' | 'roof') {
  switch (type) {
    case 'external wall': {
      return 'Wall';
    }
    case 'party wall': {
      return 'Party_wall';
    }
    case 'loft': {
      return 'Loft';
    }
    case 'roof': {
      return 'Roof';
    }
  }
}

function windowLikeTypeToLegacy(type: 'door' | 'window' | 'roof light') {
  switch (type) {
    case 'door':
      return 'Door' as const;
    case 'window':
      return 'Window' as const;
    case 'roof light':
      return 'Roof_light' as const;
  }
}

function elementToMeasure(
  wall: AppliedWallMeasure | AppliedFloorMeasure | AppliedOpeningMeasure,
): FabricMeasureInput {
  const common = {
    id: wall.id,
    lib: wall.libraryItem.tag,
    name: wall.libraryItem.name,
    source: wall.libraryItem.source,
    description: wall.libraryItem.description,
    uvalue: wall.libraryItem.uValue ?? '',
    kvalue: wall.libraryItem.kValue,
    associated_work: wall.libraryItem.associated_work,
    benefits: wall.libraryItem.benefits,
    cost: wall.libraryItem.cost,
    disruption: wall.libraryItem.disruption,
    key_risks: wall.libraryItem.key_risks,
    maintenance: wall.libraryItem.maintenance,
    min_cost: wall.libraryItem.min_cost,
    notes: wall.libraryItem.notes,
    performance: wall.libraryItem.performance,
    who_by: wall.libraryItem.who_by,
    lifetimeYears: wall.libraryItem.lifetimeYears,
    carbonType: wall.libraryItem.carbonType,
    carbonHighBaseUpfront: wall.libraryItem.carbonHighBaseUpfront,
    carbonHighBaseBiogenic: wall.libraryItem.carbonHighBaseBiogenic,
    carbonHighPerUnitUpfront: wall.libraryItem.carbonHighPerUnitUpfront,
    carbonHighPerUnitBiogenic: wall.libraryItem.carbonHighPerUnitBiogenic,
    carbonLowBaseUpfront: wall.libraryItem.carbonLowBaseUpfront,
    carbonLowBaseBiogenic: wall.libraryItem.carbonLowBaseBiogenic,
    carbonLowPerUnitUpfront: wall.libraryItem.carbonLowPerUnitUpfront,
    carbonLowPerUnitBiogenic: wall.libraryItem.carbonLowPerUnitBiogenic,
    carbonSource: wall.libraryItem.carbonSource,
  };

  if (wall.elementType === 'floor') {
    return {
      ...common,
      type: 'Floor',
      tags: ['Floor'],
      cost_units: wall.libraryItem.cost_units,
    };
  } else if (wall.elementType === 'opening') {
    if (wall.libraryItem.type === 'hatch') {
      return {
        ...common,
        type: 'Hatch',
        tags: ['Hatch'],
        cost_units: wall.libraryItem.cost_units,
      };
    } else {
      return {
        ...common,
        type: windowLikeTypeToLegacy(wall.libraryItem.type),
        tags: [windowLikeTypeToLegacy(wall.libraryItem.type)],
        g: wall.libraryItem.g,
        gL: wall.libraryItem.gL,
        ff: wall.libraryItem.ff,
        cost_units: wall.libraryItem.cost_units,
      } satisfies OpeningMeasure;
    }
  } else {
    return {
      ...common,
      type: wallLikeTypeToLegacy(wall.libraryItem.type),
      tags: [wallLikeTypeToLegacy(wall.libraryItem.type)],
      EWI: wall.libraryItem.isExternalWallInsulation,
      cost_units: wall.libraryItem.cost_units,
    };
  }
}

function elementToOutput(wall: WallLike | Floor | Opening): FabricElementInput {
  const common = {
    id: wall.id,
    lib: wall.libraryItem.tag,
    name: wall.libraryItem.name,
    source: wall.libraryItem.source,
    description: wall.libraryItem.description,
    uvalue: wall.libraryItem.uValue ?? '',
    kvalue: wall.libraryItem.kValue,
    location: wall.inputs.location,
  };

  function commonMeasure(
    measure: AppliedWallMeasure | AppliedFloorMeasure | AppliedOpeningMeasure,
  ) {
    return {
      associated_work: measure.libraryItem.associated_work,
      benefits: measure.libraryItem.benefits,
      cost: measure.libraryItem.cost,
      disruption: measure.libraryItem.disruption,
      key_risks: measure.libraryItem.key_risks,
      maintenance: measure.libraryItem.maintenance,
      min_cost: measure.libraryItem.min_cost,
      notes: measure.libraryItem.notes,
      performance: measure.libraryItem.performance,
      who_by: measure.libraryItem.who_by,
      cost_total: measure.outputs.costTotal,
      quantity: measure.outputs.costQuantity,
      cost_units: measure.libraryItem.cost_units,
      carbonType: measure.libraryItem.carbonType,
      carbonHighBaseUpfront: measure.libraryItem.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: measure.libraryItem.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: measure.libraryItem.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: measure.libraryItem.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: measure.libraryItem.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: measure.libraryItem.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: measure.libraryItem.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: measure.libraryItem.carbonLowPerUnitBiogenic,
      carbonSource: measure.libraryItem.carbonSource,
    };
  }

  if (wall.elementType === 'floor') {
    return {
      ...common,
      type: 'Floor',
      tags: ['Floor'],
      libraryFloorType: wall.libraryItem.type,
      area: wall.inputs.area ?? '',
      perimeter: wall.inputs.exposedPerimeter ?? '',
      selectedFloorType: wall.inputs.selectedFloorType,
      perFloorTypeSpec: wall.inputs.perFloorTypeSpec,
      ...(wall.type === 'measure' ? commonMeasure(wall) : null),
    };
  } else if (wall.elementType === 'opening') {
    if (wall.libraryItem.type === 'hatch') {
      return {
        ...common,
        type: 'Hatch',
        tags: ['Hatch'],
        area: wall.inputs.area[wall.inputs.area.type].area ?? '',
        areaInputs: wall.inputs.area,
        location: wall.inputs.location,
        subtractfrom: wall.inputs.subtractFrom,
        ...(wall.type === 'measure' ? commonMeasure(wall) : null),
      };
    } else {
      return {
        ...common,
        type: windowLikeTypeToLegacy(wall.libraryItem.type),
        tags: [windowLikeTypeToLegacy(wall.libraryItem.type)],
        area: wall.inputs.area[wall.inputs.area.type].area ?? '',
        areaInputs: wall.inputs.area,
        g: wall.libraryItem.g,
        gL: wall.libraryItem.gL,
        ff: wall.libraryItem.ff,
        orientation:
          wall.inputs.orientation === null
            ? ''
            : new Orientation(wall.inputs.orientation).index0,
        overshading:
          wall.inputs.overshading === null
            ? ''
            : new Overshading(wall.inputs.overshading).index0,
        subtractfrom: wall.inputs.subtractFrom,
        ...(wall.type === 'measure' ? commonMeasure(wall) : null),
      };
    }
  } else {
    return {
      ...common,
      type: wallLikeTypeToLegacy(wall.libraryItem.type),
      tags: [wallLikeTypeToLegacy(wall.libraryItem.type)],
      area: wall.inputs.area[wall.inputs.area.type].area ?? '',
      areaInputs: wall.inputs.area,
      ...(wall.type === 'measure'
        ? {
            ...commonMeasure(wall),
            EWI: wall.libraryItem.isExternalWallInsulation,
          }
        : null),
    };
  }
}

export function mutateLegacyData(
  { project: projectRaw }: { project: unknown },
  { scenarioId }: { scenarioId: string | null },
  state: State,
) {
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  const project = projectRaw as z.input<typeof projectSchema>;
  if (scenarioId === null) {
    throw new Error("can't mutate nonexistent scenario");
  }
  const data = project.data[scenarioId];
  if (data === undefined) {
    throw new Error("can't mutate nonexistent scenario");
  }

  data.fabric = data.fabric ?? {};
  data.measures = data.measures ?? {};

  const thermalMassParameterValue =
    thermalMassParameterTable.find(({ label }) => state.thermalMassParameter === label)
      ?.value ?? null;

  if (thermalMassParameterValue !== null) {
    data.fabric.global_TMP = true;
    data.fabric.global_TMP_value = thermalMassParameterValue;
  } else {
    data.fabric.global_TMP = false;
    delete data.fabric.global_TMP_value;
  }

  if (state.thermalBridgingOverride !== null) {
    data.fabric.thermal_bridging_override = true;
    data.fabric.thermal_bridging_yvalue =
      state.thermalBridgingOverride.value ?? undefined;
    data.measures.thermal_bridging = {
      measure: {
        value: state.thermalBridgingOverride.value,
        description: state.thermalBridgingOverride.reason,
      },
    };
  } else {
    data.fabric.thermal_bridging_override = false;
    delete data.fabric?.thermal_bridging_yvalue;
    delete data.measures?.thermal_bridging;
  }

  const elements: Fabric['elements'] = [];
  const measures: Fabric['measures'] = {};

  for (const element of [...state.walls, ...state.floors, ...state.openings]) {
    const id = typeof element.id === 'number' ? element.id : parseInt(element.id, 10);
    elements.push(elementToOutput(element));

    const inBulkMeasure =
      state.bulkMeasures.findIndex((bm) => bm.appliesTo.includes(element.id)) !== -1;

    if (element.type === 'measure' && !inBulkMeasure) {
      measures[id] = {
        measure: {
          ...elementToMeasure(element),
          location: element.inputs.location,
          cost_total: element.outputs.costTotal,
          quantity: element.outputs.costQuantity,
        },
      };
    }
  }

  for (const measure of state.bulkMeasures) {
    if (measure.appliesTo.length !== 0) {
      const measureElements = measure.appliesTo
        .map((id) =>
          [...state.walls, ...state.floors, ...state.openings].find(
            (element) => element.id === id,
          ),
        )
        .filter(
          (
            wall,
          ): wall is AppliedWallMeasure | AppliedFloorMeasure | AppliedOpeningMeasure =>
            wall !== undefined && wall.type === 'measure',
        );
      const firstElement = measureElements[0];
      if (firstElement === undefined) {
        throw new Error("couldn't find first element in a bulk measure");
      }

      measures[measure.id] = {
        measure: {
          ...elementToMeasure(firstElement),
          location: measureElements.map((elem) => elem.inputs.location).join(', '),
          cost_total: sum(measureElements.map((elem) => elem.outputs.costTotal)),
          quantity: sum(measureElements.map((elem) => elem.outputs.costQuantity)),
        },
        original_elements: Object.fromEntries(
          measureElements.map((elem) => [elem.id, { id: elem.id }]),
        ),
      };
    }
  }

  data.fabric.elements = elements;
  data.fabric.measures = measures;
}

import { Project } from '../../../../data-schemas/project';
import { Scenario } from '../../../../data-schemas/scenario';
import type { WallLikeElement as LegacyAppliedWall } from '../../../../data-schemas/scenario/fabric';
import { isWallLike } from '../../../../data-schemas/scenario/fabric';
import { Result } from '../../../../helpers/result';
import { calcMeasureQtyAndCost } from '../../../../measures';
import { Model } from '../../../../model/model';
import { WallLike as WallLikeModel } from '../../../../model/modules/fabric/element-types';
import type { State, WallLike } from './../state';
import { calcAreaSpec, getOriginalElement } from './helpers';

function extractWallLikeModelElement(
  model: Model | null,
  elementId: string | number,
): Result<WallLikeModel, Error> {
  const parsedElementId =
    typeof elementId === 'number' ? elementId : parseFloat(elementId);
  if (model === null) {
    return Result.err(new Error('No model to extract fabric model element from'));
  }
  const element = model.normal.fabric.getElementById(parsedElementId);
  if (element === null) {
    return Result.err(
      new Error(
        `Could not find a fabric model element with the provided ID: ${elementId}`,
      ),
    );
  }
  if (!(element instanceof WallLikeModel)) {
    return Result.err(
      new Error(`Fabric model element with ID ${elementId} was not a floor`),
    );
  }
  return Result.ok(element);
}

function extractWallElementFromLegacy(legacyWall: LegacyAppliedWall): WallLike {
  return {
    id: legacyWall.id,
    type: 'element',
    elementType: 'wall',
    libraryItem: {
      tag: legacyWall.lib,
      type: legacyWall.type,
      name: legacyWall.name,
      description: legacyWall.description,
      source: legacyWall.source,
      uValue: legacyWall.uvalue,
      kValue: legacyWall.kvalue,
    },
    inputs: {
      location: legacyWall.location,
      area: calcAreaSpec(
        legacyWall.areaInputs,
        legacyWall.area,
        legacyWall.l,
        legacyWall.h,
      ),
    },
    modelElement: null,
    revertTo: null,
  };
}

function extractWallFromLegacy(
  legacyWall: LegacyAppliedWall,
  fabric: Exclude<Scenario, undefined>['fabric'],
  bulkMeasures: State['bulkMeasures'],
): WallLike {
  function findMeasureIdx(id: string | number): string | number | null {
    const measuresList = fabric?.measures ?? {};

    const bulkMeasure = bulkMeasures.find((measure) => measure.appliesTo.includes(id));
    if (bulkMeasure !== undefined) {
      return bulkMeasure.id;
    }

    const measure = measuresList[id];
    if (measure !== undefined && measure.original_elements === undefined) {
      return id;
    }

    return null;
  }

  const measureIdx = findMeasureIdx(legacyWall.id);
  if (measureIdx !== null) {
    const area = calcAreaSpec(
      legacyWall.areaInputs,
      legacyWall.area,
      legacyWall.l,
      legacyWall.h,
    );

    // We only use measure data for stuff that is constant across
    // different elements, i.e. not inputs or calculations
    const measureData = fabric?.measures?.[measureIdx]?.measure;
    if (measureData === undefined) {
      throw new Error(`unreachable (bad array access ${measureIdx})`);
    } else if (!('type' in measureData)) {
      throw new Error(`no type in measure data (array index ${measureIdx})`);
    } else if (
      measureData.type !== 'external wall' &&
      measureData.type !== 'party wall' &&
      measureData.type !== 'roof' &&
      measureData.type !== 'loft'
    ) {
      throw new Error(`Measure data for ${legacyWall.type} is ${measureData.type})`);
    }

    const [costQuantity, costTotal] = calcMeasureQtyAndCost({
      costUnits: 'sqm',
      area: area[area.type].area ?? 0,
      costPerUnit: measureData.cost,
      baseCost: measureData.min_cost,
      isExternalWallInsulation: measureData.EWI,
    });

    let type = legacyWall.type;

    // Sometimes loft elements have the wrong type ("roof"). Fix that by using the
    // type data from the measure tags.
    if ('tags' in measureData && measureData.tags !== undefined) {
      const tagType = measureData.tags[0];
      const namedType = legacyWall.type;
      if (tagType !== namedType) {
        type = tagType;
      }
    }

    return {
      id: legacyWall.id,
      elementType: 'wall',
      type: 'measure',
      libraryItem: {
        tag: legacyWall.lib,
        type,
        name: legacyWall.name,
        source: legacyWall.source !== '' ? legacyWall.source : measureData.source,
        uValue: legacyWall.uvalue,
        kValue: legacyWall.kvalue,
        isExternalWallInsulation: measureData.EWI,
        associated_work: measureData.associated_work,
        benefits: measureData.benefits,
        cost: measureData.cost,
        cost_units: measureData.cost_units,
        description: measureData.description,
        disruption: measureData.disruption,
        key_risks: measureData.key_risks,
        min_cost: measureData.min_cost,
        maintenance: measureData.maintenance,
        lifetimeYears: measureData.lifetimeYears,
        notes: measureData.notes,
        performance: measureData.performance,
        who_by: measureData.who_by,
        carbonType: 'high/low',
        carbonHighBaseUpfront: measureData.carbonHighBaseUpfront,
        carbonHighBaseBiogenic: measureData.carbonHighBaseBiogenic,
        carbonHighPerUnitUpfront: measureData.carbonHighPerUnitUpfront,
        carbonHighPerUnitBiogenic: measureData.carbonHighPerUnitBiogenic,
        carbonLowBaseUpfront: measureData.carbonLowBaseUpfront,
        carbonLowBaseBiogenic: measureData.carbonLowBaseBiogenic,
        carbonLowPerUnitUpfront: measureData.carbonLowPerUnitUpfront,
        carbonLowPerUnitBiogenic: measureData.carbonLowPerUnitBiogenic,
        carbonSource: measureData.carbonSource,
      },
      inputs: {
        location: legacyWall.location,
        area: calcAreaSpec(
          legacyWall.areaInputs,
          legacyWall.area,
          legacyWall.l,
          legacyWall.h,
        ),
      },
      outputs: {
        costQuantity,
        costTotal,
      },
      modelElement: null,
      revertTo: null,
    };
  }

  return extractWallElementFromLegacy(legacyWall);
}

export function getWalls(
  currentScenario: Scenario,
  model: Model | null,
  bulkMeasures: State['bulkMeasures'],
  project: Project,
): WallLike[] {
  return (
    currentScenario?.fabric?.elements?.filter(isWallLike).map((legacyWall) => {
      const wall = extractWallFromLegacy(
        legacyWall,
        currentScenario.fabric,
        bulkMeasures,
      );

      wall.modelElement = extractWallLikeModelElement(model, legacyWall.id)
        .mapErr(() => null)
        .coalesce();

      if (wall.type === 'measure') {
        const element = getOriginalElement(
          wall.id,
          project.data,
          currentScenario?.created_from,
        );
        if (element !== null && isWallLike(element)) {
          wall.revertTo = extractWallElementFromLegacy(element);
        }
      }
      return wall;
    }) ?? []
  );
}

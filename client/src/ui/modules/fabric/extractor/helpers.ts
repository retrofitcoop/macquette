import { Scenario } from '../../../../data-schemas/scenario';
import type {
  FabricElement,
  AreaInputs as LegacyAreaInputs,
} from '../../../../data-schemas/scenario/fabric';
import { coalesceEmptyString } from '../../../../data-schemas/scenario/value-schemas';
import type { AreaSpec } from './../state';

export function calcAreaSpec(
  areaInputs: LegacyAreaInputs | undefined,
  area: number | '',
  length: number | '',
  height: number | '',
): AreaSpec {
  const type = areaInputs?.type ?? 'legacy';

  switch (type) {
    case 'legacy': {
      const legacyModelWouldCalculateArea =
        height !== '' && height !== 0 && length !== '' && length !== 0;

      if (legacyModelWouldCalculateArea) {
        return {
          type: 'dimensions',
          specific: {
            area: coalesceEmptyString(area, null),
          },
          dimensions: {
            length,
            height,
            area: length * height,
          },
        };
      } else {
        return {
          type: 'specific',
          specific: { area: coalesceEmptyString(area, null) },
          dimensions: { length: null, height: null, area: null },
        };
      }
    }
    case 'specific':
    case 'dimensions': {
      const specific = areaInputs?.specific ?? { area: null };
      const dimensions = areaInputs?.dimensions ?? {
        area: null,
        height: null,
        length: null,
      };

      return { type, specific, dimensions };
    }
  }
}

export function getOriginalElement(
  id: string | number,
  scenarios: Record<string, Scenario>,
  createdFrom: string | undefined,
): FabricElement | null {
  if (createdFrom === undefined) {
    return null;
  }
  const prevScenario = scenarios[createdFrom];
  if (prevScenario === undefined) {
    return null;
  }
  const legacyRevertTarget = prevScenario.fabric?.elements?.find(
    (element) => element.id === id,
  );
  return legacyRevertTarget ?? null;
}

import React from 'react';

import { z } from 'zod';
import { HotWaterControlTypeMeasure } from '../../../data-schemas/libraries/hot-water-control-type';
import {
  HotWaterStorage,
  HotWaterStorageMeasure,
} from '../../../data-schemas/libraries/storage-type';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { DeepPartial, safeMerge } from '../../../helpers/safe-merge';
import { Model } from '../../../model/model';
import { HammerIcon, PlusIcon } from '../../icons';
import { Button } from '../../input-components/button';
import { CheckboxInput } from '../../input-components/checkbox';
import { FormGrid } from '../../input-components/forms';
import { SelectWaterStorageControlType } from '../../input-components/library-picker/hot-water-controls';
import {
  SelectHotWaterStorage,
  SelectHotWaterStorageMeasure,
} from '../../input-components/library-picker/hot-water-storage';
import {
  FixedPipeworkInsulationMeasure,
  SelectPipeworkInsulationMeasure,
} from '../../input-components/library-picker/pipework-insulation';
import { NumberInput } from '../../input-components/number';
import { RadioGroup } from '../../input-components/radio-group';
import { colourForStatus } from '../fabric/components/measure-status';

export type HotWaterSystemAction =
  | {
      type: 'hws/external update';
      state: Pick<
        HotWaterSystemState,
        | 'model'
        | 'solarHotWater'
        | 'storageControlType'
        | 'storageControlTypeMeasure'
        | 'primaryPipeworkInsulation'
        | 'primaryPipeworkInsulationMeasure'
        | 'storageSystem'
      >;
    }
  | {
      type: 'hws/modify';
      update: DeepPartial<
        Pick<
          HotWaterSystemState,
          | 'solarHotWater'
          | 'storageControlType'
          | 'primaryPipeworkInsulation'
          | 'storageSystem'
        >
      >;
    }
  | { type: 'hws/show modal'; modal: HotWaterSystemState['modal'] }
  | {
      type: 'hws/apply control measure';
      libraryItem: HotWaterControlTypeMeasure;
    }
  | {
      type: 'hws/apply pipework measure';
      libraryItem: FixedPipeworkInsulationMeasure;
    }
  | {
      type: 'hws/add storage';
      libraryItem: HotWaterStorage;
    }
  | {
      type: 'hws/apply storage measure';
      libraryItem: HotWaterStorageMeasure;
    }
  | {
      type: 'hws/delete storage system';
    };

export type HotWaterSystemState = {
  modal: 'control type' | 'pipework insulation' | 'storage' | 'storage measure' | null;
  model: Model | null;

  solarHotWater: boolean;
  storageControlType:
    | null
    | 'no_cylinder_thermostat'
    | 'Cylinder thermostat, water heating not separately timed'
    | 'Cylinder thermostat, water heating separately timed';
  storageControlTypeMeasure: HotWaterControlTypeMeasure | null;
  primaryPipeworkInsulation:
    | null
    | 'All accesible piperwok insulated'
    | 'First 1m from cylinder insulated'
    | 'Fully insulated primary pipework'
    | 'Uninsulated primary pipework';
  primaryPipeworkInsulationMeasure: FixedPipeworkInsulationMeasure | null;
  storageSystem:
    | { type: 'none' }
    | {
        type: 'item';
        inputs: { inside: boolean; dedicatedStorage: number | null };
        libraryItem: HotWaterStorage;
      }
    | {
        type: 'measure';
        inputs: { inside: boolean; dedicatedStorage: number | null };
        libraryItem: HotWaterStorageMeasure;
      };
};

export const initialHotWaterSystemState: HotWaterSystemState = {
  modal: null,
  model: null,
  solarHotWater: false,
  storageControlType: null,
  storageControlTypeMeasure: null,
  primaryPipeworkInsulation: null,
  primaryPipeworkInsulationMeasure: null,
  storageSystem: { type: 'none' },
};

export function hotWaterSystemReducer(
  state: HotWaterSystemState,
  action: HotWaterSystemAction,
): HotWaterSystemState {
  switch (action.type) {
    case 'hws/external update':
      return { ...state, ...action.state };
    case 'hws/modify':
      return safeMerge(state, action.update);
    case 'hws/show modal':
      return { ...state, modal: action.modal };
    case 'hws/apply control measure': {
      state.modal = null;
      state.storageControlType = action.libraryItem.control_type;
      state.storageControlTypeMeasure = action.libraryItem;
      return state;
    }
    case 'hws/apply pipework measure': {
      state.modal = null;
      state.primaryPipeworkInsulation = action.libraryItem.pipework_insulation;
      state.primaryPipeworkInsulationMeasure = action.libraryItem;
      return state;
    }
    case 'hws/add storage': {
      state.modal = null;
      state.storageSystem = {
        type: 'item',
        inputs: { inside: true, dedicatedStorage: null },
        libraryItem: action.libraryItem,
      };
      return state;
    }
    case 'hws/apply storage measure': {
      state.modal = null;
      state.storageSystem = {
        type: 'measure',
        inputs:
          state.storageSystem.type !== 'none'
            ? state.storageSystem.inputs
            : { inside: true, dedicatedStorage: null },
        libraryItem: action.libraryItem,
      };
      return state;
    }
    case 'hws/delete storage system': {
      state.storageSystem = { type: 'none' };
      return state;
    }
  }
}

export function hotWaterSystemMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: HotWaterSystemState,
): void {
  scenario.water_heating ??= {};

  scenario.use_SHW = state.solarHotWater;
  scenario.water_heating.solar_water_heating = state.solarHotWater;
  scenario.water_heating.hot_water_control_type = state.storageControlType ?? undefined;
  scenario.water_heating.pipework_insulation =
    state.primaryPipeworkInsulation ?? undefined;

  scenario.measures ??= {};
  scenario.measures.water_heating ??= {};
  if (state.storageControlTypeMeasure !== null) {
    scenario.measures.water_heating.hot_water_control_type = {
      measure: {
        ...state.storageControlTypeMeasure,
        cost_total: state.storageControlTypeMeasure.cost,
        quantity: 1,
      },
    };
  } else {
    delete scenario.measures.water_heating.hot_water_control_type;
  }

  if (state.primaryPipeworkInsulationMeasure !== null) {
    scenario.measures.water_heating.pipework_insulation = {
      measure: {
        ...state.primaryPipeworkInsulationMeasure,
        cost_total: state.primaryPipeworkInsulationMeasure.cost,
        quantity: 1,
      },
    };
  } else {
    delete scenario.measures.water_heating.pipework_insulation;
  }

  if (state.storageSystem.type === 'none') {
    delete scenario.water_heating.storage_type;
    delete scenario.water_heating.contains_dedicated_solar_storage_or_WWHRS;
    delete scenario.water_heating.hot_water_store_in_dwelling;
    delete scenario.measures.water_heating.storage_type_measures;
  } else {
    scenario.water_heating.storage_type = state.storageSystem.libraryItem;
    scenario.water_heating.contains_dedicated_solar_storage_or_WWHRS =
      state.storageSystem.inputs.dedicatedStorage ?? undefined;
    scenario.water_heating.hot_water_store_in_dwelling =
      state.storageSystem.inputs.inside;

    if (state.storageSystem.type === 'item') {
      delete scenario.measures.water_heating.storage_type_measures;
    } else if (state.storageSystem.type === 'measure') {
      scenario.measures.water_heating.storage_type_measures = {
        measure: {
          ...state.storageSystem.libraryItem,
          quantity: 1,
          cost_total: state.storageSystem.libraryItem.cost,
        },
      };
    }
  }
}

export function hotWaterSystemExtractor(
  scenario: Scenario,
  model: Model | null,
): HotWaterSystemAction[] {
  let storageSystem: HotWaterSystemState['storageSystem'] = { type: 'none' };

  const item = scenario?.water_heating?.storage_type ?? null;
  if (item !== null) {
    const manufacturer_loss_factor =
      coalesceEmptyString(item.manufacturer_loss_factor, 0) ?? 0;

    const measure = scenario?.measures?.water_heating?.storage_type_measures?.measure;
    if (measure !== undefined) {
      storageSystem = {
        type: 'measure',
        inputs: {
          inside: scenario?.water_heating?.hot_water_store_in_dwelling ?? false,
          dedicatedStorage:
            coalesceEmptyString(
              scenario?.water_heating?.contains_dedicated_solar_storage_or_WWHRS,
              null,
            ) ?? null,
        },
        libraryItem: {
          ...measure,
          storage_volume: coalesceEmptyString(measure.storage_volume, 0) ?? 0,
          declared_loss_factor_known: measure.declared_loss_factor_known ?? false,
          manufacturer_loss_factor:
            manufacturer_loss_factor === false ? 0 : manufacturer_loss_factor,
          temperature_factor_a: coalesceEmptyString(measure.temperature_factor_a, 0) ?? 0,
          loss_factor_b: coalesceEmptyString(measure.loss_factor_b, 0) ?? 0,
          volume_factor_b: coalesceEmptyString(measure.volume_factor_b, 0) ?? 0,
          temperature_factor_b: coalesceEmptyString(measure.temperature_factor_b, 0) ?? 0,
        },
      };
    } else {
      storageSystem = {
        type: 'item',
        inputs: {
          inside: scenario?.water_heating?.hot_water_store_in_dwelling ?? false,
          dedicatedStorage:
            coalesceEmptyString(
              scenario?.water_heating?.contains_dedicated_solar_storage_or_WWHRS,
              null,
            ) ?? null,
        },
        libraryItem: {
          name: item.name ?? '',
          tag: item.tag ?? '',
          source: item.source ?? '',
          category: item.category ?? 'Cylinders with inmersion',
          storage_volume: coalesceEmptyString(item.storage_volume, 0) ?? 0,
          declared_loss_factor_known: item.declared_loss_factor_known ?? false,
          manufacturer_loss_factor:
            manufacturer_loss_factor === false ? 0 : manufacturer_loss_factor,
          temperature_factor_a: coalesceEmptyString(item.temperature_factor_a, 0) ?? 0,
          loss_factor_b: coalesceEmptyString(item.loss_factor_b, 0) ?? 0,
          volume_factor_b: coalesceEmptyString(item.volume_factor_b, 0) ?? 0,
          temperature_factor_b: coalesceEmptyString(item.temperature_factor_b, 0) ?? 0,
        },
      };
    }
  }

  return [
    {
      type: 'hws/external update',
      state: {
        model,
        solarHotWater: scenario?.use_SHW ?? false,
        storageControlType: scenario?.water_heating?.hot_water_control_type ?? null,
        storageControlTypeMeasure:
          scenario?.measures?.water_heating?.hot_water_control_type?.measure ?? null,
        primaryPipeworkInsulation: scenario?.water_heating?.pipework_insulation ?? null,
        primaryPipeworkInsulationMeasure:
          scenario?.measures?.water_heating?.pipework_insulation?.measure ?? null,
        storageSystem,
      },
    },
  ];
}

export function HotWaterSystem({
  state,
  dispatch,
  isBaseline,
}: {
  state: HotWaterSystemState;
  dispatch: (a: HotWaterSystemAction) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="line-top mb-45">
      <h3 className="ma-0 mb-15">Hot water system</h3>

      {state.storageSystem.type === 'none' &&
        (isBaseline ? (
          <Button
            title="Add storage system"
            icon={PlusIcon}
            onClick={() => dispatch({ type: 'hws/show modal', modal: 'storage' })}
          />
        ) : (
          <Button
            title="Apply storage system measure"
            icon={HammerIcon}
            onClick={() => dispatch({ type: 'hws/show modal', modal: 'storage measure' })}
          />
        ))}

      {state.storageSystem.type !== 'none' && (
        <FormGrid>
          <span>Storage system:</span>
          <div>
            <div>
              {state.storageSystem.type === 'measure' && (
                <span
                  className="circle mr-7"
                  title="Measure applied"
                  style={{
                    backgroundColor: `var(${colourForStatus['new measure']})`,
                  }}
                />
              )}
              {state.storageSystem.libraryItem.tag}:{' '}
              {state.storageSystem.libraryItem.name}
            </div>

            <div className="d-flex gap-7">
              {state.storageSystem.type === 'measure' ? (
                <Button
                  title="Replace storage system measure"
                  icon={HammerIcon}
                  onClick={() =>
                    dispatch({ type: 'hws/show modal', modal: 'storage measure' })
                  }
                />
              ) : isBaseline ? (
                <>
                  <Button
                    title="Delete storage system"
                    onClick={() => dispatch({ type: 'hws/delete storage system' })}
                  />
                  <Button
                    title="Replace storage system"
                    onClick={() => dispatch({ type: 'hws/show modal', modal: 'storage' })}
                  />
                </>
              ) : (
                <Button
                  title="Apply storage system measure"
                  icon={HammerIcon}
                  onClick={() =>
                    dispatch({ type: 'hws/show modal', modal: 'storage measure' })
                  }
                />
              )}
            </div>
          </div>

          <span>Hot water storage control type:</span>
          <div>
            <div>
              <RadioGroup
                value={state.storageControlType}
                onChange={(storageControlType) =>
                  dispatch({ type: 'hws/modify', update: { storageControlType } })
                }
                radioClasses={['mr-7']}
                disabled={state.storageControlTypeMeasure !== null}
                options={[
                  {
                    value: 'no_cylinder_thermostat',
                    display: 'No cylinder thermostat',
                  },
                  {
                    value: 'Cylinder thermostat, water heating not separately timed',
                    display: 'Cylinder thermostat, water heating not separately timed',
                  },
                  {
                    value: 'Cylinder thermostat, water heating separately timed',
                    display: 'Cylinder thermostat, water heating separately timed',
                  },
                ]}
              />
            </div>
            {!isBaseline &&
              (state.storageControlTypeMeasure === null ? (
                <Button
                  title="Apply measure"
                  icon={HammerIcon}
                  onClick={() =>
                    dispatch({ type: 'hws/show modal', modal: 'control type' })
                  }
                />
              ) : (
                <div className="mt-15">
                  <div>
                    <span
                      className="circle mr-7"
                      title="Measure applied"
                      style={{
                        backgroundColor: `var(${colourForStatus['new measure']})`,
                      }}
                    />
                    {state.storageControlTypeMeasure.tag}:{' '}
                    {state.storageControlTypeMeasure.name}
                  </div>

                  <Button
                    title="Replace measure"
                    icon={HammerIcon}
                    onClick={() =>
                      dispatch({ type: 'hws/show modal', modal: 'control type' })
                    }
                  />
                </div>
              ))}
          </div>

          <span>Primary pipework insulation:</span>
          <div>
            <div>
              <RadioGroup
                value={state.primaryPipeworkInsulation}
                onChange={(primaryPipeworkInsulation) =>
                  dispatch({
                    type: 'hws/modify',
                    update: { primaryPipeworkInsulation },
                  })
                }
                radioClasses={['mr-7']}
                disabled={state.primaryPipeworkInsulationMeasure !== null}
                options={[
                  { value: 'Uninsulated primary pipework', display: 'Uninsulated' },
                  {
                    value: 'First 1m from cylinder insulated',
                    display: 'First 1m from cylinder',
                  },
                  {
                    value: 'All accesible piperwok insulated',
                    display: 'All accessible',
                  },
                  {
                    value: 'Fully insulated primary pipework',
                    display: 'Fully insulated',
                  },
                ]}
              />
            </div>
            {!isBaseline &&
              (state.primaryPipeworkInsulationMeasure === null ? (
                <Button
                  title="Apply measure"
                  icon={HammerIcon}
                  onClick={() =>
                    dispatch({ type: 'hws/show modal', modal: 'pipework insulation' })
                  }
                />
              ) : (
                <div className="mt-15">
                  <div>
                    <span
                      className="circle mr-7"
                      title="Measure applied"
                      style={{
                        backgroundColor: `var(${colourForStatus['new measure']})`,
                      }}
                    />
                    {state.primaryPipeworkInsulationMeasure.tag}:{' '}
                    {state.primaryPipeworkInsulationMeasure.name}
                  </div>

                  <Button
                    title="Replace measure"
                    icon={HammerIcon}
                    onClick={() =>
                      dispatch({ type: 'hws/show modal', modal: 'pipework insulation' })
                    }
                  />
                </div>
              ))}
          </div>

          <span>Is system inside dwelling?</span>
          <div>
            <CheckboxInput
              value={state.storageSystem.inputs.inside}
              onChange={(inside) =>
                dispatch({
                  type: 'hws/modify',
                  update: { storageSystem: { inputs: { inside } } },
                })
              }
            />
          </div>

          <label htmlFor="solarHotWater">Is there a solar hot water system?</label>
          <div>
            <CheckboxInput
              id="solarHotWater"
              value={state.solarHotWater}
              onChange={(solarHotWater) =>
                dispatch({ type: 'hws/modify', update: { solarHotWater } })
              }
            />
          </div>

          <span>
            Dedicated solar/
            <abbr title="WWHRS: Waste Water Heat Recovery">WWHRS</abbr> volume size:
          </span>
          <div>
            <NumberInput
              value={state.storageSystem.inputs.dedicatedStorage}
              onChange={(dedicatedStorage) =>
                dispatch({
                  type: 'hws/modify',
                  update: { storageSystem: { inputs: { dedicatedStorage } } },
                })
              }
            />
          </div>
        </FormGrid>
      )}

      {state.modal === 'control type' && (
        <SelectWaterStorageControlType
          onSelect={(libraryItem) =>
            dispatch({
              type: 'hws/apply control measure',
              libraryItem,
            })
          }
          onClose={() => dispatch({ type: 'hws/show modal', modal: null })}
          currentItemTag={null}
        />
      )}

      {state.modal === 'pipework insulation' && (
        <SelectPipeworkInsulationMeasure
          onSelect={(libraryItem) =>
            dispatch({
              type: 'hws/apply pipework measure',
              libraryItem,
            })
          }
          onClose={() => dispatch({ type: 'hws/show modal', modal: null })}
        />
      )}

      {state.modal === 'storage' && (
        <SelectHotWaterStorage
          onSelect={(libraryItem) =>
            dispatch({
              type: 'hws/add storage',
              libraryItem,
            })
          }
          onClose={() => dispatch({ type: 'hws/show modal', modal: null })}
        />
      )}

      {state.modal === 'storage measure' && (
        <SelectHotWaterStorageMeasure
          onSelect={(libraryItem) =>
            dispatch({
              type: 'hws/apply storage measure',
              libraryItem,
            })
          }
          onClose={() => dispatch({ type: 'hws/show modal', modal: null })}
        />
      )}
    </section>
  );
}

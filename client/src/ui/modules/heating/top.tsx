import React from 'react';
import { assertNever } from '../../../helpers/assert-never';

import { z } from 'zod';
import { projectSchema } from '../../../data-schemas/project';
import { Result } from '../../../helpers/result';
import { Model } from '../../../model/model';
import { LockableContext } from '../../input-components/lockable-context';
import { AppContext } from '../../module-management/app-context';
import type { UiModule } from '../../module-management/module-type';
import {
  HotWaterDemand,
  Action as HotWaterDemandAction,
  extractor as hotWaterDemandExtractor,
  mutator as hotWaterDemandMutator,
  reducer as hotWaterDemandReducer,
  State as HotWaterDemandState,
  initialState as initialHotWaterDemandState,
  isHotWaterDemandAction,
} from './hot-water-demand';
import {
  initialState as initialSpaceHeatingDemandState,
  isSpaceHeatingDemandAction,
  SpaceHeatingDemand,
  Action as SpaceHeatingDemandAction,
  extractor as spaceHeatingDemandExtractor,
  reducer as spaceHeatingDemandReducer,
  State as SpaceHeatingDemandState,
  mutator as spaceHeatingMutator,
} from './space-heating-demand';

type State = {
  isBaseline: boolean;
  isLocked: boolean;
  model: Model | null;
  hotWaterDemand: HotWaterDemandState;
  spaceHeatingDemand: SpaceHeatingDemandState;
};
export type Action =
  | { type: 'external update'; state: Partial<State> }
  | HotWaterDemandAction
  | SpaceHeatingDemandAction;

export const heatingModuleTop: UiModule<State, Action, never> = {
  name: 'heating-top',
  component: function Heating({ state, dispatch }) {
    return (
      <LockableContext.Provider value={{ locked: state.isLocked }}>
        <section>
          <HotWaterDemand
            state={state.hotWaterDemand}
            model={state.model}
            dispatch={dispatch}
            isBaseline={state.isBaseline}
          />
        </section>
        <section>
          <SpaceHeatingDemand state={state.spaceHeatingDemand} dispatch={dispatch} />
        </section>
      </LockableContext.Provider>
    );
  },
  initialState: () => {
    return {
      isBaseline: true,
      isLocked: false,
      model: null,
      hotWaterDemand: initialHotWaterDemandState,
      spaceHeatingDemand: initialSpaceHeatingDemandState,
    };
  },
  reducer: (state, action) => {
    if (isSpaceHeatingDemandAction(action)) {
      state.spaceHeatingDemand = spaceHeatingDemandReducer(
        state.spaceHeatingDemand,
        action,
      );
      return [state];
    }
    if (isHotWaterDemandAction(action)) {
      state.hotWaterDemand = hotWaterDemandReducer(state.hotWaterDemand, action);
      return [state];
    }
    if (action.type === 'external update') {
      return [{ ...state, ...action.state }];
    }
    assertNever(action);
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ scenarioId, currentScenario, currentModel }: AppContext) => {
      const isBaseline = scenarioId === 'master';
      return Result.ok([
        {
          type: 'external update',
          state: {
            isBaseline,
            isLocked: currentScenario?.locked ?? false,
            model: currentModel.mapErr(() => null).coalesce(),
          },
        },
        ...hotWaterDemandExtractor(currentScenario),
        ...spaceHeatingDemandExtractor(currentScenario),
      ]);
    },
    mutateLegacyData(
      { project: projectRaw }: { project: unknown },
      { scenarioId }: { scenarioId: string | null },
      state: State,
    ) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        throw new Error("can't mutate nonexistent scenario");
      }
      const scenario = project.data[scenarioId];
      if (scenario === undefined) {
        throw new Error("can't mutate nonexistent scenario");
      }

      hotWaterDemandMutator(scenario, state.hotWaterDemand);
      spaceHeatingMutator(scenario, state.spaceHeatingDemand);
    },
  },
};

import React from 'react';
import { assertNever } from '../../../helpers/assert-never';

import { z } from 'zod';
import { projectSchema } from '../../../data-schemas/project';
import { Result } from '../../../helpers/result';
import { LockableContext } from '../../input-components/lockable-context';
import { AppContext } from '../../module-management/app-context';
import type { UiModule } from '../../module-management/module-type';
import {
  HotWaterSystem,
  HotWaterSystemAction,
  HotWaterSystemState,
  hotWaterSystemExtractor,
  hotWaterSystemMutator,
  hotWaterSystemReducer,
  initialHotWaterSystemState,
} from './hot-water-system';

type State = {
  isBaseline: boolean;
  isLocked: boolean;
  hotWaterSystem: HotWaterSystemState;
};
type Action = { type: 'external update'; state: Partial<State> } | HotWaterSystemAction;

export const heatingModuleBottom: UiModule<State, Action, never> = {
  name: 'hot water system',
  component: function SplitEnd({ state, dispatch }) {
    return (
      <LockableContext.Provider value={{ locked: state.isLocked }}>
        <section>
          <HotWaterSystem
            state={state.hotWaterSystem}
            dispatch={dispatch}
            isBaseline={state.isBaseline}
          />
        </section>
      </LockableContext.Provider>
    );
  },
  initialState: () => {
    return {
      isBaseline: true,
      isLocked: false,
      hotWaterSystem: initialHotWaterSystemState,
    };
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'external update':
        return [{ ...state, ...action.state }];

      case 'hws/external update':
      case 'hws/modify':
      case 'hws/show modal':
      case 'hws/apply control measure':
      case 'hws/apply pipework measure':
      case 'hws/add storage':
      case 'hws/apply storage measure':
      case 'hws/delete storage system':
        state.hotWaterSystem = hotWaterSystemReducer(state.hotWaterSystem, action);
        return [state];
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ scenarioId, currentScenario, currentModel }: AppContext) => {
      return Result.ok([
        {
          type: 'external update',
          state: {
            isBaseline: scenarioId === 'master',
            isLocked: currentScenario?.locked ?? false,
          },
        },
        ...hotWaterSystemExtractor(
          currentScenario,
          currentModel.mapErr(() => null).coalesce(),
        ),
      ]);
    },
    mutateLegacyData(
      { project: projectRaw }: { project: unknown },
      { scenarioId }: { scenarioId: string | null },
      state: State,
    ) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        throw new Error("can't mutate nonexistent scenario");
      }
      const scenario = project.data[scenarioId];
      if (scenario === undefined) {
        throw new Error("can't mutate nonexistent scenario");
      }

      hotWaterSystemMutator(scenario, state.hotWaterSystem);
    },
  },
};

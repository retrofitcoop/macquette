import { UnknownLibraryTypeError } from '../../data-schemas/api-metadata';
import { Library } from '../../data-schemas/libraries';

export type LibraryType = Library['type'];

const libraryTypeNames: { [Type in LibraryType]: string } = {
  appliances_and_cooking: 'Appliances and cooking',
  clothes_drying_facilities: 'Clothes drying facilities',
  draught_proofing_measures: 'Draughtproofing measures',
  extract_ventilation_points: 'Extract ventilation points',
  walls: 'Fabric (wall-like)',
  wall_measures: 'Fabric measures (wall-like)',
  floors: 'Fabric (floors)',
  floor_measures: 'Fabric measures (floors)',
  openings: 'Fabric (openings)',
  opening_measures: 'Fabric measures (openings)',
  floor_insulation_materials: 'Floor materials',
  generation_measures: 'Generation measures',
  heating_systems: 'Heating systems',
  heating_systems_measures: 'Heating system measures',
  hot_water_control_type: 'Hot water control types',
  intentional_vents_and_flues: 'Intentional vents and flues',
  intentional_vents_and_flues_measures: 'Intentional vent and flue measures',
  pipework_insulation: 'Pipework insulation measures',
  space_heating_control_type: 'Space heating controls',
  storage_type: 'Hot water storage',
  storage_type_measures: 'Hot water storage measures',
  ventilation_systems: 'Ventilation systems',
  ventilation_systems_measures: 'Ventilation systems measures',
  water_usage: 'Water usage measures',
};

export const libraryTypeOptions: { value: LibraryType; display: string }[] = Object.keys(
  libraryTypeNames,
).map((key) => ({
  // SAFETY: the keys in libraryTypeNames all satisfy LibraryType.
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  value: key as LibraryType,
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  display: libraryTypeNames[key as LibraryType],
}));

export function formatType(type: Library['type'] | UnknownLibraryTypeError) {
  if (type instanceof UnknownLibraryTypeError) {
    return type.type;
  } else {
    return libraryTypeNames[type];
  }
}

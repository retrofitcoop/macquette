import React, { useId, useMemo, useState, useSyncExternalStore } from 'react';

import { sortBy } from 'lodash';
import { Link } from 'wouter';
import {
  LibraryMetadata,
  UnknownLibraryTypeError,
} from '../../data-schemas/api-metadata';
import { DeleteIcon2, PersonPlusFilled, PlusIcon } from '../../ui/icons';
import { Button } from '../../ui/input-components/button';
import { LibraryListStoreType } from '../stores/library-list';
import { OrganisationStoreType } from '../stores/organisation-list';
import { NewLibraryModal } from './new-library-modal';
import { ShareModal } from './share-modal';
import { formatType } from './types';

function formatOwner(owner: LibraryMetadata['owner']) {
  switch (owner.type) {
    case 'global':
      return 'Global';
    case 'personal':
      return owner.email === undefined ? owner.name : `${owner.name} <${owner.email}>`;
    case 'organisation':
      return owner.name;
  }
}

function formatPermissions(access: LibraryMetadata['permissions']) {
  if (!access.can_write) {
    return <span className="badge ml-7">Read-only</span>;
  } else {
    return null;
  }
}

function formatSharedWith(sharedWith: LibraryMetadata['shared_with']) {
  const orgNames = sharedWith.map((org) => org.name);
  if (orgNames.length === 0) {
    return '-';
  } else if (orgNames.length > 1) {
    return `${orgNames.length} organisations`;
  } else {
    return orgNames.join(', ');
  }
}

type ModalType = { type: 'new' } | { type: 'share'; libraryId: string };

type LibraryListStoreParams = {
  libraryStore: LibraryListStoreType;
  organisationStore?: OrganisationStoreType;
};

export function LibraryManager({
  libraryStore,
  organisationStore,
}: LibraryListStoreParams) {
  const libraries = useSyncExternalStore(
    libraryStore.subscribe.bind(libraryStore),
    libraryStore.getSnapshot.bind(libraryStore),
  );
  const [modal, setModal] = useState<null | ModalType>(null);
  const uniqueId = useId();

  const sortedLibraries = useMemo(
    () => sortBy(libraries, (lib) => formatType(lib.type)),
    [libraries],
  );

  return (
    <>
      <div className="mx-auto max-width-1100">
        <section className="px-0 line-top">
          <h1 className="ma-0">Libraries</h1>
        </section>
      </div>

      {modal?.type === 'new' && (
        <NewLibraryModal
          libraryStore={libraryStore}
          organisationStore={organisationStore}
          onClose={() => {
            setModal(null);
          }}
        />
      )}
      {modal !== null && modal.type === 'share' && (
        <ShareModal
          libraryStore={libraryStore}
          libraryId={modal.libraryId}
          organisationStore={organisationStore}
          onClose={() => {
            setModal(null);
          }}
        />
      )}

      <div className="mx-auto max-width-1100 pt-15 d-flex gap-7">
        <Button
          title="New library"
          icon={PlusIcon}
          className="btn-primary"
          onClick={() => setModal({ type: 'new' })}
        />
      </div>

      <div className="mx-auto max-width-1100 mt-15 rounded bg-white border-box-sizing">
        <table className="table table--vertical-middle">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th></th>
              <th scope="col">Type</th>
              <th scope="col">Owner</th>
              <th scope="col">Shared with</th>
              <th scope="col">Last updated</th>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {sortedLibraries.map((lib, idx) => (
              <tr key={lib.id}>
                <th
                  scope="row"
                  id={`${uniqueId}-${idx}-name`}
                  style={{ verticalAlign: 'middle' }}
                >
                  {lib.type instanceof UnknownLibraryTypeError ? (
                    lib.name
                  ) : (
                    <Link href={`/libraries/${lib.id}/`}>
                      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                      <a>{lib.name}</a>
                    </Link>
                  )}
                </th>
                <td>{formatPermissions(lib.permissions)}</td>
                <td>{formatType(lib.type)}</td>
                <td>{formatOwner(lib.owner)}</td>
                <td>
                  <div className="d-flex justify-content-between gap-7">
                    <span>{formatSharedWith(lib.shared_with)}</span>
                    {lib.permissions.can_share && (
                      <Button
                        id={`${uniqueId}-${idx}-sharing`}
                        title=""
                        aria-label="Edit sharing for "
                        aria-labelledby={`${uniqueId}-${idx}-sharing ${uniqueId}-${idx}-name`}
                        icon={PersonPlusFilled}
                        onClick={() =>
                          setModal({
                            type: 'share',
                            libraryId: lib.id,
                          })
                        }
                      />
                    )}
                  </div>
                </td>
                <td className="text-right">
                  {Intl.DateTimeFormat().format(lib.updated_at)}
                </td>
                <td>
                  <Button
                    id={`${uniqueId}-${idx}-delete`}
                    title=""
                    aria-label="Delete"
                    aria-labelledby={`${uniqueId}-${idx}-delete ${uniqueId}-${idx}-name`}
                    icon={DeleteIcon2}
                    className="btn-danger"
                    disabled={lib.permissions.can_write === false}
                    onClick={() => {
                      if (window.confirm(`Really delete library?`)) {
                        libraryStore.delete(lib.id).catch((err) => {
                          alert('Error while deleting library');
                          console.error('Failed while deleting library', err);
                        });
                      }
                    }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

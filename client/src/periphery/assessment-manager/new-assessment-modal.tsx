import React, { useId, useMemo, useState, useSyncExternalStore } from 'react';
import { FormGrid } from '../../ui/input-components/forms';
import { RadioGroup } from '../../ui/input-components/radio-group';
import { TextInput } from '../../ui/input-components/text';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../ui/output-components/modal';
import { AssessmentListStoreType } from '../stores/assessment-list';
import {
  OrganisationListStore,
  OrganisationStoreType,
} from '../stores/organisation-list';

export function NewAssessmentModal({
  assessmentStore,
  organisationStore: orgStoreIn,
  onClose,
}: {
  assessmentStore: AssessmentListStoreType;
  organisationStore?: OrganisationStoreType | undefined;
  onClose: (newId?: string) => void;
}) {
  const organisationStore = useMemo(
    () => (orgStoreIn !== undefined ? orgStoreIn : new OrganisationListStore()),
    [orgStoreIn],
  );
  const organisations = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );

  const ownershipOptions = [
    { value: '__PRIVATE', display: 'Private' },
    ...organisations.map((organisation) => ({
      value: organisation.id,
      display: organisation.name,
    })),
  ];

  const headerId = useId();
  const [name, setName] = useState('');
  const [ownership, setOwnership] = useState<string | null>(null);

  return (
    <Modal headerId={headerId} onClose={onClose}>
      <ModalHeader title="New assessment" onClose={onClose} />
      <ModalBody>
        <FormGrid>
          <label htmlFor="name">Name:</label>
          <div>
            <TextInput id="name" value={name} onChange={setName} autoFocus={true} />
          </div>
          <div id="ownership-label">Ownership:</div>
          <div>
            <RadioGroup
              options={ownershipOptions}
              ariaLabelledBy={'ownership-label'}
              value={ownership}
              onChange={setOwnership}
              radioClasses={['mr-3']}
            />
          </div>
        </FormGrid>
      </ModalBody>
      <ModalFooter>
        <button onClick={() => onClose()} className="btn">
          Cancel
        </button>
        <button
          onClick={() => {
            if (name === '') {
              alert('Name must be set');
              return;
            }
            if (ownership === null) {
              alert('Ownership must be set');
              return;
            }
            assessmentStore
              .create(name, ownership === '__PRIVATE' ? null : ownership)
              .then((id) => {
                onClose(id);
              })
              .catch((err) => {
                alert('Error while creating assessment');
                console.error('Error while creating assessment', err);
              });
          }}
          className="btn btn-primary"
        >
          Create
        </button>
      </ModalFooter>
    </Modal>
  );
}

import React, { useSyncExternalStore } from 'react';
import { formatType } from '../library-manager/types';
import { LibraryStoreType } from '../stores/library';
import { LibraryTable } from './editor';
import { appliancesDefinition } from './types/appliances';
import { clothesDryingMeasureDefinition } from './types/clothes-drying-measures';
import { draughtProofingMeasureDefinition } from './types/draught-proofing-measures';
import { extractVentilationPointsDefinition } from './types/extract-ventilation-points';
import { floorDefinition, floorMeasureDefinition } from './types/fabric-floors';
import { openingDefinition, openingMeasureDefinition } from './types/fabric-openings';
import { wallDefinition, wallMeasureDefinition } from './types/fabric-walls';
import { floorInsulationDefinition } from './types/floor-insulation-materials';
import { generationMeasureDefinition } from './types/generation-measures';
import {
  heatingSystemDefinition,
  heatingSystemMeasureDefinition,
} from './types/heating-systems';
import { hotWaterControlDefinition } from './types/hot-water-controls';
import {
  hotWaterStorageDefinition,
  hotWaterStorageMeasureDefinition,
} from './types/hot-water-storage';
import {
  ventsAndFluesDefinition,
  ventsAndFluesMeasureDefinition,
} from './types/intentional-vents-and-flues';
import { pipeworkMeasureDefinition } from './types/pipework-measures';
import { spaceHeatingControlsDefinition } from './types/space-heating-controls';
import {
  ventilationSystemDefinition,
  ventilationSystemMeasureDefinition,
} from './types/ventilation-systems';
import { waterUsageMeasuresDefinition } from './types/water-usage-measures';

type LibraryEditorParams = {
  libraryStore: LibraryStoreType;
};

export function LibraryEditor({ libraryStore }: LibraryEditorParams) {
  const libraryState = useSyncExternalStore(
    libraryStore.subscribe.bind(libraryStore),
    libraryStore.getSnapshot.bind(libraryStore),
  );

  return libraryState.status === 'unfetched' ? (
    <div className="mx-auto max-width-1100 pt-15">
      <span className="spinner" />
    </div>
  ) : (
    <>
      <div className="mx-auto max-width-1100">
        <section className="px-0 line-top">
          <h1 className="ma-0">{libraryState.data.name}</h1>
          <p>{formatType(libraryState.data.type)}</p>
        </section>

        {(() => {
          const library = libraryState.data;
          switch (library.type) {
            case 'appliances_and_cooking':
              return (
                <LibraryTable
                  definition={appliancesDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'clothes_drying_facilities':
              return (
                <LibraryTable
                  definition={clothesDryingMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'draught_proofing_measures':
              return (
                <LibraryTable
                  definition={draughtProofingMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'extract_ventilation_points':
              return (
                <LibraryTable
                  definition={extractVentilationPointsDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'floors':
              return (
                <LibraryTable
                  definition={floorDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'floor_measures':
              return (
                <LibraryTable
                  definition={floorMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'floor_insulation_materials':
              return (
                <LibraryTable
                  definition={floorInsulationDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'generation_measures':
              return (
                <LibraryTable
                  definition={generationMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'heating_systems':
              return (
                <LibraryTable
                  definition={heatingSystemDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'heating_systems_measures':
              return (
                <LibraryTable
                  definition={heatingSystemMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'hot_water_control_type':
              return (
                <LibraryTable
                  definition={hotWaterControlDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'intentional_vents_and_flues':
              return (
                <LibraryTable
                  definition={ventsAndFluesDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'intentional_vents_and_flues_measures':
              return (
                <LibraryTable
                  definition={ventsAndFluesMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'openings':
              return (
                <LibraryTable
                  definition={openingDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'opening_measures':
              return (
                <LibraryTable
                  definition={openingMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'pipework_insulation':
              return (
                <LibraryTable
                  definition={pipeworkMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'space_heating_control_type':
              return (
                <LibraryTable
                  definition={spaceHeatingControlsDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'storage_type':
              return (
                <LibraryTable
                  definition={hotWaterStorageDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'storage_type_measures':
              return (
                <LibraryTable
                  definition={hotWaterStorageMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'ventilation_systems':
              return (
                <LibraryTable
                  definition={ventilationSystemDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'ventilation_systems_measures':
              return (
                <LibraryTable
                  definition={ventilationSystemMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'walls':
              return (
                <LibraryTable
                  definition={wallDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'wall_measures':
              return (
                <LibraryTable
                  definition={wallMeasureDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            case 'water_usage':
              return (
                <LibraryTable
                  definition={waterUsageMeasuresDefinition}
                  data={library.data}
                  onSave={async (dataset) => await libraryStore.save(dataset)}
                />
              );
            default:
              return <div>Library type not supported</div>;
          }
        })()}
      </div>
    </>
  );
}

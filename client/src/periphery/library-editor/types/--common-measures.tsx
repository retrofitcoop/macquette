import React from 'react';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { Textarea } from '../../../ui/input-components/textarea';
import { EditableBase, EditorDefinition, StoredBase } from '../editor';

export type CommonMeasureFields = {
  measureDescription: string;
  measurePerformance: string;
  measureBenefits: string;
  measureBaseCost: number | null;
  measureCostQty: number | null;
  measureCostUnits: string;
  measureInstaller: string;
  measureDisruption: string;
  measureAssociatedWork: string;
  measureRisks: string;
  measureNotes: string;
  measureMaintenance: string;
  measureLifetimeYears: number | null;
};

export const blankMeasure: CommonMeasureFields = {
  measureDescription: '',
  measurePerformance: '',
  measureBenefits: '',
  measureBaseCost: null,
  measureCostQty: null,
  measureCostUnits: '',
  measureInstaller: '',
  measureDisruption: '',
  measureAssociatedWork: '',
  measureRisks: '',
  measureNotes: '',
  measureMaintenance: '',
  measureLifetimeYears: null,
};

export type BasicCarbon = {
  carbonType: 'basic';
  carbonBaseUpfront: number | null;
  carbonBaseBiogenic: number | null;
  carbonPerUnitUpfront: number | null;
  carbonPerUnitBiogenic: number | null;
  carbonSource: string;
};
export type HighLowCarbon = {
  carbonType: 'high/low';
  carbonHighBaseUpfront: number | null;
  carbonHighBaseBiogenic: number | null;
  carbonHighPerUnitUpfront: number | null;
  carbonHighPerUnitBiogenic: number | null;
  carbonLowBaseUpfront: number | null;
  carbonLowBaseBiogenic: number | null;
  carbonLowPerUnitUpfront: number | null;
  carbonLowPerUnitBiogenic: number | null;
  carbonSource: string;
};

export const blankBasicCarbon: BasicCarbon = {
  carbonType: 'basic',
  carbonBaseUpfront: null,
  carbonBaseBiogenic: null,
  carbonPerUnitUpfront: null,
  carbonPerUnitBiogenic: null,
  carbonSource: '',
};
export const blankHighLowCarbon: HighLowCarbon = {
  carbonType: 'high/low',
  carbonHighBaseUpfront: null,
  carbonHighBaseBiogenic: null,
  carbonHighPerUnitUpfront: null,
  carbonHighPerUnitBiogenic: null,
  carbonLowBaseUpfront: null,
  carbonLowBaseBiogenic: null,
  carbonLowPerUnitUpfront: null,
  carbonLowPerUnitBiogenic: null,
  carbonSource: '',
};

export function measureDefinition<
  T extends EditableBase & CommonMeasureFields,
  U extends StoredBase,
>({
  includeBaseCost = true,
  costUnits = { type: 'fixed' },
  excludeFields = [],
}: {
  includeBaseCost?: boolean;
  costUnits?: { type: 'fixed' } | { type: 'selectable'; options: string[] };
  excludeFields?: (
    | 'Description'
    | '£'
    | 'Performance'
    | 'Benefits'
    | 'Installer'
    | 'Disruption'
    | 'Associated work'
    | 'Risks'
    | 'Notes'
    | 'Maintenance'
    | 'Estimated lifetime'
  )[];
}): EditorDefinition<T, U>['columns'] {
  return (
    [
      {
        title: 'Description' as const,
        display: (measure) => measure.measureDescription,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            style={{ width: '22rem' }}
            value={measure.measureDescription}
            onChange={(measureDescription) =>
              onUpdate({ ...measure, measureDescription })
            }
          />
        ),
      },
      {
        title: '£' as const,
        display: (measure) =>
          `${measure.measureCostQty ?? 0} per ${measure.measureCostUnits}`,
        edit: (measure, errors, onUpdate) => (
          <div className="d-flex flex-nowrap gap-7">
            {includeBaseCost && (
              <>
                <NumberInput
                  onChange={(measureBaseCost) =>
                    onUpdate({ ...measure, measureBaseCost })
                  }
                  value={measure.measureBaseCost}
                  error={errors['measureBaseCost']}
                  style={{ width: '2rem' }}
                />
                <span>+</span>
              </>
            )}
            <NumberInput
              onChange={(measureCostQty) => onUpdate({ ...measure, measureCostQty })}
              value={measure.measureCostQty}
              error={errors['measureCostQty']}
              style={{ width: '2rem' }}
            />
            <span className="text-nowrap">
              per {costUnits.type === 'fixed' && measure.measureCostUnits}
            </span>
            {costUnits.type === 'selectable' && (
              <Select
                id="measureCostUnits"
                options={costUnits.options.map((opt) => ({ value: opt, display: opt }))}
                value={measure.measureCostUnits}
                error={errors['measureCostUnits']}
                onChange={(measureCostUnits) => {
                  onUpdate({ ...measure, measureCostUnits });
                }}
                className="mb-0 input--auto-width"
              />
            )}
          </div>
        ),
      },
      {
        title: 'Performance' as const,
        display: (measure) => measure.measurePerformance,
        edit: (measure, errors, onUpdate) => (
          <input
            type="text"
            value={measure.measurePerformance}
            onChange={(evt) =>
              onUpdate({ ...measure, measurePerformance: evt.target.value })
            }
            style={{ width: '8rem' }}
          />
        ),
      },
      {
        title: 'Benefits' as const,
        display: (measure) => measure.measureBenefits,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            value={measure.measureBenefits}
            onChange={(measureBenefits) => onUpdate({ ...measure, measureBenefits })}
          />
        ),
      },
      {
        title: 'Installer' as const,
        display: (measure) => measure.measureInstaller,
        edit: (measure, errors, onUpdate) => (
          <input
            type="text"
            value={measure.measureInstaller}
            onChange={(evt) =>
              onUpdate({ ...measure, measureInstaller: evt.target.value })
            }
            style={{ width: '12rem' }}
          />
        ),
      },
      {
        title: 'Disruption' as const,
        display: (measure) => measure.measureDisruption,
        edit: (measure, errors, onUpdate) => (
          <input
            type="text"
            value={measure.measureDisruption}
            onChange={(evt) =>
              onUpdate({ ...measure, measureDisruption: evt.target.value })
            }
            style={{ width: '8rem' }}
          />
        ),
      },
      {
        title: 'Associated work' as const,
        display: (measure) => measure.measureAssociatedWork,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            value={measure.measureAssociatedWork}
            onChange={(measureAssociatedWork) =>
              onUpdate({ ...measure, measureAssociatedWork })
            }
            style={{ width: '14rem' }}
          />
        ),
      },
      {
        title: 'Risks' as const,
        display: (measure) => measure.measureRisks,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            value={measure.measureRisks}
            onChange={(measureRisks) => onUpdate({ ...measure, measureRisks })}
            style={{ width: '14rem' }}
          />
        ),
      },
      {
        title: 'Notes' as const,
        display: (measure) => measure.measureNotes,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            style={{ width: '26rem' }}
            value={measure.measureNotes}
            onChange={(measureNotes) => onUpdate({ ...measure, measureNotes })}
          />
        ),
      },
      {
        title: 'Maintenance' as const,
        display: (measure) => measure.measureMaintenance,
        edit: (measure, errors, onUpdate) => (
          <Textarea
            style={{ width: '14rem' }}
            value={measure.measureMaintenance}
            onChange={(measureMaintenance) =>
              onUpdate({ ...measure, measureMaintenance })
            }
          />
        ),
      },
      {
        title: 'Estimated lifetime' as const,
        display: (measure) => measure.measureLifetimeYears,
        edit: (measure, errors, onUpdate) => (
          <NumberInput
            value={measure.measureLifetimeYears}
            onChange={(measureLifetimeYears) =>
              onUpdate({ ...measure, measureLifetimeYears })
            }
            error={errors['measureLifetimeYears']}
            unit="years"
          />
        ),
      },
    ] satisfies EditorDefinition<T, U>['columns']
  ).filter((row) => !excludeFields.includes(row.title));
}

export function carbonBasicDefinition<
  T extends EditableBase & CommonMeasureFields & BasicCarbon,
  U extends StoredBase,
>({
  includeBaseCost = true,
}: {
  includeBaseCost?: boolean;
}): EditorDefinition<T, U>['columns'] {
  return [
    {
      title: 'Upfront carbon',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonBaseUpfront ?? '?'} base + </>}
          {measure.carbonPerUnitUpfront ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonBaseUpfront) =>
                  onUpdate({ ...measure, carbonBaseUpfront })
                }
                value={measure.carbonBaseUpfront}
                error={errors['carbonBaseUpfront']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonPerUnitUpfront) =>
              onUpdate({ ...measure, carbonPerUnitUpfront })
            }
            value={measure.carbonPerUnitUpfront}
            error={errors['carbonPerUnitUpfront']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
    {
      title: 'Biogenic carbon',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonBaseBiogenic ?? '?'} base + </>}
          {measure.carbonPerUnitBiogenic ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonBaseBiogenic) =>
                  onUpdate({ ...measure, carbonBaseBiogenic })
                }
                value={measure.carbonBaseBiogenic}
                error={errors['carbonBaseBiogenic']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonPerUnitBiogenic) =>
              onUpdate({ ...measure, carbonPerUnitBiogenic })
            }
            value={measure.carbonPerUnitBiogenic}
            error={errors['carbonPerUnitBiogenic']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
  ];
}

export function carbonHighLowDefinition<
  T extends EditableBase & CommonMeasureFields & HighLowCarbon,
  U extends StoredBase,
>({
  includeBaseCost = true,
}: {
  includeBaseCost?: boolean;
}): EditorDefinition<T, U>['columns'] {
  return [
    {
      title: 'Upfront carbon (high)',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonHighBaseUpfront ?? '?'} base + </>}
          {measure.carbonHighPerUnitUpfront ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonHighBaseUpfront) =>
                  onUpdate({ ...measure, carbonHighBaseUpfront })
                }
                value={measure.carbonHighBaseUpfront}
                error={errors['carbonHighBaseUpfront']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonHighPerUnitUpfront) =>
              onUpdate({ ...measure, carbonHighPerUnitUpfront })
            }
            value={measure.carbonHighPerUnitUpfront}
            error={errors['carbonHighPerUnitUpfront']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
    {
      title: 'Biogenic carbon (high)',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonHighBaseBiogenic ?? '?'} base + </>}
          {measure.carbonHighPerUnitBiogenic ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonHighBaseBiogenic) =>
                  onUpdate({ ...measure, carbonHighBaseBiogenic })
                }
                value={measure.carbonHighBaseBiogenic}
                error={errors['carbonHighBaseBiogenic']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonHighPerUnitBiogenic) =>
              onUpdate({ ...measure, carbonHighPerUnitBiogenic })
            }
            value={measure.carbonHighPerUnitBiogenic}
            error={errors['carbonHighPerUnitBiogenic']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
    {
      title: 'Upfront carbon (low)',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonLowBaseUpfront ?? '?'} base + </>}
          {measure.carbonLowPerUnitUpfront ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonLowBaseUpfront) =>
                  onUpdate({ ...measure, carbonLowBaseUpfront })
                }
                value={measure.carbonLowBaseUpfront}
                error={errors['carbonLowBaseUpfront']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonLowPerUnitUpfront) =>
              onUpdate({ ...measure, carbonLowPerUnitUpfront })
            }
            value={measure.carbonLowPerUnitUpfront}
            error={errors['carbonLowPerUnitUpfront']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
    {
      title: 'Biogenic carbon (low)',
      display: (measure) => (
        <div>
          {includeBaseCost && <>{measure.carbonLowBaseBiogenic ?? '?'} base + </>}
          {measure.carbonLowPerUnitBiogenic ?? '?'} per {measure.measureCostUnits}
        </div>
      ),
      edit: (measure, errors, onUpdate) => (
        <div className="d-flex flex-nowrap gap-7">
          {includeBaseCost && (
            <>
              <NumberInput
                onChange={(carbonLowBaseBiogenic) =>
                  onUpdate({ ...measure, carbonLowBaseBiogenic })
                }
                value={measure.carbonLowBaseBiogenic}
                error={errors['carbonLowBaseBiogenic']}
                style={{ width: '2rem' }}
              />{' '}
              +
            </>
          )}
          <NumberInput
            onChange={(carbonLowPerUnitBiogenic) =>
              onUpdate({ ...measure, carbonLowPerUnitBiogenic })
            }
            value={measure.carbonLowPerUnitBiogenic}
            error={errors['carbonLowPerUnitBiogenic']}
            style={{ width: '2rem' }}
          />{' '}
          per {measure.measureCostUnits}
        </div>
      ),
    },
  ];
}

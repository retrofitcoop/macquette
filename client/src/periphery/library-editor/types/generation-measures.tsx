import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  CommonMeasureFields,
  HighLowCarbon,
  blankHighLowCarbon,
  blankMeasure,
  carbonHighLowDefinition,
  measureDefinition,
} from './--common-measures';

type StoredCollection = Extract<Library, { type: 'generation_measures' }>['data'];
type StoredMeasure = StoredCollection[string];

type EditableMeasure = {
  id: number;
  tag: string;
  name: string;
  source: string;
  kWp: number | null;
  measureCostUnits: 'kWp' | 'unit';
} & Omit<CommonMeasureFields, 'measureCostUnits'> &
  HighLowCarbon;

export const generationMeasureDefinition: EditorDefinition<
  EditableMeasure,
  StoredMeasure
> = {
  toStored,
  fromStored,
  blank: {
    id: 0,
    tag: '',
    name: '',
    source: '',
    kWp: null,
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankHighLowCarbon,
  },
  columns: [
    {
      title: 'Name',
      display: (measure) => measure.name,
      edit: (measure, errors, onUpdate) => (
        <TextInput
          value={measure.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...measure, name })}
          style={{ width: '18rem' }}
        />
      ),
    },
    {
      title: 'kWp',
      display: (measure) => measure.kWp,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.kWp}
          error={errors['kWp']}
          onChange={(kWp) => onUpdate({ ...measure, kWp })}
          style={{ width: '2rem' }}
        />
      ),
    },
    {
      title: 'Source',
      display: (item) => item.source,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...item, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: false,
      excludeFields: ['Performance'],
      costUnits: { type: 'selectable', options: ['unit', 'kWp'] },
    }),
    ...carbonHighLowDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: false,
    }),
  ],
};

function toStored(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const errors: ValidationErrors<EditableMeasure> = {};

  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.kWp === null) {
    errors.kWp = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    source: data.source,
    kWp: data.kWp ?? 0,
    associated_work: data.measureAssociatedWork,
    benefits: data.measureBenefits,
    cost: data.measureCostQty ?? 0,
    cost_units: data.measureCostUnits ?? 'unit',
    min_cost: data.measureBaseCost ?? 0,
    description: data.measureDescription,
    disruption: data.measureDisruption,
    key_risks: data.measureRisks,
    maintenance: data.measureMaintenance,
    lifetimeYears: data.measureLifetimeYears,
    notes: data.measureNotes,
    performance: `${data.kWp ?? 0} kWp`,
    who_by: data.measureInstaller,
    carbonType: data.carbonType,
    carbonHighBaseUpfront: 0,
    carbonHighBaseBiogenic: 0,
    carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
    carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
    carbonLowBaseUpfront: 0,
    carbonLowBaseBiogenic: 0,
    carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
    carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

function fromStored(data: StoredMeasure, idx: number): Result<EditableMeasure, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    source: data.source,
    kWp: data.kWp,
    measureDescription: data.description,
    measurePerformance: data.performance,
    measureBenefits: data.benefits,
    measureBaseCost: data.min_cost,
    measureCostQty: data.cost,
    measureCostUnits: data.cost_units,
    measureInstaller: data.who_by,
    measureDisruption: data.disruption,
    measureAssociatedWork: data.associated_work,
    measureRisks: data.key_risks,
    measureNotes: data.notes,
    measureMaintenance: data.maintenance,
    measureLifetimeYears: data.lifetimeYears,
    carbonType: data.carbonType,
    carbonHighBaseUpfront: data.carbonHighBaseUpfront,
    carbonHighBaseBiogenic: data.carbonHighBaseBiogenic,
    carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
    carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
    carbonLowBaseUpfront: data.carbonLowBaseUpfront,
    carbonLowBaseBiogenic: data.carbonLowBaseBiogenic,
    carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
    carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

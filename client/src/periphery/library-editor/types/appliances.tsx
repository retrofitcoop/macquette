import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';

type StoredCollection = Extract<Library, { type: 'appliances_and_cooking' }>['data'];
type StoredAppliance = StoredCollection[string];

type EditableAppliance = {
  id: number;
  tag: string;
  name: string;
  category:
    | 'Computing'
    | 'Cooking'
    | 'Food storage'
    | 'Laundry'
    | 'Miscelanea'
    | 'Other kitchen / cleaning'
    | 'TV'
    | null;
  demand: number | null;
  units: string;
  frequency: number | null;
  efficiency: number | null;
  fuelType: 'Electricity' | 'Gas' | 'Oil' | 'Solid fuel' | null;
  referenceQuantity: number | null;
  utilisationFactor: number | null;
  source: string;
};

export const appliancesDefinition: EditorDefinition<EditableAppliance, StoredAppliance> =
  {
    toStored,
    fromStored,
    blank: {
      id: 0,
      tag: '',
      name: '',
      units: '',
      category: null,
      demand: null,
      frequency: null,
      efficiency: null,
      fuelType: null,
      referenceQuantity: null,
      utilisationFactor: null,
      source: '',
    },
    columns: [
      {
        title: 'Name',
        display: (appliance) => appliance.name,
        edit: (appliance, errors, onUpdate) => (
          <TextInput
            value={appliance.name}
            error={errors['name']}
            onChange={(name) => onUpdate({ ...appliance, name })}
            style={{ width: '12rem' }}
          />
        ),
      },
      {
        title: 'Category',
        display: (appliance) => appliance.category,
        edit: (appliance, errors, onUpdate) => (
          <Select
            className="input--auto-width"
            onChange={(category) => onUpdate({ ...appliance, category })}
            value={appliance.category}
            error={errors['category']}
            options={[
              { display: 'Computing', value: 'Computing' },
              { display: 'Cooking', value: 'Cooking' },
              { display: 'Food storage', value: 'Food storage' },
              { display: 'Laundry', value: 'Laundry' },
              { display: 'Miscelanea', value: 'Miscelanea' },
              { display: 'Other kitchen / cleaning', value: 'Other kitchen / cleaning' },
              { display: 'TV', value: 'TV' },
            ]}
          />
        ),
      },
      {
        title: 'kWh',
        display: (appliance) => appliance.demand,
        edit: (appliance, errors, onUpdate) => (
          <NumberInput
            value={appliance.demand}
            error={errors['demand']}
            onChange={(demand) => onUpdate({ ...appliance, demand })}
          />
        ),
      },
      {
        title: 'Units',
        display: (appliance) => appliance.units,
        edit: (appliance, errors, onUpdate) => {
          return (
            <input
              type="text"
              value={appliance.units}
              onChange={(evt) => onUpdate({ ...appliance, units: evt.target.value })}
              style={{ width: '6rem' }}
            />
          );
        },
      },
      {
        title: 'Frequency',
        display: (appliance) => appliance.frequency,
        edit: (appliance, errors, onUpdate) => (
          <NumberInput
            value={appliance.frequency}
            error={errors['frequency']}
            onChange={(frequency) => onUpdate({ ...appliance, frequency })}
          />
        ),
      },
      {
        title: 'Fuel',
        display: (appliance) => appliance.fuelType,
        edit: (appliance, errors, onUpdate) => (
          <Select
            className="input--auto-width"
            onChange={(fuelType) => onUpdate({ ...appliance, fuelType })}
            value={appliance.fuelType}
            error={errors['fuelType']}
            options={[
              { display: 'Electricity', value: 'Electricity' },
              { display: 'Gas', value: 'Gas' },
              { display: 'Oil', value: 'Oil' },
              { display: 'Solid fuel', value: 'Solid fuel' },
            ]}
          />
        ),
      },
      {
        title: 'Efficiency',
        display: (appliance) => appliance.efficiency,
        edit: (appliance, errors, onUpdate) =>
          appliance.fuelType === 'Electricity' ? (
            '-'
          ) : (
            <NumberInput
              value={appliance.efficiency}
              error={errors['efficiency']}
              onChange={(efficiency) => onUpdate({ ...appliance, efficiency })}
            />
          ),
      },
      {
        title: 'Qty',
        display: (appliance) => appliance.referenceQuantity,
        edit: (appliance, errors, onUpdate) => (
          <NumberInput
            value={appliance.referenceQuantity}
            error={errors['referenceQuantity']}
            onChange={(referenceQuantity) =>
              onUpdate({ ...appliance, referenceQuantity })
            }
          />
        ),
      },
      {
        title: 'Utilisation factor',
        display: (appliance) => appliance.utilisationFactor,
        edit: (appliance, errors, onUpdate) => (
          <NumberInput
            value={appliance.utilisationFactor}
            error={errors['utilisationFactor']}
            onChange={(utilisationFactor) =>
              onUpdate({ ...appliance, utilisationFactor })
            }
          />
        ),
      },
      {
        title: 'Source',
        display: (item) => item.source,
        edit: (item, errors, onUpdate) => (
          <TextInput
            type="text"
            value={item.source}
            error={errors['source']}
            onChange={(source) => onUpdate({ ...item, source })}
            style={{ width: '18rem' }}
          />
        ),
      },
    ],
  };

function fromStored(
  data: StoredAppliance,
  idx: number,
): Result<EditableAppliance, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    source: data.source,
    name: data.name,
    units: data.units,
    category: data.category,
    demand: data.norm_demand,
    frequency: data.frequency,
    efficiency: data.efficiency,
    fuelType: data.type_of_fuel,
    referenceQuantity: data.reference_quantity,
    utilisationFactor: data.utilisation_factor,
  });
}

function toStored(
  data: EditableAppliance,
): Result<StoredAppliance, ValidationErrors<EditableAppliance>> {
  const errors: ValidationErrors<EditableAppliance> = {};

  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.category === null) {
    errors.category = 'required';
  }
  if (data.demand === null) {
    errors.demand = 'required';
  }
  if (data.frequency === null) {
    errors.frequency = 'required';
  }
  if (data.fuelType !== 'Electricity' && data.efficiency === null) {
    errors.efficiency = 'required';
  }
  if (data.fuelType === null) {
    errors.fuelType = 'required';
  }
  if (data.referenceQuantity === null) {
    errors.referenceQuantity = 'required';
  }
  if (data.utilisationFactor === null) {
    errors.utilisationFactor = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    source: data.source,
    units: data.units,
    category: data.category ?? 'Miscelanea',
    norm_demand: data.demand ?? 0,
    frequency: data.frequency ?? 0,
    efficiency: data.efficiency ?? 0,
    type_of_fuel: data.fuelType ?? 'Electricity',
    reference_quantity: data.referenceQuantity ?? 0,
    utilisation_factor: data.utilisationFactor ?? 0,
  });
}

import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredCollection = Extract<Library, { type: 'hot_water_control_type' }>['data'];
type StoredMeasure = StoredCollection[string];

type EditableMeasure = {
  id: number;
  tag: string;
  name: string;
  source: string;
  controlType:
    | 'Cylinder thermostat, water heating not separately timed'
    | 'Cylinder thermostat, water heating separately timed'
    | null;
} & CommonMeasureFields &
  BasicCarbon;

export const hotWaterControlDefinition: EditorDefinition<EditableMeasure, StoredMeasure> =
  {
    toStored,
    fromStored,
    blank: {
      id: 0,
      tag: '',
      name: '',
      source: '',
      controlType: null,
      ...blankMeasure,
      measureCostUnits: 'unit',
      ...blankBasicCarbon,
    },
    columns: [
      {
        title: 'Name',
        display: (measure) => measure.name,
        edit: (measure, errors, onUpdate) => (
          <TextInput
            value={measure.name}
            error={errors['name']}
            onChange={(name) => onUpdate({ ...measure, name })}
            style={{ width: '18rem' }}
          />
        ),
      },
      {
        title: 'Source',
        display: (measure) => measure.source,
        edit: (measure, errors, onUpdate) => (
          <TextInput
            value={measure.source}
            error={errors['source']}
            onChange={(source) => onUpdate({ ...measure, source })}
            style={{ width: '18rem' }}
          />
        ),
      },
      {
        title: 'Control type',
        display: (material) => material.controlType,
        edit: (material, errors, onUpdate) => (
          <Select
            className="input--auto-width"
            onChange={(controlType) => onUpdate({ ...material, controlType })}
            value={material.controlType}
            error={errors['controlType']}
            options={[
              {
                display: 'Cylinder thermostat, water heating not separately timed',
                value: 'Cylinder thermostat, water heating not separately timed',
              },
              {
                display: 'Cylinder thermostat, water heating separately timed',
                value: 'Cylinder thermostat, water heating separately timed',
              },
            ]}
          />
        ),
      },
      ...measureDefinition<EditableMeasure, StoredMeasure>({
        includeBaseCost: false,
      }),
      ...carbonBasicDefinition<EditableMeasure, StoredMeasure>({
        includeBaseCost: false,
      }),
    ],
  };

function toStored(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const errors: ValidationErrors<EditableMeasure> = {};

  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.controlType === null) {
    errors.controlType = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    source: data.source,
    control_type:
      data.controlType ?? 'Cylinder thermostat, water heating not separately timed',
    associated_work: data.measureAssociatedWork,
    benefits: data.measureBenefits,
    cost: data.measureCostQty ?? 0,
    cost_units: data.measureCostUnits,
    min_cost: data.measureBaseCost ?? 0,
    description: data.measureDescription,
    disruption: data.measureDisruption,
    key_risks: data.measureRisks,
    maintenance: data.measureMaintenance,
    lifetimeYears: data.measureLifetimeYears,
    notes: data.measureNotes,
    performance: data.measurePerformance,
    who_by: data.measureInstaller,
    carbonType: data.carbonType,
    carbonBaseUpfront: 0,
    carbonBaseBiogenic: 0,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

function fromStored(data: StoredMeasure, idx: number): Result<EditableMeasure, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    source: data.source,
    controlType: data.control_type,
    measureDescription: data.description,
    measurePerformance: data.performance,
    measureBenefits: data.benefits,
    measureBaseCost: data.min_cost,
    measureCostQty: data.cost,
    measureCostUnits: data.cost_units,
    measureInstaller: data.who_by,
    measureDisruption: data.disruption,
    measureAssociatedWork: data.associated_work,
    measureRisks: data.key_risks,
    measureNotes: data.notes,
    measureMaintenance: data.maintenance,
    measureLifetimeYears: data.lifetimeYears,
    carbonType: data.carbonType,
    carbonBaseUpfront: data.carbonBaseUpfront,
    carbonBaseBiogenic: data.carbonBaseBiogenic,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

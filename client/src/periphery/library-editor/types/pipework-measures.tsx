import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredCollection = Extract<Library, { type: 'pipework_insulation' }>['data'];
type StoredMeasure = StoredCollection[string];

type EditableMeasure = {
  id: number;
  tag: string;
  name: string;
  source: string;
  level: 'first 1m' | 'all accessible' | 'fully insulated' | null;
} & CommonMeasureFields &
  BasicCarbon;

export const pipeworkMeasureDefinition: EditorDefinition<EditableMeasure, StoredMeasure> =
  {
    toStored,
    fromStored,
    blank: {
      id: 0,
      tag: '',
      name: '',
      source: '',
      level: null,
      ...blankMeasure,
      measureCostUnits: 'unit',
      ...blankBasicCarbon,
    },
    columns: [
      {
        title: 'Name',
        display: (measure) => measure.name,
        edit: (measure, errors, onUpdate) => (
          <TextInput
            value={measure.name}
            error={errors['name']}
            onChange={(name) => onUpdate({ ...measure, name })}
            style={{ width: '26rem' }}
          />
        ),
      },
      {
        title: 'Insulation level',
        display: (measure) => measure.level,
        edit: (measure, errors, onUpdate) => (
          <Select
            className="input--auto-width"
            onChange={(level) => onUpdate({ ...measure, level })}
            value={measure.level}
            error={errors['level']}
            options={[
              { display: 'first 1m from cylinder', value: 'first 1m' },
              { display: 'all accessible pipework', value: 'all accessible' },
              { display: 'all primary pipework', value: 'fully insulated' },
            ]}
          />
        ),
      },
      {
        title: 'Source',
        display: (item) => item.source,
        edit: (item, errors, onUpdate) => (
          <TextInput
            type="text"
            value={item.source}
            error={errors['source']}
            onChange={(source) => onUpdate({ ...item, source })}
            style={{ width: '18rem' }}
          />
        ),
      },
      ...measureDefinition<EditableMeasure, StoredMeasure>({
        includeBaseCost: false,
      }),
      ...carbonBasicDefinition<EditableMeasure, StoredMeasure>({
        includeBaseCost: false,
      }),
    ],
  };

const levelTransform: Record<
  Exclude<EditableMeasure['level'], null>,
  StoredMeasure['SELECT']
> = {
  'first 1m': 'First 1m from cylinder insulated',
  'all accessible': 'All accesible piperwok insulated',
  'fully insulated': 'Fully insulated primary pipework',
};

const levelTransformReverse: Record<
  StoredMeasure['SELECT'],
  Exclude<EditableMeasure['level'], null>
> = {
  'First 1m from cylinder insulated': 'first 1m',
  'All accesible piperwok insulated': 'all accessible',
  'Fully insulated primary pipework': 'fully insulated',
};

function toStored(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const errors: ValidationErrors<EditableMeasure> = {};

  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.level === null) {
    errors.level = 'required';
  }
  if (data.measureCostQty === null) {
    errors.measureCostQty = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    SELECT: levelTransform[data.level ?? 'first 1m'],
    source: data.source,
    associated_work: data.measureAssociatedWork,
    benefits: data.measureBenefits,
    min_cost: 0,
    cost: data.measureCostQty ?? 0,
    cost_units: data.measureCostUnits,
    description: data.measureDescription,
    disruption: data.measureDisruption,
    key_risks: data.measureRisks,
    maintenance: data.measureMaintenance,
    lifetimeYears: data.measureLifetimeYears,
    notes: data.measureNotes,
    performance: data.measurePerformance,
    who_by: data.measureInstaller,
    carbonType: data.carbonType,
    carbonBaseUpfront: 0,
    carbonBaseBiogenic: 0,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

function fromStored(data: StoredMeasure, idx: number): Result<EditableMeasure, Error> {
  if (data.source !== '') {
    return Result.err(new Error('Source field should be empty'));
  }
  if (data.cost_units !== 'unit') {
    return Result.err(new Error('Cost units should be "unit"'));
  }

  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    source: data.source,
    level:
      data.SELECT in levelTransformReverse
        ? (levelTransformReverse[data.SELECT] ?? null)
        : null,
    measureDescription: data.description,
    measurePerformance: data.performance,
    measureBenefits: data.benefits,
    measureBaseCost: data.min_cost,
    measureCostUnits: data.cost_units,
    measureCostQty: data.cost,
    measureInstaller: data.who_by,
    measureDisruption: data.disruption,
    measureAssociatedWork: data.associated_work,
    measureRisks: data.key_risks,
    measureNotes: data.notes,
    measureMaintenance: data.maintenance,
    measureLifetimeYears: data.lifetimeYears,
    carbonType: data.carbonType,
    carbonBaseUpfront: data.carbonBaseUpfront,
    carbonBaseBiogenic: data.carbonBaseBiogenic,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

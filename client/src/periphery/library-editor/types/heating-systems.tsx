import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  CommonMeasureFields,
  HighLowCarbon,
  blankHighLowCarbon,
  blankMeasure,
  carbonHighLowDefinition,
  measureDefinition,
} from './--common-measures';

type StoredItemCollection = Extract<Library, { type: 'heating_systems' }>['data'];
type StoredItem = StoredItemCollection[string];

export type EditableItem = {
  id: number;
  tag: string;
  name: string;
  category:
    | 'Combi boilers'
    | 'Heat pumps'
    | 'Hot water only'
    | 'Room heaters'
    | 'System boilers'
    | 'Warm air systems'
    | null;
  winterEfficiency: number | null;
  summerEfficiency: number | null;
  centralHeatingPumpDemand: number | null;
  fansAndSupplyPumpDemand: number | null;
  specificFanPower: number | null;
  responsiveness: number | null;
  combiLoss:
    | '0'
    | 'Instantaneous, without keep hot-facility'
    | 'Instantaneous, with keep-hot facility controlled by time clock'
    | 'Instantaneous, with keep-hot facility not controlled by time clock'
    | 'Storage combi boiler >= 55 litres'
    | 'Storage combi boiler < 55 litres'
    | null;
  primaryCircuitLoss: 'Yes' | 'No' | null;
  source: string;
};

type StoredMeasureCollection = Extract<
  Library,
  { type: 'heating_systems_measures' }
>['data'];
type StoredMeasure = StoredMeasureCollection[string];

type EditableMeasure = EditableItem & CommonMeasureFields & HighLowCarbon;

function isWaterHeater(system: EditableItem) {
  return system.category !== 'Room heaters' && system.category !== 'Warm air systems';
}

function sharedColumns<T extends EditableItem>(): EditorDefinition<T, never>['columns'] {
  return [
    {
      title: 'Name',
      display: (system) => system.name,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...system, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Category',
      display: (system) => system.category,
      edit: (system, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(category) => onUpdate({ ...system, category })}
          value={system.category}
          error={errors['category']}
          options={[
            { display: 'Combi boilers', value: 'Combi boilers' },
            { display: 'Heat pumps', value: 'Heat pumps' },
            { display: 'Hot water only', value: 'Hot water only' },
            { display: 'Room heaters', value: 'Room heaters' },
            { display: 'System boilers', value: 'System boilers' },
            { display: 'Warm air systems', value: 'Warm air systems' },
          ]}
        />
      ),
    },
    {
      title: 'Winter efficiency / space heating',
      display: (measure) =>
        measure.category === 'Hot water only' ? '-' : measure.winterEfficiency,
      edit: (measure, errors, onUpdate) =>
        measure.category === 'Hot water only' ? (
          '-'
        ) : (
          <NumberInput
            value={measure.winterEfficiency}
            error={errors['winterEfficiency']}
            onChange={(winterEfficiency) => onUpdate({ ...measure, winterEfficiency })}
            style={{ width: '1.5rem' }}
            unit="%"
          />
        ),
    },
    {
      title: 'Summer efficiency / water heating',
      display: (measure) => (isWaterHeater(measure) ? measure.summerEfficiency : '-'),
      edit: (measure, errors, onUpdate) =>
        isWaterHeater(measure) ? (
          <NumberInput
            value={measure.summerEfficiency}
            error={errors['summerEfficiency']}
            onChange={(summerEfficiency) => onUpdate({ ...measure, summerEfficiency })}
            style={{ width: '1.5rem' }}
            unit="%"
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Central heating pump demand',
      display: (measure) => measure.centralHeatingPumpDemand,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.centralHeatingPumpDemand}
          error={errors['centralHeatingPumpDemand']}
          onChange={(centralHeatingPumpDemand) =>
            onUpdate({ ...measure, centralHeatingPumpDemand })
          }
          style={{ width: '2rem' }}
          unit="kWh/year"
        />
      ),
    },
    {
      title: 'Fans & supply pump demand',
      display: (measure) =>
        measure.category === 'Warm air systems' ? '-' : measure.fansAndSupplyPumpDemand,
      edit: (measure, errors, onUpdate) =>
        measure.category === 'Warm air systems' ? (
          '-'
        ) : (
          <NumberInput
            value={measure.fansAndSupplyPumpDemand}
            error={errors['fansAndSupplyPumpDemand']}
            onChange={(fansAndSupplyPumpDemand) =>
              onUpdate({ ...measure, fansAndSupplyPumpDemand })
            }
            style={{ width: '2rem' }}
            unit="kWh/year"
          />
        ),
    },
    {
      title: 'Specific fan power',
      display: (measure) =>
        measure.category === 'Warm air systems' ? measure.specificFanPower : '-',
      edit: (measure, errors, onUpdate) =>
        measure.category === 'Warm air systems' ? (
          <NumberInput
            value={measure.specificFanPower}
            error={errors['specificFanPower']}
            onChange={(specificFanPower) => onUpdate({ ...measure, specificFanPower })}
            style={{ width: '2rem' }}
            unit="W/(l/s)"
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Responsiveness',
      display: (measure) =>
        measure.category === 'Hot water only' ? '-' : measure.responsiveness,
      edit: (measure, errors, onUpdate) =>
        measure.category === 'Hot water only' ? (
          '-'
        ) : (
          <NumberInput
            value={measure.responsiveness}
            error={errors['responsiveness']}
            onChange={(responsiveness) => onUpdate({ ...measure, responsiveness })}
            style={{ width: '2rem' }}
          />
        ),
    },
    {
      title: 'Combi loss',
      display: (system) => system.combiLoss,
      edit: (system, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(combiLoss) => onUpdate({ ...system, combiLoss })}
          value={system.combiLoss}
          error={errors['combiLoss']}
          options={[
            { display: '0', value: '0' },
            {
              display: 'Instantaneous, without keep hot-facility',
              value: 'Instantaneous, without keep hot-facility',
            },
            {
              display: 'Instantaneous, with keep-hot facility controlled by time clock',
              value: 'Instantaneous, with keep-hot facility controlled by time clock',
            },
            {
              display:
                'Instantaneous, with keep-hot facility not controlled by time clock',
              value: 'Instantaneous, with keep-hot facility not controlled by time clock',
            },
            {
              display: 'Storage combi boiler >= 55 litres',
              value: 'Storage combi boiler >= 55 litres',
            },
            {
              display: 'Storage combi boiler < 55 litres',
              value: 'Storage combi boiler < 55 litres',
            },
          ]}
        />
      ),
    },
    {
      title: 'Primary circuit loss',
      display: (system) => system.primaryCircuitLoss,
      edit: (system, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(primaryCircuitLoss) => onUpdate({ ...system, primaryCircuitLoss })}
          value={system.primaryCircuitLoss}
          error={errors['primaryCircuitLoss']}
          options={[
            { display: 'Yes', value: 'Yes' },
            { display: 'No', value: 'No' },
          ]}
        />
      ),
    },
    {
      title: 'Source',
      display: (system) => system.source,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...system, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ];
}

export const heatingSystemDefinition: EditorDefinition<EditableItem, StoredItem> = {
  toStored: toStoredItem,
  fromStored: fromStoredItem,
  blank: {
    id: 0,
    tag: '',
    name: '',
    category: null,
    combiLoss: null,
    responsiveness: null,
    summerEfficiency: null,
    winterEfficiency: null,
    centralHeatingPumpDemand: null,
    fansAndSupplyPumpDemand: null,
    primaryCircuitLoss: null,
    specificFanPower: null,
    source: '',
  },
  columns: sharedColumns(),
};

export const heatingSystemMeasureDefinition: EditorDefinition<
  EditableMeasure,
  StoredMeasure
> = {
  toStored: toStoredMeasure,
  fromStored: fromStoredMeasure,
  blank: {
    ...heatingSystemDefinition.blank,
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankHighLowCarbon,
  },
  columns: [
    ...sharedColumns<EditableMeasure>(),
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: true,
      costUnits: { type: 'selectable', options: ['unit', 'sqm'] },
    }),
    ...carbonHighLowDefinition<EditableMeasure, StoredMeasure>({ includeBaseCost: true }),
  ],
};

function fromStoredItem(data: StoredItem, idx: number): Result<EditableItem, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    category: data.category,
    combiLoss: coalesceEmptyString(data.combi_loss, null),
    responsiveness: coalesceEmptyString(data.responsiveness, null),
    summerEfficiency: coalesceEmptyString(data.summer_efficiency, null),
    winterEfficiency: data.winter_efficiency,
    centralHeatingPumpDemand: coalesceEmptyString(data.central_heating_pump, null),
    fansAndSupplyPumpDemand: coalesceEmptyString(data.fans_and_supply_pumps, null),
    specificFanPower: data.sfp,
    primaryCircuitLoss: data.primary_circuit_loss,
    source: data.source,
  });
}

function fromStoredMeasure(
  data: StoredMeasure,
  idx: number,
): Result<EditableMeasure, Error> {
  return fromStoredItem(data, idx).chain((item) =>
    Result.ok({
      ...item,
      measureDescription: data.description,
      measurePerformance: data.performance,
      measureBenefits: data.benefits,
      measureBaseCost: data.min_cost,
      measureCostQty: data.cost,
      measureCostUnits: data.cost_units,
      measureInstaller: data.who_by,
      measureDisruption: data.disruption,
      measureAssociatedWork: data.associated_work,
      measureRisks: data.key_risks,
      measureNotes: data.notes,
      measureMaintenance: data.maintenance,
      measureLifetimeYears: data.lifetimeYears,
      carbonType: data.carbonType,
      carbonHighBaseUpfront: data.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: data.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: data.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: data.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies EditableMeasure),
  );
}

function toStoredMeasure(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const result = toStoredItem(data);

  return result.chain((item) =>
    Result.ok({
      ...item,
      associated_work: data.measureAssociatedWork,
      benefits: data.measureBenefits,
      cost: data.measureCostQty ?? 0,
      cost_units: data.measureCostUnits,
      min_cost: data.measureBaseCost ?? 0,
      description: data.measureDescription,
      disruption: data.measureDisruption,
      key_risks: data.measureRisks,
      maintenance: data.measureMaintenance,
      lifetimeYears: data.measureLifetimeYears,
      notes: data.measureNotes,
      performance: data.measurePerformance,
      who_by: data.measureInstaller,
      carbonType: data.carbonType,
      carbonHighBaseUpfront: data.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: data.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: data.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: data.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies StoredMeasure),
  );
}

function toStoredItem(
  data: EditableItem,
): Result<StoredItem, ValidationErrors<EditableItem>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.category === null) {
    errors.category = 'required';
  }
  if (data.combiLoss === null) {
    errors.combiLoss = 'required';
  }
  if (data.primaryCircuitLoss === null) {
    errors.primaryCircuitLoss = 'required';
  }
  if (data.winterEfficiency === null) {
    errors.winterEfficiency = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    category: data.category ?? 'Heat pumps',
    combi_loss: data.combiLoss ?? '0',
    responsiveness: data.responsiveness ?? '',
    summer_efficiency: data.summerEfficiency ?? '',
    winter_efficiency: data.winterEfficiency ?? 0,
    central_heating_pump: data.centralHeatingPumpDemand ?? '',
    fans_and_supply_pumps: data.fansAndSupplyPumpDemand ?? '',
    primary_circuit_loss: data.primaryCircuitLoss ?? 'Yes',
    sfp: data.specificFanPower,
    source: data.source,
  });
}

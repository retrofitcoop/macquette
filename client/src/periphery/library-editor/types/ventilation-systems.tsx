import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredItemCollection = Extract<Library, { type: 'ventilation_systems' }>['data'];
type StoredItem = StoredItemCollection[string];

export type EditableItem = {
  id: number;
  tag: string;
  name: string;
  type: 'NV' | 'IE' | 'MEV' | 'PS' | 'MVHR' | 'MV' | 'DEV' | null;
  airChangeRate: number | null;
  specificFanPower: number | null;
  balancedHeatRecoveryEfficiency: number | null;
  source: string;
};

type StoredMeasureCollection = Extract<
  Library,
  { type: 'ventilation_systems_measures' }
>['data'];
type StoredMeasure = StoredMeasureCollection[string];

type EditableMeasure = EditableItem &
  Omit<CommonMeasureFields, 'measureCostUnits'> &
  BasicCarbon & {
    measureCostUnits: 'sqm' | 'unit';
  };

function isMechanicalExtract(measure: EditableItem) {
  switch (measure.type) {
    case 'MEV':
    case 'MVHR':
    case 'MV':
    case 'DEV':
      return true;
    default:
      return false;
  }
}

function sharedColumns<T extends EditableItem>(): EditorDefinition<T, never>['columns'] {
  return [
    {
      title: 'Name',
      display: (system) => system.name,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...system, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Type',
      display: (system) => system.type,
      edit: (system, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(type) => onUpdate({ ...system, type })}
          value={system.type}
          error={errors['type']}
          options={[
            { value: 'NV', display: 'unplanned/natural ventilation' },
            { value: 'IE', display: 'intermittent extract' },
            { value: 'MEV', display: 'mechanical extract' },
            { value: 'DEV', display: 'continuous decentralised mechanical extract' },
            { value: 'PS', display: 'passive stack' },
            { value: 'MV', display: 'mechanical ventilation' },
            { value: 'MVHR', display: 'mechanical ventilation with heat recovery' },
          ]}
        />
      ),
    },
    {
      title: 'Air change rate',
      display: (measure) => (isMechanicalExtract(measure) ? measure.airChangeRate : '-'),
      edit: (measure, errors, onUpdate) =>
        isMechanicalExtract(measure) ? (
          <NumberInput
            value={measure.airChangeRate}
            error={errors['airChangeRate']}
            onChange={(airChangeRate) => onUpdate({ ...measure, airChangeRate })}
            style={{ width: '2rem' }}
            unit="ACH"
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Specific fan power',
      display: (measure) => measure.specificFanPower,
      edit: (measure, errors, onUpdate) =>
        isMechanicalExtract(measure) ? (
          <NumberInput
            value={measure.specificFanPower}
            error={errors['specificFanPower']}
            onChange={(specificFanPower) => onUpdate({ ...measure, specificFanPower })}
            style={{ width: '1.5rem' }}
            unit="W/(litre.sec)"
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Balanced heat recovery efficiency',
      display: (measure) => measure.balancedHeatRecoveryEfficiency ?? '-',
      edit: (measure, errors, onUpdate) =>
        measure.type === 'MVHR' ? (
          <NumberInput
            value={measure.balancedHeatRecoveryEfficiency}
            error={errors['balancedHeatRecoveryEfficiency']}
            onChange={(balancedHeatRecoveryEfficiency) =>
              onUpdate({ ...measure, balancedHeatRecoveryEfficiency })
            }
            style={{ width: '1.5rem' }}
            unit="%"
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Source',
      display: (system) => system.source,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...system, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ];
}

export const ventilationSystemDefinition: EditorDefinition<EditableItem, StoredItem> = {
  toStored: toStoredItem,
  fromStored: fromStoredItem,
  blank: {
    id: 0,
    tag: '',
    name: '',
    type: null,
    airChangeRate: null,
    specificFanPower: null,
    balancedHeatRecoveryEfficiency: null,
    source: '',
  },
  columns: sharedColumns(),
};

export const ventilationSystemMeasureDefinition: EditorDefinition<
  EditableMeasure,
  StoredMeasure
> = {
  toStored: toStoredMeasure,
  fromStored: fromStoredMeasure,
  blank: {
    ...ventilationSystemDefinition.blank,
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankBasicCarbon,
  },
  columns: [
    ...sharedColumns<EditableMeasure>(),
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: true,
      costUnits: { type: 'selectable', options: ['unit', 'sqm'] },
    }),
    ...carbonBasicDefinition<EditableMeasure, StoredMeasure>({ includeBaseCost: true }),
  ],
};

function fromStoredItem(data: StoredItem, idx: number): Result<EditableItem, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    type: data.ventilation_type,
    airChangeRate: coalesceEmptyString(data.system_air_change_rate, null),
    specificFanPower: coalesceEmptyString(data.specific_fan_power, null),
    balancedHeatRecoveryEfficiency: coalesceEmptyString(
      data.balanced_heat_recovery_efficiency,
      null,
    ),
    source: data.source,
  });
}

function fromStoredMeasure(
  data: StoredMeasure,
  idx: number,
): Result<EditableMeasure, Error> {
  return fromStoredItem(data, idx).chain((item) =>
    Result.ok({
      ...item,
      measureDescription: data.description,
      measurePerformance: data.performance,
      measureBenefits: data.benefits,
      measureBaseCost: data.min_cost,
      measureCostQty: data.cost,
      measureCostUnits: data.cost_units,
      measureInstaller: data.who_by,
      measureDisruption: data.disruption,
      measureAssociatedWork: data.associated_work,
      measureRisks: data.key_risks,
      measureNotes: data.notes,
      measureMaintenance: data.maintenance,
      measureLifetimeYears: data.lifetimeYears,
      carbonType: data.carbonType,
      carbonBaseUpfront: data.carbonBaseUpfront,
      carbonBaseBiogenic: data.carbonBaseBiogenic,
      carbonPerUnitUpfront: data.carbonPerUnitUpfront,
      carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies EditableMeasure),
  );
}

function toStoredMeasure(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const result = toStoredItem(data);

  return result.chain((item) =>
    Result.ok({
      ...item,
      associated_work: data.measureAssociatedWork,
      benefits: data.measureBenefits,
      cost: data.measureCostQty ?? 0,
      cost_units: data.measureCostUnits,
      min_cost: data.measureBaseCost ?? 0,
      description: data.measureDescription,
      disruption: data.measureDisruption,
      key_risks: data.measureRisks,
      maintenance: data.measureMaintenance,
      lifetimeYears: data.measureLifetimeYears,
      notes: data.measureNotes,
      performance: data.measurePerformance,
      who_by: data.measureInstaller,
      carbonType: data.carbonType,
      carbonBaseUpfront: data.carbonBaseUpfront,
      carbonBaseBiogenic: data.carbonBaseBiogenic,
      carbonPerUnitUpfront: data.carbonPerUnitUpfront,
      carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies StoredMeasure),
  );
}

function toStoredItem(
  data: EditableItem,
): Result<StoredItem, ValidationErrors<EditableItem>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.type === null) {
    errors.type = 'required';
  }
  const requiresFigures: (string | null)[] = ['NV', 'IE', 'PS'];
  if (!requiresFigures.includes(data.type)) {
    if (data.airChangeRate === null) {
      errors.airChangeRate = 'required';
    }
    if (data.specificFanPower === null) {
      errors.specificFanPower = 'required';
    }
  }
  if (data.type === 'MVHR' && data.balancedHeatRecoveryEfficiency === null) {
    errors.balancedHeatRecoveryEfficiency = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    ventilation_type: data.type ?? 'NV',
    system_air_change_rate: data.airChangeRate ?? 0,
    specific_fan_power: data.specificFanPower ?? 0,
    balanced_heat_recovery_efficiency: data.balancedHeatRecoveryEfficiency ?? 0,
    source: data.source,
  });
}

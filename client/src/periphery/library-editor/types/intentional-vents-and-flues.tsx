import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredItemCollection = Extract<
  Library,
  { type: 'intentional_vents_and_flues' }
>['data'];
type StoredItem = StoredItemCollection[string];

export type EditableItem = {
  id: number;
  tag: string;
  name: string;
  ventilationRate: number | null;
  source: string;
};

type StoredMeasureCollection = Extract<
  Library,
  { type: 'intentional_vents_and_flues_measures' }
>['data'];
type StoredMeasure = StoredMeasureCollection[string];

type EditableMeasure = Omit<EditableItem, 'type'> & CommonMeasureFields & BasicCarbon;

export const ventsAndFluesDefinition: EditorDefinition<EditableItem, StoredItem> = {
  toStored: toStoredItem,
  fromStored: fromStoredItem,
  blank: {
    id: 0,
    tag: '',
    name: '',
    ventilationRate: null,
    source: '',
  },
  columns: [
    {
      title: 'Name',
      display: (system) => system.name,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...system, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Ventilation rate',
      display: (measure) => <>{measure.ventilationRate} m³/h</>,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.ventilationRate}
          error={errors['ventilationRate']}
          onChange={(ventilationRate) => onUpdate({ ...measure, ventilationRate })}
          style={{ width: '1.5rem' }}
          unit="m³/h"
        />
      ),
    },
    {
      title: 'Source',
      display: (system) => system.source,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...system, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ],
};

export const ventsAndFluesMeasureDefinition: EditorDefinition<
  EditableMeasure,
  StoredMeasure
> = {
  toStored: toStoredMeasure,
  fromStored: fromStoredMeasure,
  blank: {
    id: 0,
    tag: '',
    name: '',
    ventilationRate: null,
    source: '',
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankBasicCarbon,
  },
  columns: [
    {
      title: 'Name',
      display: (system) => system.name,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...system, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Ventilation rate',
      display: (measure) => measure.ventilationRate,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.ventilationRate}
          error={errors['ventilationRate']}
          onChange={(ventilationRate) => onUpdate({ ...measure, ventilationRate })}
          style={{ width: '1.5rem' }}
          unit="m³/h"
        />
      ),
    },
    {
      title: 'Source',
      display: (system) => system.source,
      edit: (system, errors, onUpdate) => (
        <TextInput
          type="text"
          value={system.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...system, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: false,
    }),
    ...carbonBasicDefinition<EditableMeasure, StoredMeasure>({ includeBaseCost: false }),
  ],
};

function fromStoredItem(data: StoredItem, idx: number): Result<EditableItem, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    ventilationRate: data.ventilation_rate,
    source: data.source,
  });
}

function fromStoredMeasure(
  data: StoredMeasure,
  idx: number,
): Result<EditableMeasure, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    ventilationRate: data.ventilation_rate,
    source: data.source,
    measureDescription: data.description,
    measurePerformance: data.performance,
    measureBenefits: data.benefits,
    measureBaseCost: data.min_cost,
    measureCostQty: data.cost,
    measureCostUnits: data.cost_units,
    measureInstaller: data.who_by,
    measureDisruption: data.disruption,
    measureAssociatedWork: data.associated_work,
    measureRisks: data.key_risks,
    measureNotes: data.notes,
    measureMaintenance: data.maintenance,
    measureLifetimeYears: data.lifetimeYears,
    carbonType: data.carbonType,
    carbonBaseUpfront: data.carbonBaseUpfront,
    carbonBaseBiogenic: data.carbonBaseBiogenic,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  } satisfies EditableMeasure);
}

function toStoredMeasure(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.ventilationRate === null) {
    errors.ventilationRate = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    ventilation_rate: data.ventilationRate ?? 0,
    source: data.source,
    associated_work: data.measureAssociatedWork,
    benefits: data.measureBenefits,
    cost: data.measureCostQty ?? 0,
    cost_units: data.measureCostUnits,
    min_cost: data.measureBaseCost ?? 0,
    description: data.measureDescription,
    disruption: data.measureDisruption,
    key_risks: data.measureRisks,
    maintenance: data.measureMaintenance,
    lifetimeYears: data.measureLifetimeYears,
    notes: data.measureNotes,
    performance: data.measurePerformance,
    who_by: data.measureInstaller,
    carbonType: data.carbonType,
    carbonBaseUpfront: 0,
    carbonBaseBiogenic: 0,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  } satisfies StoredMeasure);
}

function toStoredItem(
  data: EditableItem,
): Result<StoredItem, ValidationErrors<EditableItem>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.ventilationRate === null) {
    errors.ventilationRate = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    ventilation_rate: data.ventilationRate ?? 0,
    source: data.source,
  });
}

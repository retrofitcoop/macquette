import React, { useId, useState } from 'react';
import { FormGrid } from '../../ui/input-components/forms';
import { TextInput } from '../../ui/input-components/text';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../ui/output-components/modal';
import { OrganisationStoreType } from '../stores/organisation';

export function InviteUserModal({
  organisationStore,
  onClose,
}: {
  organisationStore: OrganisationStoreType;
  onClose: () => void;
}) {
  const headerId = useId();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  return (
    <Modal headerId={headerId} onClose={onClose}>
      <ModalHeader title={'Invite user'} onClose={onClose} />
      <ModalBody>
        <FormGrid>
          <label htmlFor="name">Name:</label>
          <div>
            <TextInput id="name" value={name} onChange={setName} autoFocus={true} />
          </div>
          <label htmlFor="email">Email:</label>
          <div>
            <TextInput id="email" value={email} onChange={setEmail} />
          </div>
        </FormGrid>
      </ModalBody>
      <ModalFooter>
        <button onClick={onClose} className="btn">
          Cancel
        </button>
        <button
          onClick={() => {
            if (name === '') {
              alert('User name must be set');
              return;
            }
            if (email === null) {
              alert('User email must be set');
              return;
            }
            organisationStore.inviteMembers([{ name, email }]).catch((err) => {
              alert('Error while inviting user');
              console.error('Error while inviting user', err);
            });
            onClose();
          }}
          className="btn btn-primary"
        >
          Invite
        </button>
      </ModalFooter>
    </Modal>
  );
}

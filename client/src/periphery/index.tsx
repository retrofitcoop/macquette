import React from 'react';
import { Link, Route } from 'wouter';
import { AssessmentManager } from './assessment-manager';
import { LibraryEditor } from './library-editor';
import { LibraryManager } from './library-manager';
import { OrganisationEditor } from './organisation-editor';
import { OrganisationList } from './organisation-list';
import { AssessmentListStore } from './stores/assessment-list';
import { LibraryStore } from './stores/library';
import { LibraryListStore } from './stores/library-list';
import { OrganisationStore } from './stores/organisation';
import { OrganisationListStore } from './stores/organisation-list';

export function Router({ appName, userId }: { appName: string; userId: string }) {
  const assessmentStore = new AssessmentListStore();
  const organisationStore = new OrganisationListStore();
  const libraryStore = new LibraryListStore();

  return (
    <div>
      <div className="nav-container">
        <nav className="nav-main mx-auto max-width-1100">
          <h1 className="nav-main-heading">{appName}</h1>
          <div className="d-flex align-items-center pl-15">
            <Link href="/assessments/">
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <a>Assessments</a>
            </Link>
            <Link href="/libraries/">
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <a>Libraries</a>
            </Link>
            <Link href="/organisations/">
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <a>Organisations</a>
            </Link>
            <a href="/logout">Log out</a>
          </div>
        </nav>
      </div>

      <Route path="/assessments/">
        <AssessmentManager
          assessmentStore={assessmentStore}
          organisationStore={organisationStore}
        />
      </Route>
      <Route path="/libraries/">
        <LibraryManager libraryStore={libraryStore} />
      </Route>
      <Route path="/libraries/:pk/">
        {(params) => {
          const libraryStore = new LibraryStore(params.pk);
          return <LibraryEditor libraryStore={libraryStore} />;
        }}
      </Route>
      <Route path="/organisations/">
        <OrganisationList organisationStore={organisationStore} />
      </Route>
      <Route path="/organisations/:pk/">
        {(params) => {
          const organisationStore = new OrganisationStore(params.pk);
          return (
            <OrganisationEditor organisationStore={organisationStore} userId={userId} />
          );
        }}
      </Route>
    </div>
  );
}

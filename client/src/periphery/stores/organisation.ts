import { HTTPClient } from '../../api/http';
import { Organisation } from '../../data-schemas/organisations';
import { Store } from './base';

export class OrganisationStore extends Store {
  private state: Organisation | null = null;
  private client = new HTTPClient();

  constructor(private id: string) {
    super();
    this.fetch().catch((err) => {
      console.error("Couldn't fetch", err);
    });
  }

  getSnapshot(): Organisation | null {
    return this.state;
  }

  private async fetch() {
    const result = await this.client.getOrganisation(this.id);
    this.state = result.unwrap();
    this.notifySubscribers();
  }

  async inviteMembers(users: { name: string; email: string }[]) {
    await this.client.inviteMembers(this.id, users);
    await this.fetch();
  }
  async promoteLibrarian(id: string) {
    await this.client.promoteUserAsLibrarian(this.id, id);
    await this.fetch();
  }
  async demoteLibrarian(id: string) {
    await this.client.demoteUserAsLibrarian(this.id, id);
    await this.fetch();
  }
  async promoteAdmin(id: string) {
    await this.client.promoteUserAsAdmin(this.id, id);
    await this.fetch();
  }
  async demoteAdmin(id: string) {
    await this.client.demoteUserAsAdmin(this.id, id);
    await this.fetch();
  }
  async removeMember(id: string) {
    await this.client.removeMember(this.id, id);
    await this.fetch();
  }
}

type OrgMember = Organisation['members'][number];

export class FakeOrganisationStore extends Store {
  constructor(private state: Organisation | null) {
    super();
  }
  getSnapshot(): Organisation | null {
    return this.state;
  }

  // We're don't do any async stuff in the mock versions.
  /* eslint-disable @typescript-eslint/require-await */
  async inviteMembers(users: { name: string; email: string }[]) {
    if (this.state === null) {
      throw new Error('No state to modify');
    }
    this.state = {
      ...this.state,
      members: [
        ...this.state.members,
        ...users.map((user) => ({
          ...user,
          id: 'FAKE-ID',
          lastLogin: 'never' as const,
          isAdmin: false,
          isLibrarian: false,
        })),
      ],
    };
    this.notifySubscribers();
  }

  private updateMember(id: string, fn: (member: OrgMember) => OrgMember) {
    if (this.state === null) {
      throw new Error('No state to modify');
    }
    this.state = {
      ...this.state,
      members: this.state.members.map((user) => (user.id !== id ? user : fn(user))),
    };
  }

  async promoteLibrarian(id: string) {
    this.updateMember(id, (member) => ({ ...member, isLibrarian: true }));
    this.notifySubscribers();
  }
  async demoteLibrarian(id: string) {
    this.updateMember(id, (member) => ({ ...member, isLibrarian: false }));
    this.notifySubscribers();
  }
  async promoteAdmin(id: string) {
    this.updateMember(id, (member) => ({ ...member, isAdmin: true }));
    this.notifySubscribers();
  }
  async demoteAdmin(id: string) {
    this.updateMember(id, (member) => ({ ...member, isAdmin: false }));
    this.notifySubscribers();
  }

  async removeMember(id: string) {
    if (this.state === null) {
      throw new Error('No state to modify');
    }
    this.state = {
      ...this.state,
      members: this.state.members.filter((user) => user.id !== id),
    };
    this.notifySubscribers();
  }
}

export type OrganisationStoreType = Omit<OrganisationStore, ''>;

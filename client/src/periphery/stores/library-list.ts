import { HTTPClient } from '../../api/http';
import { LibraryMetadata } from '../../data-schemas/api-metadata';
import type { LibraryType } from '../library-manager/types';
import { Store } from './base';

export class LibraryListStore extends Store {
  private state: LibraryMetadata[] = [];
  private client = new HTTPClient();

  constructor() {
    super();
    this.fetch().catch((err) => {
      console.error("Couldn't fetch", err);
    });
  }

  getSnapshot(): readonly LibraryMetadata[] {
    return this.state;
  }

  private async fetch() {
    const result = await this.client.listLibrariesMetadata();
    this.state = result;
    this.notifySubscribers();
  }

  async create({
    name,
    type,
    ownership,
  }: {
    name: string;
    type: LibraryType;
    ownership: { type: 'private' } | { type: 'organisation'; id: string };
  }) {
    await this.client.createLibrary(
      { name, type },
      ownership.type === 'private' ? null : ownership.id,
    );
    await this.fetch();
  }

  async delete(id: string) {
    await this.client.deleteLibrary(id);

    this.state = this.state.filter((library) => library.id !== id);
    this.notifySubscribers();

    await this.fetch();
    this.notifySubscribers();
  }

  async share(fromOrgId: string, libraryId: string, toOrgId: string) {
    await this.client.shareLibraryWithOrganisation(fromOrgId, libraryId, toOrgId);
    await this.fetch();
  }

  async unshare(fromOrgId: string, libraryId: string, toOrgId: string) {
    await this.client.stopSharingLibraryWithOrganisation(fromOrgId, libraryId, toOrgId);
    await this.fetch();
  }
}

export class FakeLibraryListStore extends Store {
  constructor(private state: LibraryMetadata[] = []) {
    super();
  }

  getSnapshot(): readonly LibraryMetadata[] {
    return this.state;
  }

  // We're don't do any async stuff in the mock versions.
  /* eslint-disable @typescript-eslint/require-await */
  async delete(id: string) {
    const library = this.state.find((library) => library.id === id);
    if (library === undefined) {
      throw new Error("Library ID to select didn't exist");
    }

    this.state = this.state.filter((library) => library.id !== id);
    this.notifySubscribers();
  }

  async create({
    name,
    type,
    ownership,
  }: {
    name: string;
    type: LibraryType;
    ownership: { type: 'private' } | { type: 'organisation'; id: string };
  }) {
    this.state = [
      {
        id: 'id',
        name,
        type,
        created_at: new Date(),
        updated_at: new Date(),
        permissions: { can_write: true, can_share: false },
        owner:
          ownership.type === 'private'
            ? {
                type: 'personal',
                id: 'user',
                name: 'username',
                email: 'email@example.com',
              }
            : {
                id: ownership.id,
                name: ownership.id,
                type: 'organisation',
              },
        shared_with: [],
      },
      ...this.state,
    ];
    this.notifySubscribers();
  }

  async share(fromOrgId: string, libraryId: string, toOrgId: string) {
    const library = this.state.find((library) => library.id === libraryId);
    if (library === undefined) {
      throw new Error("Library to share didn't exist");
    }
    const sharingEntry = library.shared_with.find((entry) => entry.id === toOrgId);
    if (sharingEntry !== undefined) {
      throw new Error('Already sharing with organisation');
    }
    this.state = this.state.map((library) =>
      library.id !== libraryId
        ? library
        : {
            ...library,
            shared_with: [
              ...library.shared_with,
              { id: toOrgId, name: 'Random organisation name' },
            ],
          },
    );
    this.notifySubscribers();
  }

  async unshare(fromOrgId: string, libraryId: string, toOrgId: string) {
    const library = this.state.find((library) => library.id === libraryId);
    if (library === undefined) {
      throw new Error("Library to select didn't exist");
    }
    const sharingEntryIdx = library.shared_with.findIndex(
      (entry) => entry.id === toOrgId,
    );
    if (sharingEntryIdx === -1) {
      throw new Error('Not sharing with that organisation');
    }
    this.state = this.state.map((library) =>
      library.id !== libraryId
        ? library
        : {
            ...library,
            shared_with: library.shared_with.filter(
              (sharingEntry) => sharingEntry.id !== toOrgId,
            ),
          },
    );
    this.notifySubscribers();
  }
}

export type LibraryListStoreType = Omit<LibraryListStore, ''>;

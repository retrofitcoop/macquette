import { HTTPClient } from '../../api/http';
import { Library } from '../../data-schemas/libraries';
import { Store } from './base';

type LibraryStoreState =
  | { status: 'unfetched' }
  | { status: 'fetched'; data: Library }
  | { status: 'saving'; data: Library; saveStatus: null | 'in progress' | 'failed' };

export class LibraryStore extends Store {
  private state: LibraryStoreState = { status: 'unfetched' };
  private client = new HTTPClient();

  constructor(private id: string) {
    super();
    this.fetch().catch((err) => {
      console.error("Couldn't fetch", err);
    });
  }

  getSnapshot(): LibraryStoreState {
    return this.state;
  }

  private async fetch() {
    if (this.state.status === 'saving') {
      throw new Error("can't fetch while save is in progress");
    }
    const result = await this.client.getLibrary(this.id);
    this.state = { status: 'fetched', data: result.unwrap() };
    this.notifySubscribers();
  }

  async save(data: unknown) {
    if (this.state.status === 'unfetched') {
      throw new Error("can't save when no data was fetched");
    }

    this.state = {
      status: 'saving',
      data: this.state.data,
      saveStatus: 'in progress',
    };
    this.notifySubscribers();

    let result;
    try {
      result = await this.client.updateLibrary(this.id, { data });
    } catch (_err) {
      this.state = {
        status: 'saving',
        data: this.state.data,
        saveStatus: 'failed',
      };
      this.notifySubscribers();
      return;
    }

    this.state = {
      status: 'fetched',
      data: result.unwrap(),
    };
    this.notifySubscribers();
  }
}

export type LibraryStoreType = Omit<LibraryStore, ''>;

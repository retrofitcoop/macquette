export const urls = {
  addressSuggestions(): string {
    return `/address-search/v1/suggestions/`;
  },

  resolveAddress(): string {
    return '/address-search/v1/lookup/';
  },

  assessments(): string {
    return `/api/assessments/`;
  },

  assessmentHTML(assessmentId: string): string {
    return `/assessments/${assessmentId}/`;
  },

  assessment(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/`;
  },

  duplicateAssessment(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/duplicate/`;
  },

  shareAssessment(assessmentId: string, userId: string): string {
    return `/api/assessments/${assessmentId}/shares/${userId}/`;
  },

  assignAssessmentToOrganisation(assessmentId: string, organisationId: string): string {
    return `/api/assessments/${assessmentId}/organisation/${organisationId}/`;
  },

  uploadImage(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/images/`;
  },

  setFeaturedImage(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/images/featured/`;
  },

  image(imageId: number): string {
    return `/api/images/${imageId}/`;
  },

  organisation(organisationId: string): string {
    return `/api/organisations/${organisationId}/`;
  },

  organisationAssessments(organisationId: string): string {
    return `/api/organisations/${organisationId}/assessments/`;
  },

  organisations(): string {
    return `/api/organisations/`;
  },

  users(): string {
    return `/api/users/`;
  },

  organisationMember(organisationId: string, userId: string): string {
    return `/api/organisations/${organisationId}/members/${userId}/`;
  },

  inviteMembers(organisationId: string): string {
    return `/api/organisations/${organisationId}/members/`;
  },

  libraries(): string {
    return `/api/libraries/`;
  },

  organisationLibraries(organisationId: string): string {
    return `/api/organisations/${organisationId}/libraries/`;
  },

  shareUnshareOrganisationLibraries(
    fromOrgId: string,
    libraryId: string,
    toOrgId: string,
  ): string {
    return `/api/organisations/${fromOrgId}/libraries/${libraryId}/shares/${toOrgId}/`;
  },

  libraryOrganisationLibraryShares(organisationId: string, libraryId: string): string {
    return `/api/organisations/${organisationId}/libraries/${libraryId}/shares/`;
  },

  library(libraryId: string): string {
    return `/api/libraries/${libraryId}/`;
  },

  libraryItems(libraryId: string): string {
    return `/api/libraries/${libraryId}/items/`;
  },

  libraryItem(libraryId: string, tag: string): string {
    return `/api/libraries/${libraryId}/items/${tag}/`;
  },

  admins(organisationId: string, userId: string): string {
    return `/api/organisations/${organisationId}/admins/${userId}/`;
  },

  librarians(organisationId: string, userId: string): string {
    return `/api/organisations/${organisationId}/librarians/${userId}/`;
  },

  reports(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/reports/`;
  },

  report(assessmentId: string, reportId: string): string {
    return `/api/assessments/${assessmentId}/reports/${reportId}`;
  },

  reportPreview(assessmentId: string): string {
    return `/api/assessments/${assessmentId}/reports/preview`;
  },
};

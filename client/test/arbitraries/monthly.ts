import fc from 'fast-check';
import { Month } from '../../src/model/enums/month';

export function arbMonthly<T>(inner: fc.Arbitrary<T>): fc.Arbitrary<(m: Month) => T> {
  return fc
    .array(inner, { minLength: Month.all.length, maxLength: Month.all.length })
    .map(monthly);
}

export function monthly<T>(values: T[]): (m: Month) => T {
  if (values.length !== Month.all.length) {
    throw new Error(`must call ${monthly.name}() with ${Month.all.length} values`);
  }
  function monthlyInner(m: Month) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return values[m.index0]!;
  }
  Object.defineProperty(monthlyInner, fc.toStringMethod, {
    value: () => `monthly([${values.map((value) => fc.stringify(value)).join(', ')}])`,
  });
  Object.defineProperty(monthlyInner, '_debugValues', {
    value: values,
  });
  return monthlyInner;
}

import fc from 'fast-check';
import { Mode } from '../../src/model/modules/mode';

export const arbModelMode: fc.Arbitrary<Mode> = fc.constantFrom('normal', 'regulated');

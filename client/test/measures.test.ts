import { GenericMeasure } from '../src/data-schemas/scenario/measures';
import { embodiedCarbonFromMeasure } from '../src/measures';

describe('embodiedCarbonFromMeasure', () => {
  test('that it calculates upper and lower correctly for a carbonType=basic measure', () => {
    const item: GenericMeasure = {
      tag: 'TAG',
      cost: 65,
      name: 'name',
      notes: 'notes',
      source: 'source',
      who_by: 'who_by',
      benefits: 'benefits',
      location: '',
      min_cost: 2500,
      quantity: 78.5,
      key_risks: 'key_risks',
      carbonType: 'basic',
      cost_total: 7602.5,
      cost_units: 'sqm',
      disruption: 'disruption',
      description: 'description',
      maintenance: 'maintenance',
      performance: 'performance',
      carbonSource: 'carbonSource',
      lifetimeYears: 15,
      associated_work: 'associated_work',
      carbonBaseUpfront: 270,
      carbonBaseBiogenic: 0,
      carbonPerUnitUpfront: 0,
      carbonPerUnitBiogenic: 0,
    };
    const record = embodiedCarbonFromMeasure(item);
    expect(record.upperUpfrontCarbon).toBe(270);
    expect(record.lowerUpfrontCarbon).toBe(270);
  });
});

/**
 * @jest-environment jsdom
 */

import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import { nl2br } from '../../../src/ui/output-components/nl2br';

test('nl2br', () => {
  const { asFragment } = render(<div>{nl2br('line1\nline2\n\nline3\nline4')}</div>);
  expect(asFragment()).toMatchInlineSnapshot(`
    <DocumentFragment>
      <div>
        line1
        <br />
        line2
        <br />
        <br />
        line3
        <br />
        line4
      </div>
    </DocumentFragment>
  `);
});

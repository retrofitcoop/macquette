import { crossProduct } from '../../src/helpers/cross-product';
import { Action, State, energyUseModule } from '../../src/ui/modules/energy-use';
import { withSuppressedConsoleWarn } from '../model/with-suppressed-console-warn';

describe('reducer', () => {
  const states: State[] = [
    { model: null, fansAndPumpsFuelFractions: [] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: 0 }] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: 0.5 }] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: 1 }] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: 1.5 }] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: -1.5 }] },
    { model: null, fansAndPumpsFuelFractions: [{ fuelName: 'a fuel', fraction: null }] },
    {
      model: null,
      fansAndPumpsFuelFractions: [
        { fuelName: 'a fuel', fraction: 0 },
        { fuelName: 'b fuel', fraction: 0 },
      ],
    },
    {
      model: null,
      fansAndPumpsFuelFractions: [
        { fuelName: 'a fuel', fraction: 0.5 },
        { fuelName: 'b fuel', fraction: 0.5 },
      ],
    },
    {
      model: null,
      fansAndPumpsFuelFractions: [
        { fuelName: 'a fuel', fraction: 1 },
        { fuelName: 'b fuel', fraction: 0 },
      ],
    },
    {
      model: null,
      fansAndPumpsFuelFractions: [
        { fuelName: 'a fuel', fraction: 1 },
        { fuelName: 'b fuel', fraction: 1 },
      ],
    },
    {
      model: null,
      fansAndPumpsFuelFractions: [
        { fuelName: 'a fuel', fraction: null },
        { fuelName: 'b fuel', fraction: null },
      ],
    },
  ];
  const actions: Action[] = [
    { type: 'external update', model: null, fansAndPumpsFuelFractions: [] },
    ...states.map(({ fansAndPumpsFuelFractions }) => ({
      type: 'external update' as const,
      model: null,
      fansAndPumpsFuelFractions,
    })),
    { type: 'fp/add fraction' as const, validFuelNames: [] },
    { type: 'fp/add fraction' as const, validFuelNames: ['a fuel'] },
    { type: 'fp/add fraction' as const, validFuelNames: ['a fuel', 'b fuel'] },
    ...[0, 1, 2, 0.5, -1].flatMap((index) => [
      { type: 'fp/remove fraction' as const, index },
      { type: 'fp/update' as const, index, update: {} } satisfies Action,
    ]),
  ];
  it.each(crossProduct(states, actions))(
    'never lets the fans and pumps fuel fractions array become empty: %#',
    (state, action) => {
      withSuppressedConsoleWarn(() => {
        const [newState] = energyUseModule.reducer(state, action);
        expect(newState.fansAndPumpsFuelFractions).not.toHaveLength(0);
      });
    },
  );
});

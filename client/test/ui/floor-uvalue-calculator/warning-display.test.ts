import fc from 'fast-check';

import { Floor } from '../../../src/model/modules/fabric/element-types';
import { constructFloorUValueModel } from '../../../src/model/modules/fabric/floor-u-value-calculator';
import { warningDisplay } from '../../../src/ui/modules/fabric/components/u-value-calculator/warnings';
import { arbFloorUValueModelInput } from '../../arbitraries/scenario/floor-u-value-calculator/model-input';
import { arbFloorSpec } from '../../arbitraries/scenario/floor-u-value-calculator/scenario-spec';

describe('warning display', () => {
  it('handles a Floor model warning', () => {
    const arbWarnings = arbFloorSpec
      .map((spec) => new Floor(spec))
      .map((floor) => floor.warnings.all);
    fc.assert(
      fc.property(arbWarnings, (warnings) => {
        const warningsDisplay = warnings.map((w) => warningDisplay(w));
        expect(warningsDisplay).not.toContain(null);

        // Test for no duplication
        expect(Array.from(new Set(warningsDisplay)).sort()).toEqual(
          warningsDisplay.sort(),
        );
      }),
      {
        numRuns: 10000,
      },
    );
  });
  it('handles a FUVC model warning', () => {
    const arbWarnings = arbFloorUValueModelInput()
      .map((input) => {
        const model = constructFloorUValueModel(input);
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        model.uValue; // Populate warnings
        return model;
      })
      .map((model) => model.warnings.all)
      .filter((warnings) => warnings.length !== 0);
    fc.assert(
      fc.property(arbWarnings, (warnings) => {
        const warningsDisplay = warnings.map((w) => warningDisplay(w));
        expect(warningsDisplay).not.toContain(null);

        // Test for no duplication
        expect(Array.from(new Set(warningsDisplay)).sort()).toEqual(
          warningsDisplay.sort(),
        );
      }),
      { numRuns: 10000 },
    );
  });
});

import { fabricModule } from '../../../src/ui/modules/fabric';
import { State } from '../../../src/ui/modules/fabric/state';

const externalWallElement = {
  tag: 'NONE',
  type: 'external wall' as const,
  name: 'no wall',
  description: 'not a wall',
  source: 'ERB',
  uValue: 1,
  kValue: 2,
};

describe('adding a wall should use the same dimension/area setting as the previous wall', () => {
  test('for dimensions input', () => {
    const state: State = {
      ...fabricModule.initialState(null),
      walls: [
        {
          id: 0,
          type: 'element',
          elementType: 'wall',
          libraryItem: externalWallElement,
          inputs: {
            location: 'nowhere',
            area: {
              type: 'dimensions',
              specific: { area: null },
              dimensions: { area: null, length: null, height: null },
            },
          },
          modelElement: null,
          revertTo: null,
        },
      ],
    };

    const [newState] = fabricModule.reducer(state, {
      type: 'fabric/add wall',
      item: externalWallElement,
    });

    expect(newState.walls[1]!.inputs.area.type).toBe('dimensions');
  });

  test('for specific input', () => {
    const state: State = {
      ...fabricModule.initialState(null),
      walls: [
        {
          id: 0,
          type: 'element',
          elementType: 'wall',
          libraryItem: externalWallElement,
          inputs: {
            location: 'nowhere',
            area: {
              type: 'specific',
              specific: { area: null },
              dimensions: { area: null, length: null, height: null },
            },
          },
          modelElement: null,
          revertTo: null,
        },
      ],
    };

    const [newState] = fabricModule.reducer(state, {
      type: 'fabric/add wall',
      item: externalWallElement,
    });

    expect(newState.walls[1]!.inputs.area.type).toBe('specific');
  });
});

import { LinkError } from '../../src/helpers/tree';
import { deleteScenario } from '../../src/ui/modules/editor-sidebar/delete-scenario';

describe('deleteScenario', () => {
  it('should delete the last scenario with no fuss', () => {
    const scenarioData = {
      master: { created_from: null },
      scenario1: { created_from: 'master' },
      scenario2: { created_from: 'scenario1' },
      scenario3: { created_from: 'scenario2' },
    };

    const result = deleteScenario(scenarioData, 'scenario3');

    expect(result).toStrictEqual({
      master: { created_from: null },
      scenario1: { created_from: 'master' },
      scenario2: { created_from: 'scenario1' },
    });
  });

  it('should delete the middle scenario in a chain and renumber', () => {
    const scenarioData = {
      master: { created_from: null, description: 'master' },
      scenario1: { created_from: 'master', description: 'scenario1' },
      scenario2: { created_from: 'scenario1', description: 'scenario2' },
      scenario3: { created_from: 'scenario2', description: 'scenario3' },
    };

    const result = deleteScenario(scenarioData, 'scenario2');

    expect(result).toStrictEqual({
      master: { created_from: null, description: 'master' },
      scenario1: { created_from: 'master', description: 'scenario1' },
      scenario2: { created_from: 'scenario1', description: 'scenario3' },
    });
  });

  it('should cope with a deletion that is the basis for multiple other scenarios', () => {
    const scenarioData = {
      master: { created_from: null, description: 'master' },
      scenario1: { created_from: 'master', description: 'scenario1' },
      scenario2: { created_from: 'scenario1', description: 'scenario2' },
      scenario3: { created_from: 'scenario2', description: 'scenario3' },
      scenario4: { created_from: 'scenario2', description: 'scenario4' },
    };

    const result = deleteScenario(scenarioData, 'scenario2');

    expect(result).toStrictEqual({
      master: { created_from: null, description: 'master' },
      scenario1: { created_from: 'master', description: 'scenario1' },
      scenario2: { created_from: 'scenario1', description: 'scenario3' },
      scenario3: { created_from: 'scenario1', description: 'scenario4' },
    });
  });

  it('should error out with broken scenario chains', () => {
    const scenarioData = {
      master: { created_from: null, description: 'master' },
      scenario1: { created_from: 'master', description: 'scenario1' },
      scenario2: { created_from: 'scenario1', description: 'scenario2' },
      scenario3: { created_from: 'scenario2', description: 'scenario3' },
      scenario5: { created_from: 'scenario4', description: 'scenario5' },
    };

    expect(() => deleteScenario(scenarioData, 'scenario2')).toThrow(LinkError);
  });
});

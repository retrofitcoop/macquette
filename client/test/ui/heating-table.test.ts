import { Action, reducer, State } from '../../src/ui/input-components/heating-table';

describe('heating table', () => {
  describe('reducer', () => {
    test('set start period 1', () => {
      const state: State = [{ start: null, end: null }];
      const action: Action = {
        type: 'update period',
        idx: 0,
        part: 'start',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([{ start: 1, end: null }] satisfies State);
    });
    test('set end period 1', () => {
      const state: State = [{ start: null, end: null }];
      const action: Action = {
        type: 'update period',
        idx: 0,
        part: 'end',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([{ start: null, end: 1 }] satisfies State);
    });
    test('set start period 2 in order', () => {
      const state: State = [
        { start: 1, end: 2 },
        { start: null, end: null },
      ];
      const action: Action = {
        type: 'update period',
        idx: 1,
        part: 'start',
        time: 3,
      };
      expect(reducer(state, action)).toEqual([
        { start: 1, end: 2 },
        { start: 3, end: null },
      ] satisfies State);
    });
    test('set end period 2', () => {
      const state: State = [
        { start: 2, end: 3 },
        { start: null, end: null },
      ];
      const action: Action = {
        type: 'update period',
        idx: 1,
        part: 'end',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([
        { start: 2, end: 3 },
        { start: null, end: 1 },
      ] satisfies State);
    });
    test('set start period 2 out of order but incomplete', () => {
      const state: State = [
        { start: 2, end: 3 },
        { start: null, end: null },
      ];
      const action: Action = {
        type: 'update period',
        idx: 1,
        part: 'start',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([
        { start: 2, end: 3 },
        { start: 1, end: null },
      ] satisfies State);
    });
    test('set start period 2 out of order, which completes', () => {
      const state: State = [
        { start: 2, end: 3 },
        { start: null, end: 2 },
      ];
      const action: Action = {
        type: 'update period',
        idx: 1,
        part: 'start',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([
        { start: 1, end: 2 },
        { start: 2, end: 3 },
      ] satisfies State);
    });
    test('pattern can have periods with both times null and still count as complete', () => {
      const state: State = [
        { start: 2, end: 3 },
        { start: null, end: 2 },
        { start: null, end: null },
      ];
      const action: Action = {
        type: 'update period',
        idx: 1,
        part: 'start',
        time: 1,
      };
      expect(reducer(state, action)).toEqual([
        { start: 1, end: 2 },
        { start: 2, end: 3 },
        { start: null, end: null },
      ] satisfies State);
    });
  });
});

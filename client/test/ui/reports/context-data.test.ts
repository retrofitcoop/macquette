import { cloneDeep } from 'lodash';
import { z } from 'zod';
import { projectSchema } from '../../../src/data-schemas/project';
import { calcRun } from '../../../src/model/calc-run';
import { generatePayload } from '../../../src/ui/modules/reports/context';
import { findProjectFixture } from '../../fixtures';

function calcAllScenarios(project: z.input<typeof projectSchema>) {
  const scenarioIds: string[] = Object.keys(project.data);
  const oldConsoleWarn = console.warn;
  console.warn = () => undefined;
  for (const id of scenarioIds) {
    calcRun(project.data[id]);
  }
  console.warn = oldConsoleWarn;
}

test('context structure', () => {
  const project = cloneDeep(findProjectFixture('household-empty.json').rawData);
  calcAllScenarios(project);
  const parsedData = projectSchema.parse(project);

  const newContext = generatePayload(parsedData, Object.keys(project.data), {
    isSample: false,
  });

  expect(newContext.context).toEqual({
    options: {
      is_sample: false,
    },
    aims: {
      aesthetics: {
        external: false,
        internal: false,
        note: '',
      },
      future: {
        lifestyle_change: false,
        lifestyle_change_notes: '',
        occupancy_actual: undefined,
        occupancy_notes: '',
        occupancy_planned: '',
        other_works: false,
        other_works_notes: '',
        works_start: null,
        works_start_notes: '',
      },
      logistics: {
        brief: '',
        budget_comment: '',
        can_diy: false,
        disruption: null,
        disruption_comment: '',
        diy_note: '',
        has_budget: false,
        packaging: null,
      },
      priorities: [],
      qual_criteria: '',
    },
    any_scenario_has_biomass: false,
    any_scenario_has_generation: false,
    any_scenario_has_non_electricity: false,
    commentary: {
      brief: '',
      context: '',
      decisions: '',
    },
    front: {
      address: '',
      assessor_name: '',
      local_authority: null,
      name: '',
      report_date: '',
      report_reference: '',
      report_version: '',
      signoff_name: '',
      survey_date: '',
    },
    now: {
      climate_region: 'UK average',
      construction: {
        drainage: undefined,
        floors: undefined,
        ingress: undefined,
        openings: undefined,
        roof: undefined,
        ventiliation: undefined,
        walls: undefined,
      },
      context_and_other_points: '',
      damp: {
        we_noted: '',
        you_said: '',
      },
      experience: {
        daylight: {
          amount: null,
          note: '',
          problems: false,
        },
        noise: {
          note: '',
          problems: false,
        },
        rooms: {
          favourite: '',
          unloved: '',
        },
        thermal: {
          airquality_summer: null,
          airquality_winter: null,
          avg_temp: undefined,
          draughts_summer: null,
          draughts_winter: null,
          note: '',
          problems: null,
          temperature_summer: null,
          temperature_winter: null,
        },
      },
      floor_area: 0,
      generation: false,
      heating: {
        hours_on_weekday: 0,
        hours_on_weekend: 0,
      },
      historic: {
        comments: '',
        conservation: false,
        listed: undefined,
        when_built: undefined,
      },
      home_type: null,
      hot_water: '',
      laundry_habits: '',
      num_bedrooms: '',
      occupancy: 1,
      previous_works: '',
      radon_chance: undefined,
      space_heating: '',
      space_heating_controls: '',
      structural: {
        we_noted: '',
        you_said: '',
      },
      theworldisburning: {
        flooding_comments: '',
        flooding_history: false,
        flooding_rivers_sea: undefined,
        flooding_surface_water: undefined,
        overheating_comments: '',
      },
      utility: {
        electricity: null,
        gas: null,
        sewer: null,
        water: null,
      },
      ventilation: {
        adequate_paths: false,
        avg_humidity: undefined,
        fuel_burner: undefined,
        fuel_burner_note: undefined,
        gaps: false,
        note: undefined,
        purge_vents: false,
        system_name: 'Unplanned Ventilation (Window Opening Only)',
        ventilation_suggestion: undefined,
      },
    },
    scenario_data: [
      {
        co2: {
          per_person: 0,
          total: 0,
        },
        created_from: {
          num: -1,
        },
        description: undefined,
        heating_hours_on: {
          weekday: 0,
          weekend: 0,
        },
        heating_load: {
          peak_heat: 0,
          peak_heat_m2: NaN,
          temp: 20,
          temp_low: -4,
        },
        heatloss: {
          floorwk: 0,
          infiltrationwk: 0,
          roofwk: 0,
          thermalbridgewk: 0,
          totalwk: 0,
          ventilationwk: 0,
          wallswk: 0,
          windowswk: 0,
        },
        id: 'master',
        measures: [],
        measures_additive_cost: 0,
        measures_cost: 0,
        name: undefined,
        num: 0,
        target_temp: 0,
      },
    ],
    scenarios: {
      list: [],
    },
    used_fuels: [],
  });
});

test('context structure outputs', () => {
  const project = cloneDeep(findProjectFixture('household-1.json').rawData);
  calcAllScenarios(project);
  const parsedData = projectSchema.parse(project);

  const newContext = generatePayload(parsedData, Object.keys(project.data), {
    isSample: false,
  });

  expect(newContext.context).toEqual({
    options: {
      is_sample: false,
    },
    aims: {
      aesthetics: {
        external: false,
        internal: true,
        note: 'aesthetics_note',
      },
      future: {
        lifestyle_change: false,
        lifestyle_change_notes: 'expected_lifestyle_change_note',
        occupancy_actual: 'not yet moved in',
        occupancy_notes: '',
        occupancy_planned: 'long term (10 years plus)',
        other_works: true,
        other_works_notes: 'expected_other_works_note',
        works_start: '3–6 months',
        works_start_notes: '',
      },
      logistics: {
        brief: 'commentary_brief',
        budget_comment: 'logistics_budget_note',
        can_diy: true,
        disruption: 'live elsewhere during the works',
        disruption_comment: 'logistics_disruption_note',
        diy_note: '',
        has_budget: true,
        packaging: 'in phases over time',
      },
      priorities: ['Save carbon', 'Improve comfort', 'Save money'],
      qual_criteria: '',
    },
    any_scenario_has_biomass: false,
    any_scenario_has_generation: false,
    any_scenario_has_non_electricity: false,
    commentary: {
      brief: 'commentary_brief',
      context: 'commentary_context',
      decisions: 'commentary_decisions',
    },
    front: {
      address: '22 Golborne Ave\nManchestpool\nM12 2HX',
      assessor_name: 'A N Surveyor',
      local_authority: 'Manchestpool',
      name: 'A N Other',
      report_date: '06/06/2023',
      report_reference: 'TT005',
      report_version: 'v2',
      signoff_name: 'XX',
      survey_date: '20/03/2022',
    },
    now: {
      climate_region: 'UK average',
      construction: {
        drainage: 'construct_note_drainage',
        floors: 'construct_note_floors',
        ingress: 'construct_note_ingress',
        openings: 'construct_note_openings',
        roof: 'construct_note_roof',
        ventiliation: 'construct_note_ventiliation',
        walls: 'construct_note_walls',
      },
      context_and_other_points: 'context_and_other_points',
      damp: {
        we_noted: 'damp_note',
        you_said: 'damp',
      },
      experience: {
        daylight: {
          amount: 'LOW',
          note: 'daylight_note',
          problems: true,
        },
        noise: {
          note: 'noise_note',
          problems: true,
        },
        rooms: {
          favourite: 'rooms_favourite',
          unloved: 'Dining Room',
        },
        thermal: {
          airquality_summer: 'HIGH',
          airquality_winter: 'LOW',
          avg_temp: 18.5,
          draughts_summer: 'MID',
          draughts_winter: 'LOW',
          note: 'thermal_comfort_note',
          problems: true,
          temperature_summer: 'HIGH',
          temperature_winter: 'LOW',
        },
      },
      floor_area: 0,
      generation: false,
      heating: {
        hours_on_weekday: 5.5,
        hours_on_weekend: 7,
      },
      historic: {
        comments: '',
        conservation: false,
        listed: 'listed as Grade I',
        when_built: '1902',
      },
      home_type: 'mid-terrace',
      hot_water: 'hot_water_provided',
      laundry_habits: 'laundry',
      num_bedrooms: '3',
      occupancy: 1,
      previous_works: 'previous_works',
      radon_chance: 'less than 1%',
      space_heating: 'Combi condensing boiler',
      space_heating_controls: 'Programmer and room stat. ',
      structural: {
        we_noted: '',
        you_said: 'None known. ',
      },
      theworldisburning: {
        flooding_comments: 'flooding_note',
        flooding_history: false,
        flooding_rivers_sea: 'low',
        flooding_surface_water: 'low',
        overheating_comments: 'overheating_note',
      },
      utility: {
        electricity: true,
        gas: true,
        sewer: true,
        water: true,
      },
      ventilation: {
        adequate_paths: false,
        avg_humidity: 67.5,
        fuel_burner: false,
        fuel_burner_note: 'fuel_burner_note',
        gaps: false,
        note: undefined,
        purge_vents: true,
        system_name: 'Unplanned Ventilation (Window Opening Only)',
        ventilation_suggestion: 'Inadequate and should be upgraded as soon as possible',
      },
    },
    scenario_data: [
      {
        co2: {
          per_person: 0,
          total: 0,
        },
        created_from: {
          num: -1,
        },
        description: undefined,
        heating_hours_on: {
          weekday: 5.5,
          weekend: 7,
        },
        heating_load: {
          peak_heat: 0,
          peak_heat_m2: NaN,
          temp: 20,
          temp_low: -4,
        },
        heatloss: {
          floorwk: 0,
          infiltrationwk: 0,
          roofwk: 0,
          thermalbridgewk: 0,
          totalwk: 0,
          ventilationwk: 0,
          wallswk: 0,
          windowswk: 0,
        },
        id: 'master',
        measures: [],
        measures_additive_cost: 0,
        measures_cost: 0,
        name: undefined,
        num: 0,
        target_temp: 0,
      },
    ],
    scenarios: {
      list: [],
    },
    used_fuels: [],
  });
});

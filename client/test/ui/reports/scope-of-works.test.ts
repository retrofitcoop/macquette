import { cloneDeep } from 'lodash';
import { projectSchema } from '../../../src/data-schemas/project';
import { extractScopeOfWorksData } from '../../../src/ui/modules/scope-of-works';
import { findProjectFixture } from '../../fixtures';

test('context structure outputs', () => {
  const project = cloneDeep(findProjectFixture('household-1.json').rawData);
  const parsedData = projectSchema.parse(project);

  const newContext = extractScopeOfWorksData(parsedData.data['master']);

  expect(newContext).toEqual({
    TFA: undefined,
    construct_note_drainage: 'construct_note_drainage',
    construct_note_floors: 'construct_note_floors',
    construct_note_ingress: 'construct_note_ingress',
    construct_note_openings: 'construct_note_openings',
    construct_note_roof: 'construct_note_roof',
    construct_note_ventiliation: 'construct_note_ventiliation',
    construct_note_walls: 'construct_note_walls',
    context_and_other_points: 'context_and_other_points',
    damp: 'damp',
    damp_note: 'damp_note',
    expected_other_works_note: 'expected_other_works_note',
    expected_start_note: '',
    flooding_history: 'no',
    flooding_reservoirs: 'not within potential extent of flooding from reservoir',
    flooding_rivers_sea: 'low',
    flooding_surface_water: 'low',
    historic_age_band: 'before 1900',
    historic_conserved: 'no',
    historic_listed: 'listed as Grade I',
    house_nr_bedrooms: '3',
    house_type: 'mid-terrace',
    overheating_note: 'overheating_note',
    previous_works: 'previous_works',
    radon_risk: 'less than 1%',
    structural_issues: 'None known. ',
    structural_issues_note: '',
    ventilation_adequate_paths: 'No',
    ventilation_gaps: 'No',
    ventilation_name: undefined,
    ventilation_note: undefined,
    ventilation_purge_vents: 'Yes',
    ventilation_suggestion: 'Inadequate and should be upgraded as soon as possible',
    ventilation_type: undefined,
  });
});

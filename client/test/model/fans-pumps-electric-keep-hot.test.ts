import fc from 'fast-check';
import { cloneDeep, pick } from 'lodash';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import { sum } from '../../src/helpers/array-reducers';
import { isNotNull } from '../../src/helpers/null-checking';
import { ModelBehaviourFlags } from '../../src/model/modules/behaviour-version';
import {
  FansPumpsElectricKeepHot,
  FansPumpsElectricKeepHotDependencies,
  FansPumpsElectricKeepHotInput,
  extractFansPumpsElectricKeepHotInputFromLegacy,
} from '../../src/model/modules/fans-pumps-electric-keep-hot';
import {
  HeatingSystemFansAndPumpsCalculator,
  HeatingSystemFansAndPumpsDependencies,
  HeatingSystemFansAndPumpsParameters,
  extractHeatingSystemFansAndPumpsParameters,
  heatingSystemFansAndPumpsParameters,
} from '../../src/model/modules/heating-systems/fans-and-pumps';
import {
  SolarHotWater,
  SolarHotWaterDependencies,
  SolarHotWaterInput,
  extractSolarHotWaterInputFromScenario,
} from '../../src/model/modules/solar-hot-water';
import {
  Ventilation,
  VentilationDependencies,
  VentilationInput,
  extractVentilationInputFromLegacy,
} from '../../src/model/modules/ventilation-infiltration/ventilation';
import { WaterCommon, WaterCommonInput } from '../../src/model/modules/water-common';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbMonthly } from '../arbitraries/monthly';
import { ArbParam, resolveArbParam } from '../helpers/arbitraries';
import { arbitraryRegion } from '../helpers/arbitrary-enums';
import { makeArbitrary } from '../helpers/make-arbitrary';
import { makeLegacyHeatingSystemsForFansAndPumps } from './fans-and-pumps-gains/arbitraries';
import { legacyFansAndPumpsAndCombiKeepHot } from './golden-master/fans-and-pumps-and-combi-keep-hot';
import { arbVentilationInput } from './ventilation-infiltration-arbitraries';

type GoldenMasterInput = {
  fansPumpsElectricKeepHot: FansPumpsElectricKeepHotInput;
  heatingSystems: HeatingSystemFansAndPumpsParameters[];
  ventilation: VentilationInput;
  solarHotWater: Pick<SolarHotWaterInput, 'pump'>;
};
type GoldenMasterDependencies = Omit<
  Pick<FansPumpsElectricKeepHotDependencies, 'fuels'> &
    HeatingSystemFansAndPumpsDependencies &
    VentilationDependencies &
    SolarHotWaterDependencies,
  'modelBehaviourFlags'
>;

function arbGoldenMasterInput(opts?: {
  fuelNames?: ArbParam<string[]>;
  solarHotWater?: ArbParam<WaterCommon['solarHotWater']>;
  makeCompatibleWithLegacy?: boolean;
}): fc.Arbitrary<GoldenMasterInput> {
  const fuelNames = resolveArbParam(opts?.fuelNames, fc.uniqueArray(fc.string()));
  const fuelFractions = fuelNames.chain((names) => {
    const arbRawFloats = fc.tuple(
      ...names.map((name) =>
        sensibleFloat.map((rawFraction) => [name, rawFraction] as const),
      ),
    );
    const fuelFractions = arbRawFloats
      .map((rawFloats) => {
        const total = sum(rawFloats.map(([, fraction]) => fraction));
        if (total === 0) return rawFloats;
        return rawFloats.map(([name, fraction]) => [name, fraction / total] as const);
      })
      .map((normalised) => {
        return Object.fromEntries(normalised);
      });
    return fuelFractions;
  });
  const heatingSystems = fc
    .oneof(sensibleFloat, fc.constant(null), fc.constant('>= 55 litres' as const))
    .chain((globalCombiStorageCapacity) =>
      fc.array(
        makeArbitrary(heatingSystemFansAndPumpsParameters).map((systemInput) => {
          if (systemInput.combiLoss?.type === 'storage') {
            return {
              ...systemInput,
              combiLoss: {
                ...systemInput.combiLoss,
                capacity: globalCombiStorageCapacity,
              },
            };
          } else {
            return systemInput;
          }
        }),
      ),
    );
  const ventilation = arbVentilationInput;
  return fc.record({
    fansPumpsElectricKeepHot: fc.record({
      fuelFractions,
    }),
    heatingSystems,
    ventilation,
    solarHotWater: fc.record({
      pump: fc.constantFrom('electric', 'PV'),
    }),
  });
}

function arbGoldenMasterDependencies(): fc.Arbitrary<GoldenMasterDependencies> {
  return fc.record({
    fuels: fc.record({
      names: fc.uniqueArray(fc.string()),
    }),
    floors: fc.record({
      totalVolume: sensibleFloat,
    }),
    ventilationInfiltrationCommon: fc.record({
      shelterFactor: sensibleFloat,
      windFactor: arbMonthly(sensibleFloat),
    }),
    region: arbitraryRegion,
    waterCommon: fc.record({
      dailyHotWaterUsageLitresMeanAnnual: sensibleFloat,
      hotWaterEnergyContentAnnual: sensibleFloat,
      solarHotWater: fc.constantFrom<WaterCommonInput['solarHotWater']>(
        true,
        'only for water heating',
        'only for fans and pumps energy',
        false,
      ),
    }),
  });
}

function makeLegacyDataForFansPumpsElectricKeepHot(
  input: GoldenMasterInput,
  dependencies: GoldenMasterDependencies,
): fc.Arbitrary<any> {
  let ventilation: fc.Arbitrary<Record<string, unknown>>;
  switch (input.ventilation.type) {
    case 'unplanned/natural ventilation':
      ventilation = fc.constant({ ventilation_type: 'NV' });
      break;
    case 'intermittent extract':
      ventilation = fc.constant({
        ventilation_type: 'IE',
        EVP: input.ventilation.extractVentilationPoints.map(
          ({ ventilationRate }, idx) => ({
            id: idx,
            tag: 'TAG',
            name: 'NAME',
            ventilation_rate: ventilationRate,
          }),
        ),
      });
      break;
    case 'mechanical extract': {
      const ventilationInput = input.ventilation;
      ventilation = fc.boolean().map((isDecentralised) =>
        isDecentralised
          ? {
              ventilation_type: 'DEV',
              system_air_change_rate: ventilationInput.systemAirChangeRate,
              system_specific_fan_power: ventilationInput.systemSpecificFanPower,
            }
          : {
              ventilation_type: 'MEV',
              system_air_change_rate: ventilationInput.systemAirChangeRate,
              system_specific_fan_power: ventilationInput.systemSpecificFanPower,
            },
      );
      break;
    }
    case 'passive stack':
      ventilation = fc.constant({
        ventilation_type: 'PS',
        EVP: input.ventilation.extractVentilationPoints.map(
          ({ ventilationRate }, idx) => ({
            id: idx,
            tag: 'TAG',
            name: 'NAME',
            ventilation_rate: ventilationRate,
          }),
        ),
      });
      break;
    case 'mechanical ventilation with heat recovery':
      ventilation = fc.constant({
        ventilation_type: 'MVHR',

        system_air_change_rate: input.ventilation.systemAirChangeRate,
        balanced_heat_recovery_efficiency: input.ventilation.efficiencyProportion * 100,
        system_specific_fan_power: input.ventilation.systemSpecificFanPower,
      });
      break;
    case 'mechanical ventilation':
      ventilation = fc.constant({
        ventilation_type: 'MV',
        system_air_change_rate: input.ventilation.systemAirChangeRate,
        system_specific_fan_power: input.ventilation.systemSpecificFanPower,
      });
      break;
  }
  return fc
    .record({
      heatingSystemsPieces: makeLegacyHeatingSystemsForFansAndPumps(input.heatingSystems),
      gains_W: fc.constant({}),
      volume: fc.constant(dependencies.floors.totalVolume),
      energy_requirements: fc.constant({}),
      ventilation,
      fuel_requirements: fc.constant({ fans_and_pumps: { list: [] } }),
      fans_and_pumps: fc.constant(
        Object.entries(input.fansPumpsElectricKeepHot.fuelFractions).map(
          ([fuelName, fraction]) => ({
            fuel: fuelName,
            fraction: fraction,
          }),
        ),
      ),
      use_SHW:
        dependencies.waterCommon.solarHotWater === true ||
        dependencies.waterCommon.solarHotWater === 'only for fans and pumps energy'
          ? fc.constantFrom(1, true, '1')
          : fc.constantFrom(undefined, 0, false),
      SHW: fc.constant({
        pump: input.solarHotWater.pump,
      }),
    })
    .map(({ heatingSystemsPieces: { heating_systems, Vc }, ...rest }) => ({
      ...rest,
      water_heating: { Vc },
      heating_systems,
    }));
}

describe('fans, pumps and electric keep-hot', () => {
  const goldenMasterArb = arbGoldenMasterDependencies()
    .chain((dependencies) =>
      arbGoldenMasterInput({
        fuelNames: dependencies.fuels.names,
        solarHotWater: dependencies.waterCommon.solarHotWater,
        makeCompatibleWithLegacy: true,
      }).map((input) => ({
        input,
        dependencies,
      })),
    )
    .chain(({ input, dependencies }) =>
      makeLegacyDataForFansPumpsElectricKeepHot(input, dependencies).map(
        (legacyData) => ({ input, dependencies, legacyData }),
      ),
    );
  test('golden master', () => {
    const fansAndPumpsFlags: ModelBehaviourFlags['heatingSystems']['fansAndPumps'] = {
      fixUndefinedSpecificFanPowerInWarmAirSystems: false,
      warmAirSystemsZeroGainInSummer: false,
      allowFansPumpsEnergyToUseSpecificFanPower: false,
      fixHeatGainsForWarmAirSystems: false,
    };
    fc.assert(
      fc.property(
        goldenMasterArb,
        ({ input, dependencies: dependencies_, legacyData: legacyData_ }) => {
          const dependencies = {
            ...dependencies_,
            modelBehaviourFlags: {
              heatingSystems: { fansAndPumps: fansAndPumpsFlags },
              fansPumpsElectricKeepHot: {
                useDefaultFuelIfFuelFractionsIsEmpty: false,
              },
            },
          };
          const heatingSystems = input.heatingSystems.map(
            (hsInput) => new HeatingSystemFansAndPumpsCalculator(hsInput, dependencies),
          );
          const ventilation = new Ventilation(input.ventilation, dependencies);
          const solarHotWater = new SolarHotWater(
            input.solarHotWater as any,
            dependencies,
          );
          const fansPumpsElectricKeepHot = new FansPumpsElectricKeepHot(
            input.fansPumpsElectricKeepHot,
            {
              ...dependencies,
              heatingSystems: { fansAndPumps: heatingSystems },
              ventilation,
              solarHotWater,
            },
          );
          const legacyData = cloneDeep(legacyData_);
          legacyFansAndPumpsAndCombiKeepHot(legacyData);
          fc.pre(legacyData.energy_requirements?.fans_and_pumps?.quantity !== undefined);
          expect(fansPumpsElectricKeepHot.annualEnergy).toBeApproximately(
            legacyData.energy_requirements.fans_and_pumps.quantity,
          );
          const fuelsNotIncluded = new Set(dependencies.fuels.names);
          for (const legacyFuelRequirement of legacyData.fuel_requirements.fans_and_pumps
            .list) {
            fuelsNotIncluded.delete(legacyFuelRequirement.fuel);
            expect(
              fansPumpsElectricKeepHot.fuelDemandByFuelAnnual.get(
                legacyFuelRequirement.fuel,
              )!.energyDemand,
            ).toBeApproximately(legacyFuelRequirement.demand);
          }
          for (const fuelName of fuelsNotIncluded) {
            expect(
              fansPumpsElectricKeepHot.fuelDemandByFuelAnnual.get(fuelName)!.energyDemand,
            ).toBe(0);
          }
        },
      ),
    );
  });

  test('combined extractors', () => {
    fc.assert(
      fc.property(goldenMasterArb, ({ input, legacyData }) => {
        const parsed = scenarioSchema.parse(legacyData);
        expect(extractVentilationInputFromLegacy(parsed)).toEqual(input.ventilation);
        expect(extractSolarHotWaterInputFromScenario(parsed).pump).toEqual(
          input.solarHotWater.pump,
        );
        const extractedHeatingSystems =
          parsed?.heating_systems
            ?.map((legacyHeatingSystem) =>
              extractHeatingSystemFansAndPumpsParameters(
                legacyHeatingSystem,
                parsed.water_heating?.Vc ?? null,
              ),
            )
            .filter(isNotNull) ?? [];
        expect(extractedHeatingSystems).toEqual(input.heatingSystems);
        expect(extractFansPumpsElectricKeepHotInputFromLegacy(parsed)).toEqual(
          input.fansPumpsElectricKeepHot,
        );
      }),
    );
  });

  const newFlags: Pick<
    ModelBehaviourFlags,
    'heatingSystems' | 'fansPumpsElectricKeepHot'
  > = {
    heatingSystems: {
      fansAndPumps: {
        fixUndefinedSpecificFanPowerInWarmAirSystems: true,
        warmAirSystemsZeroGainInSummer: true,
        allowFansPumpsEnergyToUseSpecificFanPower: true,
        fixHeatGainsForWarmAirSystems: true,
      },
      waterHeating: {
        fixLossesForStorageCombiOfUnknownSize: true,
      },
    },
    fansPumpsElectricKeepHot: {
      useDefaultFuelIfFuelFractionsIsEmpty: true,
    },
  };

  describe('mutator', () => {
    test('works', () => {
      type Dependencies = Omit<
        FansPumpsElectricKeepHotDependencies,
        'modelBehaviourFlags'
      >;
      function arbDependencies(): fc.Arbitrary<Dependencies> {
        return fc.record({
          heatingSystems: fc.record({
            fansAndPumps: fc.array(fc.record({ annualEnergyFansPumps: sensibleFloat })),
          }),
          ventilation: fc.record({
            annualEnergyFans: sensibleFloat,
          }),
          waterCommon: fc.record({
            solarHotWater: fc.constantFrom(
              true,
              false,
              'only for water heating' as const,
              'only for fans and pumps energy' as const,
            ),
          }),
          solarHotWater: fc.record({
            annualEnergyPumps: sensibleFloat,
          }),
          fuels: fc.record({ names: fc.uniqueArray(fc.string()) }),
        });
      }
      function arbInput(
        dependencies: Dependencies,
      ): fc.Arbitrary<FansPumpsElectricKeepHotInput> {
        const arbRawFloats = fc.tuple(
          ...dependencies.fuels.names.map((name) =>
            fc.tuple(fc.constant(name), sensibleFloat),
          ),
        );
        const arbNormalised = arbRawFloats.map((rawFloats) => {
          const total = sum(rawFloats.map(([, float]) => float));
          if (total === 0) return rawFloats;
          return rawFloats.map(([name, float]) => [name, float / total] as const);
        });
        const arbFuelFractions = arbNormalised.map((normalised) =>
          Object.fromEntries(normalised),
        );
        return fc.record({
          fuelFractions: arbFuelFractions,
        });
      }
      const arb = arbDependencies().chain((dependencies) =>
        arbInput(dependencies).map((input) => ({
          input,
          dependencies,
        })),
      );
      fc.assert(
        fc.property(arb, ({ input, dependencies: dependencies_ }) => {
          const dependencies = {
            ...dependencies_,
            modelBehaviourFlags: {
              ...pick(newFlags, 'heatingSystems'),
              fansPumpsElectricKeepHot: {
                useDefaultFuelIfFuelFractionsIsEmpty: false,
              },
            },
          };
          const fansPumpsElectricKeepHot = new FansPumpsElectricKeepHot(
            input,
            dependencies,
          );
          const legacyData: any = {};
          fansPumpsElectricKeepHot.mutateLegacyData(legacyData);
          expect(legacyData.energy_requirements.fans_and_pumps).toEqual({
            name: 'Fans and pumps',
            quantity: fansPumpsElectricKeepHot.annualEnergy,
            monthly: expect.arrayContaining([expect.any(Number)]),
          });
          expect(
            sum(legacyData.energy_requirements.fans_and_pumps.monthly),
          ).toBeApproximately(fansPumpsElectricKeepHot.annualEnergy);
          expect(legacyData.fuel_requirements.fans_and_pumps.quantity).toEqual(
            fansPumpsElectricKeepHot.totalFuelDemandAnnual,
          );
          expect(legacyData.fuel_requirements.fans_and_pumps.list).toEqual(
            legacyData.fans_and_pumps,
          );

          for (const legacyFuelRequirement of legacyData.fuel_requirements.fans_and_pumps
            .list ?? []) {
            expect(fansPumpsElectricKeepHot.fuelDemandByFuelAnnual).toContain(
              legacyFuelRequirement.fuel,
            );
            const fuelDemand = fansPumpsElectricKeepHot.fuelDemandByFuelAnnual.get(
              legacyFuelRequirement.fuel,
            )!;
            expect(legacyFuelRequirement.demand).toBe(fuelDemand.energyDemand);
            expect(legacyFuelRequirement.fuel_input).toBe(fuelDemand.fuelInput);
            expect(legacyFuelRequirement.fraction).toBe(fuelDemand.relativeFraction);
          }
        }),
      );
    });
  });
  describe('new behaviour', () => {
    describe('warm air systems', () => {
      test('if energy override is not specified, system calculates energy from SFP', () => {
        const input: HeatingSystemFansAndPumpsParameters = {
          type: 'warm air system',
          specificFanPower: 1,
          annualEnergyFansPumpsOverride: null,
          pumpPowerKWhPerYear: 0,
          combiLoss: null,
        };
        const dependencies: HeatingSystemFansAndPumpsDependencies = {
          floors: { totalVolume: 1 },
          modelBehaviourFlags: newFlags,
        };
        const system = new HeatingSystemFansAndPumpsCalculator(input, dependencies);
        expect(system.annualEnergyFansPumps).toBe(0.4);
      });

      test('if energy override is specified, system uses it', () => {
        const input: HeatingSystemFansAndPumpsParameters = {
          type: 'warm air system',
          specificFanPower: 1,
          annualEnergyFansPumpsOverride: 100,
          pumpPowerKWhPerYear: 0,
          combiLoss: null,
        };
        const dependencies: HeatingSystemFansAndPumpsDependencies = {
          floors: { totalVolume: 1 },
          modelBehaviourFlags: newFlags,
        };
        const system = new HeatingSystemFansAndPumpsCalculator(input, dependencies);
        expect(system.annualEnergyFansPumps).toBe(100);
      });
    });
  });

  test('fuel demand should not be empty even when no fuel fractions are specified in input', () => {
    const input: FansPumpsElectricKeepHotInput = {
      fuelFractions: {},
    };
    const dependencies: FansPumpsElectricKeepHotDependencies = {
      heatingSystems: {
        fansAndPumps: [],
      },
      ventilation: { annualEnergyFans: 1 },
      waterCommon: { solarHotWater: true },
      solarHotWater: { annualEnergyPumps: 1 },
      fuels: { names: [] },
      modelBehaviourFlags: {
        fansPumpsElectricKeepHot: {
          useDefaultFuelIfFuelFractionsIsEmpty: true,
        },
      },
    };
    const fpekh = new FansPumpsElectricKeepHot(input, dependencies);
    expect(fpekh.fuelDemandByFuelAnnual.size).not.toBe(0);
  });
});

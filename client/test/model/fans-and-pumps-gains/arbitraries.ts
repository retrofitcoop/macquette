/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-explicit-any */

import fc from 'fast-check';
import { CombiLossParameters } from '../../../src/model/modules/heating-systems/combi-loss';
import { HeatingSystemFansAndPumpsParameters } from '../../../src/model/modules/heating-systems/fans-and-pumps';

export function makeLegacyHeatingSystemsForFansAndPumps(
  inputs: HeatingSystemFansAndPumpsParameters[],
): fc.Arbitrary<{ heating_systems: unknown[]; Vc: number | undefined }> {
  let Vc: number | undefined = undefined;
  const systemsArbs = inputs.map((input) => {
    if (
      input.combiLoss?.type === 'storage' &&
      typeof input.combiLoss.capacity === 'number'
    ) {
      Vc = input.combiLoss.capacity;
    }
    let category: fc.Arbitrary<string>;
    let fans_and_supply_pumps: fc.Arbitrary<number | undefined>;
    switch (input.type) {
      case 'warm air system':
        category = fc.constant('Warm air systems');
        fans_and_supply_pumps = fc.constant(
          input.annualEnergyFansPumpsOverride ?? undefined,
        );
        break;
      case 'central heating system with pump inside':
      case 'other':
        category = fc.constantFrom(
          'Combi boilers',
          'Heat pumps',
          'System boilers',
          'Hot water only',
          'Room heaters',
        );
        fans_and_supply_pumps = fc.constant(input.annualEnergyFansPumps);
        break;
    }
    return fc
      .record({
        category,
        sfp:
          input.type === 'warm air system' && input.specificFanPower !== null
            ? fc.constant(input.specificFanPower)
            : fc.constantFrom('', null, undefined, 'undefined'),
        central_heating_pump_inside: fc.constant(
          input.type === 'central heating system with pump inside',
        ),
        central_heating_pump: fc.constant(input.pumpPowerKWhPerYear),
        fuel: fc.constant(''),
        combiLossFields: makeLegacyCombiLossSystemFields(input.combiLoss),
        fans_and_supply_pumps,
      })
      .map(({ combiLossFields, ...rest }) => ({ ...combiLossFields, ...rest }));
  });
  return fc.record({
    Vc: fc.constant(Vc),
    heating_systems: fc.tuple(...systemsArbs),
  });
}

function makeLegacyCombiLossSystemFields(
  combiLoss: CombiLossParameters,
): fc.Arbitrary<any> {
  let isInstantaneous: fc.Arbitrary<boolean>;
  if (combiLoss === null) {
    isInstantaneous = fc.boolean();
  } else {
    isInstantaneous = fc.constant(false); // Legacy model checks first for combi loss, then for point-of-use instantaneousness
  }
  return isInstantaneous.chain((isInstantaneous): fc.Arbitrary<any> => {
    if (combiLoss === null) {
      if (isInstantaneous) {
        return fc.record({
          systemMixin: fc.record({
            instantaneous_water_heating: fc.constant(1),
            combi_loss: fc.constantFrom('0', 0, undefined),
          }),
        });
      } else {
        return fc.record({
          systemMixin: fc.record({
            instantaneous_water_heating: fc.constant(false),
            combi_loss: fc.constantFrom('0', 0, undefined),
          }),
        });
      }
    } else {
      let combi_loss: fc.Arbitrary<string>;
      switch (combiLoss.type) {
        case 'instantaneous': {
          if (combiLoss.keepHotFacility === null) {
            combi_loss = fc.constant('Instantaneous, without keep hot-facility');
          } else if (combiLoss.keepHotFacility.controlledByTimeClock) {
            combi_loss = fc.constant(
              'Instantaneous, with keep-hot facility controlled by time clock',
            );
          } else {
            combi_loss = fc.constant(
              'Instantaneous, with keep-hot facility not controlled by time clock',
            );
          }
          break;
        }
        case 'storage': {
          if (combiLoss.capacity === '>= 55 litres') {
            combi_loss = fc.constant('Storage combi boiler >= 55 litres');
          } else if (combiLoss.capacity === null) {
            combi_loss = fc.constant('Storage combi boiler < 55 litres');
          } else {
            combi_loss = fc.constant('Storage combi boiler < 55 litres');
          }
        }
      }
      return fc.record({
        instantaneous_water_heating: fc.constant(false),
        combi_loss,
      });
    }
  });
}

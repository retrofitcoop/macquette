import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { sum } from '../../../src/helpers/array-reducers';
import { Month } from '../../../src/model/enums/month';
import { Fuels } from '../../../src/model/modules/fuels';
import {
  AppliancesCookingLoadCollections,
  AppliancesCookingLoadCollectionsDependencies,
  AppliancesCookingLoadCollectionsInput,
  FuelType,
  extractAppliancesCookingLoadCollectionsInputFromLegacy,
} from '../../../src/model/modules/lighting-appliances-cooking/loads/load-collection';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { legacyApplianceCarbonCoop } from '../golden-master/appliance-carbon-coop';
import { withSuppressedConsoleWarn } from '../with-suppressed-console-warn';

const arbAppliancesCookingLoadCollectionsInput: fc.Arbitrary<AppliancesCookingLoadCollectionsInput> =
  fc.record({
    enabled: fc.boolean(),
    loads: fc.array(
      fc.record({
        numberUsed: sensibleFloat,
        aPlusRated: fc.boolean(),
        normalisedDemand: sensibleFloat,
        utilisationFactor: sensibleFloat,
        referenceQuantity: sensibleFloat,
        annualUseFrequency: sensibleFloat,
        fuel: fc.record({
          type: fc.constantFrom('electricity', 'gas', 'oil'),
          name: fc.string(),
          efficiency: sensibleFloat,
        }),
        category: fc.constantFrom('appliances', 'cooking'),
      }),
    ),
  });

function arbAppliancesCookingLoadCollectionsDependencies(): fc.Arbitrary<AppliancesCookingLoadCollectionsDependencies> {
  return fc.record({
    fuels: fc.record({ names: fc.array(fc.string()) }),
    modelBehaviourFlags: fc.record({
      appliancesCookingLoadCollections: fc.record({
        treatMonthlyGainAsPower: fc.boolean(),
        convertGainsToWatts: fc.boolean(),
        useFuelInputForFuelFraction: fc.boolean(),
        useWeightedMonthsForEnergyDemand: fc.boolean(),
      }),
    }),
  });
}

function makeLegacyDataForAppliancesCookingLoadCollections(
  input: AppliancesCookingLoadCollectionsInput,
) {
  return {
    LAC_calculation_type: input.enabled ? 'carboncoop_SAPlighting' : 'SAP',
    fuel_requirements: {
      cooking: { quantity: 0, list: [] },
      appliances: { quantity: 0, list: [] },
    },
    energy_requirements: { appliances: {}, cooking: {} },
    gains_W: {},
    applianceCarbonCoop: {
      list: input.loads.map((load) => {
        let type_of_fuel: string;
        switch (load.fuel.type) {
          case 'electricity':
            type_of_fuel = 'Electricity';
            break;
          case 'gas':
            type_of_fuel = 'Gas';
            break;
          case 'oil':
            type_of_fuel = 'Oil';
            break;
        }
        let category: string;
        switch (load.category) {
          case 'appliances':
            category = 'Appliances';
            break;
          case 'cooking':
            category = 'Cooking';
            break;
        }
        return {
          fuel: load.fuel.name,
          type_of_fuel,
          efficiency: load.fuel.efficiency,
          number_used: load.numberUsed,
          a_plus_rated: load.aPlusRated ? 1 : false,
          norm_demand: load.normalisedDemand,
          utilisation_factor: load.utilisationFactor,
          reference_quantity: load.referenceQuantity,
          frequency: load.annualUseFrequency,
          category,
        };
      }),
    },
  };
}
function toLegacyFuelTypeName(fuelType: FuelType): string {
  switch (fuelType) {
    case 'electricity':
      return 'Electricity';
    case 'gas':
      return 'Gas';
    case 'oil':
      return 'Oil';
  }
}

const exampleLoads = {
  laptop: {
    numberUsed: 2,
    aPlusRated: false,
    normalisedDemand: 42,
    utilisationFactor: 1,
    referenceQuantity: 1,
    annualUseFrequency: 1,
    fuel: { type: 'electricity', name: Fuels.STANDARD_TARIFF, efficiency: 1 },
    category: 'appliances',
  },
  fridge: {
    numberUsed: 1,
    aPlusRated: true,
    normalisedDemand: 2,
    utilisationFactor: 1,
    referenceQuantity: 1,
    annualUseFrequency: 365,
    fuel: { type: 'electricity', name: Fuels.STANDARD_TARIFF, efficiency: 1 },
    category: 'appliances',
  },
  oven: {
    numberUsed: 1,
    aPlusRated: false,
    normalisedDemand: 5,
    utilisationFactor: 1,
    referenceQuantity: 1,
    annualUseFrequency: 365,
    fuel: { type: 'electricity', name: Fuels.STANDARD_TARIFF, efficiency: 1 },
    category: 'cooking',
  },
} satisfies Record<string, AppliancesCookingLoadCollectionsInput['loads'][number]>;

describe('appliances & cooking load collections', () => {
  test('golden master', () => {
    // Disallow "toString", "__proto__", etc. as fuel names. These cause
    // prototype pollution in the legacy fuel requirements calcs.
    const disallowedFuelNames = Object.getOwnPropertyNames(Object.getPrototypeOf({}));

    withSuppressedConsoleWarn(() =>
      fc.assert(
        fc.property(
          arbAppliancesCookingLoadCollectionsInput.filter(
            (input) =>
              input.enabled &&
              !input.loads.some((load) => disallowedFuelNames.includes(load.fuel.name)),
          ),
          arbAppliancesCookingLoadCollectionsDependencies(),
          (input, dependencies_) => {
            const dependencies = cloneDeep(dependencies_);
            dependencies.fuels.names.push(...input.loads.map(({ fuel }) => fuel.name));
            const appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
              input,
              dependencies,
            );
            const legacyData: any =
              makeLegacyDataForAppliancesCookingLoadCollections(input);
            legacyApplianceCarbonCoop(legacyData);

            // Energy demand
            expect(
              legacyData.applianceCarbonCoop.energy_demand_total.appliances,
            ).toBeApproximately(
              appliancesCookingLoadCollections.appliances.energyDemandAnnual,
            );
            expect(
              legacyData.applianceCarbonCoop.energy_demand_total.cooking,
            ).toBeApproximately(
              appliancesCookingLoadCollections.cooking.energyDemandAnnual,
            );
            expect(
              legacyData.applianceCarbonCoop.energy_demand_total.total,
            ).toBeApproximately(
              appliancesCookingLoadCollections.appliances.energyDemandAnnual +
                appliancesCookingLoadCollections.cooking.energyDemandAnnual,
            );

            // Gains
            if (
              !dependencies.modelBehaviourFlags.appliancesCookingLoadCollections
                .convertGainsToWatts
            ) {
              // eslint-disable-next-line jest/no-conditional-expect
              expect(legacyData.applianceCarbonCoop.gains_W.Appliances).toBeApproximately(
                appliancesCookingLoadCollections.appliances.heatGainAverageAnnual,
              );
              // eslint-disable-next-line jest/no-conditional-expect
              expect(legacyData.applianceCarbonCoop.gains_W.Cooking).toBeApproximately(
                appliancesCookingLoadCollections.cooking.heatGainAverageAnnual,
              );
            }

            // Fuel
            expect(
              legacyData.applianceCarbonCoop.fuel_input_total.appliances,
            ).toBeApproximately(
              appliancesCookingLoadCollections.appliances.fuelInputAnnual,
            );
            expect(
              legacyData.applianceCarbonCoop.fuel_input_total.cooking,
            ).toBeApproximately(appliancesCookingLoadCollections.cooking.fuelInputAnnual);
            for (const fuelType of ['electricity', 'gas', 'oil'] as const) {
              expect(
                legacyData.applianceCarbonCoop.energy_demand_by_type_of_fuel[
                  toLegacyFuelTypeName(fuelType)
                ] ?? 0,
              ).toBeApproximately(
                appliancesCookingLoadCollections.appliances.energyDemandAnnualByFuelType(
                  fuelType,
                ) +
                  appliancesCookingLoadCollections.cooking.energyDemandAnnualByFuelType(
                    fuelType,
                  ),
              );
            }
            expect(legacyData.fuel_requirements.appliances.list).toHaveLength(
              appliancesCookingLoadCollections.appliances.fuelDemandByFuelAnnual.size,
            );
            expect(legacyData.fuel_requirements.cooking.list).toHaveLength(
              appliancesCookingLoadCollections.cooking.fuelDemandByFuelAnnual.size,
            );
            for (const section of ['appliances', 'cooking'] as const) {
              for (const legacyFuelRequirement of legacyData.fuel_requirements[section]
                .list) {
                const fuelInfo = appliancesCookingLoadCollections[
                  section
                ].fuelDemandByFuelAnnual.get(legacyFuelRequirement.fuel)!;
                expect(legacyFuelRequirement.demand).toBeApproximately(
                  fuelInfo.energyDemand,
                );
                if (
                  !dependencies.modelBehaviourFlags.appliancesCookingLoadCollections
                    .useFuelInputForFuelFraction
                ) {
                  // eslint-disable-next-line jest/no-conditional-expect
                  expect(legacyFuelRequirement.fraction).toBeApproximately(
                    fuelInfo.relativeFraction,
                  );
                }
                expect(legacyFuelRequirement.fuel_input).toBeApproximately(
                  fuelInfo.fuelInput,
                );
              }
            }

            // By load
            const combinedLoads = [
              ...appliancesCookingLoadCollections.appliances.loads,
              ...appliancesCookingLoadCollections.cooking.loads,
            ];
            (legacyData.applianceCarbonCoop.list as any[]).forEach(
              (legacyLoad, index) => {
                const load = combinedLoads.find(
                  (load) => load.input.originalIndex === index,
                );
                expect(load).toBeDefined();
                expect(legacyLoad.energy_demand).toBeApproximately(
                  load!.energyDemandAnnual,
                );
                expect(legacyLoad.fuel_input).toBeApproximately(load!.fuelInputAnnual);
              },
            );

            // By month
            for (const month of Month.all) {
              // Energy
              if (
                !dependencies.modelBehaviourFlags.appliancesCookingLoadCollections
                  .useWeightedMonthsForEnergyDemand
              ) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(
                  legacyData.applianceCarbonCoop.energy_demand_monthly.appliances[
                    month.index0
                  ],
                ).toBeApproximately(
                  appliancesCookingLoadCollections.appliances.energyDemandMonthly(month),
                );
                // eslint-disable-next-line jest/no-conditional-expect
                expect(
                  legacyData.applianceCarbonCoop.energy_demand_monthly.cooking[
                    month.index0
                  ],
                ).toBeApproximately(
                  appliancesCookingLoadCollections.cooking.energyDemandMonthly(month),
                );
                // eslint-disable-next-line jest/no-conditional-expect
                expect(
                  legacyData.applianceCarbonCoop.energy_demand_monthly.total[
                    month.index0
                  ],
                ).toBeApproximately(
                  appliancesCookingLoadCollections.appliances.energyDemandMonthly(month) +
                    appliancesCookingLoadCollections.cooking.energyDemandMonthly(month),
                );
              }

              // Gains
              if (
                !dependencies.modelBehaviourFlags.appliancesCookingLoadCollections
                  .convertGainsToWatts &&
                !dependencies.modelBehaviourFlags.appliancesCookingLoadCollections
                  .treatMonthlyGainAsPower
              ) {
                // eslint-disable-next-line jest/no-conditional-expect
                expect(
                  legacyData.applianceCarbonCoop.gains_W_monthly.Appliances[month.index0],
                ).toBeApproximately(
                  appliancesCookingLoadCollections.appliances.heatGain(month),
                );
                // eslint-disable-next-line jest/no-conditional-expect
                expect(
                  legacyData.applianceCarbonCoop.gains_W_monthly.Cooking[month.index0],
                ).toBeApproximately(
                  appliancesCookingLoadCollections.cooking.heatGain(month),
                );
              }
            }
          },
        ),
      ),
    );
  });

  test('extractor', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(arbAppliancesCookingLoadCollectionsInput, (input) => {
          const roundTripped = extractAppliancesCookingLoadCollectionsInputFromLegacy(
            scenarioSchema.parse(
              makeLegacyDataForAppliancesCookingLoadCollections(input),
            ),
          );
          expect(roundTripped).toEqual(input);
        }),
      );
    });
  });

  test('when treatMonthlyGainAsPower is enabled, monthly gains equal annual gains', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(
          arbAppliancesCookingLoadCollectionsInput,
          arbAppliancesCookingLoadCollectionsDependencies().filter(
            (deps) =>
              deps.modelBehaviourFlags.appliancesCookingLoadCollections
                .treatMonthlyGainAsPower === true,
          ),
          (input, dependencies_) => {
            const dependencies = cloneDeep(dependencies_);
            dependencies.fuels.names.push(...input.loads.map(({ fuel }) => fuel.name));
            const appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
              input,
              dependencies,
            );
            for (const category of ['appliances', 'cooking'] as const) {
              for (const month of Month.all) {
                expect(appliancesCookingLoadCollections[category].heatGain(month)).toBe(
                  appliancesCookingLoadCollections[category].heatGainAverageAnnual,
                );
              }
            }
          },
        ),
      );
    });
  });

  test('monthly energy demands sum to annual energy demand', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(
          arbAppliancesCookingLoadCollectionsInput,
          arbAppliancesCookingLoadCollectionsDependencies(),
          (input, dependencies_) => {
            const dependencies = cloneDeep(dependencies_);
            dependencies.fuels.names.push(...input.loads.map(({ fuel }) => fuel.name));
            const appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
              input,
              dependencies,
            );
            for (const category of ['appliances', 'cooking'] as const) {
              const sumMonthly = sum(
                Month.all.map((month) =>
                  appliancesCookingLoadCollections[category].energyDemandMonthly(month),
                ),
              );
              expect(sumMonthly).toBeApproximately(
                appliancesCookingLoadCollections[category].energyDemandAnnual,
              );
            }
          },
        ),
      );
    });
  });

  test('gains are computed correctly with example data', () => {
    const input: AppliancesCookingLoadCollectionsInput = {
      enabled: true,
      loads: [exampleLoads.laptop, exampleLoads.fridge],
    };
    const dependencies: AppliancesCookingLoadCollectionsDependencies = {
      fuels: { names: [Fuels.STANDARD_TARIFF] },
      modelBehaviourFlags: {
        appliancesCookingLoadCollections: {
          treatMonthlyGainAsPower: true,
          convertGainsToWatts: true,
          useWeightedMonthsForEnergyDemand: true,
          useFuelInputForFuelFraction: true,
        },
      },
    };
    const appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
      input,
      dependencies,
    );
    const kWhPerYearToWatts = 1000 / (365 * 24);
    const kWhPerDayToWatts = 1000 / 24;
    const aPlusAdjustment = 0.75;
    expect(appliancesCookingLoadCollections.appliances.heatGainAverageAnnual).toBe(
      2 * 42 * kWhPerYearToWatts + 2 * kWhPerDayToWatts * aPlusAdjustment,
    );
  });

  test('when loads input is an empty array, everything returns 0', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(
          fc.constantFrom('appliances' as const, 'cooking' as const),
          fc.constantFrom(...Month.all),
          arbAppliancesCookingLoadCollectionsDependencies(),
          (category, month, dependencies) => {
            const appliancesCookingLoadCollections = new AppliancesCookingLoadCollections(
              { enabled: true, loads: [] },
              dependencies,
            );
            expect(
              appliancesCookingLoadCollections[category].energyDemandMonthly(month),
            ).toBe(0);
          },
        ),
      );
    });
  });
});

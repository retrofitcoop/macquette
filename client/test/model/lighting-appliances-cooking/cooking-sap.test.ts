import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { Month } from '../../../src/model/enums/month';
import { FuelName, Fuels } from '../../../src/model/modules/fuels';
import {
  CookingSAP,
  CookingSAPDependencies,
  CookingSAPInput,
  extractCookingSAPInputFromLegacy,
} from '../../../src/model/modules/lighting-appliances-cooking/sap/cooking';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { ArbParam, resolveArbParam } from '../../helpers/arbitraries';
import { legacyLightingAppliancesCooking } from '../golden-master/lighting-appliances-cooking';

function arbCookingSAPInput(opts?: {
  enabled?: ArbParam<boolean>;
}): fc.Arbitrary<CookingSAPInput> {
  return fc.record({
    enabled: resolveArbParam(opts?.enabled, fc.boolean()),
    energyEfficient: fc.boolean(),
    fuelFractions: fc.dictionary(fc.string(), sensibleFloat),
  });
}
function arbCookingSAPDependencies(): fc.Arbitrary<CookingSAPDependencies> {
  return fc.record({
    fuels: fc.record({
      names: fc.array(fc.string()),
      standardTariff: fc.record({ carbonEmissionsFactor: sensibleFloat }),
    }),
    floors: fc.record({
      totalFloorArea: sensibleFloat.filter((value) => value >= 0),
    }),
    occupancy: fc.record({
      occupancy: sensibleFloat.filter((value) => value >= 0),
    }),
  });
}

function makeLegacyDataForCookingSAP(
  input: CookingSAPInput,
  dependencies: CookingSAPDependencies,
) {
  const fuelFractionsEntries = Object.entries(input.fuelFractions);
  let fuels_cooking: any[] | undefined;
  if (fuelFractionsEntries.length === 0) {
    fuels_cooking = undefined;
  } else {
    fuels_cooking = fuelFractionsEntries.map(([fuelName, fraction]) => ({
      fuel: fuelName,
      fraction,
    }));
  }
  return {
    occupancy: dependencies.occupancy.occupancy,
    TFA: dependencies.floors.totalFloorArea,
    LAC_calculation_type: input.enabled ? 'SAP' : 'carboncoop_SAPlighting',
    LAC: {
      energy_efficient_cooking: input.energyEfficient,
      fuels_cooking,
    },
    fuel_requirements: {
      cooking: { quantity: 0 },
      lighting: { quantity: 0 },
      appliances: { quantity: 0 },
    },
    gains_W: {},
    energy_requirements: {},
    fuels: {
      [Fuels.STANDARD_TARIFF]: {
        co2factor: dependencies.fuels.standardTariff.carbonEmissionsFactor,
        primaryenergyfactor: 0,
        fuelcost: 0,
        standingcharge: 0,
        category: 'Electricity',
      },
    },
  };
}

describe('cooking SAP', () => {
  test('golden master', () => {
    fc.assert(
      fc.property(
        arbCookingSAPInput({ enabled: true }),
        arbCookingSAPDependencies().filter(({ floors }) => floors.totalFloorArea !== 0),
        (input, dependencies_) => {
          const dependencies = cloneDeep(dependencies_);
          dependencies.fuels.names.push(...Object.keys(input.fuelFractions));
          const cookingSAP = new CookingSAP(input, dependencies);
          const legacyData: any = makeLegacyDataForCookingSAP(input, dependencies);
          legacyLightingAppliancesCooking(legacyData);
          expect(legacyData.LAC.EC).toBeApproximately(cookingSAP.energyAnnual);
          expect(legacyData.LAC.EC_monthly).toBeApproximately(cookingSAP.energyMonthly);
          for (const month of Month.all) {
            expect(legacyData.gains_W.Cooking[month.index0]).toBeApproximately(
              cookingSAP.heatGain,
            );
          }
          const fuelsNotCounted = new Set<FuelName>(Object.keys(input.fuelFractions));
          for (const legacyFuel of legacyData.LAC.fuels_cooking) {
            expect(
              cookingSAP.fuelDemandByFuelAnnual.get(legacyFuel.fuel)?.energyDemand,
            ).toBeApproximately(legacyFuel.demand);
            fuelsNotCounted.delete(legacyFuel.fuel);
          }
          expect(fuelsNotCounted).toEqual(new Set());
          expect(legacyData.fuel_requirements.cooking.quantity).toBeApproximately(
            cookingSAP.totalFuelRequirement,
          );
        },
      ),
    );
  });

  test('extractor', () => {
    fc.assert(
      fc.property(
        arbCookingSAPInput(),
        arbCookingSAPDependencies(),
        (input, dependencies) => {
          const roundTripped = extractCookingSAPInputFromLegacy(
            scenarioSchema.parse(makeLegacyDataForCookingSAP(input, dependencies)),
          );
          expect(roundTripped).toEqual(input);
        },
      ),
    );
  });

  test('when module is disabled, everything returns 0', () => {
    fc.assert(
      fc.property(
        arbCookingSAPInput({ enabled: false }),
        arbCookingSAPDependencies(),
        (input, dependencies_) => {
          const dependencies = cloneDeep(dependencies_);
          dependencies.fuels.names.push(...Object.keys(input.fuelFractions));
          const cookingSAP = new CookingSAP(input, dependencies);
          expect(cookingSAP.heatGain).toBe(0);
          for (const [, { fuelInput }] of cookingSAP.fuelDemandByFuelAnnual) {
            expect(Math.abs(fuelInput)).toBe(0);
          }
          expect(cookingSAP.totalFuelRequirement).toBe(0);
          expect(cookingSAP.emissionsAnnualPerArea).toBe(0);
        },
      ),
    );
  });
});

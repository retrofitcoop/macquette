import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { Month } from '../../../src/model/enums/month';
import { FuelName } from '../../../src/model/modules/fuels';
import {
  LightingSAP,
  LightingSAPDependencies,
  LightingSAPInput,
  extractLightingSAPInputFromLegacy,
} from '../../../src/model/modules/lighting-appliances-cooking/sap/lighting';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { legacyLightingAppliancesCooking } from '../golden-master/lighting-appliances-cooking';
import { withSuppressedConsoleWarn } from '../with-suppressed-console-warn';

const arbLightingSAPInput: fc.Arbitrary<LightingSAPInput> = fc.record({
  outlets: fc.record({
    total: sensibleFloat,
    lowEnergy: sensibleFloat,
  }),
  reducedHeatGains: fc.boolean(),
  fuelFractions: fc.dictionary(fc.string(), sensibleFloat),
});

function arbLightingSAPDependencies(
  input: LightingSAPInput,
): fc.Arbitrary<LightingSAPDependencies> {
  return fc.record({
    fuels: fc.record({ names: fc.constant(Object.keys(input.fuelFractions)) }),
    floors: fc.record({
      totalFloorArea: sensibleFloat.filter((value) => value >= 0),
    }),
    occupancy: fc.record({
      occupancy: sensibleFloat.filter((value) => value >= 0),
    }),
    fabric: fc.record({ naturalLight: sensibleFloat }),
  });
}

function makeLegacyDataForLightingSAP(
  input: LightingSAPInput,
  dependencies: LightingSAPDependencies,
) {
  const fuelFractionsEntries = Object.entries(input.fuelFractions);
  let fuels_lighting: any[] | undefined;
  if (fuelFractionsEntries.length === 0) {
    fuels_lighting = undefined;
  } else {
    fuels_lighting = fuelFractionsEntries.map(([fuelName, fraction]) => ({
      fuel: fuelName,
      fraction,
    }));
  }
  return {
    occupancy: dependencies.occupancy.occupancy,
    TFA: dependencies.floors.totalFloorArea,
    GL: dependencies.fabric.naturalLight,
    LAC_calculation_type: 'SAP',
    LAC: {
      L: input.outlets.total,
      LLE: input.outlets.lowEnergy,
      reduced_heat_gains_lighting: input.reducedHeatGains,
      fuels_lighting,
    },
    fuel_requirements: {
      cooking: { quantity: 0 },
      lighting: { quantity: 0 },
      appliances: { quantity: 0 },
    },
    gains_W: { Lighting: new Array(12).fill(0) },
    energy_requirements: {
      lighting: { quantity: 0, monthly: new Array(12).fill(0) },
    },
    fuels: {},
  };
}

describe('lighting SAP', () => {
  test('golden master', () => {
    function isLightingInputCompatibleWithLegacy(input: LightingSAPInput) {
      return (
        input.outlets.total > 0 &&
        input.outlets.lowEnergy > 0 &&
        input.outlets.total >= input.outlets.lowEnergy
      );
    }
    const arb = arbLightingSAPInput
      .filter(isLightingInputCompatibleWithLegacy)
      .chain((input) =>
        fc.record({
          input: fc.constant(input),
          dependencies: arbLightingSAPDependencies(input),
        }),
      );
    fc.assert(
      fc.property(arb, ({ input, dependencies: dependencies_ }) => {
        const dependencies = cloneDeep(dependencies_);
        dependencies.fuels.names.push(...Object.keys(input.fuelFractions));
        const lightingSAP = new LightingSAP(input, dependencies);
        const legacyData: any = makeLegacyDataForLightingSAP(input, dependencies_);
        legacyLightingAppliancesCooking(legacyData);
        expect(legacyData.LAC.EB).toBeApproximately(lightingSAP.baselineEnergyAnnual);
        /* eslint-disable jest/no-conditional-expect */
        if (legacyData.LAC.C1 === undefined) {
          expect(lightingSAP.lowEnergyCorrectionFactor).toBeNull();
        } else {
          expect(lightingSAP.lowEnergyCorrectionFactor).not.toBeNull();
          expect(legacyData.LAC.C1).toBeApproximately(
            lightingSAP.lowEnergyCorrectionFactor!,
          );
        }
        if (legacyData.LAC.C2 !== undefined) {
          expect(legacyData.LAC.C2).toBeApproximately(
            lightingSAP.daylightingCorrectionFactor,
          );
        }
        if (legacyData.LAC.EL !== undefined) {
          expect(legacyData.LAC.EL).toBeApproximately(lightingSAP.initialEnergyAnnual);
        }
        /* eslint-enable */
        expect(legacyData.energy_requirements.lighting.quantity).toBeApproximately(
          lightingSAP.energyAnnual,
        );
        for (const month of Month.all) {
          expect(
            legacyData.energy_requirements.lighting.monthly[month.index0],
          ).toBeApproximately(lightingSAP.energyMonthly(month));
          expect(legacyData.gains_W.Lighting[month.index0]).toBeApproximately(
            lightingSAP.heatGain(month),
          );
        }
        const fuelsNotCounted = new Set<FuelName>(Object.keys(input.fuelFractions));
        for (const legacyFuel of legacyData.LAC.fuels_lighting as any[]) {
          expect(
            lightingSAP.fuelDemandByFuelAnnual.get(legacyFuel.fuel)?.energyDemand,
          ).toBeApproximately(legacyFuel.demand);
          fuelsNotCounted.delete(legacyFuel.fuel);
        }
        expect(fuelsNotCounted).toEqual(new Set());
        expect(legacyData.fuel_requirements.lighting.quantity).toBeApproximately(
          lightingSAP.totalFuelRequirement,
        );
      }),
    );
  });

  test('extractor', () => {
    const arb = arbLightingSAPInput.chain((input) =>
      fc.record({
        input: fc.constant(input),
        dependencies: arbLightingSAPDependencies(input),
      }),
    );
    fc.assert(
      fc.property(arb, ({ input, dependencies }) => {
        const roundTripped = extractLightingSAPInputFromLegacy(
          scenarioSchema.parse(makeLegacyDataForLightingSAP(input, dependencies)),
        );
        expect(roundTripped).toEqual(input);
      }),
    );
  });

  test('should never return negative gain given non-negative inputs', () => {
    const arb = arbLightingSAPInput
      .filter((input) => input.outlets.total >= 0 && input.outlets.lowEnergy >= 0)
      .chain((input) =>
        fc.record({
          month: fc.constantFrom(...Month.all),
          input: fc.constant(input),
          dependencies: arbLightingSAPDependencies(input),
        }),
      );
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(arb, ({ month, input, dependencies }) => {
          const lightingSAP = new LightingSAP(input, dependencies);
          expect(lightingSAP.heatGain(month)).toBeGreaterThanOrEqual(0);
        }),
      );
    });
  });
});

import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import { Month } from '../../src/model/enums/month';
import {
  VentilationInfiltrationCommon,
  extractVentilationInfiltrationCommonInputFromLegacy,
} from '../../src/model/modules/ventilation-infiltration/common-input';
import {
  Ventilation,
  extractVentilationInputFromLegacy,
} from '../../src/model/modules/ventilation-infiltration/ventilation';
import { legacyVentilation } from './golden-master/ventilation';
import {
  arbCommonInput,
  arbDependencies,
  arbVentilationInput,
  makeLegacyDataForVentilation,
} from './ventilation-infiltration-arbitraries';

describe('ventilation', () => {
  const arb = fc.record({
    commonInput: arbCommonInput,
    ventilationInput: arbVentilationInput,
    dependencies: arbDependencies,
  });
  const arbLegacy = arb.chain(({ commonInput, ventilationInput, dependencies }) =>
    makeLegacyDataForVentilation(commonInput, ventilationInput, dependencies).map(
      (legacyData) => ({ commonInput, ventilationInput, dependencies, legacyData }),
    ),
  );
  test('golden master (legacy ventilation module)', () => {
    fc.assert(
      fc.property(
        arbLegacy,
        ({ commonInput, ventilationInput, dependencies, legacyData: legacyData_ }) => {
          const common = new VentilationInfiltrationCommon(commonInput, dependencies);
          const ventilation = new Ventilation(ventilationInput, {
            ...dependencies,
            ventilationInfiltrationCommon: common,
          });
          const legacyData: any = cloneDeep(legacyData_);
          legacyVentilation(legacyData);
          expect(ventilation.heatLossAverage).toBeApproximately(
            legacyData.ventilation.average_ventilation_WK,
          );
          for (const month of Month.all) {
            expect(ventilation.heatLossMonthly(month)).toBeApproximately(
              legacyData.losses_WK.ventilation[month.index0],
            );
            expect(ventilation.airChangesPerHour(month)).toBeApproximately(
              legacyData.ventilation.effective_air_change_rate[month.index0] -
                legacyData.ventilation.adjusted_infiltration[month.index0],
            );
          }
        },
      ),
    );
  });

  test('ventilation extractor round trip', () => {
    fc.assert(
      fc.property(arbLegacy, ({ ventilationInput, legacyData }) => {
        const extracted = extractVentilationInputFromLegacy(
          scenarioSchema.parse(legacyData),
        );
        expect(extracted).toStrictEqual(ventilationInput);
      }),
    );
  });

  test('common extractor round trip', () => {
    fc.assert(
      fc.property(arbLegacy, ({ commonInput, legacyData }) => {
        const extracted = extractVentilationInfiltrationCommonInputFromLegacy(
          scenarioSchema.parse(legacyData),
        );
        expect(extracted).toStrictEqual(commonInput);
      }),
    );
  });

  test('should always produce EVP ventilation or system ventilation, but never both', () => {
    fc.assert(
      fc.property(
        arb,
        fc.constantFrom(...Month.all),
        ({ commonInput, ventilationInput, dependencies }, month) => {
          const common = new VentilationInfiltrationCommon(commonInput, dependencies);
          const ventilation = new Ventilation(ventilationInput, {
            ...dependencies,
            ventilationInfiltrationCommon: common,
          });
          expect(
            Math.abs(
              ventilation.adjustedEVPAirChanges(month) * ventilation.systemAirChanges,
            ),
          ).toBe(0);
        },
      ),
    );
  });
});

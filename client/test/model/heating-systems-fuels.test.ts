import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { z } from 'zod';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import { sum } from '../../src/helpers/array-reducers';
import { zip } from '../../src/helpers/zip';
import { Month } from '../../src/model/enums/month';
import { ModelBehaviourFlags } from '../../src/model/modules/behaviour-version';
import {
  HeatingSystemInput,
  HeatingSystems,
  HeatingSystemsDependencies,
  extractHeatingSystemsInputFromLegacy,
} from '../../src/model/modules/heating-systems';
import { HeatingSystemFansAndPumpsParameters } from '../../src/model/modules/heating-systems/fans-and-pumps';
import {
  HeatingSystemsFuelRequirements,
  HeatingSystemsFuelRequirementsDependencies,
} from '../../src/model/modules/heating-systems/fuel-requirements';
import { heatingSystemWaterParameters } from '../../src/model/modules/heating-systems/water';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbMonthly } from '../arbitraries/monthly';
import { ArbParam, fractionsOfUnity, resolveArbParam } from '../helpers/arbitraries';
import { makeArbitrary } from '../helpers/make-arbitrary';
import { legacyHeatingSystemsFuels } from './golden-master/heating-systems-fuels';
import { arbSpaceHeatingSystem } from './heating-systems/arbitraries';

const arbWaterHeatingSystemInput = makeArbitrary(heatingSystemWaterParameters).filter(
  (system) =>
    (system.combiLoss === null || system.primaryCircuitLoss === null) &&
    (system.combiLoss === null || system.distributionLoss === true) &&
    (system.primaryCircuitLoss === null || system.distributionLoss === true),
);

type TestDependencies = Omit<
  HeatingSystemsFuelRequirementsDependencies,
  'heatingSystems' | 'modelBehaviourFlags'
> &
  Omit<HeatingSystemsDependencies, 'modelBehaviourFlags'>;

// Disallow "toString", "__proto__", etc. as fuel names. These cause prototype pollution
// in the legacy fuel requirements calcs.
const disallowedFuelNames = [
  ...Object.getOwnPropertyNames(Object.getPrototypeOf({})),
  ...Object.getOwnPropertyNames(Object.getPrototypeOf([])),
];

function arbDependencies(): fc.Arbitrary<TestDependencies> {
  return fc.record({
    fuels: fc
      .record({
        fuels: fc.dictionary(
          fc.string().filter((s) => !disallowedFuelNames.includes(s)),
          fc.record({
            category: fc.constantFrom(
              'gas' as const,
              'solid fuel' as const,
              'generation' as const,
              'oil' as const,
              'electricity' as const,
            ),
            standingCharge: sensibleFloat,
            unitPrice: sensibleFloat,
            carbonEmissionsFactor: sensibleFloat,
            primaryEnergyFactor: sensibleFloat,
          }),
          { minKeys: 1 },
        ),
      })
      .map((fuels) => ({
        ...fuels,
        names: Object.keys(fuels.fuels),
      })),
    spaceHeating: fc
      .record({
        spaceHeatingDemandEnergy: arbMonthly(sensibleFloat),
      })
      .map(({ spaceHeatingDemandEnergy }) => ({
        spaceHeatingDemandEnergy,
        spaceHeatingDemandEnergyAnnual: sum(
          Month.all.map((m) => spaceHeatingDemandEnergy(m)),
        ),
      })),
    waterHeating: fc
      .record({
        // Technically water heating heat output is dependent on the heating systems'
        // losses, so isn't a free variable, but it doesn't matter for this module and is
        // easier than pulling in the whole WaterHeating module
        heatOutputMonthly: arbMonthly(sensibleFloat),
      })
      .map(({ heatOutputMonthly }) => ({
        heatOutputMonthly,
        heatOutputAnnual: sum(Month.all.map((m) => heatOutputMonthly(m))),
      })),
    waterCommon: fc.record({
      hotWaterEnergyContentByMonth: arbMonthly(sensibleFloat),
      dailyHotWaterUsageLitresByMonth: arbMonthly(sensibleFloat),
      annualEnergyContentOverride: fc.oneof(fc.constant(false as const), sensibleFloat),
      solarHotWater: fc.constantFrom(
        true,
        false,
        'only for water heating' as const,
        'only for fans and pumps energy' as const,
      ),
    }),
    floors: fc.record({
      totalVolume: sensibleFloat,
    }),
  });
}

function arbInput(
  dependencies: TestDependencies,
  maxSystems?: ArbParam<number>,
): fc.Arbitrary<HeatingSystemInput[]> {
  function heatingSystems(maxSystems: number) {
    const fractions = fc
      .tuple(
        fractionsOfUnity({
          minLength: maxSystems,
          maxLength: maxSystems,
          withNulls: true,
        }),
        fractionsOfUnity({
          minLength: maxSystems,
          maxLength: maxSystems,
          withNulls: true,
        }),
      )
      .map(([waterFractions, spaceFractions]) =>
        zip(waterFractions, spaceFractions).map(([waterFraction, spaceFraction]) => ({
          waterFraction,
          spaceFraction,
        })),
      );
    const systems = fractions.chain((fractions) => {
      return fc.tuple(
        ...fractions.map(
          ({ waterFraction, spaceFraction }, index): fc.Arbitrary<HeatingSystemInput> => {
            return fc.record({
              id: fc.constant(index),
              waterHeating:
                waterFraction === null
                  ? fc.constant(null)
                  : arbWaterHeatingSystemInput.map((input) => ({
                      ...input,
                      fractionWaterHeating: waterFraction,
                    })),
              spaceHeating:
                spaceFraction === null
                  ? fc.constant(null)
                  : arbSpaceHeatingSystem({
                      fraction: spaceFraction,
                    }),
              fansAndPumps: fc.constant<HeatingSystemFansAndPumpsParameters>({
                type: 'other',
                annualEnergyFansPumps: 0,
                pumpPowerKWhPerYear: 0,
                combiLoss: null,
              }),
              fuel: fc.record({
                name: fc.constantFrom(...Object.keys(dependencies.fuels.fuels)),
                summerEfficiency: sensibleFloat.filter((n) => n !== 0),
                winterEfficiency: sensibleFloat.filter((n) => n !== 0),
              }),
            });
          },
        ),
      );
    });
    return systems.map((systems) =>
      systems.filter(
        (system) => system.spaceHeating !== null || system.waterHeating !== null,
      ),
    );
  }
  return resolveArbParam(maxSystems, fc.nat({ max: 8 })).chain((maxSystems) =>
    heatingSystems(maxSystems),
  );
}

function makeLegacyDataForHeatingSystemsFuelRequirements(
  input: HeatingSystemInput[],
  dependencies: TestDependencies,
): fc.Arbitrary<unknown> {
  return fc.constant({
    heating_systems: input.map((system) => {
      let provides: string;
      if (system.waterHeating !== null && system.spaceHeating !== null) {
        provides = 'heating_and_water';
      } else if (system.waterHeating !== null) {
        provides = 'water';
      } else if (system.spaceHeating !== null) {
        provides = 'heating';
      } else {
        throw new Error('waterHeating and spaceHeating were both null');
      }
      return {
        id: system.id,
        provides,
        fuel: system.fuel.name,
        responsiveness: system.spaceHeating?.responsiveness,
        fraction_space: system.spaceHeating?.fraction,
        fraction_water_heating: system.waterHeating?.fractionWaterHeating,
        summer_efficiency: system.fuel.summerEfficiency * 100,
        winter_efficiency: system.fuel.winterEfficiency * 100,
      };
    }),
    fuel_requirements: {
      waterheating: {
        list: [],
      },
      space_heating: {
        list: [],
      },
    },
    energy_requirements: {
      waterheating: { quantity: dependencies.waterHeating.heatOutputAnnual },
      space_heating: {
        quantity: dependencies.spaceHeating.spaceHeatingDemandEnergyAnnual,
      },
    },
  });
}

describe('heating systems fuel requirements', () => {
  const goldenMasterArb = arbDependencies()
    .chain((dependencies) =>
      fc.record({
        dependencies: fc.constant(dependencies),
        input: arbInput(dependencies),
      }),
    )
    .chain(({ input, dependencies }) => {
      return fc.record({
        input: fc.constant(input),
        dependencies: fc.constant(dependencies),
        legacyData: makeLegacyDataForHeatingSystemsFuelRequirements(input, dependencies),
      });
    });

  test('input round trips extractor', () => {
    fc.assert(
      fc.property(goldenMasterArb, ({ input, legacyData }) => {
        const roundTripped = extractHeatingSystemsInputFromLegacy(
          scenarioSchema.parse(legacyData),
        );
        function normalise(systems: HeatingSystemInput[]) {
          // Overwrite values we are not interested in with default values
          for (const system of systems) {
            if (system.waterHeating !== null) {
              system.waterHeating.combiLoss = null;
              system.waterHeating.distributionLoss = false;
              system.waterHeating.primaryCircuitLoss = null;
            }
            if (system.spaceHeating !== null) {
              system.spaceHeating.temperatureAdjustment = 0;
              system.spaceHeating.type = 'main';
              system.spaceHeating.controlType = null;
            }
          }
        }
        normalise(input);
        normalise(roundTripped);
        expect(roundTripped).toEqual(input);
      }),
    );
  });

  test('system outputs are finite', () => {
    const arb = arbDependencies().chain((dependencies) =>
      fc.record({
        input: arbInput(dependencies),
        dependencies: fc.constant(dependencies),
      }),
    );
    fc.assert(
      fc.property(arb, ({ input, dependencies }) => {
        const heatingSystems = new HeatingSystems(input, {
          ...dependencies,
          modelBehaviourFlags: allFlagsTrue,
        });
        const heatingSystemsFuelRequirements = new HeatingSystemsFuelRequirements(null, {
          ...dependencies,
          heatingSystems,
          modelBehaviourFlags: allFlagsTrue,
        });
        for (const system of heatingSystemsFuelRequirements.systems) {
          expect(Number.isFinite(system.spaceHeatingEnergyDemandAnnual)).toBe(true);
          expect(Number.isFinite(system.waterHeatingEnergyDemandAnnual)).toBe(true);
          expect(Number.isFinite(system.spaceHeatingEfficiency)).toBe(true);
          expect(Number.isFinite(system.waterHeatingEfficiency)).toBe(true);
          expect(Number.isFinite(system.spaceHeatingFuelInputAnnual)).toBe(true);
          expect(Number.isFinite(system.waterHeatingFuelInputAnnual)).toBe(true);
        }
      }),
    );
  });

  test('heating system water efficiency uses summer efficiency if higher than winter efficiency', () => {
    const arb = arbDependencies().chain((dependencies) =>
      fc.record({
        input: arbInput(dependencies, 1).filter(
          (systems) =>
            systems.length === 1 &&
            systems[0]!.waterHeating !== null &&
            systems[0]!.fuel.summerEfficiency > systems[0]!.fuel.winterEfficiency,
        ),
        dependencies: fc.constant(dependencies),
      }),
    );
    fc.assert(
      fc.property(arb, ({ input, dependencies }) => {
        const heatingSystems = new HeatingSystems(input, {
          ...dependencies,
          modelBehaviourFlags: allFlagsTrue,
        });
        const heatingSystemsFuelRequirements = new HeatingSystemsFuelRequirements(null, {
          ...dependencies,
          heatingSystems,
          modelBehaviourFlags: allFlagsTrue,
        });
        expect(
          heatingSystemsFuelRequirements.systems[0]!.waterHeatingEfficiency,
        ).toBeApproximately(input[0]!.fuel.summerEfficiency);
      }),
    );
  });

  describe('golden masters', () => {
    function instantiateModules(
      input: HeatingSystemInput[],
      dependencies: TestDependencies,
    ) {
      const modelBehaviourFlags = {
        heatingSystems: {
          fansAndPumps: {
            fixUndefinedSpecificFanPowerInWarmAirSystems: true,
            warmAirSystemsZeroGainInSummer: true,
            fixHeatGainsForWarmAirSystems: true,
            allowFansPumpsEnergyToUseSpecificFanPower: true,
          },
          waterHeating: {
            fixLossesForStorageCombiOfUnknownSize: true,
          },
        } satisfies ModelBehaviourFlags['heatingSystems'],
        fuelRequirements: {
          heatingSystems: {
            fixSwappedSeasonalEfficiencies: false,
            useSummerEfficiencyIfHigherThanWinter: false,
          },
        } satisfies Pick<ModelBehaviourFlags['fuelRequirements'], 'heatingSystems'>,
      };
      const heatingSystems = new HeatingSystems(input, {
        ...dependencies,
        modelBehaviourFlags,
      });
      const heatingSystemsFuels = new HeatingSystemsFuelRequirements(null, {
        ...dependencies,
        heatingSystems,
        modelBehaviourFlags,
      });
      return { heatingSystems, heatingSystemsFuels };
    }
    function runGoldenMaster(legacyData: any) {
      const legacyDataPostRun: any = cloneDeep(legacyData);
      legacyHeatingSystemsFuels(legacyDataPostRun);
      return legacyDataPostRun;
    }

    test('system efficiencies', () => {
      fc.assert(
        fc.property(goldenMasterArb, ({ input, dependencies, legacyData }) => {
          const { heatingSystemsFuels } = instantiateModules(input, dependencies);
          const legacyDataPostRun = runGoldenMaster(legacyData);
          const legacyHeatingSystems = legacyDataPostRun.heating_systems;
          expect(heatingSystemsFuels.systems).toHaveLength(
            legacyDataPostRun.heating_systems.length,
          );
          for (const [heatingSystem, legacyHeatingSystem] of zip(
            heatingSystemsFuels.systems,
            legacyHeatingSystems as any[],
          )) {
            expect(heatingSystem.waterHeatingEfficiency).toBeApproximately(
              legacyHeatingSystem.efficiency ?? 0,
            );
            expect(heatingSystem.spaceHeatingEfficiency).toBeApproximately(
              heatingSystem.baseSystem.spaceHeating === null
                ? 0
                : legacyHeatingSystem.winter_efficiency / 100,
            );
          }
        }),
      );
    });

    test('water heating fuel use', () => {
      fc.assert(
        fc.property(goldenMasterArb, ({ input, dependencies, legacyData }) => {
          const { heatingSystemsFuels } = instantiateModules(input, dependencies);
          const legacyDataPostRun = runGoldenMaster(legacyData);
          const legacyFuelList = legacyDataPostRun.fuel_requirements.waterheating.list;
          expect(heatingSystemsFuels.waterHeatingFuelDemandByFuelAnnual.size).toEqual(
            legacyFuelList.length,
          );
          for (const legacyFuelDemandParameters of legacyFuelList as any[]) {
            const fuelDemandParameters =
              heatingSystemsFuels.waterHeatingFuelDemandByFuelAnnual.get(
                legacyFuelDemandParameters.fuel,
              );
            expect(fuelDemandParameters).toBeDefined();
            expect(fuelDemandParameters!.energyDemand).toBeApproximately(
              legacyFuelDemandParameters.demand,
            );
            expect(fuelDemandParameters!.fuelInput).toBeApproximately(
              legacyFuelDemandParameters.fuel_input,
            );
          }
        }),
      );
    });

    test('space heating fuel use', () => {
      fc.assert(
        fc.property(goldenMasterArb, ({ input, dependencies, legacyData }) => {
          const { heatingSystemsFuels } = instantiateModules(input, dependencies);
          const legacyDataPostRun = runGoldenMaster(legacyData);
          const legacyFuelList = legacyDataPostRun.fuel_requirements.space_heating.list;
          expect(heatingSystemsFuels.spaceHeatingFuelDemandByFuelAnnual.size).toEqual(
            legacyFuelList.length,
          );
          for (const legacyFuelDemandParameters of legacyFuelList as any[]) {
            const fuelDemandParameters =
              heatingSystemsFuels.spaceHeatingFuelDemandByFuelAnnual.get(
                legacyFuelDemandParameters.fuel,
              );
            expect(fuelDemandParameters).toBeDefined();
            expect(fuelDemandParameters!.energyDemand).toBeApproximately(
              legacyFuelDemandParameters.demand,
            );
            expect(fuelDemandParameters!.fuelInput).toBeApproximately(
              legacyFuelDemandParameters.fuel_input,
            );
          }
        }),
      );
    });
  });

  const allFlagsTrue = {
    heatingSystems: {
      fansAndPumps: {
        fixUndefinedSpecificFanPowerInWarmAirSystems: true,
        warmAirSystemsZeroGainInSummer: true,
        fixHeatGainsForWarmAirSystems: true,
        allowFansPumpsEnergyToUseSpecificFanPower: true,
      },
      waterHeating: {
        fixLossesForStorageCombiOfUnknownSize: true,
      },
    },
    fuelRequirements: {
      heatingSystems: {
        fixSwappedSeasonalEfficiencies: true,
        useSummerEfficiencyIfHigherThanWinter: true,
      },
    },
  } satisfies HeatingSystemsDependencies['modelBehaviourFlags'] &
    HeatingSystemsFuelRequirementsDependencies['modelBehaviourFlags'];

  test('mutator', () => {
    const schema = z.object({
      heating_systems: z.array(
        z.object({
          efficiency: z.number(),
        }),
      ),
      fuel_requirements: z.object({
        waterheating: z.object({
          quantity: z.number(),
          list: z.array(
            z.object({
              fuel: z.string(),
              demand: z.number(),
              fraction: z.number(),
              fuel_input: z.number(),
            }),
          ),
        }),
        space_heating: z.object({
          quantity: z.number(),
          list: z.array(
            z.object({
              fuel: z.string(),
              demand: z.number(),
              fraction: z.number(),
              fuel_input: z.number(),
            }),
          ),
        }),
      }),
    });
    const arb = arbDependencies().chain((dependencies) =>
      fc.record({
        input: arbInput(dependencies),
        dependencies: fc.constant(dependencies),
      }),
    );
    fc.assert(
      fc.property(arb, ({ input, dependencies }) => {
        const heatingSystems = new HeatingSystems(input, {
          ...dependencies,
          modelBehaviourFlags: allFlagsTrue,
        });
        const heatingSystemsFuels = new HeatingSystemsFuelRequirements(null, {
          ...dependencies,
          heatingSystems,
          modelBehaviourFlags: allFlagsTrue,
        });
        const legacyData = {
          heating_systems: input.map(({ id }) => ({ id })),
        };
        heatingSystemsFuels.mutateLegacyData(legacyData);
        expect(() => schema.parse(legacyData)).not.toThrow();
      }),
    );
  });
});

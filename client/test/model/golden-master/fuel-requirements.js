/* eslint-disable */
/* eslint-enable no-debugger */
let x, z;

/*---------------------------------------------------------------------------------------------
 // FUEL REQUIREMENTS
 // Calculates the totals for each type of fuel (Mains Gas, Standard Tariff, etc) from
 // the fuel requirements (appliances, cooking, space_heating, etc)
 //
 // Inputs from user:
 //      -  data.use_generation
 //
 // Inputs from other modules:
 //      - data.fuel_requirements
 //	- data.fuels
 //	- data.generation
 //
 // Global Outputs:
 //	- data.fuel_totals
 //	- data.energy_use
 //	- data.annualco2
 //	- data.energy_delivered
 //	- data.total_cost
 //	- data.primary_energy_use
 //	- data.net_cost
 //
 //---------------------------------------------------------------------------------------------*/
export function legacyFuelRequirements(data) {
    // Fuel totals
    data.fuel_totals = {}; // remove this line when we get rif of energy_systems
    for (z in data.fuel_requirements) {
        for (x in data.fuel_requirements[z].list) {
            data.fuel_requirements[z].list[x].fuel_input_monthly = [
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            ];

            let fuel = data.fuel_requirements[z].list[x].fuel;
            if (data.fuel_totals[fuel] == undefined) {
                data.fuel_totals[fuel] = { name: fuel, quantity: 0 };
            }
            if (isNaN(data.fuel_requirements[z].list[x].fuel_input) != true) {
                data.fuel_totals[fuel].quantity +=
                    data.fuel_requirements[z].list[x].fuel_input;
            }
        }
    }

    // Total energy use, fuel costs, Annual CO2 and primary energy due to the energy requirements
    data.energy_use = 0;
    data.annualco2 = 0;
    data.energy_delivered = 0;
    for (z in data.fuel_totals) {
        data.fuel_totals[z].annualcost =
            (data.fuel_totals[z].quantity * data.fuels[z].fuelcost) / 100 +
            data.fuels[z].standingcharge;
        //data.fuel_totals[z].fuelcost = data.fuels[z].fuelcost;
        data.fuel_totals[z].primaryenergy =
            data.fuel_totals[z].quantity * data.fuels[z].primaryenergyfactor;
        data.fuel_totals[z].annualco2 =
            data.fuel_totals[z].quantity * data.fuels[z].co2factor;
        data.total_cost += data.fuel_totals[z].annualcost;
        data.energy_use += data.fuel_totals[z].quantity;
        data.primary_energy_use += data.fuel_totals[z].primaryenergy;
        data.annualco2 += data.fuel_totals[z].annualco2;
    }
    data.energy_delivered = data.energy_use;
    // Annual CO2, primary energy and cost saved due to generation. Be aware generation is not used for the calculation of Energy use
    if (data.use_generation == 1) {
        data.fuel_totals['generation'] = {
            name: 'generation',
            quantity: -data.generation.total_generation,
            annualco2: -data.generation.total_CO2,
            primaryenergy: -data.generation.total_primaryenergy,
            annualcost:
                (-data.generation.total_used_onsite * data.fuels.generation.fuelcost) /
                100,
        };
        data.primary_energy_use += data.fuel_totals['generation'].primaryenergy;
        data.annualco2 += data.fuel_totals['generation'].annualco2;
        data.total_cost += data.fuel_totals['generation'].annualcost;
        data.energy_delivered += data.fuel_totals['generation'].quantity;
    }

    data.net_cost =
        data.use_generation == 1 ? data.total_cost - data.total_income : data.total_cost;
    return data;
}

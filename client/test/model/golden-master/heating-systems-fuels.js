/* eslint-disable */
/* eslint-enable no-debugger */

let fuel;

/*---------------------------------------------------------------------------------------------
 // HEATING SYSTEMS
 //
 // Calculates fuel requirements for water heating
 //  and space heating
 //
 // Inputs from user:
 //      - data.heating_systems
 //
 // Inputs from other modules:
 //      - data.energy_requirements.waterheating
 //	- data.energy_requirements.space_heating
 //
 // Global Outputs:
 //	- data.fuel_requirements.waterheating
 //	- data.fuel_requirements.space_heating
 //
 //---------------------------------------------------------------------------------------------*/

export function legacyHeatingSystemsFuels(data) {
    if (data.heating_systems == undefined) {
        data.heating_systems = [];
    }

    //////////////////////////////////////
    // Fuel requirements  Water Heating //
    //////////////////////////////////////
    var f_requirements = [];
    data.heating_systems.forEach(function (system) {
        if (system.provides == 'water' || system.provides == 'heating_and_water') {
            // Calculate system efficiency
            switch (system.provides) {
                case 'water':
                    system.efficiency = system.summer_efficiency / 100;
                    break;
                case 'heating_and_water':
                    var Q_water =
                        system.fraction_water_heating *
                        data.energy_requirements.waterheating.quantity;
                    if (data.energy_requirements.space_heating == undefined) {
                        data.energy_requirements.space_heating = { quantity: 0 };
                    }
                    var Q_space =
                        system.fraction_space *
                        data.energy_requirements.space_heating.quantity;
                    var n_winter = system.summer_efficiency / 100;
                    var n_summer = system.winter_efficiency / 100;
                    system.efficiency =
                        (Q_water + Q_space) / (Q_space / n_winter + Q_water / n_summer);
                    break;
            }

            // Sort them by 'fuel'
            if (f_requirements[system.fuel] == undefined) {
                f_requirements[system.fuel] = {
                    demand: 0,
                    fraction: 0,
                    fuel: system.fuel,
                    fuel_input: 0,
                };
            }
            let demand =
                system.fraction_water_heating *
                data.energy_requirements.waterheating.quantity;
            f_requirements[system.fuel].demand += demand;
            f_requirements[system.fuel].fuel_input += demand / system.efficiency;
            f_requirements[system.fuel].fraction += system.fraction_water_heating;
        }
    });
    // Copy over to data.fuel_requirements and calculate total fuel input
    data.fuel_requirements.waterheating.quantity = 0;
    for (fuel in f_requirements) {
        data.fuel_requirements.waterheating.list.push(f_requirements[fuel]);
        data.fuel_requirements.waterheating.quantity += f_requirements[fuel].fuel_input;
    }

    //////////////////////////////////////
    // Fuel requirements  Space Heating //
    //////////////////////////////////////
    var f_requirements = [];
    data.heating_systems.forEach(function (system) {
        if (system.provides == 'heating' || system.provides == 'heating_and_water') {
            // Sort them by 'fuel'
            if (f_requirements[system.fuel] == undefined) {
                f_requirements[system.fuel] = {
                    demand: 0,
                    fraction: 0,
                    fuel: system.fuel,
                    fuel_input: 0,
                };
            }
            let demand;
            if (data.energy_requirements.space_heating === undefined) {
                demand = 0;
            } else {
                demand =
                    system.fraction_space *
                    data.energy_requirements.space_heating.quantity;
            }
            f_requirements[system.fuel].demand += demand;
            f_requirements[system.fuel].fuel_input +=
                demand / (system.winter_efficiency / 100);
            f_requirements[system.fuel].fraction += system.fraction_space;
        }
    });
    // Copy over to data.fuel_requirements and calculate total fuel input
    data.fuel_requirements.space_heating.quantity = 0;
    for (fuel in f_requirements) {
        data.fuel_requirements.space_heating.list.push(f_requirements[fuel]);
        data.fuel_requirements.space_heating.quantity += f_requirements[fuel].fuel_input;
    }
}

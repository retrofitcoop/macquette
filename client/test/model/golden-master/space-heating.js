/* eslint-disable */
/* eslint-enable no-debugger */

import { datasets } from '../reference-datasets';
let m, z;
function isNaN() {
    // Override builtin `isNaN` so that NaNs propagate rather than being coalesced.
    // This is so that new code can handle them differently.
    return false;
}

/*---------------------------------------------------------------------------------------------
 // SPACE HEATING AND COOLING
 // Calculates space heating and cooling demand.
 //
 // Inputs from user:
 //      - data.space_heating.use_utilfactor_forgains
 //	- data.space_heating.heating_off_summer
 //
 // Inputs from other modules:
 //      - data.internal_temperature
 //	- data.external_temperature
 //	- data.losses_WK
 //	- data.gains_W
 //	- data.TFA
 //	- data.TMP
 //
 // Global Outputs:
 //	- data.annual_useful_gains_kWh_m2
 //	- data.annual_losses_kWh_m2
 //	- data.space_heating_demand_m2
 //	- data.energy_requirements.space_heating
 //	- data.energy_requirements.space_cooling
 //
 // Module Variables:
 //	- data.space_heating.delta_T
 //	- data.space_heating.total_losses
 //	- data.space_heating.total_gains
 //	- data.space_heating.utilisation_factor
 //	- data.space_heating.useful_gains
 //	- data.space_heating.heat_demand
 //	- data.space_heating.cooling_demand
 //	- data.space_heating.heat_demand_kwh
 //	- data.space_heating.cooling_demand_kwh
 //	- data.space_heating.annual_heating_demand
 //	- data.space_heating.annual_cooling_demand
 //	- data.space_heating.annual_heating_demand_m2
 //	- data.space_heating.annual_cooling_demand_m2
 //
 // Datasets:
 //      - datasets.table_1a
 //
 // Uses external function:
 //      - calc_utilisation_factor
 //
 //------------------------------------------------------------------------------------------*/

export function legacySpaceHeating(data) {
    if (data.space_heating == undefined) {
        data.space_heating = {};
    }
    if (data.space_heating.use_utilfactor_forgains == undefined) {
        data.space_heating.use_utilfactor_forgains = true;
    }
    if (data.space_heating.heating_off_summer == undefined) {
        data.space_heating.heating_off_summer = true;
    }
    // These might all need to be defined within the space_heating namespace to be accessible in the ui.
    let delta_T = [];
    let total_losses = [];
    let total_gains = [];
    let utilisation_factor = [];
    let useful_gains = [];
    let annual_useful_gains_kWh_m2 = { Internal: 0, Solar: 0 }; //  Units: kwh/m2/year
    let annual_losses_kWh_m2 = {};
    let heat_demand = [];
    let cooling_demand = [];
    let heat_demand_kwh = [];
    let cooling_demand_kwh = [];
    let annual_heating_demand = 0;
    let annual_cooling_demand = 0;

    for (m = 0; m < 12; m++) {
        // DeltaT (Difference between Internal and External temperature)
        delta_T[m] = data.internal_temperature[m] - data.external_temperature[m];
        // Monthly heat loss totals
        let H = 0; // heat transfer coefficient
        for (z in data.losses_WK) {
            H += data.losses_WK[z][m];
        }
        total_losses[m] = H * delta_T[m];
        // SAP2012, p.220
        if (data.space_heating.heating_off_summer == 1 && m >= 5 && m <= 8) {
            total_losses[m] = 0;
        }
        // Monthly heat gains total
        let G = 0;
        for (z in data.gains_W) {
            G += data.gains_W[z][m];
        }
        total_gains[m] = G;
        // SAP2012, p.220
        if (data.space_heating.heating_off_summer == 1 && m >= 5 && m <= 8) {
            total_gains[m] = 0;
        }
        // Calculate overall utilisation factor for gains
        let HLP = H / data.TFA;
        utilisation_factor[m] = calc_utilisation_factor(
            data.TMP,
            HLP,
            H,
            data.internal_temperature[m],
            data.external_temperature[m],
            total_gains[m],
        );
        // Apply utilisation factor if chosen:
        if (data.space_heating.use_utilfactor_forgains) {
            useful_gains[m] = total_gains[m] * utilisation_factor[m];
        } else {
            useful_gains[m] = total_gains[m];
        }

        //      Space heating demand is simply the difference between the heat loss rate
        //      for our target internal temperature and the gains.
        heat_demand[m] = total_losses[m] - useful_gains[m];
        cooling_demand[m] = 0;
        // Case of cooling:
        if (heat_demand[m] < 0) {
            cooling_demand[m] = useful_gains[m] - total_losses[m];
            heat_demand[m] = 0;
        }

        // SAP2012, p.220
        if (data.space_heating.heating_off_summer == 1 && m >= 5 && m <= 8) {
            heat_demand[m] = 0;
        }
        heat_demand_kwh[m] = 0.024 * heat_demand[m] * datasets.table_1a[m];
        cooling_demand_kwh[m] = 0.024 * cooling_demand[m] * datasets.table_1a[m];
        annual_heating_demand += heat_demand_kwh[m];
        annual_cooling_demand += cooling_demand_kwh[m];

        ///////////////////////////////////////////////////////
        //Annual useful gains and losses. Units: kwh/m2/year //
        ///////////////////////////////////////////////////////
        if (data.space_heating.heating_off_summer == 0 || m < 5 || m > 8) {
            let gains_source = '';
            for (z in data.gains_W) {
                if (
                    z === 'Appliances' ||
                    z === 'Lighting' ||
                    z === 'Cooking' ||
                    z === 'waterheating' ||
                    z === 'fans_and_pumps' ||
                    z === 'metabolic' ||
                    z === 'losses'
                ) {
                    gains_source = 'Internal';
                }
                if (z === 'solar') {
                    gains_source = 'Solar';
                }
                // Apply utilisation factor if chosen:
                if (data.space_heating.use_utilfactor_forgains) {
                    annual_useful_gains_kWh_m2[gains_source] +=
                        (utilisation_factor[m] *
                            data.gains_W[z][m] *
                            0.024 *
                            datasets.table_1a[m]) /
                        data.TFA;
                } else {
                    // use_utilfactor_forgains is 1 in all assessments
                    throw new Error('unexpected model path');
                    annual_useful_gains_kWh_m2[gains_source] +=
                        (data.gains_W[z][m] * 0.024) / data.TFA;
                }
            }
            // Annual losses. Units: kwh/m2/year
            for (z in data.losses_WK) {
                if (annual_losses_kWh_m2[z] == undefined) {
                    annual_losses_kWh_m2[z] = 0;
                }
                annual_losses_kWh_m2[z] +=
                    (data.losses_WK[z][m] * 0.024 * delta_T[m] * datasets.table_1a[m]) /
                    data.TFA;
            }
        }
    }

    data.space_heating.delta_T = delta_T;
    data.space_heating.total_losses = total_losses;
    data.space_heating.total_gains = total_gains;
    data.space_heating.utilisation_factor = utilisation_factor;
    data.space_heating.useful_gains = useful_gains;
    data.annual_useful_gains_kWh_m2 = annual_useful_gains_kWh_m2;
    data.annual_losses_kWh_m2 = annual_losses_kWh_m2;
    data.space_heating.heat_demand = heat_demand;
    data.space_heating.cooling_demand = cooling_demand;
    data.space_heating.heat_demand_kwh = heat_demand_kwh;
    data.space_heating.cooling_demand_kwh = cooling_demand_kwh;
    data.space_heating.annual_heating_demand = annual_heating_demand;
    data.space_heating.annual_cooling_demand = annual_cooling_demand;
    data.space_heating.annual_heating_demand_m2 = annual_heating_demand / data.TFA;
    data.space_heating.annual_cooling_demand_m2 = annual_cooling_demand / data.TFA;
    if (annual_heating_demand > 0) {
        data.energy_requirements.space_heating = {
            name: 'Space Heating',
            quantity: annual_heating_demand,
            monthly: heat_demand_kwh,
        };
    }
    data.space_heating_demand_m2 =
        (annual_heating_demand + annual_cooling_demand) / data.TFA;
    return data;
}

function calc_utilisation_factor(TMP, HLP, H, Ti, Te, G) {
    /*
     Symbols and units
     H = heat transfer coefficient, (39)m (W/K)
     G = total gains, (84)m (W)
     Ti = internal temperature (°C)
     Te = external temperature, (96)m (°C)
     TMP = Thermal Mass Parameter, (35), (kJ/m2K) (= Cm for building / total floor area)
     HLP = Heat Loss Parameter, (40)m (W/m2K)
     τ = time constant (h)
     η = utilisation factor
     L = heat loss rate (W)
     */

    // Calculation of utilisation factor

    // TMP = thermal Mass / Total floor area
    // HLP = heat transfer coefficient (H) / Total floor area

    let tau = TMP / (3.6 * HLP);
    let a = 1.0 + tau / 15.0;
    // calc losses
    let L = H * (Ti - Te);
    // ratio of gains to losses
    let y = G / L;
    // Note: to avoid instability when γ is close to 1 round γ to 8 decimal places
    // y = y.toFixed(8);
    y = Math.round(y * 100000000.0) / 100000000.0;
    let n = 0.0;
    if (y > 0.0 && y != 1.0) {
        n = (1.0 - Math.pow(y, a)) / (1.0 - Math.pow(y, a + 1.0));
    }
    if (y == 1.0) {
        n = a / (a + 1.0);
    }
    if (y <= 0.0) {
        n = 1.0;
    }
    if (isNaN(n)) {
        n = 0;
    }
    return n;
}

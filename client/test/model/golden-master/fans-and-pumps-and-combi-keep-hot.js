/* eslint-disable */
/* eslint-enable no-debugger */
/* eslint-enable no-warning-comments */

let z, m;

/*---------------------------------------------------------------------------------------------
 // fans_and_pumps_and_combi_keep_hot
 // Calculates Annual energy requirements for pumps, fans and electric keep-hot
 //
 // Inputs from other modules:
 //      - data.heating_systems
 //	- data.ventilation.ventilation_type
 //	- data.ventilation.EVP
 //	- data.use_SHW
 //	- data.SHW.pump
 //
 // Global Outputs:
 //	- data.fans_and_pumps
 //      - data.energy_requirements.fans_and_pumps
 //	- data.fuel_requirements.fans_and_pumps
 //
 //---------------------------------------------------------------------------------------------*/

export function legacyFansAndPumpsAndCombiKeepHot(data) {
    // 1.- Annual energy requirements for pumps, fans and electric keep-hot
    let annual_energy = 0;
    let monthly_energy = [];
    // From heating systems (Central heating pump, fans and supply pumps, keep hot facility
    data.heating_systems.forEach(function (system) {
        annual_energy += 1.0 * system.central_heating_pump;
        if (system.category != 'Warm air system') {
            annual_energy += 1.0 * system.fans_and_supply_pumps;
        } else {
            annual_energy += 0.4 * system.sfp * data.volume;
        }
        switch (system.combi_loss) {
            case 'Instantaneous, with keep-hot facility controlled by time clock':
                annual_energy += 600;
                break;
            case 'Instantaneous, with keep-hot facility not controlled by time clock':
                annual_energy += 900;
                break;
        }
    });
    // From Ventilation (SAP2012 document page 213)
    let ventilation_type = '';
    switch (data.ventilation.ventilation_type) {
        case 'NV':
        case 'IE':
        case 'PS':
            ventilation_type = 'd'; // Natural ventilation or whole house positive input ventilation from loft or passive stack'
            break;
        case 'DEV':
        case 'MEV':
            ventilation_type = 'c'; // Whole house extract ventilation or positive input ventilation from outside
            break;
        case 'MV':
            ventilation_type = 'b'; // Balanced mechanical ventilation without heat recovery (MV)
            break;
        case 'MVHR':
            ventilation_type = 'a'; //Balanced mechanical ventilation with heat recovery (MVHR)
            break;
        default:
            data.ventilation.ventilation_type = 'NV';
            ventilation_type = 'd';
            break;
    }
    switch (ventilation_type) {
        case 'd': // Natural ventilation or whole house positive input ventilation from loft or passive stack'
            // According to SAP we should do nothing - In this case annual energy is 0, see SAP2012 2.6.1: The energy used by the fan is taken as counterbalancing the effect of using slightly warmer air from the loft space compared with outside
            // But we think this is not accurate, so we add 28kWh/year per Extract Ventilation Point (BREDEM)
            for (z in data.ventilation.EVP) {
                let v_rate = 1.0 * data.ventilation.EVP[z].ventilation_rate;
                if (v_rate > 0) {
                    annual_energy += 28;
                }
            }
            break;
        case 'c': //Positive input ventilation (from outside) or mechanical extract ventilation
            annual_energy +=
                2.5 * data.ventilation.system_specific_fan_power * 1.22 * data.volume; // annual_energy += IUF * SFP * 1.22 * V;
            break;
        case 'a': //Balanced mechanical ventilation with heat recovery (MVHR)
            annual_energy +=
                2.5 *
                data.ventilation.system_specific_fan_power *
                2.44 *
                data.ventilation.system_air_change_rate *
                data.volume; //annual_energy += IUF * SFP * 2.44 * nmech * V;
            break;
        case 'b': //Balanced mechanical ventilation without heat recovery (MV)
            annual_energy +=
                2.5 *
                data.ventilation.system_specific_fan_power *
                2.44 *
                data.ventilation.system_air_change_rate *
                data.volume; //annual_energy += IUF * SFP * 2.44 * nmech * V;
            break;
    }

    // From Solar Hot Water
    if (data.use_SHW == 1) {
        if (data.SHW.pump != undefined && data.SHW.pump == 'electric') {
            annual_energy += 50;
        }
    }

    // Energy and fuel requirements
    for (m = 0; m < 12; m++) {
        monthly_energy[m] = annual_energy / 12;
    }
    if (annual_energy > 0) {
        data.energy_requirements.fans_and_pumps = {
            name: 'Fans and pumps',
            quantity: annual_energy,
            monthly: monthly_energy,
        };

        if (data.fans_and_pumps == undefined) {
            data.fans_and_pumps = [{ fuel: 'Standard Tariff', fraction: 1 }];
        }

        data.fuel_requirements.fans_and_pumps.quantity = 0;

        data.fans_and_pumps.forEach(function (fuel_requirement, index) {
            fuel_requirement.demand = annual_energy * fuel_requirement.fraction;
            fuel_requirement.fuel_input = annual_energy * fuel_requirement.fraction; // We assume efficiency of Electrical system is 1
            data.fuel_requirements.fans_and_pumps.quantity += fuel_requirement.fuel_input;
            data.fuel_requirements.fans_and_pumps.list.push(fuel_requirement);
        });
    }
}

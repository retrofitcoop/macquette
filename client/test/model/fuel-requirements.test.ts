import fc from 'fast-check';
import { cloneDeep, mapValues } from 'lodash';
import { z } from 'zod';
import {
  FuelDemandByFuel,
  FuelDemandParameters,
  FuelRequirements,
  FuelRequirementsDependencies,
} from '../../src/model/modules/fuel-requirements';
import { Fuel, FuelName, Fuels, FuelsDict } from '../../src/model/modules/fuels';
import { Mode } from '../../src/model/modules/mode';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbModelMode } from '../arbitraries/model-mode';
import { ArbParam, resolveArbParam } from '../helpers/arbitraries';
import { legacyFuelRequirements } from './golden-master/fuel-requirements';

// Disallow "toString", "__proto__", etc. as fuel names. These cause prototype pollution
// in the legacy fuel requirements calcs.
const disallowedFuelNames = [
  ...Object.getOwnPropertyNames(Object.getPrototypeOf({})),
  'length',
];

function arbFuelDemandByFuel(allFuelNames: FuelName[]): fc.Arbitrary<FuelDemandByFuel> {
  return fc
    .shuffledSubarray(allFuelNames.filter((name) => name !== 'generation')) // Putting "generation" in a fuel requirement causes problems in legacy
    .chain((fuelNames) =>
      fc.tuple(
        ...fuelNames.map((fuelName) =>
          fc.tuple(
            fc.constant(fuelName),
            fc.record<FuelDemandParameters>({
              energyDemand: sensibleFloat,
              fuelInput: sensibleFloat.filter((i) => i >= 0),
              relativeFraction: sensibleFloat,
            }),
          ),
        ),
      ),
    )
    .map((keysValuesArray) => new Map(keysValuesArray));
}

function arbDependencies(opts?: {
  zeroQuantityFuelsDoNotIncurStandingCharge?: ArbParam<boolean>;
  propagateInfiniteFuelInputInRegulatedMode?: ArbParam<boolean>;
  mode?: ArbParam<Mode>;
}): fc.Arbitrary<FuelRequirementsDependencies> {
  const fuelRecordMixin = {
    standingCharge: sensibleFloat,
    unitPrice: sensibleFloat,
    carbonEmissionsFactor: sensibleFloat,
    primaryEnergyFactor: sensibleFloat,
  };
  const arbFuelsDict: fc.Arbitrary<FuelsDict> = fc.dictionary(
    fc.string().filter((s) => !disallowedFuelNames.includes(s)),
    fc.record({
      category: fc.constantFrom(
        ...(['gas', 'solid fuel', 'generation', 'oil', 'electricity'] as const),
      ),
      ...fuelRecordMixin,
    }),
  );
  const arbFuels = fc
    .tuple(
      fc.record<Fuel>({
        category: fc.constant('generation'),
        ...fuelRecordMixin,
      }),
      arbFuelsDict,
    )
    .map(([generationFuel, fuelsDict]) => ({
      ...fuelsDict,
      [Fuels.GENERATION]: generationFuel,
    }));
  return arbFuels.chain((fuels) => {
    const fuelNames = Object.keys(fuels);
    return fc.record({
      fuels: fc.constant({
        fuels,
        generation: fuels[Fuels.GENERATION]!,
      }),
      fansPumpsElectricKeepHot: fc.record({
        fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
      }),
      heatingSystemsFuelRequirements: fc.record({
        waterHeatingFuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
        spaceHeatingFuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
      }),
      appliancesCookingLoadCollections: fc.record({
        cooking: fc.record({
          fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
        }),
        appliances: fc.record({
          fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
        }),
      }),
      appliancesSap: fc.record({
        fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
      }),
      cookingSap: fc.record({
        fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
      }),
      lighting: fc.record({
        fuelDemandByFuelAnnual: arbFuelDemandByFuel(fuelNames),
      }),
      generation: fc.record({
        energyAnnual: sensibleFloat,
        energyUsedOnsiteAnnual: sensibleFloat,
        carbonEmissionsAnnual: sensibleFloat,
        primaryEnergyAnnual: sensibleFloat,
        incomeAnnual: sensibleFloat,
      }),
      floors: fc.record({
        totalFloorArea: sensibleFloat,
      }),
      occupancy: fc.record({
        occupancy: sensibleFloat,
      }),
      modelBehaviourFlags: fc.record({
        fuelRequirements: fc.record({
          zeroQuantityFuelsDoNotIncurStandingCharge: resolveArbParam(
            opts?.zeroQuantityFuelsDoNotIncurStandingCharge,
            fc.boolean(),
          ),
          propagateInfiniteFuelInputInRegulatedMode: resolveArbParam(
            opts?.propagateInfiniteFuelInputInRegulatedMode,
            fc.boolean(),
          ),
        }),
      }),
      mode: resolveArbParam(opts?.mode, arbModelMode),
    });
  });
}

describe('fuel requirements', () => {
  describe('golden master', () => {
    function makeLegacyDataForFuelRequirements(
      dependencies: FuelRequirementsDependencies,
    ): fc.Arbitrary<unknown> {
      function convertFuelDemandMapToLegacy(demand: FuelDemandByFuel): unknown[] {
        return Array.from(demand).map(
          ([name, { fuelInput, energyDemand, relativeFraction }]) => ({
            fuel: name,
            fuel_input: fuelInput,
            demand: energyDemand,
            fraction: relativeFraction,
          }),
        );
      }
      return fc.constant({
        fuels: mapValues(
          dependencies.fuels.fuels,
          ({
            standingCharge,
            unitPrice,
            carbonEmissionsFactor,
            primaryEnergyFactor,
          }) => ({
            standingcharge: standingCharge,
            fuelcost: unitPrice,
            co2factor: carbonEmissionsFactor,
            primaryenergyfactor: primaryEnergyFactor,
          }),
        ),
        fuel_requirements: {
          fans_and_pumps: {
            list: convertFuelDemandMapToLegacy(
              dependencies.fansPumpsElectricKeepHot.fuelDemandByFuelAnnual,
            ),
          },
          waterheating: {
            list: convertFuelDemandMapToLegacy(
              dependencies.heatingSystemsFuelRequirements
                .waterHeatingFuelDemandByFuelAnnual,
            ),
          },
          space_heating: {
            list: convertFuelDemandMapToLegacy(
              dependencies.heatingSystemsFuelRequirements
                .spaceHeatingFuelDemandByFuelAnnual,
            ),
          },
          appliances_sap: {
            list: convertFuelDemandMapToLegacy(
              dependencies.appliancesSap.fuelDemandByFuelAnnual,
            ),
          },
          appliances_load_collections: {
            list: convertFuelDemandMapToLegacy(
              dependencies.appliancesCookingLoadCollections.appliances
                .fuelDemandByFuelAnnual,
            ),
          },
          cooking_sap: {
            list: convertFuelDemandMapToLegacy(
              dependencies.cookingSap.fuelDemandByFuelAnnual,
            ),
          },
          cooking_load_collections: {
            list: convertFuelDemandMapToLegacy(
              dependencies.appliancesCookingLoadCollections.cooking
                .fuelDemandByFuelAnnual,
            ),
          },
          lighting: {
            list: convertFuelDemandMapToLegacy(
              dependencies.lighting.fuelDemandByFuelAnnual,
            ),
          },
        },
        use_generation: 1,
        generation: {
          total_generation: dependencies.generation.energyAnnual,
          total_used_onsite: dependencies.generation.energyUsedOnsiteAnnual,
          total_CO2: dependencies.generation.carbonEmissionsAnnual,
          total_primaryenergy: dependencies.generation.primaryEnergyAnnual,
        },
        total_income: dependencies.generation.incomeAnnual,

        // outputs
        total_cost: 0,
        net_cost: 0,
        annualco2: 0,
        energy_use: 0,
        primary_energy_use: 0,
      });
    }

    test('per-fuel totals (including generation)', () => {
      const arb = arbDependencies({
        mode: 'normal',
        zeroQuantityFuelsDoNotIncurStandingCharge: false,
      }).chain((dependencies) =>
        fc.record({
          fuelName: fc.constantFrom(...Object.keys(dependencies.fuels.fuels)),
          dependencies: fc.constant(dependencies),
          legacyData: makeLegacyDataForFuelRequirements(dependencies),
        }),
      );
      fc.assert(
        fc.property(arb, ({ fuelName, dependencies, legacyData: legacyData_ }) => {
          const legacyData: any = cloneDeep(legacyData_);
          legacyFuelRequirements(legacyData);
          const fuelRequirements = new FuelRequirements(null, dependencies);
          expect(fuelRequirements.totalsByFuelAnnual.size).toBe(
            Object.keys(legacyData.fuel_totals).length,
          );
          expect(Object.keys(dependencies.fuels.fuels)).toContain(Fuels.GENERATION);
          const fuelTotals = fuelRequirements.totalsByFuelAnnual.get(fuelName);
          const legacyFuelTotals = legacyData.fuel_totals[fuelName];
          expect(fuelTotals?.quantity ?? 0).toBeApproximately(
            legacyFuelTotals?.quantity ?? 0,
          );
          expect(fuelTotals?.cost ?? 0).toBeApproximately(
            legacyFuelTotals?.annualcost ?? 0,
          );
          expect(fuelTotals?.carbonEmissions ?? 0).toBeApproximately(
            legacyFuelTotals?.annualco2 ?? 0,
          );
          expect(fuelTotals?.primaryEnergy ?? 0).toBeApproximately(
            legacyFuelTotals?.primaryenergy ?? 0,
          );
        }),
      );
    });

    test('grand totals', () => {
      const arb = arbDependencies({
        mode: 'normal',
        zeroQuantityFuelsDoNotIncurStandingCharge: false,
      }).chain((dependencies) =>
        fc.record({
          dependencies: fc.constant(dependencies),
          legacyData: makeLegacyDataForFuelRequirements(dependencies),
        }),
      );
      fc.assert(
        fc.property(arb, ({ dependencies, legacyData: legacyData_ }) => {
          const legacyData: any = cloneDeep(legacyData_);
          legacyFuelRequirements(legacyData);
          const fuelRequirements = new FuelRequirements(null, dependencies);
          expect(fuelRequirements.totalCostAnnual).toBeApproximately(
            legacyData.total_cost,
          );
          expect(fuelRequirements.totalNetCostAnnual).toBeApproximately(
            legacyData.net_cost,
          );
          expect(fuelRequirements.totalCarbonEmissionsAnnual).toBeApproximately(
            legacyData.annualco2,
          );
          expect(fuelRequirements.totalEnergyAnnual).toBeApproximately(
            legacyData.energy_use,
          );
          expect(fuelRequirements.totalPrimaryEnergyAnnual).toBeApproximately(
            legacyData.primary_energy_use,
          );
        }),
      );
    });

    test('mutator', () => {
      const schema = z.object({
        fuel_totals: z.record(
          z.object({
            name: z.string(),
            quantity: z.number(),
            annualcost: z.number(),
            primaryenergy: z.number(),
            annualco2: z.number(),
          }),
        ),
        total_cost: z.number(),
        net_cost: z.number(),
        annualco2: z.number(),
        primary_energy_use: z.number(),
      });
      fc.assert(
        fc.property(arbDependencies(), (dependencies) => {
          const fuelRequirements = new FuelRequirements(null, dependencies);
          const data: unknown = {};
          fuelRequirements.mutateLegacyData(data);
          expect(() => schema.parse(data)).not.toThrow();
        }),
      );
    });
  });

  test('does not list fuels with 0 quantity', () => {
    fc.assert(
      fc.property(
        arbDependencies({ zeroQuantityFuelsDoNotIncurStandingCharge: true })
          .filter((dependencies) => Object.keys(dependencies.fuels.fuels).length !== 0)
          .chain((dependencies) =>
            fc.record({
              fuel: fc.constantFrom(...Object.keys(dependencies.fuels.fuels)),
              dependencies: fc.constant(dependencies),
            }),
          ),
        ({ fuel, dependencies }) => {
          const fuelRequirements = new FuelRequirements(null, dependencies);
          expect(fuelRequirements.totalsByFuelAnnual.get(fuel)?.quantity).not.toBe(0);
        },
      ),
    );
  });
});

import { isIndexable } from '../../src/helpers/is-indexable';
import { safeIsArray } from '../../src/helpers/safe-is-array';
import { constructModelBehaviourFlags } from '../../src/model/modules/behaviour-version';

describe('model behaviour version', () => {
  test('latest version should have all flags true', () => {
    const latestVersion = 15;
    const flags = constructModelBehaviourFlags(latestVersion);
    deepForEach(flags, (flag) => {
      expect(flag).toBe(true);
    });
  });
});

function deepForEach(val: unknown, fn: (val: unknown) => void) {
  if (isIndexable(val)) {
    for (const subVal of Object.values(val)) {
      deepForEach(subVal, fn);
    }
  } else if (safeIsArray(val)) {
    for (const subVal of val) {
      deepForEach(subVal, fn);
    }
  } else {
    fn(val);
  }
}

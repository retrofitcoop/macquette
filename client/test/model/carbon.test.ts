import { electricityCarbonIntensityByYear } from '../../src/model/datasets/electricity-carbon-intensity';
import { Carbon } from '../../src/model/modules/carbon';

describe('carbon accounting', () => {
  test('it calculates correctly', () => {
    const carbon = new Carbon(null, {
      fuels: {
        fuels: {
          electricity: {
            category: 'electricity',
            unitPrice: 0,
            standingCharge: 0,
            primaryEnergyFactor: 0,
            carbonEmissionsFactor: 0,
          },
          generation: {
            category: 'generation',
            unitPrice: 0,
            standingCharge: 0,
            primaryEnergyFactor: 0,
            carbonEmissionsFactor: 0,
          },
          gas: {
            category: 'gas',
            unitPrice: 0,
            standingCharge: 0,
            primaryEnergyFactor: 0,
            carbonEmissionsFactor: 20,
          },
        },
      },
      fuelRequirements: {
        totalsByFuelAnnual: new Map([
          ['electricity', { quantity: 50 }],
          ['generation', { quantity: -25 }],
          ['gas', { quantity: 100 }],
        ]),
      },
    });

    const result = carbon.cumulativeEmissionsByYear(2025, 2030);

    expect(result[0]!.year).toBe(2025);
    expect(result[0]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2025) / 1000) * 50 + 20 * 100,
    );
    expect(result[1]!.year).toBe(2026);
    expect(result[1]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2026) / 1000) * 50 +
        20 * 100 +
        result[0]!.cumulativeEmissions,
    );
    expect(result[2]!.year).toBe(2027);
    expect(result[2]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2027) / 1000) * 50 +
        20 * 100 +
        result[1]!.cumulativeEmissions,
    );
    expect(result[3]!.year).toBe(2028);
    expect(result[3]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2028) / 1000) * 50 +
        20 * 100 +
        result[2]!.cumulativeEmissions,
    );
    expect(result[4]!.year).toBe(2029);
    expect(result[4]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2029) / 1000) * 50 +
        20 * 100 +
        result[3]!.cumulativeEmissions,
    );
    expect(result[5]!.year).toBe(2030);
    expect(result[5]!.cumulativeEmissions).toBeApproximately(
      (electricityCarbonIntensityByYear(2030) / 1000) * 50 +
        20 * 100 +
        result[4]!.cumulativeEmissions,
    );
  });
});

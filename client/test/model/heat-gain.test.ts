import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { z } from 'zod';
import { Month } from '../../src/model/enums/month';
import { HeatGain, HeatGainDependencies } from '../../src/model/modules/heat-gain';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbMonthly } from '../arbitraries/monthly';
import { legacyGainsSummary } from './golden-master/gains-summary';

function arbDependencies(): fc.Arbitrary<HeatGainDependencies> {
  const heatGainMonthlyModule = fc.record({
    heatGain: arbMonthly(sensibleFloat),
  });
  const heatGainAnnualModule = fc.record({
    heatGain: sensibleFloat,
  });
  return fc.record({
    fansAndPumpsGains: heatGainMonthlyModule,
    miscellaneousInternalGains: heatGainAnnualModule,
    lighting: heatGainMonthlyModule,
    appliancesSap: heatGainMonthlyModule,
    cookingSap: heatGainAnnualModule,
    appliancesCookingLoadCollections: heatGainMonthlyModule,
    waterHeating: heatGainMonthlyModule,
    fabric: fc.record({
      heatGainSolar: arbMonthly(sensibleFloat),
    }),
  });
}

function makeLegacyDataForHeatGain(
  dependencies: HeatGainDependencies,
): fc.Arbitrary<unknown> {
  const metabolicAndLosses = fc
    .float({ min: 0, max: 1, noNaN: true })
    .map((proportionMetabolic) => {
      const gain = dependencies.miscellaneousInternalGains.heatGain;
      return {
        metabolic: proportionMetabolic * gain,
        losses: (1 - proportionMetabolic) * gain,
      };
    });
  return metabolicAndLosses.map(({ metabolic, losses }) => ({
    gains_W: {
      fans_and_pumps: Month.all.map((m) => dependencies.fansAndPumpsGains.heatGain(m)),
      metabolic: Month.all.map(() => metabolic),
      losses: Month.all.map(() => losses),
      Lighting: Month.all.map((m) => dependencies.lighting.heatGain(m)),
      // In legacy, Appliances and Cooking fields are populated by whichever module for them is enabled, but we want to
      // test that heat gain takes all outputs, so we use separate keys here.
      Appliances: Month.all.map((m) => dependencies.appliancesSap.heatGain(m)),
      Cooking: Month.all.map(() => dependencies.cookingSap.heatGain),
      AppliancesCooking: Month.all.map((m) =>
        dependencies.appliancesCookingLoadCollections.heatGain(m),
      ),
      waterheating: Month.all.map((m) => dependencies.waterHeating.heatGain(m)),
      solar: Month.all.map((m) => dependencies.fabric.heatGainSolar(m)),
    },
  }));
}

describe('heat gain', () => {
  test('golden master', () => {
    const arb = arbDependencies().chain((dependencies) =>
      fc.record({
        dependencies: fc.constant(dependencies),
        legacyData: makeLegacyDataForHeatGain(dependencies),
      }),
    );
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arb,
        (month, { dependencies, legacyData: legacyData_ }) => {
          const heatGain = new HeatGain(null, dependencies);
          const legacyData: any = cloneDeep(legacyData_);
          legacyGainsSummary(legacyData);
          expect(legacyData.total_internal_and_solar_gains[month.index0]).not.toBeNaN();
          expect(heatGain.heatGainInternal(month)).toBeApproximately(
            legacyData.total_internal_gains[month.index0],
          );
          expect(heatGain.heatGainSolar(month)).toBeApproximately(
            legacyData.total_solar_gains[month.index0],
          );
          expect(heatGain.heatGain(month)).toBeApproximately(
            legacyData.total_internal_and_solar_gains[month.index0],
          );
        },
      ),
    );
  });
  test('mutator', () => {
    fc.assert(
      fc.property(arbDependencies(), (dependencies) => {
        const heatGain = new HeatGain(null, dependencies);
        const legacyData: any = {};
        heatGain.mutateLegacyData(legacyData);
        expect(() =>
          z.array(z.number()).parse(legacyData.total_internal_gains),
        ).not.toThrow();
        expect(() =>
          z.array(z.number()).parse(legacyData.total_solar_gains),
        ).not.toThrow();
        expect(() =>
          z.array(z.number()).parse(legacyData.total_internal_and_solar_gains),
        ).not.toThrow();
      }),
    );
  });
});

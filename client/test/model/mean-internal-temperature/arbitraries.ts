import fc from 'fast-check';
import { Month } from '../../../src/model/enums/month';
import {
  MeanInternalTemperatureDependencies,
  MeanInternalTemperatureInput,
} from '../../../src/model/modules/mean-internal-temperature';
import { Mode } from '../../../src/model/modules/mode';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { arbModelMode } from '../../arbitraries/model-mode';
import { arbMonthly, monthly } from '../../arbitraries/monthly';
import { ArbParam, resolveArbParam } from '../../helpers/arbitraries';
import { arbitraryRegion } from '../../helpers/arbitrary-enums';
import { arbSpaceHeatingSystem } from '../heating-systems/arbitraries';

export function arbMeanInternalTemperatureInput(): fc.Arbitrary<MeanInternalTemperatureInput> {
  return fc.record({
    targetTemperature: sensibleFloat,
    livingArea: fc.option(sensibleFloat),
  });
}

export function arbMeanInternalTemperatureDependencies(opts?: {
  mode?: ArbParam<Mode>;
  makeCompatibleWithLegacy?: boolean;
}): fc.Arbitrary<Omit<MeanInternalTemperatureDependencies, 'modelBehaviourFlags'>> {
  return fc
    .record({
      floors: fc.record({
        totalFloorArea:
          opts?.makeCompatibleWithLegacy === true
            ? sensibleFloat.filter((val) => val > 0)
            : sensibleFloat,
      }),
      fabric: fc.record({ thermalMassParameter: sensibleFloat.filter((v) => v >= 0) }),
      heatGain: fc.record({
        heatGain: arbMonthly(sensibleFloat),
        heatGainSolar: arbMonthly(sensibleFloat),
      }),
      heatLoss: fc.record({
        heatLossMonthly: arbMonthly(
          opts?.makeCompatibleWithLegacy === true
            ? sensibleFloat.filter((val) => val >= 0)
            : sensibleFloat,
        ),
      }),
      region: arbitraryRegion,
      heatingSystems: fc.record({
        spaceHeatingSystems: fc
          .record({
            main: arbSpaceHeatingSystem({
              type: 'main',
              fraction:
                opts?.makeCompatibleWithLegacy === true
                  ? sensibleFloat.filter((val) => val >= 0)
                  : sensibleFloat,
            }),
            secondMain: fc.option(
              arbSpaceHeatingSystem({
                type: fc.constantFrom(
                  'second main (whole house)' as const,
                  'second main (part of house)' as const,
                ),
              }),
            ),
            // fraction <= 0 is used to signal a system which has been removed from a
            // non-baseline scenario
            extrasWithZeroFraction: fc.array(
              arbSpaceHeatingSystem({
                fraction: sensibleFloat.filter((val) => val <= 0),
              }),
            ),
          })
          .map(({ main, secondMain, extrasWithZeroFraction }) => [
            ...(secondMain === null ? [] : [secondMain]),
            ...extrasWithZeroFraction,
            main,
          ]),
      }),
      mode: resolveArbParam(opts?.mode, arbModelMode),
      heatingHours: fc.record({
        weekday: fc.record({
          hoursOffPattern: fc.array(sensibleFloat, { minLength: 1 }),
        }),
        weekend: fc.record({
          hoursOffPattern: fc.array(sensibleFloat, { minLength: 1 }),
        }),
      }),
    })
    .map(({ heatLoss, floors, ...rest }) => ({
      ...rest,
      heatLoss: {
        ...heatLoss,
        heatLossParameter: monthly(
          Month.all.map((m) => heatLoss.heatLossMonthly(m) / floors.totalFloorArea),
        ),
      },
      floors,
    }));
}

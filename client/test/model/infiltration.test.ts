import fc from 'fast-check';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import { Month } from '../../src/model/enums/month';
import { Region } from '../../src/model/enums/region';
import { VentilationInfiltrationCommon } from '../../src/model/modules/ventilation-infiltration/common-input';
import {
  Infiltration,
  InfiltrationDependencies,
  extractInfiltrationInputFromLegacy,
} from '../../src/model/modules/ventilation-infiltration/infiltration';
import { legacyVentilation } from './golden-master/ventilation';
import {
  arbCommonInput,
  arbDependencies,
  arbInfiltrationInput,
  makeLegacyDataForInfiltration,
} from './ventilation-infiltration-arbitraries';

const arbExtras = fc.record({
  partyWallAreaProportionOfEnvelope: fc.float({
    min: 0,
    max: 1,
    noNaN: true,
  }),
});

describe('infiltration', () => {
  test('golden master', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arbCommonInput,
        arbInfiltrationInput.filter(
          (input) =>
            input.estimateFrom.type === 'fabric elements' &&
            input.estimateFrom.numberOfFloorsOverride !== null &&
            input.estimateFrom.numberOfFloorsOverride > 0,
        ),
        arbDependencies,
        arbExtras,
        (month, commonInput, infiltrationInput, dependencies, extras) => {
          const common = new VentilationInfiltrationCommon(commonInput, dependencies);
          const infiltration = new Infiltration(infiltrationInput, {
            ...dependencies,
            ventilationInfiltrationCommon: common,
          });
          const legacyData: any = makeLegacyDataForInfiltration(
            commonInput,
            infiltrationInput,
            dependencies,
            extras,
          );
          legacyVentilation(legacyData);
          expect(infiltration.heatLossMonthly(month)).toBeApproximately(
            legacyData.ventilation.infiltration_WK[month.index0],
          );
          expect(infiltration.airChangesPerHour(month)).toBeApproximately(
            legacyData.ventilation.adjusted_infiltration[month.index0],
          );
          expect(infiltration.heatLossAverage).toBeApproximately(
            legacyData.ventilation.average_infiltration_WK,
          );
        },
      ),
    );
  });
  test('extractor', () => {
    fc.assert(
      fc.property(
        arbCommonInput,
        arbInfiltrationInput,
        arbDependencies,
        arbExtras,
        (commonInput, infiltrationInput, dependencies, extras) => {
          const roundTripped = extractInfiltrationInputFromLegacy(
            scenarioSchema.parse(
              makeLegacyDataForInfiltration(
                commonInput,
                infiltrationInput,
                dependencies,
                extras,
              ),
            ),
          );
          expect(roundTripped).toEqual(infiltrationInput);
        },
      ),
    );
  });

  test('q50 as input should be the same as reverse-calculated airPermeability', () => {
    const dependencies: InfiltrationDependencies = {
      floors: { totalVolume: 204.3, numberOfFloors: 2 },
      fabric: { envelopeArea: 101.23 },
      ventilationInfiltrationCommon: new VentilationInfiltrationCommon(
        { numberOfSidesSheltered: 2 },
        { region: Region.fromIndex0(0) },
      ),
    };

    const inputValue = 18;
    const pressureCalc = new Infiltration(
      {
        estimateFrom: { type: 'pressure test', airPermeability: inputValue },
        intentionalVentsFlues: [],
      },
      dependencies,
    );

    expect(pressureCalc.airPermeability).toEqual(inputValue);
  });

  test('assumed air permeability can be calculated from ACH, and if used in its place, produces the same ACH', () => {
    const dependencies: InfiltrationDependencies = {
      floors: { totalVolume: 204.3, numberOfFloors: 2 },
      fabric: { envelopeArea: 101.23 },
      ventilationInfiltrationCommon: new VentilationInfiltrationCommon(
        { numberOfSidesSheltered: 2 },
        { region: Region.fromIndex0(0) },
      ),
    };

    const sapCalc = new Infiltration(
      {
        estimateFrom: {
          type: 'fabric elements',
          numberOfFloorsOverride: 2,
          walls: 'masonry',
          floor: 'suspended unsealed',
          draughtProofedProportion: 0.8,
          draughtLobby: false,
        },
        intentionalVentsFlues: [],
      },
      dependencies,
    );

    const airPermeability = sapCalc.airPermeability;
    if (airPermeability === null) {
      throw new Error('air permeability should have been calculable');
    }

    const pressureCalc = new Infiltration(
      {
        estimateFrom: { type: 'pressure test', airPermeability },
        intentionalVentsFlues: [],
      },
      dependencies,
    );

    expect(pressureCalc.withShelterFactorAirChangesPerHour).toBeCloseTo(
      sapCalc.withShelterFactorAirChangesPerHour,
      3,
    );
    expect(sapCalc.airPermeability).toEqual(pressureCalc.airPermeability);
  });
});

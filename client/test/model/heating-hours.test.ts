import fc from 'fast-check';
import { sum } from '../../src/helpers/array-reducers';
import { positiveMod } from '../../src/helpers/positive-mod';
import {
  HeatingHours,
  HeatingHoursDependencies,
  HeatingPattern,
} from '../../src/model/modules/heating-hours';
import { TimeOfDay, TimePeriod } from '../../src/periods/core';
import { fractionsOfUnity } from '../helpers/arbitraries';
import { legacyHoursOff } from './golden-master/hours-off-calculation';

function time(hour: number, minute = 0) {
  return hour * 60 + minute;
}

const stubDependencies: HeatingHoursDependencies = {
  modelBehaviourFlags: {
    heatingHours: {
      fixHoursOffCalculation: true,
    },
  },
};

function arbHeatingPatternInput(opts?: { makeCompatibleWithLegacy?: boolean }) {
  function sortPattern(pattern: Array<TimePeriod>) {
    return [...pattern].sort(({ start: startA }, { start: startB }) => startA - startB);
  }
  const makeCompatibleWithLegacy = opts?.makeCompatibleWithLegacy ?? false;
  return fc
    .record({
      startTime: fc.double({ min: 0, max: 24 * 60, maxExcluded: true }),
      startsOn: fc.boolean(),
      patternFractions: fractionsOfUnity({
        withNulls: false,
        ...(makeCompatibleWithLegacy ? { maxLength: 3 } : {}),
      }).map((fs) => fs.map((f) => f!)),
    })
    .map(({ startTime, startsOn, patternFractions }) => {
      let curTime = startTime;
      const pattern: TimePeriod[] = [];
      let on = startsOn;
      for (const fraction of patternFractions) {
        if (on) {
          const start = curTime;
          const end = (curTime + fraction * 24 * 60) % (24 * 60);
          pattern.push({
            start: makeCompatibleWithLegacy ? Math.round(start) : start,
            end: makeCompatibleWithLegacy ? Math.round(end) : end,
          });
        }
        on = !on;
        curTime += fraction * 24 * 60;
      }
      return { startTime, curTime, pattern };
    })
    .filter(({ startTime, curTime }) => curTime - startTime <= 24 * 60)
    .map(({ pattern }) => pattern)
    .chain((pattern) =>
      makeCompatibleWithLegacy
        ? fc.constant(sortPattern(pattern))
        : fc.shuffledSubarray(pattern),
    );
}

describe('heating hours', () => {
  describe('hours sum', () => {
    const cases = [
      { pattern: [], expected: 0 },
      {
        pattern: [
          { start: time(10), end: time(12) },
          { start: time(17), end: time(18, 30) },
        ],
        expected: 12 - 10 + (18.5 - 17),
      },
      { pattern: [{ start: time(12), end: 0 }], expected: (time(24) - time(12)) / 60 },
      {
        pattern: [
          { start: time(0), end: time(8) },
          { start: time(16), end: time(0) },
        ],
        expected: 8 + (24 - 16),
      },
    ];
    it.each(cases)('calculates the total hours correctly', ({ pattern, expected }) => {
      const heatingPattern = new HeatingPattern(
        { pattern, unvalidatedPattern: [] },
        stubDependencies.modelBehaviourFlags.heatingHours,
      );
      expect(heatingPattern.totalHoursOn).toEqual(expected);
    });
  });

  describe('hours off pattern', () => {
    test('property: sum of hours off = 24 - sum of hours on', () => {
      fc.assert(
        fc.property(arbHeatingPatternInput(), (pattern) => {
          const heatingPattern = new HeatingPattern(
            { pattern, unvalidatedPattern: [] },
            stubDependencies.modelBehaviourFlags.heatingHours,
          );
          expect(sum(heatingPattern.hoursOffPattern)).toBeApproximately(
            24 - heatingPattern.totalHoursOn,
          );
        }),
      );
    });
    const cases = [
      { pattern: [], expected: [24] },
      { pattern: [{ start: time(8), end: time(10) }], expected: [22] },
      {
        pattern: [
          { start: time(8), end: time(10) },
          { start: time(12), end: time(17) },
        ],
        expected: [15, 2],
      },
      {
        pattern: [
          { start: time(8), end: time(10) },
          { start: time(17), end: time(2) },
        ],
        expected: [6, 7],
      },
      {
        pattern: [
          { start: time(0), end: time(8) },
          { start: time(16), end: time(0) },
        ],
        expected: [8],
      },
      {
        pattern: [{ start: time(0), end: time(0) }],
        expected: [], // i.e. constant heating
      },
      { pattern: [{ start: time(2), end: time(1) }], expected: [1] },
      {
        pattern: [
          { start: time(2), end: time(3) },
          { start: time(23), end: time(1) },
        ],
        expected: [1, 20],
      },
    ];
    test.each(cases)('example test: $pattern = $expected', ({ pattern, expected }) => {
      const heatingHours = new HeatingPattern(
        { pattern, unvalidatedPattern: [] },
        stubDependencies.modelBehaviourFlags.heatingHours,
      );
      expect(heatingHours.hoursOffPattern).toEqual(expected);
    });
  });

  test('golden master - legacy mode', () => {
    const arbInput = fc.record({
      weekday: arbHeatingPatternInput({ makeCompatibleWithLegacy: true }).map(
        (pattern) => ({ pattern: [], unvalidatedPattern: pattern }),
      ),
      weekend: arbHeatingPatternInput({ makeCompatibleWithLegacy: true }).map(
        (pattern) => ({ pattern: [], unvalidatedPattern: pattern }),
      ),
    });
    function hours(t: TimeOfDay | undefined) {
      return t === undefined ? undefined : Math.floor(t / 60);
    }
    function mins(t: TimeOfDay | undefined) {
      return t === undefined ? undefined : positiveMod(t, 60);
    }
    fc.assert(
      fc.property(arbInput, (input) => {
        const pattern = new HeatingHours(input, {
          modelBehaviourFlags: {
            heatingHours: { fixHoursOffCalculation: false },
          },
        });
        const legacyData: any = {
          household: {
            heating_weekday_on1_hours: hours(input.weekday.unvalidatedPattern[0]?.start),
            heating_weekday_on1_mins: mins(input.weekday.unvalidatedPattern[0]?.start),
            heating_weekday_off1_hours: hours(input.weekday.unvalidatedPattern[0]?.end),
            heating_weekday_off1_mins: mins(input.weekday.unvalidatedPattern[0]?.end),
            heating_weekday_on2_hours: hours(input.weekday.unvalidatedPattern[1]?.start),
            heating_weekday_on2_mins: mins(input.weekday.unvalidatedPattern[1]?.start),
            heating_weekday_off2_hours: hours(input.weekday.unvalidatedPattern[1]?.end),
            heating_weekday_off2_mins: mins(input.weekday.unvalidatedPattern[1]?.end),
            heating_weekday_on3_hours: hours(input.weekday.unvalidatedPattern[2]?.start),
            heating_weekday_on3_mins: mins(input.weekday.unvalidatedPattern[2]?.start),
            heating_weekday_off3_hours: hours(input.weekday.unvalidatedPattern[2]?.end),
            heating_weekday_off3_mins: mins(input.weekday.unvalidatedPattern[2]?.end),
            heating_weekend_on1_hours: hours(input.weekend.unvalidatedPattern[0]?.start),
            heating_weekend_on1_mins: mins(input.weekend.unvalidatedPattern[0]?.start),
            heating_weekend_off1_hours: hours(input.weekend.unvalidatedPattern[0]?.end),
            heating_weekend_off1_mins: mins(input.weekend.unvalidatedPattern[0]?.end),
            heating_weekend_on2_hours: hours(input.weekend.unvalidatedPattern[1]?.start),
            heating_weekend_on2_mins: mins(input.weekend.unvalidatedPattern[1]?.start),
            heating_weekend_off2_hours: hours(input.weekend.unvalidatedPattern[1]?.end),
            heating_weekend_off2_mins: mins(input.weekend.unvalidatedPattern[1]?.end),
            heating_weekend_on3_hours: hours(input.weekend.unvalidatedPattern[2]?.start),
            heating_weekend_on3_mins: mins(input.weekend.unvalidatedPattern[2]?.start),
            heating_weekend_off3_hours: hours(input.weekend.unvalidatedPattern[2]?.end),
            heating_weekend_off3_mins: mins(input.weekend.unvalidatedPattern[2]?.end),
          },
        };
        legacyHoursOff(legacyData);
        fc.pre(
          legacyData.temperature.hours_off.weekday.every((val: any) =>
            Number.isFinite(val),
          ),
        );
        fc.pre(
          legacyData.temperature.hours_off.weekend.every((val: any) =>
            Number.isFinite(val),
          ),
        );
        expect([...pattern.weekday.hoursOffPattern].sort()).toEqual(
          [...legacyData.temperature.hours_off.weekday].sort(),
        );
        expect([...pattern.weekend.hoursOffPattern].sort()).toEqual(
          [...legacyData.temperature.hours_off.weekend].sort(),
        );
      }),
    );
  });
});

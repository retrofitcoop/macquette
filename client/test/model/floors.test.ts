import * as fc from 'fast-check';

import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import {
  Floor,
  Floors,
  FloorsDependencies,
  FloorsInput,
  extractFloorsInputFromLegacy,
} from '../../src/model/modules/floors';
import { legacyFloors } from './golden-master/floors';

function positiveRationalFloat() {
  return fc
    .float({
      min: 0,
      noNaN: true,
      noDefaultInfinity: true,
    })
    .filter((x) => x !== 0);
}
function arbFloorSpec() {
  return fc.record({
    area: positiveRationalFloat(),
    height: positiveRationalFloat(),
    name: fc.string(),
  });
}
function arbFloorsInput() {
  return fc.record({
    floors: fc.array(arbFloorSpec()),
    volumeReductionOverride: fc.option(
      fc.double({
        noNaN: true,
      }),
    ),
  });
}

describe('floors', () => {
  describe('floor', () => {
    test('the volume is proportional to the area', () => {
      const arb = fc.record({
        area1: positiveRationalFloat(),
        area2: positiveRationalFloat(),
        floorSpec: arbFloorSpec(),
      });
      fc.assert(
        fc.property(arb, ({ area1, area2, floorSpec }) => {
          const volume1 = new Floor({ ...floorSpec, area: area1 }).volume;
          const volume2 = new Floor({ ...floorSpec, area: area2 }).volume;
          expect(volume1 / volume2).toBeApproximately(area1 / area2);
        }),
      );
    });
    test('the volume is proportional to the height', () => {
      const arb = fc.record({
        height1: positiveRationalFloat(),
        height2: positiveRationalFloat(),
        floorSpec: arbFloorSpec(),
      });
      fc.assert(
        fc.property(arb, ({ height1, height2, floorSpec }) => {
          const volume1 = new Floor({ ...floorSpec, height: height1 }).volume;
          const volume2 = new Floor({ ...floorSpec, height: height2 }).volume;
          expect(volume1 / volume2).toBeApproximately(height1 / height2);
        }),
      );
    });
  });

  describe('floors', () => {
    type TestCase = {
      floors: FloorsInput['floors'];
      volumeReductionOverride: FloorsInput['volumeReductionOverride'];
      applyVolumeReduction: boolean;
      expectedArea: number;
      expectedVolume: number;
    };
    const floor1 = { name: 'floor 1', area: 42, height: 2 };
    const floor2 = { name: 'floor 2', area: 56, height: 1 };
    const cases: TestCase[] = [
      {
        floors: [],
        volumeReductionOverride: null,
        applyVolumeReduction: false,
        expectedArea: 0,
        expectedVolume: 0,
      },
      {
        floors: [],
        volumeReductionOverride: null,
        applyVolumeReduction: true,
        expectedArea: 0,
        expectedVolume: 0,
      },
      {
        floors: [],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: false,
        expectedArea: 0,
        expectedVolume: 0,
      },
      {
        floors: [],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: true,
        expectedArea: 0,
        expectedVolume: 0,
      },
      {
        floors: [floor1],
        volumeReductionOverride: null,
        applyVolumeReduction: false,
        expectedArea: 42,
        expectedVolume: 42 * 2,
      },
      {
        floors: [floor1],
        volumeReductionOverride: null,
        applyVolumeReduction: true,
        expectedArea: 42,
        expectedVolume: 42 * 2 * 0.92,
      },
      {
        floors: [floor1],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: false,
        expectedArea: 42,
        expectedVolume: 42 * 2,
      },
      {
        floors: [floor1],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: true,
        expectedArea: 42,
        expectedVolume: 42 * 2 * 0.9,
      },
      {
        floors: [floor1, floor2],
        volumeReductionOverride: null,
        applyVolumeReduction: false,
        expectedArea: 42 + 56,
        expectedVolume: 42 * 2 + 56 * 1,
      },
      {
        floors: [floor1, floor2],
        volumeReductionOverride: null,
        applyVolumeReduction: true,
        expectedArea: 42 + 56,
        expectedVolume: (42 * 2 + 56 * 1) * 0.92,
      },
      {
        floors: [floor1, floor2],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: false,
        expectedArea: 42 + 56,
        expectedVolume: 42 * 2 + 56 * 1,
      },
      {
        floors: [floor1, floor2],
        volumeReductionOverride: 0.1,
        applyVolumeReduction: true,
        expectedArea: 42 + 56,
        expectedVolume: (42 * 2 + 56 * 1) * 0.9,
      },
    ];
    test.each(cases)('volume and area example test', (testCase) => {
      const input: FloorsInput = {
        floors: testCase.floors,
        volumeReductionOverride: testCase.volumeReductionOverride,
      };
      const dependencies: FloorsDependencies = {
        modelBehaviourFlags: {
          floors: { applyVolumeReduction: testCase.applyVolumeReduction },
        },
      };
      const floors = new Floors(input, dependencies);
      expect(floors.totalFloorArea).toBeApproximately(testCase.expectedArea);
      expect(floors.totalVolume).toBeApproximately(testCase.expectedVolume);
      expect(floors.grossVolume).toBeApproximately(
        floors.totalVolume / (1 - floors.volumeReduction),
      );
    });
  });

  test('golden master', () => {
    fc.assert(
      fc.property(arbFloorsInput(), (floorsInput) => {
        const floorsModel = new Floors(floorsInput, {
          modelBehaviourFlags: { floors: { applyVolumeReduction: false } },
        });
        const legacyData: any = {
          floors: cloneDeep(floorsInput.floors),
          num_of_floors: 0,
          TFA: 0,
          volume: 0,
        };
        legacyFloors(legacyData);
        expect(floorsModel.numberOfFloors).toEqual(legacyData.num_of_floors);
        expect(floorsModel.totalFloorArea).toEqual(legacyData.TFA);
        expect(floorsModel.totalVolume).toEqual(legacyData.volume);
      }),
    );
  });

  test('extractor', () => {
    fc.assert(
      fc.property(
        arbFloorsInput().filter((input) => !Number.isNaN(input.volumeReductionOverride)),
        fc.option(fc.double({ noNaN: true })),
        (input, reductionOverrideWhenDisabled) => {
          const roundTripped = extractFloorsInputFromLegacy(
            scenarioSchema.parse({
              floors: input.floors,
              volumeReductionOverride:
                input.volumeReductionOverride === null
                  ? {
                      enabled: false,
                      value: reductionOverrideWhenDisabled,
                    }
                  : {
                      enabled: true,
                      value: input.volumeReductionOverride,
                    },
              num_of_floors: 0,
              TFA: 0,
              volume: 0,
            }),
          );
          expect(roundTripped).toEqual(input);
        },
      ),
    );
  });
});
